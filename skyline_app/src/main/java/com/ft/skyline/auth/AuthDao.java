package com.ft.skyline.auth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ft.skyline.common.dao.GeneralDao;

import comply.SkyLine.bl.biz.User;
import oracle.jdbc.OracleTypes;

@Repository
public class AuthDao {
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;
	@Autowired
	GeneralDao generalDao;
	
	private static final Logger logger = LoggerFactory.getLogger(AuthDao.class);
	
	/**
     * @param userName - login user name
     * @return ldap name if exists or empty string
     */
	public String getLDAPNameByUserName(String userName) {
		String sql = "select user_ldap from p_user where user_name = ?";
		String toReturn = "";
		try {
			toReturn = jdbcTemplate.queryForObject(sql, String.class, userName);
			System.out.println(toReturn);
		} catch (Exception e) {
			e.printStackTrace();
			toReturn = "";
		}
		return (toReturn == null) ? "" : toReturn;
	}
	
	@SuppressWarnings("unchecked")
	public User authenticateUser(String userName, String password, boolean isLogin, String stationIpAddress, int[] aReturnCode) {
		Map<String, String> inParameters;
		try 
		{
			inParameters = new HashMap<String, String>();
			inParameters.put("user_name_in", userName);
			inParameters.put("enc_pwd_in", password);
			inParameters.put("is_login_in", isLogin ? "1" : "0");
			inParameters.put("ip_address_in", stationIpAddress);
			
			Map<String, Integer> outParameters = new HashMap<>();
			outParameters.put("ret_code_out", OracleTypes.VARCHAR);
			outParameters.put("user_out", OracleTypes.CURSOR);
			
			Map<String, Object> resultMap = generalDao.callProcedureReturnsOutObject("SP_AUTHENTICATE_USER", inParameters, outParameters); 
			aReturnCode[0] = Integer.parseInt(resultMap.get("ret_code_out").toString());
			ArrayList<Object> rsLst = (ArrayList<Object>)resultMap.get("user_out");
			Map<String, String> objMap = (Map<String, String>) rsLst.get(0);
			
			User user = new User();
			user.USER_ID = Integer.parseInt(String.valueOf(objMap.get("USER_ID")));
            user.USER_NAME = objMap.get("USER_NAME");
            user.SITE_ID = Integer.parseInt(String.valueOf(objMap.get("USER_ID")));
            user.siteName = objMap.get("SITE_NAME");
            user.ROLE = objMap.get("ROLE");
            
			return user;

		} catch (Exception ex) {
			logger.error("error in authenticateUser: " + ex);
			aReturnCode[0] = -2;
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public User getUserByUserName(String userName) {
		Map<String, String> inParameters;
		try 
		{
			inParameters = new HashMap<String, String>();
			inParameters.put("user_name_in", userName);
			
			Map<String, Integer> outParameters = new HashMap<>();
			outParameters.put("o_result", OracleTypes.CURSOR);
			
			Map<String, Object> resultMap = generalDao.callProcedureReturnsOutObject("SP_GET_USER_BY_USERNAME", inParameters, outParameters); 

			ArrayList<Object> rsLst = (ArrayList<Object>)resultMap.get("o_result");
			Map<String, String> objMap = (Map<String, String>) rsLst.get(0);
			
			User user = new User();
			user.USER_ID = Integer.parseInt(String.valueOf(objMap.get("USER_ID")));
            user.USER_NAME = objMap.get("USER_NAME");
            user.SITE_ID = Integer.parseInt(String.valueOf(objMap.get("USER_ID")));
            user.siteName = objMap.get("SITE_NAME");
            user.ROLE = objMap.get("ROLE");
            
			return user;

		} catch (Exception ex) {
			logger.error("error in authenticateUser: " + ex);
			return null;
		}
	}
	
	public void updateAccessLog(String userName, String isSuccessed, String stationIpAddress)  
	{
		Map<String, String> inParameters;
		try 
		{
			inParameters = new HashMap<String, String>();
			inParameters.put("user_name_in", userName);
			inParameters.put("is_successed_in", isSuccessed);
			inParameters.put("ip_address_in", stationIpAddress);
			
			generalDao.callProcedureReturnsOutObject("SP_UPDATE_LDAP_USER_ACCESSLOG", inParameters, null); 

		} catch (Exception ex) {
			logger.error("error in updateAccessLog: " + ex);
		}
	}
	
}
