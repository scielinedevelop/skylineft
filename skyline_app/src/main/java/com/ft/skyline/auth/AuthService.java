package com.ft.skyline.auth;

import java.security.MessageDigest;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ft.skyline.common.utils.GeneralUtils;

import comply.SkyLine.bl.biz.User;

@Service
public class AuthService {
	
	@Autowired
	AuthDao authDao; 
		
	@Value("${isLdapAuthentication}")
	private String isLdapAuthentication;
	
	@Value("${ldapUrl}")
	private String ldapUrl;
	
	private static final Logger logger = LoggerFactory.getLogger(AuthService.class);
	
	/** Used to authenticate user and fetch his record as User object (basically for Login)
     * @param userName - user name in Skyline DB
     * @param password - either LDAP password or Skyline password (if no LDAP for user)
     * @param stationIpAddress - remote address fetched from HttpRequest 
     * @param aRetCode - an array of 1 int, used to "send by reference" the return-code
     * @return  <code>User</code> object if the user was authenticated, otherwise null  
     * */
	public User authenticateUser(String userName, String password, String stationIpAddress, int[] aReturnCode) {	
		String encPassword = getMd5(password);
		boolean isLDAP = (GeneralUtils.getNull(isLdapAuthentication).equals("1")) ? true : false;
		
		if (isLDAP) {
			// get ldap name
			String ldapName = authDao.getLDAPNameByUserName(userName);
			// whether to use LDAP or local authentication
			if (ldapName.equals("")) {
				return authDao.authenticateUser(userName, encPassword, true, stationIpAddress, aReturnCode);				
			}
			// else authenticate by LDAP
			if (authenticateUserByLDAP(ldapName, password)) {	
				authDao.updateAccessLog(userName, "1", stationIpAddress);
				return authDao.getUserByUserName(userName); // successful authentication				
			} else { // LDAP authentication failed
				authDao.updateAccessLog(userName, "0", stationIpAddress);
				return null;				
			}
		} 
		else {
			return authDao.authenticateUser(userName, encPassword, true, stationIpAddress, aReturnCode);
		}
	}
	
	private boolean authenticateUserByLDAP(String ldapName, String password) {
		boolean isAuthenticated = false;
		Hashtable<Object,String> env = new Hashtable<Object, String>();

		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL,GeneralUtils.getNull(ldapUrl));
		env.put(Context.SECURITY_PRINCIPAL, ldapName);
		env.put(Context.SECURITY_CREDENTIALS, password);

		try {
			if (ldapName == null || ldapName.trim().equals("") || password == null || password.trim().equals("")) {
				isAuthenticated = false;
			} else {
				// try to authenticate
//				DirContext context = new InitialDirContext(env);
				// only if user is authenticated the code continutes (otherwise, an exception is thrown)
				isAuthenticated = true;
				String[] attributes = { "member" };
				SearchControls sc = new SearchControls();
				sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
				sc.setCountLimit(1000);
				sc.setReturningAttributes(attributes);
				sc.setReturningObjFlag(true);
				//            NamingEnumeration answer = context.search(props.getLDAPSearchDirectory(), "(objectclass=*)", sc );
			}
		} catch (Exception ex) {
			logger.error("LDAPAuthentication.authenticateUserByLDAP(): " + ex.toString());
			isAuthenticated = false;
		}

		return isAuthenticated;
	}
	
	private String getMd5(String password) {
		//Take a string and return its md5 hash as a hex digit string
		//Ilia Bulaevski	
		String result;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte[] passwordHash = md.digest();

			result = asHex(passwordHash);
		} catch (Exception ex) {
			return "";
		}
		return result;
	}
	
	private String asHex(byte hash[]) {
		//Private function to turn md5 result to 32 hex-digit string
		//Ilia Bulaevski
		StringBuffer buf = new StringBuffer(hash.length * 2);
		int i;

		for (i = 0; i < hash.length; i++) {
			if (((int) hash[i] & 0xff) < 0x10) {
				buf.append("0");
			}
			buf.append(Long.toString((int) hash[i] & 0xff, 16));
		}

		return buf.toString();
	}
}
