package com.ft.skyline.catalog;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CatalogController {
	
	private static final Logger logger = LoggerFactory.getLogger(CatalogController.class);
	
	@Autowired
	private CatalogService service;	
	
	@RequestMapping(value = "/catalog", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView initPage(HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("catalog main request call");	
		return new ModelAndView("CatalogMain");		
	}
	
	@PostMapping(value = "/catalog/getCompanies", produces="application/json")
	@ResponseBody
	public String getCompanyList() throws Exception{
	    return service.getCompanyList();
	}
	
	@PostMapping(value = "/catalog/getCatalogNames", produces="application/json")
	@ResponseBody
	public String getCatalogNameList(@RequestParam("companyId") String companyId) throws Exception{
	    return service.getCatalogNameList(companyId);
	}
	
	@PostMapping(value = "/catalog/getItemCategories", produces="application/json")
	@ResponseBody
	public String getItemCategoryList() throws Exception{
	    return service.getItemCategoryList();
	}
	
	@PostMapping(value = "/catalog/getItemTypes", produces="application/json")
	@ResponseBody
	public String getItemTypeList() throws Exception{
	    return service.getItemTypeList();
	}
	
	@PostMapping(value = "/catalog/getParameterTypes", produces="application/json")
	@ResponseBody
	public String getParameterTypeList() throws Exception{
	    return service.getParameterTypeList();
	}
	
	@PostMapping(value = "/catalog/getParameters", produces="application/json")
	@ResponseBody
	public String getParameterByTypeList(@RequestParam("parameterTypeId") String parameterTypeId) throws Exception{
	    return service.getParameterByTypeList(parameterTypeId);
	}
	
	@PostMapping(value = "/catalog/getItemsTable", produces="application/json")
	@ResponseBody
	public String getItemsParametersData(@RequestParam("filter") String filterStr) throws Exception{
	    return service.getItemsParametersData(filterStr).toString();
	}
}
