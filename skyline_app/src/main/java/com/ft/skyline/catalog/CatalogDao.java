package com.ft.skyline.catalog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ft.skyline.common.dao.GeneralDao;

import comply.SkyLine.bl.general.ComboBasic;

@Repository
public class CatalogDao extends GeneralDao{
	
	private static final Logger logger = LoggerFactory.getLogger(CatalogDao.class);
	
	public List<ComboBasic> getCompanyList() {

        String sql = "select t.company_id as id, t.company_name as name from p_ft_company t order by lower(company_name)";
        return getSimpleComboBasicList(sql);
    }
	
	public List<ComboBasic> getCatalogNameList(String companyId) {

        String sql = "select t.catalog_name_id as id, t.catalog_name as name from p_ft_catalog_name t where t.company_id = "+companyId+" order by lower(catalog_name)";
        return getSimpleComboBasicList(sql);
    }
	
	public List<ComboBasic> getItemCategoryList() {

        String sql = "select t.item_category_id as id, t.item_category_name as name from p_ft_item_category t order by lower(item_category_name)";
        return getSimpleComboBasicList(sql);
    }
	
	public List<ComboBasic> getItemTypeList() {

        String sql = "select t.item_type_id as id, t.item_type_name as name from p_ft_item_type t order by lower(item_type_name)";
        return getSimpleComboBasicList(sql);
    }
	
	public List<ComboBasic> getParameterTypeList() {

        String sql = "select t.parameter_type_id as id, t.parameter_type as name from p_ft_parameter_type t order by lower(parameter_type)";
        return getSimpleComboBasicList(sql);
    }
	
	public List<ComboBasic> getParameterByTypeList(String parameterTypeId) {

        String sql = "select t.parameter_id as id, t.parameter_name as name, t.parameter_code, t.input_type from p_ft_parameter t where t.parameter_type_id = "+parameterTypeId+" order by lower(parameter_name)";
        return getSimpleComboBasicList(sql);
    }
	
	public JSONObject getItemsParametersData(String filterStr)
	{
		Map<Integer, JSONObject> data = new LinkedHashMap<Integer, JSONObject>();
		Map<Integer, String> uniqueParams = new HashMap<>();
		JSONObject tableObj = new JSONObject();
		Connection con = null;
		PreparedStatement pr_stmt = null;
		ResultSet rs = null;
		try 
		{
			JSONObject filterObj = new JSONObject(filterStr);
			System.out.println(filterStr);
			
			StringBuilder sql = new StringBuilder();
			sql.append("select d.item_id, d.item_number, d.item_description, c.item_category_name, t.item_type_name, dpv.parameter_id, dpv.parameter_name, dpv.parameter_code, dpv.parameter_value\r\n") 
				.append("       ,d.item_type_id, d.item_category_id\r\n")
				.append(	"from d_ft_item d, d_ft_item_parameters_v dpv, p_ft_item_category c, p_ft_item_type t\r\n") 
				.append(	"where 1=1\r\n") 
				.append(	"and d.item_id = dpv.item_id\r\n") 
				.append(	"and d.item_category_id = c.item_category_id\r\n") 
				.append(	"and d.item_type_id = t.item_type_id\r\n")
				.append(	"and lower(d.item_number) like lower(?)\r\n") 
				.append(	"order by d.item_number, to_number(dpv.parameter_code)");
			
			try
	        {
				con = getConnectionFromConnectionPool();
        		pr_stmt = con.prepareStatement(sql.toString());
				pr_stmt.setString(1, "%"+filterObj.getString("item_number")+"%");
				rs = pr_stmt.executeQuery();
				
				while(rs.next())
				{
					int itemId = rs.getInt("item_id");
					String itemNumber = rs.getString("item_number");
					String itemDescription = rs.getString("item_description");
					String itemCategoryName = rs.getString("item_category_name");
					String itemTypeName = rs.getString("item_type_name");
					int parameterId = rs.getInt("parameter_id");
					String parameterName = rs.getString("parameter_name");
					String parameterCode = rs.getString("parameter_code");
					String parameterValue = rs.getString("parameter_value");
					
					if(!uniqueParams.containsKey(parameterId)) {
						uniqueParams.put(parameterId, parameterName+" ("+parameterCode+")");
					}
					
					if(data.containsKey(itemId)) 
					{
						JSONObject itemObj = data.get(itemId);
						JSONObject parameterObj = new JSONObject();
						parameterObj.put("parameter_name", parameterName).put("parameter_code", parameterCode).put("parameter_value", parameterValue);
						itemObj.put("parameter_object", itemObj.getJSONObject("parameter_object").put(String.valueOf(parameterId), parameterObj));
						
						data.put(itemId, itemObj);
					}
					else 
					{
						JSONObject itemObj = new JSONObject();
						
						JSONObject parameterObj = new JSONObject();
						parameterObj.put("parameter_name", parameterName).put("parameter_code", parameterCode).put("parameter_value", parameterValue);

						itemObj.put("item_number", itemNumber).put("item_description", itemDescription).put("item_category_name", itemCategoryName)
								.put("item_type_name", itemTypeName).put("parameter_object", new JSONObject().put(String.valueOf(parameterId), parameterObj));
						
						data.put(itemId, itemObj);
					}
				}
	        }
			catch (SQLException e) {
				logger.error("CatalogDao  getItemsParametersData():", e);
			}
			finally
	        {
	              try
	              {
	            	  if (pr_stmt != null)
	                  {
	            		  pr_stmt.close();
	                  }  
	            	  if (rs != null)
	                  {
	                      rs.close();
	                  }
	              }
	              catch (SQLException sqlEx) 
	              {
	                 System.out.println(sqlEx.toString());
	              }
	             releaseConnection(con);
	        }  
			
			
			JSONArray header = new JSONArray();
			JSONArray tableData = new JSONArray();
			JSONArray tableRowData = new JSONArray();
			SortedMap<Integer, String> sortedUniqueParams = new TreeMap<Integer, String>(uniqueParams);
			
			header.put("ITEM_ID").put("Item ID(Part Number)").put("Name (Description)").put("Category (Group)").put("Type");
			for(Map.Entry<Integer, String> currUnqParam: sortedUniqueParams.entrySet())
			{
				header.put(currUnqParam.getValue());
			}
			
			for(Map.Entry<Integer, JSONObject> currData: data.entrySet())
			{
				tableRowData = new JSONArray();
				JSONObject itemObj = currData.getValue();
				tableRowData.put(currData.getKey()).put(itemObj.get("item_number")).put(itemObj.get("item_description")).put(itemObj.optString("item_category_name")).put(itemObj.optString("item_type_name"));
				JSONObject parameterMap = itemObj.getJSONObject("parameter_object");
				
				for(Map.Entry<Integer, String> currUnqParam: sortedUniqueParams.entrySet())
				{
					String paramId = currUnqParam.getKey().toString();
					
					if(parameterMap.has(paramId))
					{
						JSONObject parameterObj = parameterMap.getJSONObject(paramId);
						tableRowData.put(parameterObj.optString("parameter_value").trim());
					}
					else {
						tableRowData.put("");
					}
				}
				tableData.put(tableRowData);
			}
			tableObj.put("data", tableData).put("header", header);
		}
		catch(Exception ex) {
			logger.error("CatalogDao  getItemsParametersData():", ex);
		}
		return tableObj;
	}
}
