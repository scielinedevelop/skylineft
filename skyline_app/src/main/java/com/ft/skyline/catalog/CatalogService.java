package com.ft.skyline.catalog;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

@Service
public class CatalogService {
	
	@Autowired
	CatalogDao catalogDao;
	
	public String getCompanyList() {
		return new Gson().toJson(catalogDao.getCompanyList());
	}
	
	public String getCatalogNameList(String companyId) {
		return new Gson().toJson(catalogDao.getCatalogNameList(companyId));
	}
	
	public String getItemCategoryList() {
		return new Gson().toJson(catalogDao.getItemCategoryList());
	}
	
	public String getItemTypeList() {
		return new Gson().toJson(catalogDao.getItemTypeList());
	}
	
	public String getParameterTypeList() {
		return new Gson().toJson(catalogDao.getParameterTypeList());
	}
	
	public String getParameterByTypeList(String parameterTypeId) {
		return new Gson().toJson(catalogDao.getParameterByTypeList(parameterTypeId));
	}
	
	public JSONObject getItemsParametersData(String filterStr)
	{
		return catalogDao.getItemsParametersData(filterStr);
	}
}
