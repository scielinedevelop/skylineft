package com.ft.skyline.catalogitem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CatalogItemController {
	
private static final Logger logger = LoggerFactory.getLogger(CatalogItemController.class);
	
	@Autowired
	private CatalogItemService service;	
	
	@RequestMapping(value = "/catalogitem", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView initPage(Model model, @RequestParam("selItemId") String selItemId) {		
		logger.info("catalog item request call for Item id: " + selItemId);	
		model.addAttribute("selItemId", selItemId);
		return new ModelAndView("CatalogItem");		
	}
	
	@PostMapping(value = "/catalogitem/getCatalogNames", produces="application/json")
	@ResponseBody
	public String getCatalogNameList(@RequestParam("companyId") String companyId) throws Exception{
//	    return service.getCatalogNameList(companyId);
		return "";
	}
}
