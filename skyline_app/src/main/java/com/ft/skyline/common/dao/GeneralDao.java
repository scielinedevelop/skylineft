package com.ft.skyline.common.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.ft.skyline.config.DBConfig;
import com.ft.skyline.login.LoginService;

import comply.SkyLine.bl.general.ComboBasic;
import oracle.jdbc.OracleTypes;

@Repository
public class GeneralDao {
	
	@Autowired
	protected JdbcTemplate jdbcTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(GeneralDao.class);
		
	public Map<String, Object> callProcedureReturnsOutObject(String procedureName,Map<String, String> inParameters, Map<String, Integer> outParameters) 
	{  
		Map<String, String> inParams = new HashMap<>();
		SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName(procedureName);
		for (Map.Entry<String, String> entry : inParameters.entrySet()) {
			jdbcCall.addDeclaredParameter(new SqlParameter(entry.getKey(), OracleTypes.VARCHAR));
			inParams.put(entry.getKey(), entry.getValue());
		}
		for (Map.Entry<String, Integer> entry : outParameters.entrySet()) {
			String outParamName = entry.getKey();
			int outType = entry.getValue();
			if (outType == OracleTypes.CURSOR) {
				jdbcCall.addDeclaredParameter(new SqlOutParameter(outParamName, outType, new ColumnMapRowMapper()));
			} else {
				jdbcCall.addDeclaredParameter(new SqlOutParameter(outParamName, outType));
			}
		}

		Map<String, Object> result = jdbcCall.execute(inParams);
		return result;
	}
	
	public List<Map<String, Object>> getListOfMapsBySql(String sql) {
		logger.info("SQL getListOfMapsBySql sql= " + sql);
		List<Map<String, Object>> returnListOfFMap;
		try 
		{
			returnListOfFMap = jdbcTemplate.queryForList(sql);
		} 
		catch (Exception e) {
			returnListOfFMap = null;
			e.printStackTrace();
		}
		return returnListOfFMap;
	}
	
	public List<ComboBasic> getSimpleComboBasicList(String sql) {

        return jdbcTemplate.query(
                sql,
                (rs, rowNum) -> {
                	ComboBasic comboBasic = new ComboBasic();
                    comboBasic.id = rs.getString("id");
                    comboBasic.text = rs.getString("name");
                    return comboBasic;
                }
        );
    }
	
	public String getSequenceId(String name) {
		String userId = "-1";
//		try {
//			userId = generalUtil.getSessionUserId();
//		} catch(Exception e) {
//			// do nothing
//		}
		MapSqlParameterSource sqlParam = new MapSqlParameterSource();
		SimpleJdbcCall packageFunction = new SimpleJdbcCall(jdbcTemplate).withFunctionName("FT_GET_SEQUENCE_ID");
		sqlParam.addValue("NAME_IN", name);
		sqlParam.addValue("USERID_IN", userId);
		sqlParam.addValue("COMMENT_IN", userId);
		sqlParam.addValue("TS_IN", String.valueOf(new Date().getTime())); // ...caching issues workaround 
		return String.valueOf(packageFunction.executeFunction(String.class, (SqlParameterSource) sqlParam));
	}
	
	public Connection getConnectionFromConnectionPool() {
    	
    	Connection _conn = null;
    	try 
    	{
    		_conn = DBConfig.getConnectionFromDataSource();
		} 
    	catch (Exception ex) {			
			ex.printStackTrace();
			logger.error("GeneralDao  getConnectionFromConnectionPool():", ex);
		}
    	return _conn;
    }
    
    public void releaseConnection(Connection conn)
    {
	      try {
	
	          if (conn != null ) 
	          {
	        	  DBConfig.releaseConnectionFromDataSource(conn);
	          }
	      }
	      catch(Exception ex)
	      {
	    	  ex.printStackTrace();
	    	  logger.error("GeneralDao  releaseConnection():", ex);
	      }

    }
}
