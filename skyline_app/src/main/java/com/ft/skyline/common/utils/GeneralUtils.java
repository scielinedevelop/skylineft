package com.ft.skyline.common.utils;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class GeneralUtils {
	
	public static String getNull (String str)
    {
        return StringUtils.hasLength(str)?str:"";
    }
    
    /* overload with defaultString returned instead of null */
    public static String getNull (String str, String defaultString)
    {
        if (str == null || str.equals("null"))
        {
            return (defaultString != null) ? defaultString:"";
        }
        
        return str;
    }
    
    /* like getNull but returns defaultString also for an empty string */
    public static String getNullOrEmpty (String str, String defaultString)
    {
        if (str == null || str.equals("null") || str.equals(""))
        {
            return (defaultString != null) ? defaultString:"";
        }
        
        return str;
    }
}
