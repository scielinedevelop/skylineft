package com.ft.skyline.config;

import java.sql.Connection;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;

@Configuration
@PropertySource("classpath:app.properties")
public class DBConfig {

	@Autowired
	private Environment env;
	private static DataSource dataSource;
	
	@Bean
    public DataSource dataSource() 
    {
    	BasicDataSource bds = new BasicDataSource();
    	
    	bds.setDriverClassName(env.getProperty("db.driverClassName"));
    	bds.setUrl(env.getProperty("db.url"));
    	bds.setUsername(env.getProperty("db.username"));
    	bds.setPassword(env.getProperty("db.password"));
    	
		bds.setInitialSize(10);
		/**
		 * setMaxIdle: The maximum number of connections that can remain idle in the pool, 
		 * 				without extra ones being released, or negative for no limit. Default is 8;
		 */
//		bds.setMaxIdle(20);
		/**
		 * setMinIdle: The minimum number of connections that can remain idle in the pool, without extra ones being created, or zero to create none.
		 */
		bds.setMinIdle(10);
		/**
		 * setMaxTotal: The maximum number of active connections that can be allocated from this pool at the same time, or negative for no limit.
		 */
		bds.setMaxTotal(10000);
		/** 
		 * setRemoveAbandonedOnMaintenance: When true removes abandoned connections on the maintenance cycle (when eviction ends).
		 * This property has no effect unless maintenance is enabled by setting timeBetweenEvictionRunsMillis to a positive value.
		 **/
//		bds.setRemoveAbandonedOnMaintenance(true);
		/**
		 * setRemoveAbandonedTimeout: Timeout in seconds before an abandoned connection can be removed.
		 */
		bds.setRemoveAbandonedTimeout(1200);// 1200 sec -> 20 min
		bds.setLogAbandoned(true);
		bds.setAbandonedUsageTracking(true);
		/**
		 * setTimeBetweenEvictionRunsMillis: The number of milliseconds to sleep between runs of the idle object evictor thread. 
		 * When non-positive, no idle object evictor thread will be run.
		 */
//		bds.setTimeBetweenEvictionRunsMillis(1200000); //1200 sec -> 20 min
		
		printDataSourceStats(bds);
		dataSource = bds;
        return bds;
    }
    
    public static void printDataSourceStats(DataSource ds) {
		BasicDataSource bds = (BasicDataSource) ds;
		System.out.println("InitialSize: " + bds.getInitialSize());
		System.out.println("NumActive: " + bds.getNumActive());
		System.out.println("NumIdle: " + bds.getNumIdle());
		System.out.println("MaxIdle: " + bds.getMaxIdle());
		
	}
    
    @Bean
	public JdbcTemplate getJdbcTemplate() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
		return jdbcTemplate;
	}
    
    public static Connection getConnectionFromDataSource() {
    	Connection conn = DataSourceUtils.getConnection(dataSource);
    	System.out.println("+++++ DBConfig getConnectionFromDataSource: " + conn.toString());
    	printDataSourceStats(dataSource);
		return conn;
	}

	public static void releaseConnectionFromDataSource(Connection con) {
		System.out.println("----- DBConfig releaseConnection: " + con.toString());
		DataSourceUtils.releaseConnection(con, dataSource);
	}
    
}
