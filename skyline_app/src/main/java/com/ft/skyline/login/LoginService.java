package com.ft.skyline.login;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.ft.skyline.auth.AuthService;

import comply.SkyLine.bl.biz.*;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.general.General;
import comply.SkyLine.bl.general.SessionObjectsStoreSingleton;

@Service
public class LoginService {
	
	@Autowired
	AuthService authService;
	private String attachmentsPath;
	
	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);
	
	public ModelAndView loginAction(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		logger.info("start login servlet...");

		String userMessage = "";
		String slashDir = "\\";
		Map<String, String> paramMap = new HashMap<String, String>();

		// Checking user and password from client
		String username = "";
		String password = "";
		String browserName = "na";

		try {
			username = request.getParameter("txtUser");
			password = request.getParameter("txtPassword");
			browserName = request.getParameter("browserName");
		} catch (Exception e) {
			logger.error(e.getMessage() + ", username= " + username + "password= " + password + ", browserName=" + browserName);
			paramMap.put("msg", "Cannot connect due to an error (0)");	
			return new ModelAndView("Login",paramMap);
		}		
		
		boolean isSuccessful = false;
		int[] aRetCode = new int[1];
		User user = authService.authenticateUser(username, password, request.getRemoteAddr(), aRetCode);

		switch (aRetCode[0]) {
		case -2: // error (probably with connection to DB)
			userMessage = "Cannot connect due to an error";
			break;
		case -1: // wrong  user/password
			userMessage = "Wrong user name or password!";
			break;
		case 0: // renew
			userMessage = "Please, change your password. First Entry";
			String userId = "1";	
			paramMap.put("USER_ID", userId);	
			paramMap.put("CHANGE_PASSWORD", "1");			
			break;
		case 1: // success
			isSuccessful = true;
			break;
		case 2: // password is due to expire (grace period)
			userMessage = "Please change your password as it will expire soon";
			isSuccessful = true;
			break;
		case 3: // the account is already locked
			userMessage = "The account is locked. Please contact your administrator";
			break;
		case 4: // expired
			userMessage = "Your password is expired. Please contact your administrator";
			break;
		case 9: // locked
			userMessage = "Maximum number of login retries exceeded.\nThe account has been locked.\nPlease, contact the system administrator";
			break;
		}

		if (isSuccessful) {			
			
			//init prop - just put it in the session (from the singleton)
			AppProperties apropST = null;
			try {
				apropST = AppPropSingleton.getInstance();
				session.setAttribute("AppProp", apropST);
			} catch (Exception e1) {
				logger.error(e1.getMessage() + " apropST= " + apropST);
				paramMap.put("msg", "Cannot connect due to an error (1)");	
				return new ModelAndView("Login",paramMap);
			}
			//set ConnUtility 
			ConnUtility connUtility = null;
			try {
				connUtility = new ConnUtility();
				connUtility.setUrl(apropST.getLookupTablesURL());
				connUtility.setDBuser(apropST.getLookupTablesUser());
				connUtility.setPassword(apropST.getLookupTablesPass());
				session.setAttribute("ConUtil", connUtility);
			} catch (Exception e) {
				logger.error(e.getMessage() + " connUtility= " + connUtility);
				paramMap.put("msg", "Cannot connect due to an error (2)");	
				return new ModelAndView("Login",paramMap);
			}
			
			try {
				handleTmpDir(apropST);
			} catch (Exception e) {
				logger.error(e.getMessage());
				paramMap.put("msg", "Cannot connect due to an error (3)");	
				return new ModelAndView("Login",paramMap);
			}
			try {
				createAttachDir(apropST, slashDir, session);
			} catch (Exception e) {
				logger.error(e.getMessage());
				paramMap.put("msg", "Cannot connect due to an error (3)");	
				return new ModelAndView("Login",paramMap);
			}

			if (attachmentsPath != null && attachmentsPath != "") {
				session.setAttribute("ATTACH_PATH", attachmentsPath);
			}
			
			session.setAttribute("USER", user);
			session.setAttribute("userId", user.USER_ID);
//			session.setAttribute("userName", user.FIRST_NAME + " " + user.LAST_NAME);// formbuilder workaround
//			session.setAttribute("userRole", user.ROLE);// formbuilder workaround
			
			/*
			 * For support Skyline old code
			 */
			General general = new General(connUtility.getUrl(), connUtility.getDBuser(), connUtility.getPassword(),apropST.getLogPath());
			HashMap<String, Object> langCach = SessionObjectsStoreSingleton.getObjCash(); // Field of Singleton object. Hold HashMap's userlang and all json's with userlang languages for all languages which was used after application was running, and users with languages logged on
			HashMap<String, String> mapLang = null; //hold hashmap with userlang language
			String userLang = user.LANG;
			String lanSettings = ""; //hold json with userlang language
			String debugLabelsUpdatesPrefix = ComplyUtils.getNull((String) session.getAttribute("LABELS_UPDATE_DEBUG_PREFIX"));
			if ((langCach.get(userLang + "_MAP") == null && langCach.get(userLang + "_STR") == null)
					|| debugLabelsUpdatesPrefix.equals("1")) //load userlang from database when language loads first time. Once for application running on the server. Or if url conatins labelsUpdatesDebugPrefix=1 
			{
				System.out.println("SessionObjectsStoreSingleton initiation...");
				String debugLangPrefix = ComplyUtils.getNull((String) session.getAttribute("LANG_DEBUG_PREFIX"));
				mapLang = new HashMap<String, String>();
				lanSettings = general.GetLanguageSettings(userLang, mapLang, debugLangPrefix);
				langCach.put(userLang + "_MAP", mapLang);
				langCach.put(userLang + "_STR", lanSettings);
			} else // Loads userlang from singleton 
			{
				System.out.println("Getting language from the session...");
				mapLang = (HashMap<String, String>) langCach.get(userLang + "_MAP");
				lanSettings = (String) langCach.get(userLang + "_STR");
			}
			session.setAttribute("LANG_SETTINGS", lanSettings);
			session.setAttribute("LANG_SETTINGS_MAP", mapLang);
			/* *************************************************** */
			
			return new ModelAndView("redirect:/homePage");
		}
		paramMap.put("userMessage", userMessage);			
		return new ModelAndView("Login", paramMap);
	}
	
	/*
	 * For support Skyline old code
	 */
	private void handleTmpDir(AppProperties apropST) { 
		
		// jasper...
		File jasperTmpDir = new File(apropST.getJasperReportsTmpDir());
		if (!jasperTmpDir.exists()) {
			jasperTmpDir.mkdir();
		}

		//attachment
		File attachMainDir = new File(apropST.getAttachDir());
		if (!attachMainDir.exists()) {
			attachMainDir.mkdir();
		}
	}
	
	/*
	 * For support Skyline old code
	 */
	private void createAttachDir(AppProperties apropST, String slashDir, HttpSession session) { 
		// Getting the path for attachment directory in current session
		attachmentsPath = apropST.getAttachDir() + slashDir + session.getId();
		File attachDir = new File(attachmentsPath);
		attachDir.mkdir();
	}
}
