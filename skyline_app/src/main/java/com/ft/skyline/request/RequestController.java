package com.ft.skyline.request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RequestController {

private static final Logger logger = LoggerFactory.getLogger(RequestController.class);
	
	@Autowired
	private RequestService service;	
	
	@RequestMapping(value = "/request", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView initPage(HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("request main call");	
		return new ModelAndView("RequestMain");		
	}
}
