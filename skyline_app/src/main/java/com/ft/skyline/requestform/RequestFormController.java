package com.ft.skyline.requestform;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RequestFormController {
	
private static final Logger logger = LoggerFactory.getLogger(RequestFormController.class);
	
	@Autowired
	private RequestFormService service;	
	
	@RequestMapping(value = "/requestform", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView initPage(HttpServletRequest request, HttpServletResponse response) {
		
		logger.info("request form call");	
		return new ModelAndView("RequestForm");		
	}
}
