package comply.SkyLine.bl.biz;

/**
 * @author kdvoyashov
 *
 */
public class AppPropSingleton {

	private static final boolean RELOAD_ALWAYS = false; // true for develop 
	private static final AppProperties instance = new AppProperties(); 
	 
	public static synchronized AppProperties getInstance() 
	{ 
		if(instance.getSystem() == null || RELOAD_ALWAYS) {
			System.out.println("propGetter call in AppPropSingleton... RELOAD_ALWAYS=" + RELOAD_ALWAYS);
			instance.propGetter();
		}
		return instance;
	} 
}