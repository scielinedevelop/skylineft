package comply.SkyLine.bl.biz;

import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ft.skyline.login.LoginService;

// This used for getting main application properties
// from configuration file

public class AppProperties {
	
	private String path = null;
	private String lookupTablesDriver = null;
	private String lookupTablesURL = null;
	private String lookupTablesUser = null;
	private String lookupTablesPass = null;
	private String ldapURL = null;
	private String logPath = null;
	private String baseDir = null;
	private String JasperReportpDir = null;
	private String JasperReportpTemplateDir = null;
	private String JasperReportpTmpDir = null;
	private String httpProtocol = null;
	private String attachDir = null;
	private String system = null;
	private String version = null;
	
	private FileBasedConfiguration properties;
	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);
	
	public void propGetter() {

		Parameters params = new Parameters();
        FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                        .configure(params.properties()
                                        .setFileName("app.properties"));
        try {
        	properties = builder.getConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

	}

	public String getLookupTablesDriver() {

		if (properties != null && lookupTablesDriver == null) {
			lookupTablesDriver = (String) properties.getProperty("db.driverClassName");
		}

		return lookupTablesDriver;
	}


	public String getLookupTablesURL() {

		if (properties != null && lookupTablesURL == null) {
			lookupTablesURL = (String) properties.getProperty("db.url");
		}

		return lookupTablesURL;
	}

	public String getLookupTablesUser() {

		if (properties != null && lookupTablesUser == null) {
			lookupTablesUser = (String) properties.getProperty("db.username");
		}

		return lookupTablesUser;
	}

	public String getLookupTablesPass() {

		if (properties != null && lookupTablesPass == null) {
			lookupTablesPass = (String) properties.getProperty("db.passwordEnc");
		}

		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword("skyline"); // could be got from web, env variable
											// using System.getenv(<some key>)
		// EncryptableProperties eprop = new EncryptableProperties(encryptor);
		// eprop.load(new FileInputStream(iniPath));
		String originalPassword = encryptor.decrypt(lookupTablesPass);

		return originalPassword;

		// TODO retrun StandardPBEStringEncryptor
		// encryptor.decr(lookupTablesPass,passKey);
	}

	public String getLDAPURL() {

		if (properties != null && ldapURL == null) {
			ldapURL = (String) properties.getProperty("ldapUrl");
		}

		return ldapURL;
	}

	
	public String getLogPath() {

		if (properties != null && logPath == null) {
			logPath = (String) properties.getProperty("logPath");
		}

		return logPath;
	}


	public String getJasperReportsDir() {

		if (properties != null && JasperReportpDir == null) {
			JasperReportpDir = (String) properties.getProperty("jasperReportsDir");
		}

		return JasperReportpDir;
	}

	public String getJasperReportsTemplateDir() {
		
		if (properties != null && JasperReportpTemplateDir == null) {
			JasperReportpTemplateDir = (String) properties.getProperty("jasperReportsTemplates");
		}

		return JasperReportpTemplateDir;
	}

	public String getJasperReportsTmpDir() {

		if (properties != null && JasperReportpTmpDir == null) {
			JasperReportpTmpDir = (String) properties.getProperty("jasperReportsTmpDir");
		}

		return JasperReportpTmpDir;
	}

	public String getHttpProtocol() {

		if (properties != null && httpProtocol == null) {
			httpProtocol = (String) properties.getProperty("httpProtocol");
		}

		return httpProtocol;
	}

	public String getAttachDir() {

		if (properties != null && attachDir == null) {
			attachDir = (String) properties.getProperty("attachDir");
		}

		return attachDir;
	}

	public String getSystem() {

		if (properties != null && system == null) {
			system = (String) properties.getProperty("system");
		}

		return system;
	}

	public String getVersion() {

		if (properties != null && version == null) {
			version = (String) properties.getProperty("appVersion");
		}

		return version;
	}

	public boolean getAuthentication() {
		
		String stAuthentication = "0";
		boolean authentication = false;

		stAuthentication = (String) properties.getProperty("isLdapAuthentication");
		//stAuthentication = Authentication;

		if (stAuthentication != null) {
			if (stAuthentication.equals("1")) {
				authentication = true;
			}
		}
		if ("1".compareTo("1") == 0) {
			System.out.println("ok");
		} else {
			System.out.println("bad");
		}

		return authentication;
	}
	

	public String getBaseDir() {
		
		if (properties != null && baseDir == null) {
			baseDir = (String) properties.getProperty("basePath");
		}

		return baseDir;
	}

	// add setters
	public void setLookupTablesDriver(String lookupTablesDriver) {
		this.lookupTablesDriver = lookupTablesDriver;
	}

	public void setLookupTablesURL(String lookupTablesURL) {
		this.lookupTablesURL = lookupTablesURL;
	}

	public void setLookupTablesUser(String lookupTablesUser) {
		this.lookupTablesUser = lookupTablesUser;
	}

	public void setLookupTablesPass(String lookupTablesPass) {
		this.lookupTablesPass = lookupTablesPass;
	}

	public void setLdapURL(String ldapURL) {
		this.ldapURL = ldapURL;
	}

	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}

	public void setJasperReportpDir(String jasperReportpDir) {
		JasperReportpDir = jasperReportpDir;
	}

	public void setJasperReportpTemplateDir(String jasperReportpTemplateDir) {
		JasperReportpTemplateDir = jasperReportpTemplateDir;
	}

	public void setJasperReportpTmpDir(String jasperReportpTmpDir) {
		JasperReportpTmpDir = jasperReportpTmpDir;
	}

	public void setHttpProtocol(String httpProtocol) {
		this.httpProtocol = httpProtocol;
	}

	public void setAttachDir(String attachDir) {
		this.attachDir = attachDir;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}
}
