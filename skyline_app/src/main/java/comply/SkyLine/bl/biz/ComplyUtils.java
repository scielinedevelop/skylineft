package comply.SkyLine.bl.biz;

import java.io.File;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;

public class ComplyUtils 
{
    public ComplyUtils() 
    {
    }
    
    /**
     * Return the time for a date
     * @param fDate
     * @param dateFormat
     * @return long
     */
    public static Long GetTimeFromStringDate(String fDate, String dateFormat)
    {
      java.util.Date tDate = new java.util.Date();
      SimpleDateFormat formatter = new SimpleDateFormat (dateFormat);
      
      try
      {
        tDate = formatter.parse(fDate);
        return tDate.getTime();
      }
      catch(Exception ex)
      {
        System.out.println("GetTimeFromStringDate: " + ex.getMessage());
        return Long.MIN_VALUE;
      }
    }
    
    public static boolean CompareDates(String firstDate,String secondDate,String operator)
    {
      SimpleDateFormat formatter = new SimpleDateFormat ("ddMMyyyy");
      java.util.Date fDate = new java.util.Date();
      java.util.Date sDate = new java.util.Date();
      
      try
      {
        fDate = formatter.parse(firstDate);
        sDate = formatter.parse(secondDate);
      }
      catch(Exception ex)
      {
        return false;
      }
      
      if(operator.equals("="))
      {
        return fDate.compareTo(sDate)==0;
      }
      else if(operator.equals("<"))
      {
        return fDate.compareTo(sDate)<0;
      }
      else if(operator.equals(">"))
      {
        return fDate.compareTo(sDate)>0;
      }
      else if(operator.equals(">="))
      {
        return fDate.compareTo(sDate)>=0;
      }
      else if(operator.equals("<="))
      {
        return fDate.compareTo(sDate)<=0;
      }
      else
      {
        return fDate.compareTo(sDate)==0;
      }
      
    }
    public static boolean CompareBigDecimals(BigDecimal firstValue, BigDecimal secondValue, String operator)
    {
      if(operator.equals("="))
      {
        return firstValue.compareTo(secondValue) ==0;
      }
      else if(operator.equals("<"))
      {
        return secondValue.subtract(firstValue).doubleValue() > 0;
      }
      else if(operator.equals(">"))
      {
        return firstValue.subtract(secondValue).doubleValue() > 0;
      }
      else if(operator.equals(">="))
      {
        return firstValue.subtract(secondValue).doubleValue() >= 0;
      }
      else if(operator.equals("<="))
      {
        return secondValue.subtract(firstValue).doubleValue() >= 0;
      }
      else
      {
        return firstValue.compareTo(secondValue) ==0;
      }
    }
    
      /** returns a string based on originalString where all toReplace substrings were replace with replaceWith substrings
       * @param originalString the string to manipulate
       * @param toReplace substring to search for and replace
       * @param replaceWith substring to put instead of all occurances of toReplace
       * @return manipulated string where all toReplace were replaced with replaceWith
       */
      public static String replaceSubstrings (String originalString, String toReplace, String replaceWith)
      { 
          int startInd = 0;
          int occurInd = 0;
          StringBuffer result = new StringBuffer();

          while ((occurInd = originalString.indexOf(toReplace, startInd)) >= 0)
          {
              result.append(originalString.substring(startInd, occurInd));
              result.append(replaceWith);
              startInd = occurInd+toReplace.length();
          }
          result.append(originalString.substring(startInd));
          return result.toString();
      }
      
      public static String getNull (String str)
      {
          if (str == null || str.equals("null"))
          {
              return "";
          }
          
          return str;
      }
      
      /* overload with defaultString returned instead of null */
      public static String getNull (String str, String defaultString)
      {
          if (str == null || str.equals("null"))
          {
              return (defaultString != null) ? defaultString:"";
          }
          
          return str;
      }
      
      /* like getNull but returns defaultString also for an empty string */
      public static String getNullOrEmpty (String str, String defaultString)
      {
          if (str == null || str.equals("null") || str.equals(""))
          {
              return (defaultString != null) ? defaultString:"";
          }
          
          return str;
      }
      
      public static String formatParamInSigns (int count)
      {
          StringBuilder sb = new StringBuilder();
          for (int i = 0; i < count; i++)
          {
              sb.append((i > 0) ? ",":"").append("?");
          }
          
          return sb.toString();
      }
      
      public static String parseENotatedNumber (String eNotatedNumber)
      {
          if (eNotatedNumber.toLowerCase().indexOf("e") == -1)
          { // no E notation
              return eNotatedNumber;
          }
          
          String strParsedNumber = eNotatedNumber; 
          try 
          {
              String[] parts = eNotatedNumber.toLowerCase().split("e");
              Double baseNumber =  Double.parseDouble(parts[0]);
              Double exponent = Math.pow(10, Double.parseDouble(parts[1]));
              strParsedNumber = String.valueOf(baseNumber * exponent);
          } 
          catch (NumberFormatException ex)
          {
              System.out.println("ComplyUtils.parseENotatedNumber: " + ex.toString());            
          }              
          
          return strParsedNumber;
      }
      
      public  static boolean isNullOrEmpty (String str)
      {
          if (str == null || str.equals(""))
          {
              return true;
          }
          
          return false;
      }
      
    /* overload with defaultString returned instead of null and instead of pipe-separated strings 
     * note: to add empty-string to the list, just add a pipe(|) either at the begining or end of the strAsNull */
    public static String getNull (String str, String defaultString, String strAsNull)
    {
        String strings = "|" + strAsNull + "|";
        if (str == null || strings.contains("|" + str + "|"))
        {
            return (defaultString != null) ? defaultString:"";
        }
        
        return str;
    }
    
    public static String getOrdinalSuffix(int number)
    {
        String suffix = "th";
        switch(number%10)
        {
            case 1:
                suffix = "st";
                break;
            case 2:
                suffix = "nd";
                break;
            case 3:
                suffix = "rd";
                break;
            default:
                suffix = "th";
                break;
        }
        
        return suffix;
    }
    
    public static void emptyAttachDirectory(String path)
    {
        File deleteDir = null;
        File tmpFile = null;
        File[] fileList; 
        deleteDir = new File(path);
        fileList = deleteDir.listFiles();

        for(int i=0 ; i<fileList.length ; i++)
        {
            tmpFile = fileList[i];
            try
            {
                 tmpFile.delete();
            }
            catch (Exception e)
            {
                System.out.print(e.toString());           
            }
        }        
    }
    
    public static String getAttachFileName(String attachPath ) 
    {
        File attachDir = new File(attachPath);
        File[] fileList = attachDir.listFiles();
         
        //sort it by lastModified desc... (for overcome emptyAttachDirectory that don't delete open (or recognized as open by JVM) file) 
        Arrays.sort( fileList, new Comparator()
        {
            public int compare(Object o1, Object o2)
            { 
                if (((File)o1).lastModified() > ((File)o2).lastModified()) 
                {
                    return -1;
                } 
                else if (((File)o1).lastModified() < ((File)o2).lastModified()) 
                {
                    return +1;
                } 
                else 
                {
                    return 0;
                }
            } 
        }); 
        
        String fileName;
        if (fileList.length > 0 ) 
        {
            fileName = fileList[0].getAbsolutePath();
        } 
        else 
        {
            fileName = "";
        } 
        return fileName;
    }
    
    public static String slashDate(String date)
    {
            if( date.indexOf("/") > -1 || date.length() != 8 )
            {
                    return date;
            }
            return date.substring(0, 2) +"/"+ date.substring(2, 4) +"/"+ date.substring(4);
    }

    public static String getSubStringLimited(String source, int maxLength,  String suffix) 
    {
        String toReturn = source;
        
        try 
        {
            if(source != null && source.length() > maxLength && maxLength - suffix.length() > 0) 
            {
                toReturn = source.substring(0,maxLength - suffix.length()) + suffix;
            }
        }
        catch (Exception e)
        {
            System.out.println("ComplyUtils.getSubStringLimiteds: " + e.toString());  
        }
        
        return toReturn;
        
    }
    
    public static String correctZeroNumericText (String str) 
    {
        String toReturn = str;
        if ( str != null && str.startsWith("-.") )
        {
            toReturn = str.replaceFirst("-.","-0.");
        } 
        else if( str != null && str.startsWith(".") )
        {
            toReturn = "0" + str;
        }
        return toReturn;
    }
}
