package comply.SkyLine.bl.biz;

import java.util.Date;

public class User
{
      public int USER_ID;
      public String USER_NAME = "";
      public String PASSWORD = "";
      public int SITE_ID;
      public String siteName;
      public String ROLE;
      public String FIRST_NAME;
      public String LAST_NAME;
      public String LDAP_NAME = "";
      public Date LAST_RETRY = new Date();
      public int RETRY_COUNT = 0;
      public int LOCKED = 0;
      public int LAB_ID = 0;
      public int MAINTENANCE_TABLES = 0;
      
      public String LANG = "ENG";

}