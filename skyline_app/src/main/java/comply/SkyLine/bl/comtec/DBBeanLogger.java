package comply.SkyLine.bl.comtec;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import comply.SkyLine.bl.biz.AppPropSingleton;
import comply.SkyLine.bl.biz.AppProperties;

public class DBBeanLogger
{ 
    private Connection con =  null; 
    Properties properties = null;
    private final int MAX_VARCHAR_LENGTH = 4000;
    
    public DBBeanLogger() 
    { 
    
    }
    // Connecting to DB 
    private boolean connect(StringBuilder sbError)
    {  
            boolean isConnect = false;
            try
            { 
            	AppProperties aprop = AppPropSingleton.getInstance(); // kd 24052016. Are got from singleton
            	String url = aprop.getLookupTablesURL();
            	String user = aprop.getLookupTablesUser();
            	String password = aprop.getLookupTablesPass();
                 
                
                if (con == null || con.isClosed()) 
                { 
                try  {
                        //Class.forName(driver); // kd 19052016
                		Class.forName(aprop.getLookupTablesDriver()); // kd 19052016

                      con =  DriverManager.getConnection(url, user, password); 
                      if(con != null) {
                          isConnect = true;
                      }

                    }
                    catch (SQLException sqlEx)
                    {
                       //LG.write("Cannot create a database connection " + sqlEx, "DBBean connect()", sqlEx);
                       sbError.append("DBBeanLogger connection error! - Cannot create a database connection " + sqlEx) ;
                       if(con != null)
                       {
                           //LG.write("The connection is not null. con.toString(): " + con.toString(), "DBBean connect()", null);
                           sbError.append("DBBeanLogger connection error! - The connection is not null. con.toString(): " + con.toString()) ;
                           //con = null;
                       }
                       System.out.print(sqlEx.toString() ); 
                    }
                    catch (Exception ex)
                    {
                        //LG.write(ex.toString(), "DBBean connect()",ex);
                        sbError.append(ex.toString()) ;
                        System.out.print("DBBeanLogger connection error! - Cannot create a database connection."); 
                    }

                }
                else {
                    isConnect = true;
                }
            }
            catch(Exception e)
            {
                //LG.write(e.toString(), "DBBean connect()",e);
                sbError.append("DBBeanLogger connection error! - " + e.toString()) ;
                System.out.print( e.getMessage());          
            }
            return isConnect;
    }
    
    private void disconnect() 
    {
        try
        { 
            if (con != null ) {
                con.close();
                con = null;
            }
        }
        catch(Exception ex)
        {
            System.out.print( ex.getMessage());    
        }
    }
      
    private boolean dbLogWrite(String msg, String source, StringBuilder sbError)
    {
        boolean isOK = false; 
        String sql = ""; 
        CallableStatement proc_stmt = null; 
        
        try
        {
          sql = " call sp_write_to_log(?,?)";
          proc_stmt = con.prepareCall(sql);
          System.out.println("call sp_write_to_log(" + msg + "," + source + ")");
          proc_stmt.setString(1, getNullRestrictedVarcharParam(msg));
          proc_stmt.setString(2, getNullRestrictedVarcharParam(source));
          proc_stmt.execute();  
          isOK = true;
        }
        catch(Exception ex) 
        {
            sbError.append("DBBeanLogger dbLogWrite error! - " + ex.toString()) ;
            System.out.println("DBBeanLogger dbLogWrite error! - " + ex.toString());
        }
        finally 
        {
            try
            { 
                if (proc_stmt != null)
                {
                    proc_stmt.close();
                }
            }
            catch (Exception ex)
            {
              // do nothing
            } 
        }
        return isOK;
    }

    public boolean write(String msg, String source, StringBuilder sbError) 
    {  
        boolean isOK = false;
        //try to connect to the DB
        if(!connect(sbError)) {
            //no connection return flae (with the error in sbError)
            return false;
        }
        else {
             //log message to the DB
             isOK = dbLogWrite(msg,source,sbError);
        }
        
        disconnect();
        
        return isOK;
    }
    
    private String getNullRestrictedVarcharParam (String str)
    {
        if (str == null)
                {
                    return "";
                }
                else {
                    if(str.length() > MAX_VARCHAR_LENGTH) {
                        return str.substring(0,MAX_VARCHAR_LENGTH - 5) + "...";
                    }
                }
                
                return str;
    }
}
