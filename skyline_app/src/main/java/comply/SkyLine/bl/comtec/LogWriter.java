package comply.SkyLine.bl.comtec;

import java.util.Date;
import java.text.*;
import java.io.*;

public class LogWriter {

	private String logPath = "C:\\SkyLineWebAppErr.log";

  
	/**
	 * Constructor for LogWriter.
	 */
	public LogWriter() {
		super();
	}

	public LogWriter(String lgPath) {
		this.logPath = lgPath;
	}
        
        public void   write(String msg, String source, Exception exSource)  { 
        	if(exSource != null)
        	{
        		write( "Error", "Messgae=" + msg + ", Source=" + source + "\n" + ExtractMethodName(exSource));
        	}
        	else
        	{
        		write( "Info", "Messgae=" + msg + ", Source=" + source);
        	}
        }
        
        public void   write(String msg, String source)  {
            StringBuilder sbError = new StringBuilder();
            if(!new DBBeanLogger().write(msg,source,sbError)) {
                //write the logger error to a file
                writeFile(sbError.toString(), "DBBeanLogger");
                //write the original message to a file
                writeFile( msg,  source);
            }
        }
        
        private String ExtractMethodName(Exception ex) { 
        	String toReturn = "";
            try
			{ 
	            if (ex != null) 
	            {
	                String methodFormat = "%s\n";
	                StackTraceElement[] st = ex.getStackTrace(); 
	                StringBuilder mName = new StringBuilder() ;
	                mName.append(String.format(methodFormat, "Exception: "  + ex.toString())); 
	                for (int i = 0; i < st.length; i++) {
	                    mName.append(String.format(methodFormat,st[i].toString())); 
	                }
	        
	                toReturn = mName.toString();
	            }
	            else {
	                toReturn = "StackTrace(logger) - no exception to trace!";
	            }
			}
			catch (Exception e)
			{
				toReturn = "StackTrace(logger) - Error!";
			}
            return toReturn;
        }
/*
	public synchronized void   writeFile(String msg, String source, Exception exSource)  {
		FileOutputStream fos = null;
		PrintWriter pw = null;
		StringBuffer formMsg1 = new StringBuffer("");
		StringBuffer formMsg2 = new StringBuffer("");
		StringBuffer formMsg3 = new StringBuffer("");

		try {
			// If LOG File Greater then 2MB Deleting it
			File logFile = new File(logPath);
			if (logFile.exists() && logFile.length() > 2048000) {
				logFile.delete();
			}

			logFile = null;

			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMM yyyy   kk:mm:ss:SS");
			Date date = new Date(System.currentTimeMillis());
			formMsg1.append("************| ").append(formatter.format(date)).append(" |************");
			formMsg2.append("From : ").append(source);
			formMsg3.append("Error: ").append(msg);

			fos = new FileOutputStream(logPath, true);
			pw = new PrintWriter(fos);
			pw.println(formMsg1.toString());
			pw.println(formMsg2.toString());
			pw.println(formMsg3.toString());
			pw.println("");
		} catch (Exception ex) {
			System.err.println("Exception in Logger :" + ex.getMessage());
		} finally {
			try {

				if (pw != null)
					pw.close();

				if (fos != null)
					fos.close();
			} catch (Exception ex1) {
				// Ignore
			}
		}

	}
 */
	private synchronized void   writeFile(String msg, String source)  {
		FileOutputStream fos = null;
		PrintWriter pw = null;
		StringBuffer formMsg1 = new StringBuffer("");
		StringBuffer formMsg2 = new StringBuffer("");
		StringBuffer formMsg3 = new StringBuffer("");

		try {
			// If LOG File Greater then 2MB Deleting it
			File logFile = new File(logPath);
			if (logFile.exists() && logFile.length() > 2048000) {
				logFile.delete();
			}

			logFile = null;

			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMMM yyyy   kk:mm:ss:SS");
			Date date = new Date(System.currentTimeMillis());
			formMsg1.append("************| ").append(formatter.format(date)).append(" |************");
			formMsg2.append("From : ").append(source);
			formMsg3.append("Error: ").append(msg);

			fos = new FileOutputStream(logPath, true);
			pw = new PrintWriter(fos);
			pw.println(formMsg1.toString());
			pw.println(formMsg2.toString());
			pw.println(formMsg3.toString());
			pw.println("");
		} catch (Exception ex) {
			System.err.println("Exception in Logger :" + ex.getMessage());
		} finally {
			try {

				if (pw != null)
					pw.close();

				if (fos != null)
					fos.close();
			} catch (Exception ex1) {
				// Ignore
			}
		}

	}

}
