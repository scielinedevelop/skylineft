package comply.SkyLine.bl.general;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;


public class BaseBL 
{
	private LogWriter LG = null;   
    private String _connUrl;
    private String _conn_Usr;
    private String _connPass;
    BaseDAL baseD = null;
    
    public BaseBL() 
    {
    	super();
    }
    
    public BaseBL(String path) 
    {
        LG = new LogWriter(path);
        baseD = new BaseDAL(path);  
    }

    public BaseBL(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
    	
    	baseD = new BaseDAL(conURL, conUSER, conPASS);  
        LG = new LogWriter(path);
    }

    public String getConnUrl() {
        return _connUrl;
    }

    public void setConnUrl(String connUrl) {
        _connUrl = connUrl;
    }

    public String getConn_Usr() {
        return _conn_Usr;
    }

    public void setConn_Usr(String conn_Usr) {
        _conn_Usr = conn_Usr;
    }

    public String getConnPass() {
        return _connPass;
    }

    public void setConnPass(String connPass) {
        _connPass = connPass;
    }
    
    public JSONObject getTableModel(List<JSONArray> arrList, final DataTableParamModel param) 
    {
        String sEcho = param.sEcho;
        int iTotalRecords; // total number of records (unfiltered)
        int iTotalDisplayRecords; //value will be set when code filters levels by keyword
        JSONObject jsonResponse = null;
         
         try 
         {
             List<JSONArray> resultsList = arrList;//data that will be shown in the table
             iTotalRecords = resultsList.size();
             List<JSONArray> currentList = new LinkedList<JSONArray>();
             
             if (param.sSearchKeyword!= null && !param.sSearchKeyword.equals(""))
             {
                   for(int i=0; i< resultsList.size(); i++)
                   {
                       JSONArray row = resultsList.get(i);
                       for(int j=0; j < row.length(); j++)
                       {
                           if(param.bSearchable[j] && row.optString(j).toLowerCase().contains(param.sSearchKeyword.toLowerCase()))
                           {
                               currentList.add(row);
                               break;
                           }
                       }
                   }
                     resultsList = currentList;
                     currentList = new LinkedList<JSONArray>(); 
             }
             
             
             
             if(resultsList.size() > 0)
             {
                 for(int i=0; i < param.sSearch.length; i++)
                 {
                     if((param.sSearch[i] != null && !param.sSearch[i].equals("")) && param.bSearchable[i])
                     {
                         for(int j=0; j< resultsList.size(); j++)
                         {
                             JSONArray row = resultsList.get(j);
                             if(row.optString(i).toLowerCase().contains(param.sSearch[i].toLowerCase()))
                             {
                                 currentList.add(row);
                             }     
                         }
                          resultsList = currentList;
                          currentList = new LinkedList<JSONArray>();  
                         //System.out.println(resultsList.toString());
                     } 
                 }  
             }
             
             iTotalDisplayRecords = resultsList.size();// number of rows that match search criterion should be returned
                        
              Collections.sort(resultsList, new Comparator<JSONArray>()
              {
                      @Override
                      public int compare(JSONArray obj1, JSONArray obj2) 
                      {
                              int result = 0;
                              String dateFormat;
                              for(int i=0; i < param.iSortingCols; i++)
                              {
                                      int sortBy = param.iSortCol[i];
                                      if(param.bSortable[sortBy])
                                      {
                                          try //string, numeric, date, double
                                          {
                                              if(param.sSortType[i] != null && !param.sSortType[i].equals(""))
                                              {
                                                  if(param.sSortType[i].equals("numeric"))
                                                  {                                                     
                                                            result = new Integer(obj1.optInt(sortBy)).compareTo(new Integer(obj2.optInt(sortBy))) *
                                                                      (param.sSortDir[i].equals("asc") ? -1 : 1);
                                                      break;
                                                  }
                                                  else if(param.sSortType[i].equals("date"))
                                                  {
                                                      dateFormat = (param.sDateFormat[i].length() == 0)? param.defaultDateFormat:param.sDateFormat[i];
                                                      if(obj1.optString(sortBy).equals("") || obj2.optString(sortBy).equals(""))
                                                      {
                                                          result = obj1.optString(sortBy).compareToIgnoreCase(obj2.optString(sortBy)) *
                                                                          (param.sSortDir[i].equals("asc") ? -1 : 1);
                                                      }
                                                      else
                                                      {
                                                          try
                                                          {
                                                              result = new SimpleDateFormat(dateFormat).parse(obj1.optString(sortBy)).
                                                                      compareTo(new SimpleDateFormat(dateFormat).parse(obj2.optString(sortBy))) *
                                                                                                      (param.sSortDir[i].equals("asc") ? -1 : 1);
                                                          }
                                                          catch (Exception e) 
                                                          {
                                                              System.out.println("BaseBL Exception:  " + e.getMessage());
                                                          }                                                     
                                                      }
                                                      break;
                                                  }
                                                  else if(param.sSortType[i].equals("double"))
                                                  {                                                      
                                                             result = new Double(obj1.optDouble(sortBy)).compareTo(new Double(obj2.optDouble(sortBy))) *
                                                                     (param.sSortDir[i].equals("asc") ? -1 : 1);
                                                      break;
                                                  }
                                                  else
                                                  {
                                                      result = obj1.optString(sortBy).compareToIgnoreCase(obj2.optString(sortBy)) *
                                                                      (param.sSortDir[i].equals("asc") ? -1 : 1);  
                                                      break;
                                                  }
                                              }
                                              else
                                              {
                                                  result = obj1.optString(sortBy).compareToIgnoreCase(obj2.optString(sortBy)) *
                                                                  (param.sSortDir[i].equals("asc") ? -1 : 1);  
                                                  break;
                                              }
                                              
                                          }
                                          catch (Exception e) 
                                          {
                                              System.out.println(e.getMessage());
                                          }
                                      }
                                      if(result!=0)
                                              return result;
                                      else
                                              continue;
                              }
                              return result;
                      }
              });
              
             /** case table use scroll iDisplayLength = -1 **/
              if(param.iDisplayLength != -1)
              {    
                  if(resultsList.size()< param.iDisplayStart + param.iDisplayLength) 
                  {
                          resultsList = resultsList.subList(param.iDisplayStart, resultsList.size());
                  } 
                  else 
                  {
                          resultsList = resultsList.subList(param.iDisplayStart, param.iDisplayStart + param.iDisplayLength);
                  } 
              }
              
             System.out.println("resultsList.toString(): " + resultsList.toString()); 
             
             jsonResponse = new JSONObject();   
             jsonResponse.put("aaData", resultsList);
             jsonResponse.put("sEcho", sEcho);
             jsonResponse.put("iTotalRecords", iTotalRecords);
             jsonResponse.put("iTotalDisplayRecords", iTotalDisplayRecords);
             jsonResponse.put("iDisplayStart", param.iDisplayStart);
             
             System.out.println("jsonResponse.toString(): " + jsonResponse.toString()); 
             }
             catch (Exception e)
             {
                System.out.println("BaseBL.getTableModel(): " + e.toString());
             }
             return jsonResponse;
    }
    
    public  String getTableProperties(String tableName) 
    {
        return baseD.getTableProperties(tableName);
    }
    
    public  JSONArray getMaintTablesStructure() 
    {       
        return baseD.getMaintenanceTablesStructure();
    }
    
    public String getClobContent(String id) 
    {
   	 	return baseD.getClobContent(id);
    }
    
    // save clob with change detection before save
    public String saveStringAsClob(String clobString, String description, String oldClobId) 
    {
	   	 if(!oldClobId.equals("")) {
	   		 String oldClobString = getClobContent(oldClobId); 
	   		 if(oldClobString.equals(clobString))
	   		 {
	   			 return oldClobId;
	   		 }
	   	 }
	   	 return baseD.saveStringAsClob(clobString, description);
    }
}
