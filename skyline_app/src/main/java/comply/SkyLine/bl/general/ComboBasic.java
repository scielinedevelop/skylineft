package comply.SkyLine.bl.general;

import java.util.HashMap;
import java.util.Map;

public class ComboBasic
{
    public String id = "";
    public String text = "";
    public String[] attributes = null;
    private Map<String,Object> attributesMap = null;
    
	public ComboBasic()
    {
		super();
    }
    
    public ComboBasic(int size)
    {
        attributes = new String[(size > 0) ? size:1];
    }
    
    public ComboBasic(int size, Object placeholder)
    {
    	attributesMap = new HashMap<>((size > 0) ? size:1);
    }
    
    public void setAttribute(String key,Object value)
    {
    	attributesMap.put(key, value);
    }
    
    public Map<String,Object> getAttributesMap()
    {
    	return attributesMap;
    }
}
