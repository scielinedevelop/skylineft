package comply.SkyLine.bl.general;

public class ConnUtility {
    public ConnUtility() {
    }
    private String url = "";
    private String DBuser = "";
    private String password = "";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setDBuser(String dBuser) {
        this.DBuser = dBuser;
    }

    public String getDBuser() {
        return DBuser;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}