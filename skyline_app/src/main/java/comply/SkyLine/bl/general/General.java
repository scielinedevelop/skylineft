package comply.SkyLine.bl.general;

import comply.SkyLine.bl.biz.ComplyUtils;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.DGeneral;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONObject;

public class General extends BaseBL
{
    private DGeneral dGeneral = null;
    private LogWriter LG = null;
    
    public General() 
    {
        super();
    }
    
    public General(String path) 
    {
        super(path);
    	dGeneral = new DGeneral(path);
        LG = new LogWriter(path);
    }
    
    public General(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dGeneral = new DGeneral(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }
    
    /** This function returns parameter value from p_system_parameters 
      * according to the parameter name given as parameter 
     * yp 042010
     * @param PARAM_NAME <code>String</code>
     * @return <code>String</code> Parameter value (or default value if value is null)
     */
    public String getSystemParameterValue(String PARAM_NAME)  
    {
      ResultSet rs = null;
      String result = "";

      try
      {
        rs = dGeneral.getSystemParameterValue(PARAM_NAME);
        
        while(rs.next())
        {
            if(rs.getString("PARAM_VALUE") != null && !rs.getString("PARAM_VALUE").equals(""))
            {
                result = rs.getString("PARAM_VALUE");
            }
            else 
            {
                result = ComplyUtils.getNull(rs.getString("DEFAULT_VALUE"));
            }
        }
      }
      catch(Exception ex)
      {
            LG.write(ex.toString(), "General   getSystemParameterValue():", ex);
      }
      finally 
      {
            try
             {
                 if (rs != null)
                 {
                   rs.close();
                 }
                 dGeneral.disconnect();
            }
            catch(SQLException sqlEx) 
            {
                 System.out.println(sqlEx.toString());
            }   
       }

      return result;
    }
    
    public String getSystemParameter (String parameterName)
    {
        ResultSet rs = null;
        String retVal = "";
        
        try
        {
             rs = dGeneral.getSystemParameter(parameterName);
             while(rs.next()) 
             {
                retVal = rs.getString("param_value");
                break;
             }
        } 
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "General.getSystemParameter(): ", ex);
            System.out.println(ex.toString());
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                    dGeneral.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        
        return retVal;                                
    }
    
    /**
     * Returns all responsibility groups that logged in user related to them.
     * @param currUserID - logged in user
     * @return JSONArray of responsibility group ids
     */
    public  JSONArray getRespGroupsByUser (int currUserID)  
    {
        ResultSet rs = null;
        JSONArray ja = new JSONArray();
        
        try
        {
            rs = dGeneral.getRespGroupsByUser(currUserID);
            while(rs.next())
            {
            	ja.put(rs.getString("group_id"));         
            }
        } 
        catch (Exception e) 
        {            
            System.out.println("General getRespGroupsByUser(): " + e);
        }
        finally
        {
    
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch (SQLException sqlEx)
               {
                   System.out.println("General: " + sqlEx);         
               }
                dGeneral.disconnect();
        }
        return ja;
    }
    
    public int saveAttachment(String fileName)
    {
        return dGeneral.saveAttachment(fileName);
    }
    
    public int saveAttachment(String fileName, StringBuilder sbFileContent)
    {
        return dGeneral.saveAttachment(fileName, sbFileContent);
        
    }
    
    public String saveUploadFile(String fileName, InputStream fileInputStream)
    {
    	return dGeneral.saveUploadFile(fileName, fileInputStream);
    }
    
    public InputStream getAttachmentContent (int attachmentID, StringBuilder sbFileName)
    {
        return dGeneral.getAttachmentBinaryStream(attachmentID, sbFileName);
    }
    
    public boolean getAttachmentStringContent(int attachmentID, StringBuilder sbFileContent)
    {
    	return dGeneral.getAttachmentStringContent(attachmentID, sbFileContent);
    }
	
	public boolean deleteAttachment(String deleteAttachID) 
    {
        return dGeneral.deleteAttachment(deleteAttachID);
    }
    
    public String getReportURL (String reportName)
    {
        return dGeneral.getReportURL(reportName);
    }
    
    
     public String GetLanguageSettings(String lang, HashMap<String,String>mLang, String debugPrefix) 
     {
         JSONObject obj = null;
         String retVal = "";
         
         ResultSet rs = null;
         try {
            
            rs = dGeneral.GetLanguageSettings(lang);
            
            obj = new JSONObject();
             while(rs.next()) 
             {
                 obj.put(rs.getString("label_code").trim(), debugPrefix +  rs.getString("display_name").trim());
                 if(mLang != null) 
                 {
                     mLang.put(rs.getString("label_code").trim(), debugPrefix +  rs.getString("display_name").trim());
                 }
             }
            System.out.println("GetLanguageSettings.toString(): " + obj.toString()); 
            retVal = obj.toString();
         }
         catch(Exception ex) {
             LG.write(ex.toString(), "General.GetLanguageSettings():", ex);
         }
         finally
         {
             try {
                 if (rs != null)
                 {
                     rs.close();
                 }
                 dGeneral.disconnect();
             }
             catch (Exception e)
             {
                 // do nothing
             }
         }
         
         return retVal;
     }
     
     public  List<ComboBasic> getUOMGroupList(String selectedGroupId)
     {
        List<ComboBasic> cbList = new ArrayList<ComboBasic>();
     
        try
        {
        	List<Map<String, String>> resultRows = dGeneral.getUOMGroupList(selectedGroupId) ;
        	
        	ComboBasic cb = new ComboBasic();

        	for(Map<String, String> row: resultRows)
             {
            	 cb = new ComboBasic(2,null);
            	 cb.id = row.get("id"); 
            	 cb.text = row.get("name");     
            	 String isActive = row.get("is_active");
            	 cb.setAttribute("is_active", isActive);
            	 if(isActive.equals("0"))
            	 {
            		 cb.setAttribute("disabled","true");
            	 }
            	 cbList.add(cb); 
             }
        } 
        catch (Exception ex) 
        {
             LG.write(ex.toString(), "General    getUOMGroupList(): ", ex);
             System.out.println(ex.toString());
        }
        
        return cbList;
     }
     
     public  List<ComboBasic> getUOMArray(String uomGroupId, String selectedUOM)
     {
        List<ComboBasic> uomList = new ArrayList<ComboBasic>();
     
        try
        {
        	List<Map<String, String>> resultRows = dGeneral.getUOMList(uomGroupId, selectedUOM) ;
        	ComboBasic cb = new ComboBasic();

        	for(Map<String, String> row: resultRows)
             {
        		cb = new ComboBasic();
        		cb.id = row.get("id"); 
        		cb.text = row.get("name");                
                
                 uomList.add(cb); 
             }
        } 
        catch (Exception ex) 
        {
             LG.write(ex.toString(), "General    getUOMArray(): ", ex);
             System.out.println(ex.toString());
        }
        
        return uomList;
     }
     
     public boolean inList(String valuesList, String value)
     {
         boolean found = false;
         String crntToken;

         StringTokenizer st = new StringTokenizer(valuesList, ",");

         while (st.hasMoreTokens()) 
         {
             crntToken = st.nextToken();
             if(crntToken.startsWith("'") && crntToken.endsWith("'"))
             {
                 crntToken = crntToken.substring(1, crntToken.length() - 1);
             }
             if(crntToken.equals(value))
             {
                 found = true;
                 break;
             }
         }

         return found;
     }  

     public  List<ComboBasic> getATUsersList()
     {
        List<ComboBasic> cbList = new LinkedList<ComboBasic>();
        ComboBasic cb = null;
        try
        {
     	   List<Map<String, String>> result = dGeneral.getATUsersList();
     	   for(Map<String, String> data: result)
   		 	{
                cb = new ComboBasic(1,null);
                cb.id = data.get("user_id"); 
                cb.text = data.get("full_name") + " ("+ data.get("user_name") +")";
                cb.setAttribute("user_full_name", data.get("full_name"));
                cbList.add(cb); 
             }
        } 
        catch (Exception ex) 
        {
             LG.write(ex.toString(), "General.getATUsersList(): ", ex);
             System.out.println(ex.toString());
        }
        
        return cbList;
     }
 	
     public  JSONArray getNavigationResult (String procedureName, String currUserID, String paramsArr)  
     {    
         return dGeneral.getNavigationResult(procedureName, currUserID, paramsArr);
     }
     
}