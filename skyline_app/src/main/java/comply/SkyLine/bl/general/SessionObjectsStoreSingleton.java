package comply.SkyLine.bl.general;

import java.util.HashMap;

/**
 * @author kdvoyashov
 * Class uses in LoginServlet. objCash can hold any common objects needs for session. For example userlang language for all session of application for all languages which was 
 * used after application was running, and users with languages logged on. Key is 3 letters of language plus underscore and 3 letter code of type obj (for example MAP or STR),
 * value is object - HashMap< String and any another
 */
public class SessionObjectsStoreSingleton {

	private static HashMap<String, Object> objCash = new HashMap<String, Object>();
	private static SessionObjectsStoreSingleton instance = null;
	private SessionObjectsStoreSingleton() {}
	public static synchronized SessionObjectsStoreSingleton getInstance() 
	{
		if (instance == null) 
		{
			instance = new SessionObjectsStoreSingleton();
			System.out.println("SessionObjectsStoreSingleton singleton initiation...");
		} else
		{
			System.out.println("Instance of SessionObjectsStoreSingleton singleton was created earlier...");
		}
		return instance;
	}
	public static HashMap<String, Object> getObjCash () 
	{
		return getInstance().objCash;
	}
}