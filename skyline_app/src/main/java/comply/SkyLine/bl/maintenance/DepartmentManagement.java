package comply.SkyLine.bl.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.BaseBL;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.dal.maintenance.DDepartmentSettings;

//import comtec.StringParsing;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DepartmentManagement extends MaintenanceBase
{
    JSONObject jsonResponse = null;
    
    private DDepartmentSettings dDept = null;
    private LogWriter LG = null;
    
    public DepartmentManagement() {
    }
    
    public DepartmentManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dDept = new DDepartmentSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }
    
    public int PerformAction(String actionName,int itemID,int userID,String comments) {
        return 0;
    }
    public int DeleteItem(int itemID,int userID,String comments) 
    {
        int retVal = 0;
        try 
        {
            retVal = dDept.DeleteItem(itemID,userID,comments);
            dDept.disconnect();
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DepartmentManagement DeleteItem(): ", ex);
        }
        
        return retVal;
    }
    
    public JSONObject GetTableData(final DataTableParamModel param,String ... methodParam) throws JSONException 
    {
        List<JSONArray> data = new LinkedList<JSONArray>(); //data that will be shown in the table
        try 
        {
            List<DeptData> results = GetDepartmentArray();
            dDept.disconnect();
            
            JSONArray row = null;
            for(DeptData dData:results) 
            {
                row = new JSONArray();
                row.put(dData.DEPT_ID).put(dData.DEPT_NAME).put(dData.ADDRESS).put(dData.TEL_NO);
                data.add(row);
            }
            jsonResponse = getTableModel(data, param);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DepartmentManagement GetTableData(): ", ex);
        }
        return jsonResponse;
    }
    
    private List<DeptData> GetDepartmentArray() 
    {
        List<DeptData> deptList = new LinkedList<DeptData>();
        ResultSet rs = null;
        int i = 1;
        
        try 
        {
            rs = dDept.GetDepartmentList();
            DeptData data = null;
            
            while(rs.next()) 
            {
                data = new DeptData();
                data.DEPT_ID = rs.getInt("dep_id");
                data.DEPT_NAME = rs.getString("dep_name");
                data.ADDRESS = rs.getString("address");
                data.TEL_NO = rs.getString("tel_number");
                data.CREATED_BY = rs.getInt("created_by");
                data.COMMENTS = rs.getString("comments");
                data.UPDATE_BY = rs.getInt("updated_by");
                deptList.add(data);
                
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DepartmentManagement GetDepartmentArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        
        return deptList;
    }
    
    public List<ComboBasic> GetDepartmentCombo() 
    {
        List <ComboBasic> toReturn = new ArrayList <ComboBasic>();
        ComboBasic cb = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dDept.GetDepartmentList();
            
            while(rs.next()) 
            {
                cb = new ComboBasic();
                cb.id = rs.getString("dep_id");
                cb.text = rs.getString("dep_name");
                toReturn.add(cb);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DepartmentManagement GetDepartmentCombo(): ", ex);
        }
        finally 
        {
            dDept.disconnect();
        }
        
        return toReturn;
    }
    
    public DeptData GetDeptData(int deptID) 
    {
        DeptData data = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dDept.GetDeptData(deptID);
            while(rs.next())
            {
                data = new DeptData();
                data.DEPT_ID = rs.getInt("dep_id");
                data.DEPT_NAME = rs.getString("dep_name");
                data.ADDRESS = rs.getString("address");
                data.TEL_NO = rs.getString("tel_number");
                data.CREATED_BY = rs.getInt("created_by");
                data.COMMENTS = rs.getString("comments");
                data.UPDATE_BY = rs.getInt("updated_by");
                break;
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DepartmentManagement GetDeptData(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
                dDept.disconnect();
        }
        return data;
    }
    
    public int SaveData(int deptID,String deptName,String address,String telNo,int userID) 
    {
        int retVal = 0;
        
        try 
        {
            if(deptID == 0 || deptID == -1) 
            {
                retVal = dDept.AddNewDept(deptName,address,telNo,userID);
            }
            else 
            {
                retVal = dDept.UpdateDept(deptID,deptName,address,telNo,userID);
            }
            dDept.disconnect();
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DepartmentManagement SaveData(): ", ex);
        }
        return retVal;
    }
}
