package comply.SkyLine.bl.maintenance;

public class DeptData 
{
    public int DEPT_ID = 0;
    public String DEPT_NAME = "";
    public String ADDRESS = "";
    public String TEL_NO = "";
    public int CREATED_BY = 0;
    public String COMMENTS = "";
    public int UPDATE_BY = 0;
    
    public DeptData() {
    }
}
