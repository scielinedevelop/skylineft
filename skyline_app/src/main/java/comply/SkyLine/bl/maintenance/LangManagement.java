package comply.SkyLine.bl.maintenance;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.dal.maintenance.DLangSettings;

public class LangManagement extends MaintenanceBase
{
    JSONObject jsonResponse = null;
    DLangSettings dLang = null;
    
    private LogWriter LG = null;
    
    public LangManagement() 
    {
        super();
    }
    
    public LangManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dLang = new DLangSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }
    
    
    public JSONObject GetTableData(final DataTableParamModel param,String[] methodParam) throws JSONException 
    {
        //TODO
        return null;
    }
    
    public int DeleteItem(int itemID, int userID, String comments) 
    {
       //TODO
       return 0;
    }
    
    public int saveData(String langCode, String langName, int userID, String comments) 
    {
       //TODO
       return 0;
    }
      
    /**
     * for using in drop down list
     * @return
     */
    public List<ComboBasic> GetLangCombo() 
    {
        List <ComboBasic> toReturn = new ArrayList <ComboBasic>();
        ComboBasic cb = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dLang.GetLangList();
            
            while(rs.next()) 
            {
                cb = new ComboBasic();
                cb.id = rs.getString("LANG_CODE");
                cb.text = rs.getString("LANG_NAME");
                toReturn.add(cb);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "LangManagement GetLangCombo(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                     dLang.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }  
        
        return toReturn;
    }
    
    private List<LangData> getLangArray() 
    {
        //TODO
        return null;
    }

    public int PerformAction(String actionName, int itemID, int userID,  String comments) 
    {
        //TODO
        return 0;
    }
}
