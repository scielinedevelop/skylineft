package comply.SkyLine.bl.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.BaseBL;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.dal.maintenance.DMaintenanceBase;
import comply.SkyLine.dal.maintenance.DUserSettings;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class MaintenanceBase extends BaseBL
{
    
    private LogWriter LG = null;
  
    public MaintenanceBase() 
    {
        super();
    }
    
    public MaintenanceBase(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);        
        
    }
    
    public abstract  JSONObject GetTableData(final DataTableParamModel param,String ... methodParam) throws JSONException;
    
    public abstract int DeleteItem(int itemID,int userID,String comments);
    
    public int deleteItemParams(int userID,String comments, String ... methodParam) 
    {
        return 0;
    }
    
    public abstract int PerformAction(String actionName,int itemID,int userID,String comments);
    
    public List<ComboBasic> getUsersArray(String conURL,String conUSER,String conPASS, String path)
    {
        DMaintenanceBase dMBase = new DMaintenanceBase(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
        List<ComboBasic> list = new ArrayList<ComboBasic>();
        ResultSet rs = null;
        ComboBasic cb = null;
        
        try
        {
            rs = dMBase.getUsersList();
            while(rs.next())
            {
                cb = new ComboBasic(1);
                cb.id = rs.getString("user_id");
                cb.text = rs.getString("full_name");
                cb.attributes[0] = rs.getString("user_name");
                list.add(cb);
            }
        }
        catch (Exception ex)
        {
          LG.write(ex.toString(), "MaintenanceBase getUsersList(): ", ex);
          System.out.println(ex.toString());
        }
        
        return list;
    }
    
}
