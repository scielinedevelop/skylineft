package comply.SkyLine.bl.maintenance;

public class MaintenanceFactory {
    public MaintenanceFactory() 
    {
        super();
    }
    
    
    public static MaintenanceBase GetMaintenanceBL(String conURL,String conUSER,String conPASS, String path,String mType) 
    {
        MaintenanceBase retVal = null;
        if(mType == null) {
            return null;
        }
        if(mType.toLowerCase().equals("users")) 
        {
            retVal = new UsersManagement(conURL,conUSER,conPASS, path);
        }
        else if(mType.toLowerCase().equals("rg")) 
        {
            retVal = new RespGroupManagement(conURL,conUSER,conPASS, path);
        }
        else if(mType.toLowerCase().equals("site")) 
        {
            retVal = new SiteManagement(conURL,conUSER,conPASS, path);
        }
        else if(mType.toLowerCase().equals("dept")) 
        {
            retVal = new DepartmentManagement(conURL,conUSER,conPASS, path);
        }
//        else if(mType.toLowerCase().equals("uom")) 
//        {
//            retVal = new UomManagement(conURL,conUSER,conPASS, path);
//        }
//        else if(mType.toLowerCase().equals("uomgroup")) 
//        {
//            retVal = new UomGroupManagement(conURL,conUSER,conPASS, path);
//        }
        else if(mType.toLowerCase().equals("qclab")) 
        {
            retVal = new QCLABManagement(conURL,conUSER,conPASS, path);
        }
//        else if(mType.toLowerCase().startsWith("parameter")) 
//        {
//            retVal = new ParameterNameManagement(path);
//        }
        
        return retVal;
        
        
    }
}
