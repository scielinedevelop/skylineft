package comply.SkyLine.bl.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.dal.maintenance.DQCLabSettings;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class QCLABManagement extends MaintenanceBase
{
    JSONObject jsonResponse = null;
    DQCLabSettings dQC = null;
    
    private LogWriter LG = null;
    
    public QCLABManagement() 
    {
        super();
    }
    
    public QCLABManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dQC = new DQCLabSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }
    
    
    public JSONObject GetTableData(final DataTableParamModel param,String[] methodParam) throws JSONException 
    {
        List<JSONArray> data = new LinkedList<JSONArray>(); 
        JSONArray row = null;
        
        try
        {
            List<QCLabData> results = getQCLabArray();

            for(QCLabData objData:results) 
            {
                row = new JSONArray();
                row.put(objData.qcLab_id).put(objData.qcLab_name);
                data.add(row);
            }
            jsonResponse = getTableModel(data, param);  
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "QCLABManagement GetTableData(): ", ex);
        }
        return jsonResponse;
    }
    
    public int DeleteItem(int itemID, int userID,String comments) 
    {
        int retVal = 0;
        try 
        {
            retVal = dQC.deleteQCLab(itemID,userID,comments);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "QCLABManagement DeleteItem(): ", ex);
        }
        
        return retVal;
    }
    
    public int saveData(int itemID, String qcName, String address, String telNo, int userID, String comments) 
    {
        int retVal = 0;
        
        try 
        {
            if(itemID == 0 || itemID == -1) 
            {
                retVal = dQC.addNewQCLab(qcName, address, telNo, userID);
            }
            else 
            {
                retVal = dQC.updateQCLab(itemID,qcName,address,telNo,userID, comments);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "QCLABManagement saveData(): ", ex);
        }
        return retVal;
    }
    
    
    
    public int PerformAction(String actionName,int itemID,int userID,String comments) {
        return 0;
    }
    
    public QCLabData getQCLabData(int itemID) 
    {
        QCLabData obj = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dQC.getQCLabData(itemID);
            while(rs.next()) 
            {
                obj = new QCLabData();
                obj.qcLab_id = rs.getInt("QCLAB_ID");
                obj.qcLab_name = rs.getString("QCLAB_NAME");
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomManagement getUomData(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                    dQC.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }                           
        }
        return obj;
    }
    
    
    /**
     * for using in drop down list
     * @return
     */
    public List<ComboBasic> GetQCLabCombo() 
    {
        List <ComboBasic> toReturn = new ArrayList <ComboBasic>();
        ComboBasic cb = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dQC.GetQCLabList();
            
            while(rs.next()) 
            {
                cb = new ComboBasic();
                cb.id = rs.getString("QCLAB_ID");
                cb.text = rs.getString("QCLAB_NAME");
                toReturn.add(cb);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "QCLABManagement GetQCLabCombo(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                     dQC.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }  
        
        return toReturn;
    }
    
    private List<QCLabData> getQCLabArray() 
    {
        List <QCLabData> toReturn = new ArrayList <QCLabData>();
        QCLabData obj = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dQC.GetQCLabList();
            
            while(rs.next()) 
            {
                obj = new QCLabData();
                obj.qcLab_id = rs.getInt("QCLAB_ID");
                obj.qcLab_name = rs.getString("QCLAB_NAME");
                toReturn.add(obj);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "QCLABManagement getQCLabArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                     dQC.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        
        return toReturn;
    }
}
