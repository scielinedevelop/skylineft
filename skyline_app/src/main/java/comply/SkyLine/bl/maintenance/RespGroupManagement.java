package comply.SkyLine.bl.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.dal.maintenance.DRespGroupSettings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RespGroupManagement extends MaintenanceBase
{
    JSONObject jsonResponse = null;
    private DRespGroupSettings dResp = null;
    private LogWriter LG = null;
    
    public RespGroupManagement() 
    {
        super();
    }
    
    public RespGroupManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dResp = new DRespGroupSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }
    
    public JSONObject GetTableData(final DataTableParamModel param,String ... methodParam) throws JSONException 
    {
        List<JSONArray> data = new LinkedList<JSONArray>(); //data that will be shown in the table
        try
        {
            List<RespGroupData> results = GetRespGroupArray();
            
            JSONArray row = null;
            for(RespGroupData gData:results) 
            {
                row = new JSONArray();
                row.put(gData.GROUP_ID).put(gData.GROUP_NAME);
                data.add(row);
            }
            jsonResponse = getTableModel(data, param);  
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement GetTableData(): ", ex);
        }
        return jsonResponse;
    }
    
    public JSONArray getMaintenanceRespGroupTable(String maintenanceType, String objectId) 
    {
        JSONArray data = new JSONArray();

        try 
        {
            List<RespGroupData> resultList = getMaintenanceRespGroupList(maintenanceType, objectId);
            
            for (RespGroupData t: resultList) 
            {
                JSONArray row = new JSONArray();
                row.put(t.GROUP_ID).put(t.selected).put(t.GROUP_NAME);
                data.put(row);
            }
            System.out.println("Maintenance RespGroup by type: " + data.toString());
        } 
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement   getMaintenanceRespGroupTable(): ", ex);
        }
        
        return data;
    }
    
    public int PerformAction(String actionName,int itemID,int userID,String comments) {
        return 0;
    }
    
    public int DeleteItem(int itemID,int userID,String comments) 
    {
        int retVal = 0;
        try 
        {
            
            retVal = dResp.DeleteItem(itemID,userID,comments);
            
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement SaveData(): ", ex);
        }
        return retVal;
    }
    
    private List<RespGroupData> getMaintenanceRespGroupList(String maintenanceType, String objectId) 
    {
        List<RespGroupData> grList = new LinkedList<RespGroupData>();
        RespGroupData data = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dResp.getMaintenanceRespGroupList(maintenanceType, objectId);
            
            while(rs.next()) 
            {
                data = new RespGroupData();
                data.GROUP_ID = rs.getInt("GROUP_ID");
                data.GROUP_NAME = rs.getString("GROUP_NAME");
                data.selected = rs.getInt("selected");
                grList.add(data);
            }
        }
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement getMaintenanceRespGroupList(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                     dResp.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        
        return grList;
    }
    
    private List<RespGroupData> GetRespGroupArray() 
    {
        List<RespGroupData> grList = new LinkedList<RespGroupData>();
        ResultSet rs = null;
        
        try 
        {
            rs = dResp.GetRespGroupsList();
            RespGroupData data = null;
            while(rs.next()) 
            {
                data = new RespGroupData();
                data.GROUP_ID = rs.getInt("GROUP_ID");
                data.GROUP_NAME = rs.getString("GROUP_NAME");
                grList.add(data);
            }
        }
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement GetRespGroupArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                    dResp.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        
        return grList;
    }
    
    public RespGroupData GetGroupData(int groupID) 
    {
        RespGroupData data = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dResp.GetGroupData(groupID);
            while(rs.next()) 
            {
                data = new RespGroupData();
                data.GROUP_ID = rs.getInt("group_id");
                data.GROUP_NAME = rs.getString("GROUP_NAME");
                break;
            }
        }
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement GetGroupData(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                    dResp.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }                           
        }
        return data;
    }
    
    public List<ComboBasic> GetAvailibleUsers(int groupID) 
    {
        List <ComboBasic> retVal = new ArrayList <ComboBasic>();
        ComboBasic cb = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dResp.GetAvailibleUsers(groupID);
            
            while(rs.next()) 
            {
                cb = new ComboBasic();
                cb.id = rs.getString("user_id");
                cb.text = rs.getString("user_name");
                retVal.add(cb);
            }
        }
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement GetAvailibleUsers(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                    dResp.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        return retVal;
    }
    
    public List<ComboBasic> GetSelectedUsers(int groupID) 
    {
        List <ComboBasic> retVal = new ArrayList <ComboBasic>();
        ComboBasic cb = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dResp.GetSelectedUsers(groupID);
            
            while(rs.next()) 
            {
                cb = new ComboBasic();
                cb.id = rs.getString("user_id");
                cb.text = rs.getString("user_name");
                retVal.add(cb);
            }
        }
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement GetSelectedUsers(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                      dResp.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        return retVal;
    }
    
    
    public int SaveData(int groupID,String groupName,String groupMembers,int userID,String comments) 
    {
        int retVal = 0;
        
        try 
        {
            if(groupID == 0 || groupID == -1) 
            {
                retVal = dResp.AddNewGroup(groupName,groupMembers,userID);
            }
            else 
            {
                retVal = dResp.UpdateGroupData(groupID,groupName,groupMembers,userID,comments);
            }
            
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "RespGroupManagement SaveData(): ", ex);
        }
        return retVal;
    }
    
    
}
