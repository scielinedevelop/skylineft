package comply.SkyLine.bl.maintenance;

public class SiteData 
{
    public int SITE_ID = 0;
    public String SITE_NAME = "";
    public String SITE_DISPLAY_NAME = "";
    public int CREATOR_ID = 0;
    public String TIME_STAMP = "";
    public String COMMENTS = "";
    public int UPDATE_BY = 0;
    public int ROW_NUMBER = 0;
    
    public SiteData() 
    {    }
}
