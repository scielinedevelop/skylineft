package comply.SkyLine.bl.maintenance;

//import comtec.StringParsing;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.BaseBL;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.dal.maintenance.DSiteSettings;
import comply.SkyLine.dal.maintenance.DUserSettings;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

public class SiteManagement extends MaintenanceBase
{
    
    JSONObject jsonResponse = null;
    
    private DSiteSettings dSite = null;
    private LogWriter LG = null;
    private int tableRowNumber = 0;
    
    public SiteManagement() 
    {
        super();
    }
    
    public int DeleteItem(int itemID,int userID,String comments) 
    {
        int retVal = 0;
        
        try 
        {
            retVal = dSite.DeleteSite(itemID,userID,comments);
            dSite.disconnect();
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "SieManagement DeleteItem(): ", ex);
        }
        return retVal;
    }
    
    public SiteManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dSite = new DSiteSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }
    
    public JSONObject GetTableData(final DataTableParamModel param,String ... methodParam) throws JSONException 
    {
        List<JSONArray> data = new LinkedList<JSONArray>(); //data that will be shown in the table
        try
        {
            List<SiteData> results = GetSitesArray();
            dSite.disconnect();
        
            JSONArray row = null;
            for(SiteData sData:results) 
            {
                row = new JSONArray();
                row.put(sData.SITE_ID).put(sData.SITE_NAME).put(sData.SITE_DISPLAY_NAME);
                data.add(row);
            }
            jsonResponse = getTableModel(data, param);  
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "SieManagement GetTableData(): ", ex);
        }
        return jsonResponse;
    }
    
    public int PerformAction(String actionName,int itemID,int userID,String comments) {
        return 0;
    }
    
    public List<SiteData> GetSitesArray() 
    {
        List<SiteData> retVal = new LinkedList<SiteData>();
        ResultSet rs = null;
        int i=1;
        
        try 
        {
            rs =  dSite.GetSiteList();
            SiteData data = null;
            
            while(rs.next()) 
            {
                data = new SiteData();
                data.SITE_ID = rs.getInt("SITE_ID");
                data.SITE_NAME = rs.getString("site_name");
                data.SITE_DISPLAY_NAME = rs.getString("site_display_name");
                data.CREATOR_ID = rs.getInt("site_creator_id");
                data.TIME_STAMP = rs.getString("time_stamp");
                data.COMMENTS = rs.getString("comments");
                data.UPDATE_BY = rs.getInt("updated_by");
                data.ROW_NUMBER = i;
                i++;
                retVal.add(data);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "SieManagement getSieArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        return retVal;
    }
    
    /**
     * for use in drop down list 
     * @return
     */
    public List<ComboBasic> GetSiteComboList() 
    {
        List <ComboBasic> toReturn = new ArrayList <ComboBasic>();
        ComboBasic cb = null;
        
        try 
        {
            List<SiteData> siteList = GetSitesArray();
            
            for(SiteData data:siteList) 
            {
                cb = new ComboBasic();
                cb.id = String.valueOf(data.SITE_ID);
                cb.text = data.SITE_NAME;
                toReturn.add(cb);                
            }
            
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "SieManagement GetSiteComboList(): ", ex);
        }
        finally 
        {
            dSite.disconnect();
        }
        
        return toReturn;
    }
    
    public SiteData GetSiteData(int siteID) 
    {
        SiteData data = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dSite.GetSiteData(siteID);
            while(rs.next()) 
            {
                data = new SiteData();
                data.SITE_ID = rs.getInt("SITE_ID");
                data.SITE_NAME = rs.getString("site_name");
                data.SITE_DISPLAY_NAME = rs.getString("site_display_name");
                break;
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "SieManagement GetSiteData(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
                dSite.disconnect();
        }
        return data;
    }
    
    public int SaveData(int siteID,String siteName,String displayName,int userID,String commets) 
    {
        int retVal = 0;
        
        try 
        {
            if(siteID == 0 || siteID == -1) 
            {
                retVal = dSite.AddNewSite(siteName,displayName,userID);
            }
            else 
            {
                retVal = dSite.UpdateSite(siteID,siteName,displayName,userID,commets);
            }
            dSite.disconnect();
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "SieManagement SaveData(): ", ex);
        }
        return retVal;
    }
}
