package comply.SkyLine.bl.maintenance;

public class UomData 
{
    public String uom_name;
    public String uom_description;
    public int uom_group_id;
    public String uom_group;
    public String factor;
    public int is_normal;
    public int is_active;
    
    public UomData() {
    }
}
