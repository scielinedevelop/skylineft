package comply.SkyLine.bl.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.bl.maintenance.MaintenanceBase;
import comply.SkyLine.bl.maintenance.UomGroupData;
import comply.SkyLine.dal.maintenance.DUomGroupSettings;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class UomGroupManagement extends MaintenanceBase
{
    JSONObject jsonResponse = null;        
    private DUomGroupSettings dUomGroup = null;
    private LogWriter LG = null;
    
    public UomGroupManagement() 
    {
        super();
    }
    
    public UomGroupManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dUomGroup = new DUomGroupSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }

    public JSONObject GetTableData(DataTableParamModel param, String... methodParam) 
    {
        List<JSONArray> data = new LinkedList<JSONArray>(); 
        JSONArray row = null;
        
        try
        {
            List<UomGroupData> results = getUomGroupArray();

            for(UomGroupData sData:results) 
            {
                row = new JSONArray();
                row.put(sData.uom_group_id).put(sData.uom_group).put(sData.is_active).put(sData.in_use);
                data.add(row);
            }
            jsonResponse = getTableModel(data, param);  
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomGroupManagement GetTableData(): ", ex);
        }
        return jsonResponse;
    }
    
    @Override
    public int deleteItemParams(int userID, String comments, String[] params) 
    {
//        int retVal = 0;
//        retVal = dUomGroup.deleteUomGroup(params[0], userID, comments);
//        return retVal;
        return 0;
    }
    
    public int DeleteItem(int itemID, int userID, String comments) 
    {
//        return 0;
    	int retVal = 0;
    	retVal = dUomGroup.deleteUomGroup(itemID, userID, comments);
    	return retVal;
    }
    public int PerformAction(String actionName, int itemID, int userID, String comments) 
    {
        return 0;
    }
    
    private List<UomGroupData> getUomGroupArray() 
    {
        List<UomGroupData> retVal = new ArrayList<UomGroupData>();
        ResultSet rs = null;
        UomGroupData data = null;

        try 
        {
            rs =  dUomGroup.getUomGroupList();
            while(rs.next()) 
            {
                data = new UomGroupData();
                data.uom_group_id = rs.getInt("uom_group_id");
                data.uom_group = rs.getString("uom_group");
                data.is_active = rs.getString("is_active");
                data.in_use = rs.getInt("in_use");
                retVal.add(data);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomGroupManagement UomGroupDataArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                     dUomGroup.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        return retVal;        
    }
    
    public UomGroupData getUomGroupData(String uomGroupId) 
    {
        UomGroupData data = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dUomGroup.getUomGroupData(uomGroupId);
            while(rs.next()) 
            {
                data = new UomGroupData();
                data.uom_group_id = rs.getInt("uom_group_id");
                data.uom_group = rs.getString("uom_group");
                data.is_active = rs.getString("is_active");
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomGroupManagement getUomGroupData(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                    dUomGroup.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }                           
        }
        return data;
    }
    
    public int saveData(String uomGroupId, String uomGroup, int active, int userID, String comments) 
    {
        int retVal = 0;
        
        try 
        {
            if(uomGroupId.equals("-1")) 
            {
                retVal = dUomGroup.addNewUomGroup(uomGroup, active, userID);
            }
            else 
            {
                retVal = dUomGroup.updateUomGroup(uomGroupId, uomGroup, active,userID,comments);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomGroupManagement saveData(): ", ex);
        }
        return retVal;
    }

    
}