package comply.SkyLine.bl.maintenance;

import comply.SkyLine.bl.biz.ComplyUtils;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.bl.maintenance.MaintenanceBase;
import comply.SkyLine.bl.maintenance.UomData;
import comply.SkyLine.dal.maintenance.DUomSettings;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class UomManagement extends MaintenanceBase
{
    JSONObject jsonResponse = null;        
    private DUomSettings dUom = null;
    private LogWriter LG = null;
    
    public UomManagement() 
    {
        super();
    }
    
    public UomManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dUom = new DUomSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }

    public JSONObject GetTableData(DataTableParamModel param, String[] methodParam) 
    {
        List<JSONArray> data = new LinkedList<JSONArray>(); 
        JSONArray row = null;
        
        try
        {
            List<UomData> results = getUomArray();

            for(UomData sData:results) 
            {
                row = new JSONArray();
                row.put(sData.uom_name).put(sData.uom_description).put(sData.factor).put(sData.uom_group).put((sData.is_normal == 1)?"Yes":"No").put((sData.is_active == 1)?"Yes":"No");
                data.add(row);
            }
            jsonResponse = getTableModel(data, param);  
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomManagement GetTableData(): ", ex);
        }
        return jsonResponse;
    }
    
    @Override
    public int deleteItemParams(int userID, String comments, String[] params) 
    {
        int retVal = 0;
        retVal = dUom.deleteUom(params[0], userID, comments);
        return retVal;
    }
    
    public int DeleteItem(int itemID, int userID, String comments) 
    {
        return 0;
    }
    public int PerformAction(String actionName, int itemID, int userID, String comments) 
    {
        return 0;
    }
    
    private List<UomData> getUomArray() 
    {
        List<UomData> retVal = new ArrayList<UomData>();
        ResultSet rs = null;
        UomData data = null;

        try 
        {
            rs =  dUom.getUomList();
            while(rs.next()) 
            {
                data = new UomData();
                data.uom_name = rs.getString("name");
                data.uom_description = rs.getString("description");
                data.uom_group = rs.getString("uom_group");
                data.factor = ComplyUtils.getNull(rs.getString("factor"));
                data.is_normal =  rs.getInt("is_normal");
                data.is_active =  rs.getInt("is_active");
                retVal.add(data);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomManagement getUomArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                     dUom.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        return retVal;        
    }
    
    /**
     * for use in drop down list 
     * @return
     */
    public List<ComboBasic> getUomCombo() 
    {
        List<ComboBasic> retVal = new ArrayList<ComboBasic>();
        ResultSet rs = null;
        ComboBasic data = null;

        try 
        {
            rs =  dUom.getUomList();
            while(rs.next()) 
            {
                data = new ComboBasic();
                data.id = rs.getString("id");
                data.text = rs.getString("name");
                retVal.add(data);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomManagement getUomArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                     dUom.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        return retVal;        
    }
    
    public UomData getUomData(String uom) 
    {
        UomData data = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dUom.getUomData(uom);
            while(rs.next()) 
            {
                data = new UomData();
                data.uom_name = rs.getString("uom");
                data.uom_description = rs.getString("description");
                data.uom_group_id = rs.getInt("uom_group_id");
                data.factor = ComplyUtils.getNull(rs.getString("factor"));
                data.is_normal =  rs.getInt("is_normal");
                data.is_active =  rs.getInt("is_active");
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomManagement getUomData(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                    dUom.disconnect();
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }                           
        }
        return data;
    }
    
    public int saveData(String uomOld, String uom, String descr, int userID, String comments, int uomGroupId, String factor, int normal, int active) 
    {
        int retVal = 0;
        
        try 
        {
            if(uomOld.equals("-1")) 
            {
                retVal = dUom.addNewUom(uom, descr, userID, uomGroupId, factor, normal, active);
            }
            else 
            {
                retVal = dUom.updateUom(uomOld, uom, descr,userID,comments, uomGroupId, factor, normal, active);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UomManagement saveData(): ", ex);
        }
        return retVal;
    }
    public String checkIfNormal(int uomGroupId) 
    {
    	String retVal = "0";
    	
    	try 
    	{
    		retVal = dUom.checkIfNormal(uomGroupId);
    	}
    	catch(Exception ex) 
    	{
    		LG.write(ex.toString(), "UomManagement checkIfNormal(): ", ex);
    	}
    	return retVal;
    }
    
    public int unselectLastNormal(String uomForUnselectNormal) 
    {
    	int retVal = 0;
    	
    	try 
    	{
    		retVal = dUom.unselectLastNormal(uomForUnselectNormal);
    	}
    	catch(Exception ex) 
    	{
    		LG.write(ex.toString(), "UomManagement unselectLatNormal(): ", ex);
    	}
    	return retVal;
    }

    
}