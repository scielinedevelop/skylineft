package comply.SkyLine.bl.maintenance;

public class UserData {
    
    
    public int USER_ID = 0;
    public String USER_NAME = "";
    public String SITE_NAME = "";
    public String FIRST_NAME = "";
    public String LAST_NAME = "";
    public String TITLE = "";
    public String ROLE = "";
    public int HIDDEN =0;
    public int DELETED = 0;
    public String ROLE_NAME = "";    
    public int TYPE_ID = 0;
    public int SITE_ID = 0;
    public String PASSWORD = "";
    public String OLD_PASS = "";
    public int LOCKED = 0;
    public String LDAP_NAME = "";
    public int DEPT_ID = 0;
    public String EMAIL = "";
    public int DEVIATION = 0;
    public int ACTIVITY_APP = 0;
    public String INITALS = "";
    public int LAB_ID = 0;
    public int INVENTORY_LIST = 0;
    public int INVENTORY_USAGE = 0;
    public int INVENTORY_TYPE = 0;
    public int MAINTENANCE = 0;
    public int COA = 0;
    public int SAMPLE_APP = 0;
    public int TEST_APP = 0;
    public String LAST_RETRY = "";
    public int RETRY_COUNT;
    public String LAST_PASS_DATE = "";
    public String NEW_PASS = "";
    public String ADD_STATES = "";
    public String REMOVE_STATES = "";
    public int RowOrder = 0;
    public int changed_by;
    public String LANG_CODE = "";
    
    public UserData() {
    }
}
