package comply.SkyLine.bl.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.BaseBL;
import comply.SkyLine.bl.general.ComboBasic;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.bl.general.PasswordCtypt;
import comply.SkyLine.dal.maintenance.DUserSettings;

//import comtec.StringParsing;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UsersManagement  extends MaintenanceBase
{
    
    JSONObject jsonResponse = null;
    
    private DUserSettings dUser = null;
    private LogWriter LG = null;
    private int tableRowNumber = 0;
    
    public UsersManagement() {
        super();
    }
    
    public UsersManagement(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        dUser = new DUserSettings(conURL,conUSER,conPASS, path);
        LG = new LogWriter(path);
    }
    
    public int PerformAction(String actionName,int itemID,int userID,String comments) {
        return 0;
    }
    
    public int DeleteItem(int itemID,int userID,String comments) 
    {
        if(itemID == userID) 
        {
            return -2;
        }
        
        int retVal = 0;
        
        try 
        {
            retVal = dUser.DeleteUser(itemID,userID,comments);
            
        }
        catch(Exception ex) 
        {
            retVal = -1;
        }
        finally 
        {
            dUser.disconnect();
        }
        return retVal;
    }
    
    public JSONObject GetTableData(final DataTableParamModel param,String ... methodParam) throws JSONException 
    {
        List<JSONArray> data = new LinkedList<JSONArray>(); //data that will be shown in the table
        
        
        int showDeleted =  Integer.parseInt(methodParam[0]);
        int showHidden =  Integer.parseInt(methodParam[1]);
        
        try 
        {
            //showDeleted = (int)methodParam[0];
            //showHidden =  (int)methodParam[1];
        }
        catch(Exception ex) {
            
        }
        
        List<UserData>results = GetUsersArray(showDeleted,showHidden);
        dUser.disconnect();
        
        JSONArray row = null;
        for(UserData uData :results) 
        {
            row = new JSONArray();
            row.put(uData.USER_ID).put(uData.USER_NAME).put(uData.SITE_NAME).put(uData.TITLE).put(uData.ROLE_NAME).put(uData.FIRST_NAME).put(uData.LAST_NAME)
            .put(uData.RowOrder).put(uData.DELETED).put(uData.HIDDEN);
            data.add(row);
        }
        
        jsonResponse = getTableModel(data, param);  
        return jsonResponse;
    }
    
    public List<UserData> GetUsersArray(int showDeleted,int showHidden) 
    {
        List<UserData> userList = new LinkedList<UserData>();
        ResultSet rs = null;
        int i = 1;
        
        try 
        {
            rs = dUser.GetUsersList(showDeleted,showHidden);
            UserData data = null;
            while(rs.next()) 
            {
                data = new UserData();
                data.USER_ID = rs.getInt("USER_ID");
                data.USER_NAME = rs.getString("USER_NAME");
                data.SITE_NAME = rs.getString("SITE_NAME");
                data.FIRST_NAME = rs.getString("FIRST_NAME");
                data.LAST_NAME = rs.getString("LAST_NAME");
                data.TITLE = rs.getString("TITLE");
                data.HIDDEN = rs.getInt("HIDDEN");
                data.DELETED = rs.getInt("DELETED");
                data.ROLE_NAME = rs.getString("Role_Name");
                data.ROLE = rs.getString("ROLE");
                data.RowOrder = i;
                i++;
                userList.add(data);
            }
        }
        catch (Exception ex) 
        {
            LG.write(ex.toString(), "UserManagement getUserArray(): ", ex);
        }
        finally
        {
                try
                 {
                     if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch(SQLException sqlEx) 
                {
                     System.out.println(sqlEx.toString());
                }           
        }
        
        return userList;
    }
    
    public List<ComboBasic> GetRoleList(String systemID) 
    {
        List <ComboBasic> toReturn = new ArrayList <ComboBasic>();
        ComboBasic cb = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dUser.GetRoleList(systemID);
            
            while(rs.next()) 
            {
                cb = new ComboBasic();
                cb.id = rs.getString("role_code");
                cb.text = rs.getString("role_name");
                toReturn.add(cb);
            }
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "UserManagement GetRoleList(): ", ex);
        }
        finally 
        {
            dUser.disconnect();
        }
        
        return toReturn;
    }
    
    public UserData GetUserData(String userID) 
    {
        UserData data = null;
        ResultSet rs = null;
        
        try 
        {
            rs = dUser.GetUserData(Integer.parseInt(userID));
            
            while(rs.next()) 
            {
                data = new UserData();
                data.USER_ID = rs.getInt("USER_ID");
                data.USER_NAME = rs.getString("USER_NAME");
                data.SITE_ID = rs.getInt("site_id");
                data.FIRST_NAME = rs.getString("FIRST_NAME");
                data.LAST_NAME = rs.getString("LAST_NAME");
                data.TITLE = rs.getString("TITLE");                                
                data.ROLE = rs.getString("ROLE");
                data.PASSWORD = rs.getString("password");
                data.LOCKED = rs.getInt("locked");
                data.LDAP_NAME = rs.getString("ldap_name");
                data.DEPT_ID = rs.getInt("dep_id");
                data.EMAIL = rs.getString("email");
                data.DEVIATION = rs.getInt("deviation");
                data.ACTIVITY_APP = rs.getInt("activity_approver");
                data.INITALS = rs.getString("initials");
                data.INVENTORY_LIST = rs.getInt("inventory_list");
                data.INVENTORY_TYPE = rs.getInt("inventory_type");
                data.LAB_ID = rs.getInt("lab_id");
                data.INVENTORY_USAGE = rs.getInt("inventory_usage");
                data.MAINTENANCE = rs.getInt("maintenance_tables");
                data.SAMPLE_APP = rs.getInt("approval_sample");
                data.TEST_APP = rs.getInt("approval_test");
                data.COA = rs.getInt("coa");
                data.LAST_PASS_DATE = rs.getString("last_password_date");
                data.RETRY_COUNT = rs.getInt("retry_count");
                data.LAST_RETRY = rs.getString("last_retry");
                data.LANG_CODE = rs.getString("lang_code");
                break;
            }
        }
        catch(Exception ex) 
        {
            System.out.println("UserManagement GetUserData(): " + ex.toString());
            LG.write(ex.toString(), "UserManagement GetUserData(): ", ex);
        }
        finally 
        {
            dUser.disconnect();
        }
        
        return data;
    }
    
    public int SaveUserData(UserData data) 
    {
        int retVal = 0;
        PasswordCtypt crypt = new PasswordCtypt();
        String encryptPass = "";
        try 
        {
            if(data.USER_ID == 0) 
            {
                data.PASSWORD = crypt.GetMD5(data.PASSWORD);
                
            }
            else 
            {
                UserData oldData = GetUserData(String.valueOf(data.USER_ID));
                
                if(oldData.LOCKED == 1 && data.LOCKED == 0) 
                {
                    data.RETRY_COUNT = 0;
                    data.LAST_RETRY = null;
                }
                else 
                {
                    data.RETRY_COUNT = oldData.RETRY_COUNT;
                    data.LAST_RETRY = oldData.LAST_RETRY;
                }
                
                if(data.NEW_PASS.equals("true")) 
                {
                    data.LAST_PASS_DATE = null;
                    //data.PASSWORD = crypt.GetMD5(data.PASSWORD);
                }
                else 
                {
                    data.LAST_PASS_DATE = oldData.LAST_PASS_DATE;
                }
                if(!oldData.PASSWORD.equals(data.PASSWORD))
                {
                    data.PASSWORD = crypt.GetMD5(data.PASSWORD); 
                }
            }
            
            retVal = dUser.SaveData(data.USER_ID,data.USER_NAME,data.PASSWORD,true,data.SITE_ID,data.ROLE,data.FIRST_NAME,data.LAST_NAME,data.INITALS,
                                                        data.TITLE,data.EMAIL,data.DEPT_ID,data.LAB_ID,data.LDAP_NAME,data.MAINTENANCE,data.LOCKED,data.SAMPLE_APP,data.TEST_APP,
                                                        data.DEVIATION,data.ACTIVITY_APP,data.INVENTORY_LIST,data.COA,data.INVENTORY_USAGE,data.changed_by,data.LAST_RETRY,data.RETRY_COUNT,data.LAST_PASS_DATE,
                                                        data.ADD_STATES,data.REMOVE_STATES, data.LANG_CODE, data.INVENTORY_TYPE);
            
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "UserManagement SaveUserData(): ", ex);
        }
        finally 
        {
            dUser.disconnect();
        }
        return retVal;
    }
}
