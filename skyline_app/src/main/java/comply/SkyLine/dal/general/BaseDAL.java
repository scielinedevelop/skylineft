package comply.SkyLine.dal.general;



import java.io.StringReader;
import java.io.Writer;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.CLOB;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.util.LinkedCaseInsensitiveMap;

import com.ft.skyline.config.DBConfig;

import comply.SkyLine.bl.biz.ComplyUtils;
import comply.SkyLine.bl.comtec.LogWriter;

public class BaseDAL 
{
	private LogWriter LG = null;
	private Connection _conn;
    private String _connUrl;
    private String _conn_Usr;
    private String _connPass;
    public static final int UNIQUE_CONSTRAINT_VIOLATION = 1;
    
    public BaseDAL() 
    {
        super();
    }
    public BaseDAL(String path) 
    {
    	LG = new LogWriter(path);
    }
    public BaseDAL(String conURL,String conUSER,String conPASS) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
    }
    
    public BaseDAL(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public Connection getConnection()
    {
        /* variable to determine if the system will connect to database through DataSource Connection Pool or through DriverManager */
    	boolean isDSConn = false;
    	if(!isDSConn)
    	{
	    	try
	        {
	            if(_conn == null || _conn.isClosed()) 
	            {
	            	//String classpath = System.getProperty("java.class.path");
	                Class.forName("oracle.jdbc.OracleDriver");
	                _conn =  DriverManager.getConnection(_connUrl, _conn_Usr, _connPass);
	                System.out.println("+++++ DriverManager.getConnection : " + _conn.toString());
	            }
	            try
                {
                    _conn.prepareStatement("alter session set nls_sort=binary_ci").execute();
                    _conn.prepareStatement("alter session set nls_date_language=american").execute();
                }
	            catch(Exception ex)
	  	      	{
	  	    	  	ex.printStackTrace();
	  	    	  	LG.write(ex.toString(), "BaseDAL getConnection() alter session: ", ex);
	  	      	}
	        }
	    	catch(Exception ex)
  	      	{
  	    	  	ex.printStackTrace();
  	    	  	LG.write(ex.toString(), "BaseDAL getConnection(): ", ex);
  	      	}
    	}
    	else
    	{
    		_conn = getConnectionFromConnectionPool();
    	}
        return _conn;
    }
    
    public void disconnect()
    {
	      try {
	
	          if (_conn != null ) 
	          {
	        	  System.out.println("----- closeConnection: " + _conn.toString());
	        	  _conn.close();
	          }
	      }
	      catch(Exception ex)
	      {
	    	  ex.printStackTrace();
	    	  LG.write(ex.toString(), "BaseDAL disconnect(): ", ex);
	      }

    }
    
    public Connection getConnectionFromConnectionPool() {
    	
    	Connection _conn = null;
    	try 
    	{
    		_conn = DBConfig.getConnectionFromDataSource();
		} 
    	catch (Exception ex) {			
			ex.printStackTrace();
			LG.write(ex.toString(), "BaseDAL getConnectionFromConnectionPool(): ", ex);
		}
    	return _conn;
    }
    
    public void releaseConnection(Connection conn)
    {
	      try {
	
	          if (conn != null ) 
	          {
	        	  DBConfig.releaseConnectionFromDataSource(conn);
	          }
	      }
	      catch(Exception ex)
	      {
	    	  ex.printStackTrace();
	    	  LG.write(ex.toString(), "BaseDAL releaseConnection(): ", ex);
	      }

    }
    
    public String getConnUrl() {
        return _connUrl;
    }

    public void setConnUrl(String connUrl) {
        _connUrl = connUrl;
    }

    public String getConn_Usr() {
        return _conn_Usr;
    }

    public void setConn_Usr(String conn_Usr) {
        _conn_Usr = conn_Usr;
    }

    public String getConnPass() {
        return _connPass;
    }

    public void setConnPass(String connPass) {
        _connPass = connPass;
    }
    
    
    public String getTableProperties (String tableName) 
    {
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = null;
        String obj = "";
        
        try 
        {
        	conn = getConnectionFromConnectionPool();
        	stmt = conn.createStatement();
            sql = "select table_column_properties from DATATABLE_CONFIGURATION where table_name = " + "'"+tableName+"'";
            System.out.println(sql); 
            rs = stmt.executeQuery(sql);   
            
            if(rs.next())
            {
               String prop = rs.getString("table_column_properties");
            	if(prop != null)
                    obj = prop;
                else
                    obj = "";
               //System.out.println(obj);               
            }
        }
        catch(Exception ex) 
        {
        	obj = "";
        	System.out.println(ex);
        }
        finally
        {

            try
             {
            	if (stmt != null)
	             {
            		stmt.close();
	             } 
            	if (rs != null)
	             {
	               rs.close();
	             }                 
            }
            catch (SQLException sqlEx)
            {
               System.out.println("BaseDAL: " + sqlEx);         
            }
            releaseConnection(conn);
        }
        
        return obj;
    }
    
    public JSONArray getMaintenanceTablesStructure () 
    {
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = null;
        JSONArray ja = new JSONArray();
        
        try 
        {
        	conn = getConnectionFromConnectionPool();
        	stmt = conn.createStatement();
            sql = "select * from MAINTENANCE_TABLES_STRUCTURE";
            System.out.println(sql); 
            rs = stmt.executeQuery(sql);      
            
            while(rs.next())
            {
                ja.put(new JSONObject(rs.getString(2)));            
            }
        }
        catch(Exception ex) 
        {
            System.out.println(ex);
        }
        finally
        {
  
                try
                 {
                	if (stmt != null)
	   	             {
	               		stmt.close();
	   	             }  
                	if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch (SQLException sqlEx)
               {
                   System.out.println("BaseBL: " + sqlEx);         
               }
               releaseConnection(conn);
        }
        
        return ja;
    }
    
    // insert varchars into TYPE_TABLE_VARCHAR_10G (global temp table)
    // Used as workaround to ArrayDescriptor with varchar type. ( Note: null values where passed when using oracle 10g + websphere (try was made to add lib from JDEVELOPER jdbc lib but it didn't work) so we insert values to TYPE_TABLE_VARCHAR_10G and use the values in the oracle store procedure)
    public boolean insertTypeTableVarchar(String [] sArray, Connection conn) 
    {
        boolean toReturn = true;
        Statement stmt = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        //Connection conn = getConnection();
         
        try 
        {
            sql.append("INSERT ALL\n");
            for (String s : sArray) {
                sql.append("  INTO TYPE_TABLE_VARCHAR_10G (ARG) VALUES('" + s + "')\n");
            }
            sql.append(" SELECT * FROM dual");
            System.out.println(sql); 
            stmt = conn.createStatement();
            stmt.executeUpdate(sql.toString());        
        }
        catch(Exception ex) 
        {
            toReturn = false;
            System.out.println(ex);
        }
        
        return toReturn;
    }
    
    public boolean insertTypeTableMap(Map<String, String> map, Connection conn) 
    {
        boolean toReturn = true;
        Statement stmt = null;
        StringBuilder sql = new StringBuilder();
//        Connection conn = null;
        String key, value;
         
        try 
        {
//        	conn = getConnection();
        	if(map.size() > 0)
        	{
	        	sql.append("INSERT ALL\n");
	            for (Map.Entry<String, String> entry : map.entrySet()) 
	            {
	                key = entry.getKey();
	                value = entry.getValue();
	                sql.append("  INTO TYPE_TABLE_MAP_10G (field_id, field_val) VALUES('" + key + "', '"+value+"')\n");
	            }
	            sql.append(" SELECT * FROM dual");
	            System.out.println(sql); 
	            stmt = conn.createStatement();
	            stmt.executeUpdate(sql.toString());   
        	}
        }
        catch(Exception ex) 
        {
            toReturn = false;
            System.out.println(ex);
        }
        
        return toReturn;
    }
    
    public ARRAY getOracleSQLNumberArray(Connection con, int[] numberArr, boolean isPoolConnection) 
    {
   	 
	   	 OracleConnection oconn = null;
	   	 ARRAY returnArr = null;
	   	 
	   	 try 
	   	 {
				if (isPoolConnection) {
					oconn = con.unwrap(oracle.jdbc.OracleConnection.class);
				}
				else
				{
					oconn = (OracleConnection) con;
				}
				
				ArrayDescriptor arrDescriptor = ArrayDescriptor.createDescriptor("TYPE_NUMBER_ARRAY", oconn);
				returnArr = new ARRAY(arrDescriptor, oconn, numberArr);
		} 
	   	catch (Exception e) {
	   		 e.printStackTrace();
	   		 LG.write(e.toString(), "BaseDAL getOracleSQLNumberArray(): ", e);
		} 
	   	return returnArr;
    }
    
    
    public List<Map<String, String>> extractDataFromResultSet(ResultSet rs) throws SQLException {
		
//    	final long sqlstartTime = System.nanoTime();
//		System.out.println("!!!START fetch data from result set");
		
    	List<Map<String, String>> currentRows = new ArrayList<Map<String, String>>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		List<String> colTypes = new ArrayList<>(columnCount);
		for (int i = 1; i <= columnCount; i++) {
			colTypes.add(rsmd.getColumnTypeName(i));
		}
		while (rs.next()) {
			Map<String, String> metaDataMap = new LinkedCaseInsensitiveMap<>(columnCount);
			for (int i = 1; i <= columnCount; i++) {
				Object colValObj = getColumnValue(colTypes.get(i - 1), i, rs);
				String colVal = "";
				if (colValObj != null) {
					colVal = ComplyUtils.getNull(colValObj.toString());
				}
				metaDataMap.put(rsmd.getColumnName(i), colVal);
			}
			currentRows.add(metaDataMap);
		}
		
//		final long sqlduration = System.nanoTime() - sqlstartTime;
//        System.out.println("!!!END fetch data from result set -> DURATION: " + sqlduration/1000000 + " milliseconds");
        
		return currentRows;
	}
    
    /**
	 * 
	 * @param columnType - the column type of the current value
	 * @param index - the column index
	 * @param rs - the result set of the current row
	 * @return the value of the column. If the column type is a Clob then it's converted into a plain text.
	 * @throws SQLException
	 */
	private Object getColumnValue(String columnType, int index, ResultSet rs) throws SQLException {
		Object toReturn;
		if (columnType.equals("CLOB")) {
			CLOB clob = (CLOB) rs.getClob(index);
			if (clob != null) {
				toReturn = clob.getSubString(1, (int) clob.length());
			} else {
				toReturn = "";
			}
		} else {
			toReturn = rs.getString(index);
		}
		return toReturn;
	}
	
	public String getClobContent(String id)
    {
    	String content = "";
    	CallableStatement stmt = null;
    	Connection conn = null;
    	
    	try
    	{
	    	conn = getConnectionFromConnectionPool();
	    	String sql = "{? = call fn_get_clob_content(?)}";
	        stmt = conn.prepareCall(sql);         
	        stmt.setString(2, id);
	        stmt.registerOutParameter(1,OracleTypes.CLOB);
	        stmt.execute();
	        Clob clob = (Clob)stmt.getObject(1);
	        
	        if(clob != null)
	        {
	        	content = clob.getSubString(1,(int)clob.length());
	        }	        
		} 
    	catch (Exception e) {
    		 LG.write(e.toString(), "DGeneral    getClobContent(): ", e);
             System.out.println(e.toString());
        }
        finally
        {
        	try
            {
        		if (stmt != null)
                {
                  stmt.close();
                }
            }
            catch (SQLException sqlEx) 
            {
               System.out.println(sqlEx.toString());
            }
           releaseConnection(conn);
        }
    	return content;
    }
    
    public String saveStringAsClob(String clobString, String description) 
    {
		String sql = "";
		CallableStatement stmtInsert = null;
		Connection con = null;
		String retVal = "-1";
		try{
			con = getConnectionFromConnectionPool();
			con.setAutoCommit(false);
			sql = "call SP_INSERT_CLOB(?,?,?)";
			stmtInsert = con.prepareCall(sql);
	        stmtInsert.setString(1,description);
	        stmtInsert.registerOutParameter(2, java.sql.Types.CLOB);  
	        stmtInsert.registerOutParameter(3, OracleTypes.INTEGER);  
	        stmtInsert.executeUpdate();
	        
	        retVal = String.valueOf(stmtInsert.getInt(3));
	        
	        CLOB clob = (CLOB) stmtInsert.getClob(2);
	        setClob(clobString, clob);
	        con.setAutoCommit(true);
	        con.commit();
		}
		catch (Exception e) {     
			e.printStackTrace();
			retVal = "-1";
		}
		finally {
			if(stmtInsert != null){
				try{
					stmtInsert.close();
                    
				}  catch (Exception e) {
					e.printStackTrace();
				}
			}
			releaseConnection(con);
		}
		return retVal;

    }
    
    private void setClob(String clobString, CLOB clob)
	{
		try 
		{
			StringReader inputStreamReader = new StringReader(clobString);	
			Writer clobWriter = clob.getCharacterOutputStream();
			// Buffer to hold chunks of data to being written to the Clob.
	        char[] buffer = new char[10* 1024];

	        // Read a chunk of data from the file input stream, and write the chunk to the Clob column output stream. 
	        // Repeat till file has been fully read.
	        int nread = 0;   // Number of bytes read
	        while((nread= inputStreamReader.read(buffer)) != -1 ) // Read from file
	        {
	        	clobWriter.write(buffer, 0, nread); 
	        }
	        
	        inputStreamReader.close();
	        clobWriter.flush();
	        clobWriter.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
    
}
