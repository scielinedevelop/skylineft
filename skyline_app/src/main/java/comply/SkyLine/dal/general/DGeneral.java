package comply.SkyLine.dal.general;

 

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.Writer;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import comply.SkyLine.bl.biz.ComplyUtils;
import comply.SkyLine.bl.comtec.LogWriter;
import oracle.jdbc.OracleTypes;
import oracle.sql.BLOB;
import oracle.sql.CLOB;

public class DGeneral extends BaseDAL
{
    private LogWriter LG = null;
    
    public DGeneral() 
    {
        super();
    }
    
    public DGeneral (String path) 
    {
        LG = new LogWriter(path);
    }
    
    public DGeneral (String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet getSystemParameterValue(String PARAM_NAME)  
    {
          CallableStatement stmt = null;
          ResultSet rs = null;
          String sql = "";
          Connection con = null;

          try
          {
            con = getConnection();
            sql = "call sp_get_system_parameter(?,?)";
            stmt = con.prepareCall(sql);
            stmt.setString(1, PARAM_NAME);
            stmt.registerOutParameter(2, OracleTypes.CURSOR);
            stmt.execute();  
            rs = (ResultSet)stmt.getObject(2);
          }
          catch(Exception ex)
          {
                LG.write(ex.toString(), "DGeneral   getSystemParameterValue():", ex);
          }

      return rs;
    }
    
    public ResultSet getRespGroupsByUser(int userId)  
    {
          CallableStatement stmt = null;
          ResultSet rs = null;
          String sql = "";
          Connection con = null;

          try
          {
            con = getConnection();
            sql = "call sp_get_resp_group_by_userid(?,?)";
            stmt = con.prepareCall(sql);
            stmt.setInt(1, userId);
            stmt.registerOutParameter(2, OracleTypes.CURSOR);
            stmt.execute();  
            rs = (ResultSet)stmt.getObject(2);
          }
          catch(Exception ex)
          {
                LG.write(ex.toString(), "DGeneral   getRespGroupsByUser():", ex);
          }

      return rs;
    }

    public ResultSet getUserLastSelScheme(int userId, String configName)  
    {
          CallableStatement stmt = null;
          ResultSet rs = null;
          String sql = "";
          Connection con = null;

          try
          {
            con = getConnection();
            sql = "call sp_get_user_config(?,?,?)";
            stmt = con.prepareCall(sql);
            stmt.setInt(1, userId);
            stmt.setString(2, configName);
            stmt.registerOutParameter(3, OracleTypes.CURSOR);
            stmt.execute();  
            rs = (ResultSet)stmt.getObject(3);
          }
          catch(Exception ex)
          {
                LG.write(ex.toString(), "DGeneral   getUserLastSelScheme():", ex);
          }

      return rs;
    }
    
    public int saveUserSchemeCurrent(int userId, String configName, String configValue)  
    {
          CallableStatement stmt = null;
          //ResultSet rs = null;
          String sql = "";
          Connection con = null;
          int result =-1;
          try
          {
            con = getConnection();
            //sql = "call sp_set_user_config(?,?,?,?)";
            sql = "call sp_set_user_config(" + ComplyUtils.formatParamInSigns(4) + ")";
            stmt = con.prepareCall(sql);
            stmt.setInt(1, userId);
            stmt.setString(2, configName);
            stmt.setString(3, configValue);
            stmt.registerOutParameter(4, OracleTypes.NUMBER);
            stmt.execute();  
            result = stmt.getInt(4);
          }
          catch(Exception ex)
          {
        	  result = -1;
              LG.write(ex.toString(), "DGeneral   saveUserSchemeCurrent():", ex);
          }

      return result;
    }
    
    public boolean CheckIfValidWherePart (String as, String sExpression)
    { 
            Statement stmt = null;
            ResultSet rs = null;
            Connection con = null;
            String sql = "";
            boolean bReturn = false;
            
            sql = " select * \n" + 
                    " from (select '' as "+ as +" from dual) \n" + 
                    "  where " + sExpression;
            try
            {
                con = getConnection();
                stmt = con.createStatement();
                rs = stmt.executeQuery(sql);
                bReturn = true;
            }
            catch(Exception ex)
            {
                 LG.write(ex.toString(), "DGeneral   CheckIfValidWherePart():", ex);
            }
            return bReturn;
    }
    
    /* merged from deleted DGenerelQueries, and used for same purpose as getSystemParameterValue() above
     * yet, DB verifies parameter-name as case-insensitive, and value/default-value is set in DB level */
    public ResultSet getSystemParameter (String parameterName)
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_SYSTEM_PARAM_VALUE(" + ComplyUtils.formatParamInSigns(2) + ")";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, parameterName);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception sqlEx) 
        {
             LG.write(sqlEx.toString(), "DGeneral.getSystemParameter(): ( connection = " + conn + ") ", sqlEx);
             System.out.println(sqlEx.toString());
        }
        
        return rs;
    }
    
    /** The method gets an absolute path to a file, creates a new record in p_attachment for the file-name
     * and streams in the file content as a BLOB
     * @parameters fileName <code>String</code> absolute path to file (including its name)
     * @return <code>int</code> attachment_id in p_attachment
     */
    public int saveAttachment(String fileName)
    {
        CallableStatement stmtInsert = null;
        Statement stmtNewAttach = null, stmtBlob = null;
        ResultSet rsNewAttach = null, rsBlob = null;
        String sql;
        int attachID = 0;    
        Connection con = null;
        
        try 
        {
            con = getConnection();
            File mapFile = new File(fileName);
        
            sql = "call SP_INSERT_ATTACHMENT(" + ComplyUtils.formatParamInSigns(3) + ")";
            con.setAutoCommit(false);
            stmtInsert = con.prepareCall(sql);
            stmtInsert.setString(1, mapFile.getName());
            
            stmtInsert.registerOutParameter(2, java.sql.Types.BLOB);
            stmtInsert.registerOutParameter(3, OracleTypes.INTEGER);
            stmtInsert.executeUpdate();
        
            // Get new attachment id
            attachID = stmtInsert.getInt(3);
            
            // Get the Blob locator and open output stream for the Blob
            BLOB mapBlob = (BLOB) stmtInsert.getBlob(2);
            
            //Blob mapBlob = rsBlob.getBlob(1);
            OutputStream blobOutputStream = mapBlob.getBinaryOutputStream();
            
            // Open the sample file as a stream for insertion into the Blob column
            InputStream is = new FileInputStream(mapFile);

            // Buffer to hold chunks of data to being written to the Blob.
            byte[] buffer = new byte[10* 1024];

            // Read a chunk of data from the file input stream, and write the chunk to the Blob column output stream. 
            // Repeat till file has been fully read.
            int nread = 0;   // Number of bytes read
            while((nread= is.read(buffer)) != -1 ) // Read from file
            {
                blobOutputStream.write(buffer, 0, nread);         // Write to Blob
            }

            // Close both streams
            is.close();
            blobOutputStream.flush();        
            blobOutputStream.close();
            con.setAutoCommit(true);
            con.commit();
        } 
        catch( Exception ex)
        { // Trap SQL errors
            try
            {
                con.rollback();
            }
            catch(Exception sqlEx)
            { /* do nothing */ }
            LG.write(ex.toString(), "DGeneral.saveAttachment()", ex);
            System.out.println(ex.toString());
        } 
        finally 
        {
            try {
                if (rsNewAttach != null)
                {
                  rsNewAttach.close();
                }
                if (rsBlob != null)
                {
                  rsBlob.close();
                }        
                if (stmtInsert != null) {
                  stmtInsert.close();
                }
                if (stmtNewAttach != null) {
                  stmtNewAttach.close();
                }  
                if (stmtBlob != null) {
                  stmtBlob.close();
                }        
            }
            catch (Exception ex)
            {
              // do nothing
            }
        }     

        return attachID;
    }
    
    /** The method gets an absolute path to a file, creates a new record in p_attachment for the file-name
     * and streams in the file content as a BLOB
     * @parameters fileName <code>String</code> absolute path to file (including its name)
     *             fileContent <code>StringBuilder</code> return file content for use in the call method
     * @return <code>int</code> attachment_id in p_attachment
     */
    public int saveAttachment(String fileName, StringBuilder sbFileContent)
    {
        CallableStatement stmtInsert = null;
        Statement stmtNewAttach = null, stmtBlob = null;
        ResultSet rsNewAttach = null, rsBlob = null;
        String sql;
        int attachID = 0;    
        Connection con = null;
        
        try 
        {
            con = getConnection();
            File mapFile = new File(fileName);
        
            sql = "call SP_INSERT_ATTACHMENT(" + ComplyUtils.formatParamInSigns(3) + ")";
            con.setAutoCommit(false);
            stmtInsert = con.prepareCall(sql);
            stmtInsert.setString(1, mapFile.getName());
            
            stmtInsert.registerOutParameter(2, java.sql.Types.BLOB);
            stmtInsert.registerOutParameter(3, OracleTypes.INTEGER);
            stmtInsert.executeUpdate();
        
            // Get new attachment id
            attachID = stmtInsert.getInt(3);
            
            // Get the Blob locator and open output stream for the Blob
            BLOB mapBlob = (BLOB) stmtInsert.getBlob(2);
            
            //Blob mapBlob = rsBlob.getBlob(1);
            OutputStream blobOutputStream = mapBlob.getBinaryOutputStream();
            
            // Open the sample file as a stream for insertion into the Blob column
            InputStream is = new FileInputStream(mapFile);

            // Buffer to hold chunks of data to being written to the Blob.
            byte[] buffer = new byte[10* 1024];

            // Read a chunk of data from the file input stream, and write the chunk to the Blob column output stream. 
            // Repeat till file has been fully read.
            int nread = 0;   // Number of bytes read
            while((nread= is.read(buffer)) != -1 ) // Read from file
            {
                 blobOutputStream.write(buffer, 0, nread);         // Write to Blob
                 sbFileContent.append(new String(buffer, 0, nread));
            }

            // Close both streams
            is.close();
            blobOutputStream.flush();        
            blobOutputStream.close();
            con.setAutoCommit(true);
            con.commit();
        } 
        catch( Exception ex)
        { // Trap SQL errors
            try
            {
                con.rollback();
            }
            catch(Exception sqlEx)
            { /* do nothing */ }
            LG.write(ex.toString(), "DGeneral.saveAttachment()", ex);
            System.out.println(ex.toString());
        } 
        finally 
        {
            try {
                if (rsNewAttach != null)
                {
                  rsNewAttach.close();
                }
                if (rsBlob != null)
                {
                  rsBlob.close();
                }        
                if (stmtInsert != null) {
                  stmtInsert.close();
                }
                if (stmtNewAttach != null) {
                  stmtNewAttach.close();
                }  
                if (stmtBlob != null) {
                  stmtBlob.close();
                }        
            }
            catch (Exception ex)
            {
              // do nothing
            }
        }     

        return attachID;
    }
    
    public String saveUploadFile(String fileName, InputStream fileInputStream)
    {
        CallableStatement stmtInsert = null;
        Statement stmtNewAttach = null, stmtBlob = null;
        ResultSet rsNewAttach = null, rsBlob = null;
        String sql;
        String attachID = "-1";    
        Connection con = null;
        
        try 
        {
            con = getConnection();
        
            sql = "call SP_INSERT_ATTACHMENT(" + ComplyUtils.formatParamInSigns(3) + ")";
            con.setAutoCommit(false);
            stmtInsert = con.prepareCall(sql);
            stmtInsert.setString(1, fileName);            
            stmtInsert.registerOutParameter(2, java.sql.Types.BLOB);
            stmtInsert.registerOutParameter(3, OracleTypes.INTEGER);
            stmtInsert.executeUpdate();
        
            // Get new attachment id
            attachID = String.valueOf(stmtInsert.getInt(3));
            
            // Get the Blob locator and open output stream for the Blob
            BLOB mapBlob = (BLOB) stmtInsert.getBlob(2);
            
            //Blob mapBlob = rsBlob.getBlob(1);
            OutputStream blobOutputStream = mapBlob.getBinaryOutputStream();
            
            // Open the sample file as a stream for insertion into the Blob column
            InputStream is = fileInputStream;

            // Buffer to hold chunks of data to being written to the Blob.
            byte[] buffer = new byte[10* 1024];

            // Read a chunk of data from the file input stream, and write the chunk to the Blob column output stream. 
            // Repeat till file has been fully read.
            int nread = 0;   // Number of bytes read
            while((nread= is.read(buffer)) != -1 ) // Read from file
            {
                blobOutputStream.write(buffer, 0, nread);         // Write to Blob
            }

            // Close both streams
            is.close();
            blobOutputStream.flush();        
            blobOutputStream.close();
            con.setAutoCommit(true);
            con.commit();
        } 
        catch( Exception ex)
        { // Trap SQL errors
            try
            {
                con.rollback();
            }
            catch(Exception sqlEx)
            { /* do nothing */ }
            LG.write(ex.toString(), "DGeneral.saveAttachment()", ex);
            System.out.println(ex.toString());
        } 
        finally 
        {
            try {
                if (rsNewAttach != null)
                {
                  rsNewAttach.close();
                }
                if (rsBlob != null)
                {
                  rsBlob.close();
                }        
                if (stmtInsert != null) {
                  stmtInsert.close();
                }
                if (stmtNewAttach != null) {
                  stmtNewAttach.close();
                }  
                if (stmtBlob != null) {
                  stmtBlob.close();
                }        
            }
            catch (Exception ex)
            {
              // do nothing
            }
            disconnect();
        }     

        return attachID;
    }
    
    
    public InputStream getAttachmentBinaryStream(int attachmentID, StringBuilder sbFileName)
    {
        CallableStatement stmt = null;
        String sql = "";
        InputStream blobStream = null;
        Blob blob = null;
        Connection con = null;
        
        try
        {
            con = getConnection();
            sql = "call SP_GET_FILE_NAME_AND_CONTENT(" + ComplyUtils.formatParamInSigns(3) + ")";
            stmt = con.prepareCall(sql);
            stmt.setInt(1, attachmentID);
            stmt.registerOutParameter(2, OracleTypes.VARCHAR);
            stmt.registerOutParameter(3, java.sql.Types.BLOB);            
            stmt.execute();
            
            sbFileName.append(stmt.getString(2));
            blob = stmt.getBlob(3);
            blobStream = blob.getBinaryStream();            
        }
        catch(Exception sqlEx ) 
        { // Trap SQL errors
            blobStream = null;
            LG.write(sqlEx.toString(), "DGeneral.getAttachmentBinaryStream(): ", sqlEx);
            System.out.println(sqlEx.toString());
        } 
        
        return blobStream;
    }
    
    public boolean getAttachmentStringContent(int attachmentID, StringBuilder sbFileContent)
    {
        CallableStatement stmt = null;
        String sql = "";
        InputStream blobStream = null;
        Blob blob = null;
        Connection con = null;
        boolean retVal = true;
        
        try
        {
            con = getConnection();
            sql = "call SP_GET_FILE_NAME_AND_CONTENT(" + ComplyUtils.formatParamInSigns(3) + ")";
            stmt = con.prepareCall(sql);
            stmt.setInt(1, attachmentID);
            stmt.registerOutParameter(2, OracleTypes.VARCHAR);
            stmt.registerOutParameter(3, java.sql.Types.BLOB);            
            stmt.execute();
            
            blob = stmt.getBlob(3);
            blobStream = blob.getBinaryStream(); 
            
            InputStream is = blobStream;
            // Buffer to hold chunks of data to being written to the Blob.
            byte[] buffer = new byte[10* 1024];
            // Read a chunk of data from the file input stream, and write the chunk to the Blob column output stream. 
            // Repeat till file has been fully read.
            int nread = 0;   // Number of bytes read
            while((nread= is.read(buffer)) != -1 ) // Read from file
            {
                 sbFileContent.append(new String(buffer, 0, nread));
            }
            // Close stream
            is.close();
        }
        catch(Exception sqlEx ) 
        { 
        	retVal = false;
        	LG.write(sqlEx.toString(), "DGeneral.getAttachmentStringContent(): ", sqlEx);
            System.out.println(sqlEx.toString());
        } 
        return retVal;
    }
	
	/** general method to delete attachment from p_attachment with assumption all attachments will be gathered in that table
     * @param deleteAttachID the uniqu id in p_attachment
     * @return <code>true</code> if delete succeed <code>false</code> otherwise
     */
    public boolean deleteAttachment(String deleteAttachID) 
    {
        PreparedStatement stmtDeleteAttach = null;
        String sql = "";
        int deletedRow = 0;
        Connection con = null;

        try
        {
            con = getConnection();
            sql = "DELETE FROM P_ATTACHMENT WHERE ATTACH_ID = ?";  
              
            stmtDeleteAttach = con.prepareStatement(sql);
            stmtDeleteAttach.setInt(1, Integer.parseInt(deleteAttachID));
            deletedRow =  stmtDeleteAttach.executeUpdate();
        }
        catch(SQLException sqlEx)
        {
              LG.write(sqlEx.toString(), "DGeneral deleteAttachment(0): ", sqlEx);
              System.out.println("DGeneral deleteAttachment: " + sqlEx.toString());
        }
        catch (Exception ex)
        {
            LG.write(ex.toString(), "DGeneral deleteAttachment(1): ", ex);
            System.out.println("DGeneral deleteAttachment: " + ex.toString());
        }
        finally
        {
          try
          {
              disconnect();
    
              if (stmtDeleteAttach != null) 
              {
                stmtDeleteAttach.close();
              }
          }
          catch (Exception ex)
          {
            // do nothing
          }
        }

       return  (deletedRow == 1);      
    }
    
    public String getReportURL (String reportName)
    {
        CallableStatement stmt = null;
        String reportUrl = "";
        String sql = "";
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_REPORT_URL(" + ComplyUtils.formatParamInSigns(2) + ")";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, reportName);
            stmt.registerOutParameter(2,OracleTypes.VARCHAR);
            stmt.execute();
            reportUrl = stmt.getString(2);
        }
        catch(Exception sqlEx) 
        {
             LG.write(sqlEx.toString(), "DGeneral.getReportURL(): ", sqlEx);
             System.out.println(sqlEx.toString());
        }
        finally
        {
            try
            {                 
                disconnect();
            }
            catch(Exception sqlEx) 
            {
                 System.out.println(sqlEx.toString());
            }           
        }
        
        return reportUrl;
    }
    
    public ResultSet getUserByUserName (String userName)
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_USER_BY_USERNAME(" + ComplyUtils.formatParamInSigns(2) + ")";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, userName);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
             System.out.println("DGeneral.getUserByUserName(): " + ex.toString());
             LG.write(ex.toString(), "DGeneral.getUserByUserName()", ex);
        }
        finally
        {
              try
              {
                  if (stmt != null) 
                  {
                        //stmt.close(); // yp 25112015 - make it a comment -  check if its prevent a bug in java 1.7 ( in Skyline v9.5 installation )
                  }
              }
              catch (Exception e)
              {
                // do nothing
              }
        }
        
        return rs;
    }
    
    public String getDebugDevelopVal() 
    {
        //call example: ((new DAL.GENERAL.DGeneral("jdbc:oracle:thin:@complyora:1521:skyline","skyline_develop","skynew75321","c:\\log")).getDebugDevelopVal())
        String toReturn = "";
        Statement stmt = null;
        String sql = "select * from debug_develop t";
        Connection conn  = getConnection();
        ResultSet rs = null;
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while(rs.next()) {
                toReturn = rs.getString(1);
                break;
            }
        }
        catch (Exception e) {
            
        }
        finally 
        {
            try
             {
                 if (rs != null)
                 {
                   rs.close();
                 }
                disconnect();
            }
            catch(SQLException sqlEx) 
            {
                 System.out.println(sqlEx.toString());
            }   
        }
        return toReturn;
    }

    public ResultSet GetLanguageSettings (String lang)
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_LANG_SETTINGS(" + ComplyUtils.formatParamInSigns(2) + ")";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, lang);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
             System.out.println("DGeneral.GetLanguageSettings(): " + ex.toString());
             LG.write(ex.toString(), "DGeneral.GetLanguageSettings()", ex);
        }
        
        return rs;
    }
    
    public List<Map<String, String>> getUOMGroupList(String selectedGroupId) 
    {
		List<Map<String, String>> result = null;       
        String sql = "call SP_GET_UOM_GROUP_LIST(?,?)";
        
        try(Connection con = getConnectionFromConnectionPool();
        		CallableStatement proc_stmt = con.prepareCall(sql);) 
        {
            proc_stmt.registerOutParameter(1, OracleTypes.CURSOR);
            proc_stmt.setString(2, selectedGroupId);
            proc_stmt.executeQuery();
            ResultSet rs = (ResultSet)proc_stmt.getObject(1);
            
           result = extractDataFromResultSet(rs);  
        }
        catch(Exception ex)
        {
            LG.write(ex.toString(), "DGeneral  getUOMGroupList():", ex);
        }
        
        return result;
    }
    
    public List<Map<String, String>> getUOMList(String uomGroupId, String selectedUOM) 
    {
        
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = null;
        List<Map<String, String>> results = new ArrayList<Map<String, String>>();
        
        try 
        {
            conn = getConnectionFromConnectionPool();
        	sql = "call SP_GET_UOM_LIST(?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.setString(2, uomGroupId);
            stmt.setString(3, selectedUOM);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
            results = extractDataFromResultSet(rs);
            
        }
        catch(Exception sqlEx) 
        {
             LG.write(sqlEx.toString(), "DGeneral    getUOMList(): ", sqlEx);
             System.out.println(sqlEx.toString());
        }
        finally
        {
        	try
            {
                  if (rs != null)
                  {
                    rs.close();
                  }
            }
            catch (SQLException sqlEx) 
            {
               System.out.println(sqlEx.toString());
            }
           releaseConnection(conn);
        }
        
        return results;
    }
    
    public List<Map<String, String>> getATUsersList() 
    {
        
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        List<Map<String, String>> result = null;
        Connection conn = null;
        
        try
        {
            conn = getConnectionFromConnectionPool();
        	sql = "call SP_GET_USERS_FOR_AUDIT_TRAIL(?)";
            stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
            
            result = extractDataFromResultSet(rs);       
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DGeneral getATUsersList(): ", ex);
            System.out.println(ex.toString());
        }
        finally {
        	try
            {
        		if (stmt != null)
                {
                  stmt.close();
                }
            }
            catch (SQLException sqlEx) 
            {
               System.out.println(sqlEx.toString());
            }
           releaseConnection(conn);
        }
        return result;
    }
    
    /**
     * Returns text message for user describes tests and parameters without value for each resultID from the list.
     * @param resultIDList
     * @return String
     */
    public String getStringOfLinkedTestsParamsWithoutValue (String resultIDList) 
    {
        CallableStatement clb_stmt = null;
        String sql = "";
        Connection conn = null;
        String retval = "";
        
        try 
        {
            conn = getConnectionFromConnectionPool();
            sql = "call get_lst_of_lnk_test_and_param(?,?)";
            clb_stmt = conn.prepareCall(sql);
            clb_stmt.setString(1, resultIDList);            
            clb_stmt.registerOutParameter(2, OracleTypes.VARCHAR);
            clb_stmt.executeQuery();
            retval = clb_stmt.getString(2);
        }
        catch(Exception ex) 
        {
            System.out.println("DGeneral getStringOfLinkedTestsParamsWithoutValue():  "+ex);
        }
        finally 
        {
            try {
                
                if (clb_stmt != null) {
                	clb_stmt.close();
                }
            }
            catch (Exception ex)
            {
            	System.out.println(ex.toString());
            	ex.printStackTrace();
            }
            
            releaseConnection(conn);
        }
        
        return retval;
    }
    
    public JSONArray getNavigationResult (String procedureName, String currUserID, String paramsArr) 
    {
    	CallableStatement clb_stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = null;
        int num = 0;
        JSONArray ja = null;
        
        try 
        {
            conn = getConnectionFromConnectionPool();
            ja = new JSONArray(paramsArr);
            num = ja.length() + 2; // +1-user param, +1-return object
            sql = "call " + procedureName + "(" +  ComplyUtils.formatParamInSigns(num) + ")";
            clb_stmt = conn.prepareCall(sql);
            clb_stmt.setString(1, currUserID);
            for(int i=0; i < ja.length();i++)
            {
                clb_stmt.setString(i+2, ja.optString(i));    
            }
            clb_stmt.registerOutParameter(num, OracleTypes.CURSOR);
            clb_stmt.executeQuery();
            rs = (ResultSet)clb_stmt.getObject(num);
            
            ja = new JSONArray();
            if(rs.next())
            {
                for(int i=1; i <= rs.getMetaData().getColumnCount();i++ )
                {
                    ja.put(ComplyUtils.getNull(rs.getString(i))); 
                }         
            }
        }
        catch(Exception ex) 
        {
            System.out.println("DGeneral getNavigationResult():  "+ex);
        }
        finally
        {
    
                try
                 {
                	 if (clb_stmt != null)
                     {
                		 clb_stmt.close();
                     } 
                	 if (rs != null)
                     {
                       rs.close();
                     }
                }
                catch (SQLException sqlEx)
               {
                   System.out.println("DGeneral: " + sqlEx);         
               }
                releaseConnection(conn);
        }
        
        return ja;
    }
}
