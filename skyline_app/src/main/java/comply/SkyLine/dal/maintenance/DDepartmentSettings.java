package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import oracle.jdbc.OracleTypes;

public class DDepartmentSettings extends BaseDAL
{
    private LogWriter LG = null;
    public static final int UniqueConstraintORA00001 = 1;
    
    public DDepartmentSettings() 
    {
        super();
    }
    
    public DDepartmentSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet GetDepartmentList() 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_GET_DEPARTMENT_LIST(?)";
            stmt = conn.prepareCall(sql);            
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DDepartmentSettings GetDepartmentList(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public ResultSet GetDeptData(int deptID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_GET_DEPT_DATA(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,deptID);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DDepartmentSettings GetDeptData(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public int AddNewDept(String deptName,String address,String telNo,int userID) 
    {
        CallableStatement stmt = null;
        int retVal = 0;
        String sql = "";
        
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_ADD_NEW_DEPT(?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1,deptName);
            stmt.setString(2,address);
            stmt.setString(3,telNo);
            stmt.setInt(4,userID);
            stmt.registerOutParameter(5,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = stmt.getInt(5);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DDepartmentSettings AddNewDept(): ", ex);
            System.out.println(ex.toString());
        }
        return retVal;
    }
    
    public int UpdateDept(int deptID,String deptName,String address,String telNo,int userID) 
    {
        CallableStatement stmt = null;
        int retVal = 0;
        String sql = "";
        
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_UPDATE_DEPT(?,?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,deptID);
            stmt.setString(2,deptName);
            stmt.setString(3,address);
            stmt.setString(4,telNo);
            stmt.setInt(5,userID);
            stmt.registerOutParameter(6,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = stmt.getInt(6);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DDepartmentSettings UpdateDept(): ", ex);
            System.out.println(ex.toString());
        }
        return retVal;
    }
    
    public int DeleteItem(int itemID,int userID,String comments) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal =0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_DELETE_DEPT(?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,itemID);
            stmt.setInt(2,userID);
            stmt.setString(3,comments);
            stmt.registerOutParameter(4,java.sql.Types.INTEGER);
            
            stmt.execute();
            retVal = stmt.getInt(4);
            
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DRespGroupSettings GetGroupData(): ", ex);
            System.out.println(ex.toString());
        }
        return retVal;
    }
}
