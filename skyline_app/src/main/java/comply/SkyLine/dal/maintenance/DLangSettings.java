package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import oracle.jdbc.OracleTypes;

public class DLangSettings extends BaseDAL
{
    private LogWriter LG = null;
    public static final int UniqueConstraintORA00001 = 1;
    
    public DLangSettings() 
    {
        super();
    }
    
    public DLangSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet GetLangList() 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_LANG_LIST(?)";
            stmt = conn.prepareCall(sql);            
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DLangSettings GetLangList(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
     
    public int addNewLang(String langCode, String langName) 
    {
        //TODO
        return 0;
    }
      
    public int deleteLang(String langCode) 
    {
        //TODO
        return 0;
    }
}
