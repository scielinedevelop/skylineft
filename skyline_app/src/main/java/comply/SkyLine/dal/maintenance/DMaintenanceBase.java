package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import oracle.jdbc.OracleTypes;

public class DMaintenanceBase extends BaseDAL
{
  private LogWriter LG = null;
  public static final int UniqueConstraintORA00001 = 1;
  
  public DMaintenanceBase() {
      super();
  }
  
  public DMaintenanceBase(String conURL,String conUSER,String conPASS, String path) 
  {
      setConnUrl(conURL);
      setConn_Usr(conUSER);
      setConnPass(conPASS);
      LG = new LogWriter(path);
  }
  
  public ResultSet getUsersList() 
  {
      
      CallableStatement stmt = null;
      ResultSet rs = null;
      String sql = "";
      
      Connection conn = getConnection();
      
      try
      {
          sql = "call SP_GET_USERS_FOR_AUDIT_TRAIL(?)";
          stmt = conn.prepareCall(sql);
          stmt.registerOutParameter(1,OracleTypes.CURSOR);
          stmt.execute();
          rs = (ResultSet)stmt.getObject(1);
      }
      catch(Exception ex) 
      {
          LG.write(ex.toString(), "DMaintenanceBase getUsersList(): ", ex);
          System.out.println(ex.toString());
      }
      return rs;
  }
}
