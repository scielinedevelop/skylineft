package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import oracle.jdbc.OracleTypes;

public class DQCLabSettings extends BaseDAL
{
    private LogWriter LG = null;
    
    public DQCLabSettings() 
    {
        super();
    }
    
    public DQCLabSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet GetQCLabList() 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_QCLAB_LIST(?)";
            stmt = conn.prepareCall(sql);            
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DQCLabSettings GetQCLabList(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public ResultSet getQCLabData(int id) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";        
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_GET_QCLAB_DATA(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1, id);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DQCLabSettings getQCLabData(): ", ex);
            System.out.println(ex.toString());
        }
        
        return rs;
    }
    
    public int addNewQCLab(String qcLabName, String address, String phone, int userID) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_ADD_NEW_QCLAB(?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, qcLabName);
            stmt.setString(2, address);
            stmt.setString(3, phone);
            stmt.setInt(4, userID);
            stmt.registerOutParameter(5,java.sql.Types.INTEGER);
            stmt.execute();
            
            retVal = stmt.getInt(5);
        }
        catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DQCLabSettings addNewQCLab(): ", ex);
            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
            {
                retVal = -2;
            }
            else
            {
                retVal = -1;    
            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        
        return retVal;
    }
    
    public int updateQCLab(int id, String name, String address, String phone, int userID, String comments) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_UPDATE_QCLAB(?,?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.setString(3, address);
            stmt.setString(4, phone);
            stmt.setInt(5, userID);
            stmt.setString(6, comments);
            stmt.executeUpdate();
        }
        catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DQCLabSettings updateQCLab(): ", ex);
            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
            {
                retVal = -2;
            }
            else
            {
                retVal = -1;    
            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        
        return retVal;
    }
    
    public int deleteQCLab(int id, int userID, String comments) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_DELETE_QCLAB(?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1, id);
            stmt.setInt(2,userID);
            stmt.setString(3,comments);           
            stmt.registerOutParameter(4,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = stmt.getInt(4);
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DQCLabSettings deleteQCLab(): ", ex);
            System.out.println(ex.toString());
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        return retVal;
    }
}
