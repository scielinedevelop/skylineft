package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import oracle.jdbc.OracleTypes;

public class DRespGroupSettings extends BaseDAL
{
    private LogWriter LG = null;
    public static final int UniqueConstraintORA00001 = 1;
    
    public DRespGroupSettings() 
    {
        super();
    }
    
    public DRespGroupSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet GetRespGroupsList() 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_RESP_GROUP_LIST(?)";
            stmt = conn.prepareCall(sql);
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DRespGroupSettings GetRespGroupsList(): ", ex);
            System.out.println(ex.toString());
        }
        
        return rs;
    }
    
    public ResultSet GetGroupData(int groupID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_RESP_GROUP_DATA(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,groupID);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DRespGroupSettings GetGroupData(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public ResultSet getMaintenanceRespGroupList(String maintenanceType, String objectId) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_RES_GROUP_BY_ID(?,?,?)";
            stmt = conn.prepareCall(sql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            stmt.setString(1,maintenanceType); 
            stmt.setString(2,objectId); 
            stmt.registerOutParameter(3,OracleTypes.CURSOR);  
            stmt.executeQuery();
            rs = (ResultSet)stmt.getObject(3);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DRespGroupSettings getMaintenanceRespGroupList(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public ResultSet GetAvailibleUsers(int groupID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_USERS_GROUP_AV(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,groupID);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DRespGroupSettings GetGroupData(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public ResultSet GetSelectedUsers(int groupID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_USERS_GROUP_SEL(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,groupID);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DRespGroupSettings GetGroupData(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public int AddNewGroup(String groupName,String groupMembers,int userID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        int retVal =0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_ADD_NEW_GROUP(?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1,groupName);
            stmt.setString(2,groupMembers);
            stmt.setInt(3,userID);
            stmt.registerOutParameter(4,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(4);
            
            while(rs.next()) 
            {
                retVal = rs.getInt(1);
                break;
            }
            
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DRespGroupSettings GetGroupData(): ", ex);
            System.out.println(ex.toString());
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        return retVal;
    }
    
    public int UpdateGroupData(int groupID,String groupName,String groupMembers,int userID,String comments) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        int retVal =0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_UPDATE_GROUP(?,?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,groupID);
            stmt.setString(2,groupName);
            stmt.setString(3,groupMembers);
            stmt.setInt(4,userID);
            stmt.setString(5,comments);
            stmt.registerOutParameter(6,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(6);
            
            while(rs.next()) 
            {
                retVal = rs.getInt(1);
                break;
            }
            
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DRespGroupSettings GetGroupData(): ", ex);
            System.out.println(ex.toString());
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        return retVal;
    }
    
    public int DeleteItem(int itemID,int userID,String comments) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        int retVal =0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_DELETE_GROUP(?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,itemID);
            stmt.setInt(2,userID);
            stmt.setString(3,comments);
            stmt.registerOutParameter(4,java.sql.Types.INTEGER);
            
            stmt.execute();
            retVal = stmt.getInt(4);
            
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DRespGroupSettings GetGroupData(): ", ex);
            System.out.println(ex.toString());
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        return retVal;
    }
}
