package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import oracle.jdbc.OracleTypes;

public class DSiteSettings extends BaseDAL
{
    private LogWriter LG = null;
    public static final int UniqueConstraintORA00001 = 1;
    
    public DSiteSettings() 
    {
        super();
    }
    
    public DSiteSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet GetSiteList() 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_SITE_LIST(?)";
            stmt = conn.prepareCall(sql);            
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DSiteSettings GetSiteList(): ", ex);
            System.out.println(ex.toString());
        }
        
        return rs;
    }
    
    public ResultSet GetSiteData(int siteID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_GET_SITE_DATA(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,siteID);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DSiteSettings GetSiteData(): ", ex);
            System.out.println(ex.toString());
        }
        
        return rs;
    }
    
    public int AddNewSite(String siteName,String displyName,int userID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_ADD_NEW_SITE(?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1,siteName);
            stmt.setString(2,displyName);
            stmt.setInt(3,userID);
            stmt.registerOutParameter(4,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = stmt.getInt(4);
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DSiteSettings AddNewSite(): ", ex);
            System.out.println(ex.toString());
        }
        return retVal;
    }
    
    public int UpdateSite(int siteID,String siteName,String displayName,int userID,String commets) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_UPDATE_SITE(?,?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,siteID);
            stmt.setString(2,siteName);
            stmt.setString(3,displayName);
            stmt.setInt(4,userID);
            stmt.setString(5,commets);
            stmt.registerOutParameter(6,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = stmt.getInt(6);
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DSiteSettings UpdateSite(): ", ex);
            System.out.println(ex.toString());
        }
        return retVal;
    }
    
    public int DeleteSite(int siteID,int userID,String comments) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_DELETE_SITE(?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,siteID);
            stmt.setInt(2,userID);
            stmt.setString(3,comments);
            
            stmt.registerOutParameter(4,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = stmt.getInt(4);
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DSiteSettings DeleteSite(): ", ex);
            System.out.println(ex.toString());
        }
        return retVal;
    }
}
