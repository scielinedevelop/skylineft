package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.SQLException;

import oracle.jdbc.OracleTypes;

public class DUomGroupSettings extends BaseDAL
{
    private LogWriter LG = null;   
    
    public DUomGroupSettings() 
    {
        super();
    }
    
    public DUomGroupSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet getUomGroupList() 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = null;

        try 
        {
            conn = getConnection();
            sql = "call SP_GET_MN_UOM_GROUP_LIST(?)";
            stmt = conn.prepareCall(sql);            
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DUomGroupSettings getUomGroupList(): ", ex);
            System.out.println(ex.toString());
        }
        
        return rs;
    }
    
    public ResultSet getUomGroupData(String uomGroupId) 
    {
    	CallableStatement stmt = null;
    	ResultSet rs = null;
    	String sql = "";        
    	Connection conn = null;
    	
    	try 
    	{
    		conn = getConnection();
    		sql = "call SP_GET_UOM_GROUP_DATA(?,?)";
    		stmt = conn.prepareCall(sql);
    		stmt.setString(1,uomGroupId);
    		stmt.registerOutParameter(2,OracleTypes.CURSOR);
    		stmt.execute();
    		rs = (ResultSet)stmt.getObject(2);
    	}
    	catch(Exception ex) 
    	{
    		LG.write(ex.toString(), "DUomGroupSettings getUomGroupData(): ", ex);
    		System.out.println(ex.toString());
    	}
    	return rs;
    }
    
    public int addNewUomGroup(String uomGroup, int active, int userID) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_ADD_NEW_UOM_GROUP(?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, uomGroup);
            stmt.setInt(2, active);
            stmt.setInt(3, userID);
            stmt.execute();
        }
        catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DUomGroupSettings addNewUom(): ", ex);
            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
            {
                retVal = -2;
            }
            else
            {
                retVal = -1;    
            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        
        return retVal;
    }
    
    public int updateUomGroup(String uomGroupId, String uomGroup, int active, int userID, String comments) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_UPDATE_UOM_GROUP(?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, uomGroup);
            stmt.setInt(2, active);
            stmt.setInt(3, userID);
            stmt.setString(4, comments);
            stmt.setString(5, uomGroupId);
            stmt.executeUpdate();
        }
        catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DUomGroupSettings updateUomGroup(): ", ex);
            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
            {
                retVal = -2;
            }
            else
            {
                retVal = -1;    
            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        
        return retVal;
    }
    
    public int deleteUomGroup(int itemID, int userID, String comments) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_DELETE_UOM_GROUP(?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1, itemID);
            stmt.setInt(2,userID);
            stmt.setString(3,comments);           
//            stmt.registerOutParameter(4,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = 1;
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DUomGroupSettings deleteUomGroup(): ", ex);
            System.out.println(ex.toString());
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        return retVal;
    }
    
}
