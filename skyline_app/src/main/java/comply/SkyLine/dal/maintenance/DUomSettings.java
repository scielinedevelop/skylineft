package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.SQLException;

import oracle.jdbc.OracleTypes;

public class DUomSettings extends BaseDAL
{
    private LogWriter LG = null;   
    
    public DUomSettings() 
    {
        super();
    }
    
    public DUomSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet getUomList() 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        Connection conn = null;

        try 
        {
            conn = getConnection();
            sql = "call SP_GET_MN_UOM_LIST(?)";
            stmt = conn.prepareCall(sql);            
            stmt.registerOutParameter(1,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(1);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DUomSettings getUomList(): ", ex);
            System.out.println(ex.toString());
        }
        
        return rs;
    }
    
	public ResultSet getUomData(String uom) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";        
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_GET_UOM_DATA(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1,uom);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DUomSettings getUomData(): ", ex);
            System.out.println(ex.toString());
        }
        
        return rs;
    }
    
    public int addNewUom(String uom, String uomDescr, int userID, int uomGroupId, String factor, int normal, int active) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_ADD_NEW_UOM(?,?,?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, uom);
            stmt.setString(2, uomDescr);
            stmt.setInt(3, userID);
            stmt.setInt(4, uomGroupId);
            stmt.setString(5, factor);
            stmt.setInt(6, normal);
            stmt.setInt(7, active);
            stmt.execute();
        }
        catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DUomSettings addNewUom(): ", ex);
            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
            {
                retVal = -2;
            }
            else
            {
                retVal = -1;    
            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        
        return retVal;
    }
    
    public int updateUom(String uomOld, String uom, String uomDescr, int userID, String comments, int uomGroupId, String factor, int normal, int active) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "call SP_UPDATE_UOM(?,?,?,?,?,?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, uom);
            stmt.setString(2, uomDescr);
            stmt.setInt(3, userID);
            stmt.setString(4, comments);
            stmt.setString(5, uomOld);
            stmt.setInt(6, uomGroupId);
            stmt.setString(7, factor);
            stmt.setInt(8, normal);
            stmt.setInt(9, active);
            stmt.executeUpdate();
        }
        catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DUomSettings updateUom(): ", ex);
            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
            {
                retVal = -2;
            }
            else
            {
                retVal = -1;    
            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        
        return retVal;
    }
    
    public String checkIfNormal(int uomGroupId) 
    {
        CallableStatement stmt = null;
        String sql = "";
        String retVal = "0";
        Connection conn = null;
        
        try 
        {
            conn = getConnection();
            sql = "{?=call FN_UOM_IS_NORMAL(?)}";
            stmt = conn.prepareCall(sql);
            stmt.setInt(2, uomGroupId);
            stmt.registerOutParameter(1,  java.sql.Types.VARCHAR);
            
            stmt.executeUpdate();
            retVal = stmt.getString(1);
        }
        catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DUomSettings checkIfNormal(): ", ex);
//            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
//            {
//                retVal = -2;
//            }
//            else
//            {
//                retVal = -1;    
//            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        
        return retVal;
    }
    public int unselectLastNormal(String uomForUnselectNormal) 
    {
    	CallableStatement stmt = null;
    	String sql = "";
    	int retVal = 0;
    	Connection conn = null;
    	
    	try 
    	{
    		conn = getConnection();
    		sql = "call SP_UOM_UNSELECT_LAST_NORMAL(?)";
    		stmt = conn.prepareCall(sql);
    		stmt.setString(1, uomForUnselectNormal);
    		//stmt.registerOutParameter(1,  java.sql.Types.VARCHAR);
    		
    		stmt.executeUpdate();
    	}
    	catch(SQLException ex) 
        {
            LG.write(ex.toString(), "DUomSettings updateUom(): ", ex);
            if(UNIQUE_CONSTRAINT_VIOLATION == ex.getErrorCode()) 
            {
                retVal = -2;
            }
            else
            {
                retVal = -1;    
            }
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
    	
    	return retVal;
    }
    
    public int deleteUom(String uom, int userID, String comments) 
    {
        CallableStatement stmt = null;
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try 
        {
            sql = "call SP_DELETE_UOM(?,?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1, uom);
            stmt.setInt(2,userID);
            stmt.setString(3,comments);           
            stmt.registerOutParameter(4,java.sql.Types.INTEGER);
            stmt.execute();
            retVal = stmt.getInt(4);
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DUomSettings deleteUom(): ", ex);
            System.out.println(ex.toString());
        }
        finally
        {
            try 
            {      
                 if (stmt != null)
                 {
                     stmt.close();
                 }
                 disconnect();
            }
            catch (Exception ex)
            {
                System.out.println(ex.toString());
            }
         }
        return retVal;
    }
    
}
