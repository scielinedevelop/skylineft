package comply.SkyLine.dal.maintenance;

import comply.SkyLine.bl.biz.ComplyUtils;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.dal.general.BaseDAL;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import oracle.jdbc.OracleTypes;

public class DUserSettings extends BaseDAL
{
    private LogWriter LG = null;
    public static final int UniqueConstraintORA00001 = 1;
    
    public DUserSettings() {
        super();
    }
    
    public DUserSettings(String conURL,String conUSER,String conPASS, String path) 
    {
        setConnUrl(conURL);
        setConn_Usr(conUSER);
        setConnPass(conPASS);
        LG = new LogWriter(path);
    }
    
    public ResultSet GetUsersList(int showDeleted,int showHidden) {
        
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_GET_USERS_LIST(?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,showDeleted);
            stmt.setInt(2,showHidden);
            stmt.registerOutParameter(3,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(3);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DUserSettings GetUsersList(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public ResultSet GetRoleList(String systemID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_GET_ROLES(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setString(1,systemID);            
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DUserSettings GetRoleList(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public ResultSet GetUserData(int userID) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_GET_USER_DATA_BY_ID(?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,userID);
            stmt.registerOutParameter(2,OracleTypes.CURSOR);
            stmt.execute();
            rs = (ResultSet)stmt.getObject(2);
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "DUserSettings GetRoleList(): ", ex);
            System.out.println(ex.toString());
        }
        return rs;
    }
    
    public int SaveData(int userID,String userName,String password,Boolean updatePass,int siteID,String role,String firstName,String lastName,String initals,
                                        String title,String email,int deptID,int labID,String ldap,int maint,int locked,int sampleApp,int testApp,int dev,int activity,int inventory,
                                        int coa,int inventoryUsage,int changed_by,String lastRetry,int retrycount,String lastpassDate,
                                        String addStates,String removeStates, String langCode, int invType) 
    {
        CallableStatement stmt = null;
        ResultSet rs = null;
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try 
        {
            if(userID == 0) 
            {
                sql = "call SP_ADD_NEW_USER("+ComplyUtils.formatParamInSigns(16)+")";
                stmt = conn.prepareCall(sql);
                stmt.setString(1,userName);
                stmt.setString(2,password);
                stmt.setString(3,String.valueOf(siteID));
                stmt.setString(4,firstName);
                stmt.setString(5,lastName);
                stmt.setString(6,role);
                stmt.setString(7,title);
                stmt.setString(8,String.valueOf(locked));
                stmt.setInt(9,changed_by);
                stmt.setString(10,ldap);
                stmt.setString(11,email);
                stmt.setString(12,String.valueOf(deptID));
                stmt.setString(13,initals);
                stmt.setString(14,String.valueOf(labID));
                stmt.setString(15,String.valueOf(maint));
                stmt.registerOutParameter(16,OracleTypes.CURSOR);
                stmt.execute();
                rs = (ResultSet)stmt.getObject(16);
            }
            else 
            {

            	sql = "call SP_UPDATE_USER("+ComplyUtils.formatParamInSigns(20)+")";
                stmt = conn.prepareCall(sql);
                stmt.setInt(1,userID);
                stmt.setString(2,userName);
                stmt.setString(3,password);
                stmt.setString(4,String.valueOf(siteID));
                stmt.setString(5,firstName);
                stmt.setString(6,lastName);
                stmt.setString(7,role);
                stmt.setString(8,title);
                stmt.setString(9,String.valueOf(locked));
                stmt.setInt(10,changed_by);
                stmt.setString(11,ldap);
                stmt.setString(12,email);
                stmt.setString(13,String.valueOf(deptID));
                stmt.setString(14,initals);
                stmt.setString(15,lastRetry);
                stmt.setInt(16,retrycount);
                stmt.setString(17,lastpassDate);
                stmt.setString(18,String.valueOf(labID));
                stmt.setString(19,String.valueOf(maint));
                stmt.registerOutParameter(20,OracleTypes.CURSOR);
                
                stmt.execute();
                rs = (ResultSet)stmt.getObject(20);
            }
            
           
            while(rs.next()) 
            {
                retVal = rs.getInt(1);
                break;
            }            
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DUserSettings SaveData(): ", ex);
            System.out.println(ex.toString());
        }
        
        return retVal;
    }
    
    public int DeleteUser(int itemID,int userID,String comments) 
    {
        CallableStatement stmt = null;        
        String sql = "";
        int retVal = 0;
        Connection conn = getConnection();
        
        try
        {
            sql = "call SP_DELETE_USER(?,?,?)";
            stmt = conn.prepareCall(sql);
            stmt.setInt(1,itemID);
            stmt.setInt(2,userID);
            stmt.setString(3,comments);
            stmt.execute();
            retVal = 1;
        }
        catch(Exception ex) 
        {
            retVal = -1;
            LG.write(ex.toString(), "DUserSettings DeleteUser(): ", ex);
            System.out.println(ex.toString());
        }
        
        return retVal;
    }
}
