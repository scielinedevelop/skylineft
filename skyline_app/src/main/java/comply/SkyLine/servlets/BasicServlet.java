package comply.SkyLine.servlets;

import comply.SkyLine.bl.biz.AppPropSingleton;
import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.general.BaseBL;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.general.General;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONArray;

public class BasicServlet extends HttpServlet
{
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    public Boolean isSessionTimeout = false;
    public Boolean subclassIsPopup = false;
    
    public void init(ServletConfig config) throws ServletException
    {
	super.init(config);
    }

    public void doPost(HttpServletRequest request,  HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        //Check that the session is alive
        HttpSession session = request.getSession();    
        isSessionTimeout = false;
        
         if (session.getAttribute("USER") == null) 
         {  
             PrintWriter out = response.getWriter();
             out.println("<html><title>TIME_IS_OUT</title>");
             out.println("<head>");
             out.println("<script language=\"javascript\">");
             if (subclassIsPopup)
             {
                 out.println("returnValue='TIME_OUT'; window.parent.close();");
             }
             else
             {
				 String rUrl = request.getRequestURL().toString();
				 String servletName = this.getServletName().toLowerCase();
				 rUrl = rUrl.replaceAll(servletName,""); 
				 
				 //set property for getting http protocol
				 AppProperties props = AppPropSingleton.getInstance();
				 // replace http to system parameter (will be https in some cases)
				 rUrl = rUrl.replaceFirst("[Hh][Tt][Tt][Pp]",props.getHttpProtocol());
                 out.println(  "top.location.href=\"" + rUrl + "\";");
             }
             out.println("</script>");
             out.println("</head>");
             out.println("</html>");
             isSessionTimeout = true;
             return;
         }
    }
    
    public boolean checkPagePermission()
    {
	return false;
    }
    
    public  String getTableProperties(HttpServletRequest request,String tableName) 
    {
        AppProperties props = AppPropSingleton.getInstance();
        String logPath = props.getLogPath(); 
        BaseBL baseB = new BaseBL(logPath);
        
        String returnValue = baseB.getTableProperties(tableName);
        //System.out.println("tableProperties: " + returnValue); 
        return returnValue;
    }
    
    public  JSONArray getMaintTablesStructure(HttpServletRequest request, String logPath) 
    {
        BaseBL baseB = new BaseBL(logPath);
        JSONArray returnValue = baseB.getMaintTablesStructure();
        return returnValue;
    }
    
    public  String getSystemParameterValue(HttpServletRequest request, String paramName) //String conURL,String conUSER,String conPASS, String path
    {
        HttpSession session = request.getSession();
        
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();   
        General general = new General(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(),logPath);
         
        String returnValue = general.getSystemParameterValue(paramName);
        
        return returnValue;
    }
    
    @SuppressWarnings("unchecked")
	public String getUniqueReportName (HttpServletRequest request,String name) // kd 06112016 fixed bug 3642
    {
    	Integer indexFileName = 0;
    	try
    	{
	    	
	    	HttpSession session = request.getSession();
	    	Map<String, Integer> mapRepName;
	    	if (session.getAttribute("UNIQUE_INDEX_MAP") != null)
	    	{
	    		mapRepName = (Map<String, Integer>)session.getAttribute("UNIQUE_INDEX_MAP");
    			if (mapRepName.get(name) == null)
	    		{
		    		mapRepName.put(name, 0);
		    		session.setAttribute("UNIQUE_INDEX_MAP", mapRepName);
	    		}
	    	}
	    	else
	    	{
	    		mapRepName = new HashMap<String,Integer>();
	    		mapRepName.put(name, 0);
	    		session.setAttribute("UNIQUE_INDEX_MAP", mapRepName);
	    	}
	    	mapRepName = (Map<String, Integer>)session.getAttribute("UNIQUE_INDEX_MAP");
	    	indexFileName = mapRepName.get(name) + 1;
	    	mapRepName.put(name, indexFileName);
	    	session.setAttribute("UNIQUE_INDEX_MAP", mapRepName);
			return name + " (" + indexFileName + ")";
    	}
    	catch (Exception e)
    	{
    		System.out.println("there are an exception on name of report creation. Exception e is: " + e);
    		return name + " (1)";
    	}
    }
}
