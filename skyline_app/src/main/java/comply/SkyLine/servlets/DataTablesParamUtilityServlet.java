package comply.SkyLine.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import comply.SkyLine.bl.general.DataTableParamModel;

@WebServlet(
        name = "DataTablesParamUtilityServlet",
        urlPatterns = "/datatablesparamutilityservlet"
)
public class DataTablesParamUtilityServlet extends HttpServlet
{
	
	public static DataTableParamModel getParam(HttpServletRequest request)
	{
            try
            {
                if(request.getParameter("sEcho")!=null && request.getParameter("sEcho")!= "")
		{
                    
                    DataTableParamModel param = new DataTableParamModel();
		    param.sEcho = request.getParameter("sEcho");
                    param.sSearchKeyword = request.getParameter("sSearch");
                    param.bRegexKeyword = Boolean.parseBoolean(request.getParameter("bRegex"));
		    param.sColumns = request.getParameter("sColumns");
		    param.iDisplayStart = Integer.parseInt( request.getParameter("iDisplayStart") );
		    param.iDisplayLength = Integer.parseInt( request.getParameter("iDisplayLength") );
		    param.iColumns = Integer.parseInt( request.getParameter("iColumns") );
		    param.sSearch = new String[param.iColumns];
		    param.bSearchable = new boolean[param.iColumns];
		    param.bSortable = new boolean[param.iColumns];
		    param.bRegex = new boolean[param.iColumns];
		    for(int i=0; i<param.iColumns; i++)
                    {
		            param.sSearch[i] = request.getParameter("sSearch_"+i);
		            param.bSearchable[i] = Boolean.parseBoolean(request.getParameter("bSearchable_"+i));
		            param.bSortable[i] = Boolean.parseBoolean(request.getParameter("bSortable_"+i));
		            param.bRegex[i] = Boolean.parseBoolean(request.getParameter("bRegex_"+i));
		    }
		    
		    param.iSortingCols = Integer.parseInt( request.getParameter("iSortingCols") );
		    param.sSortDir = new String[param.iSortingCols];
		    param.iSortCol = new int[param.iSortingCols];
		    param.sSortType = new String[param.iSortingCols];
		    param.sDateFormat = new String[param.iSortingCols];
                    
		    for(int i=0; i<param.iSortingCols; i++)
                    {
		            param.sSortDir[i] = request.getParameter("sSortDir_"+i);
		            param.iSortCol[i] = Integer.parseInt(request.getParameter("iSortCol_"+i));
                            param.sSortType[i] = request.getParameter("sSortColType_"+i);
                            param.sDateFormat[i] = request.getParameter("sSortColDateFormat_"+i);
		    }
			return param;
		}
                else
			return null;
            }
            catch(Exception ex)
            {
                return null;     
            }
	}
}
