package comply.SkyLine.servlets;

import com.google.gson.Gson;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.ComplyUtils;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.general.General;
import jasper.biz.JasperReportType;
import jasper.biz.JasperTemplateFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;

@WebServlet(
        name = "HelperServlet",
        urlPatterns = "/helperservlet"
)
public class HelperServlet extends BasicServlet { 
            private static final String CONTENT_TYPE = "text/html";
            private LogWriter LG = null;
            private HttpSession session = null;

    public void init(ServletConfig config) throws ServletException 
    {
        super.init(config);
    }
  
    public void doPost(HttpServletRequest request,  HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        
        response.setContentType(CONTENT_TYPE);
        session =  request.getSession();    
        
        if(super.isSessionTimeout)
        {
            return;
        }
                  
        //init
         int currUserID = 0;
        ConnUtility connUtility = null;
        AppProperties props = null;
        String logPath = "";
        try 
        {
            currUserID = ((User)session.getAttribute("USER")).USER_ID;
            props = (AppProperties)session.getAttribute("AppProp");
            connUtility = (ConnUtility)session.getAttribute("ConUtil");
            logPath = props.getLogPath();
            LG = new LogWriter(logPath);
        }
        catch (Exception e)
        {
            System.out.println("start helper servlet error e=" + e.toString());
        }
         
        //get the action
         String action = ComplyUtils.getNull(request.getParameter("actionid"));
         System.out.println("start helper servlet for action: "+action);
        
         if(action.equals("isAlive"))
         {
             response.getWriter().write("1");
             return;
         }
         
         else if(action.equals("saveInSession")) 
        {
            String key = request.getParameter("key"); 
            String val = request.getParameter("val");
            session.setAttribute(key, val);
            return;
        }
        
         else if(action.equals("getInSession")) 
        {
            String key = request.getParameter("key"); 
            String val =  ComplyUtils.getNull((String)(String)session.getAttribute(key)); 
            response.getWriter().write(String.valueOf(val));
            return;
        }
        
         else if(action.equals("getSystemParameter")) 
        {
            String paramName = request.getParameter("paramName"); 
            General gen =  new General(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath); 
            LG = new LogWriter(logPath);
            String retVal = ""; 
            try 
            {   
                 retVal = gen.getSystemParameterValue(paramName);
            }
            catch(Exception ex)
            {
                LG.write(ex.toString(), "HelperServlet   action=getSystemParameter", ex);
                retVal = "";
            }  
            response.getWriter().write(String.valueOf(retVal));
        }
        else if(action.equals("getRespGrList")) 
        {
            General gen =  new General(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath); 
            LG = new LogWriter(logPath);
            JSONArray resObj = new JSONArray(); 
            try 
            {   
            	resObj = gen.getRespGroupsByUser(currUserID);
            }
            catch(Exception ex)
            {
                LG.write(ex.toString(), "HelperServlet   action=getRespGrList", ex);
                resObj = new JSONArray();
            }  
            response.getWriter().print(resObj);
            return;
        }
        
         else if(action.equals("getLangSettings")) {
            try
            {
                String langSettings =  (String)session.getAttribute("LANG_SETTINGS");
                response.setHeader("Content-Type", "application/json; charset=UTF-8");
                response.getWriter().print(langSettings);
                
            }
            catch(Exception ex) {
                LG.write(ex.toString(), "HelperServlet.getLangSettings action:", ex);
                response.getWriter().print("-1");
            }
            return;
        }
         else if(action.equals("getNavigationData")) 
        {
            JSONArray resObj = null;
            try 
            {
            	General gen =  new General(logPath); 
                String dbObjName = request.getParameter("dbObjName"); 
                String paramsArr = request.getParameter("paramsDataArr"); 
                resObj = gen.getNavigationResult(dbObjName, String.valueOf(currUserID), paramsArr); 
                response.setHeader("Content-Type", "application/json; charset=UTF-8");
                response.getWriter().print(resObj);
            }
            catch (Exception e) 
            {
                LG.write(e.toString(), "HelperServlet.getNavigationData:", e);
                response.getWriter().print(resObj);
            }
            return;
        }
        
         else if(action.equals("logReport"))
        {
            String DB_URL = props.getLookupTablesURL(); // can also use connUtility.getUrl(),connUtility.getDBuser(), connUtility.getPassword()
            String DB_USER = props.getLookupTablesUser(); 
            String DB_PASSWORD = props.getLookupTablesPass();
            String DIR_JASPER_XML = props.getJasperReportsDir(); 
            String DIR_JASPER_XML_TEMPLATES = props.getJasperReportsTemplateDir();
            String DIR_JASPER_XML_TMP = props.getJasperReportsTmpDir();
          
          
            try
            {
                JasperTemplateFactory jf = new JasperTemplateFactory(DB_URL,DB_USER,DB_PASSWORD,DIR_JASPER_XML,DIR_JASPER_XML_TEMPLATES,DIR_JASPER_XML_TMP, (HashMap<String,String>)session.getAttribute("LANG_SETTINGS_MAP")); 
                ByteArrayOutputStream out = jf.getJasperReportStream(  session.getId() + (new Date()).getTime(),
                                                                                                          JasperReportType.PDF,
                                                                                                          "Log Report",
                                                                                                          "",
                                                                                                          "d_logger_v",
                                                                                                          "");
                response.setContentType("application/octet-stream");                                                                                            
                response.setHeader("Content-Disposition", "attachment; filename=\"" +"log_report" + ".pdf\";");                
                OutputStream stream = response.getOutputStream();                 
                try 
                {
                    out.writeTo(stream);
                    stream.flush();                                                                                                                                       
                    stream.close(); 
                }
                catch (IOException e) 
                {
                    System.out.println("report close by the user");
                }
            }
            catch (Exception ex)
            {
                System.out.println("HelperServlet action: " + ex.toString());
                LG.write(ex.toString(), "HelperServlet logReport action: ", ex);                     
            }
            catch (OutOfMemoryError er)
            {
               System.out.println("HelperServlet action: " + er.toString());
               LG.write(er.toString(), "HelperServlet logReport action: ", null);  
            }  
            
            return;
        }
         else if(action.equals("logEventReport"))
        {
            String DB_URL = props.getLookupTablesURL(); // can also use connUtility.getUrl(),connUtility.getDBuser(), connUtility.getPassword()
            String DB_USER = props.getLookupTablesUser(); 
            String DB_PASSWORD = props.getLookupTablesPass();
            String DIR_JASPER_XML = props.getJasperReportsDir(); 
            String DIR_JASPER_XML_TEMPLATES = props.getJasperReportsTemplateDir();
            String DIR_JASPER_XML_TMP = props.getJasperReportsTmpDir();
          
          
            try
            {
                JasperTemplateFactory jf = new JasperTemplateFactory(DB_URL,DB_USER,DB_PASSWORD,DIR_JASPER_XML,DIR_JASPER_XML_TEMPLATES,DIR_JASPER_XML_TMP, (HashMap<String,String>)session.getAttribute("LANG_SETTINGS_MAP")); 
                ByteArrayOutputStream out = jf.getJasperReportStream(  session.getId() + (new Date()).getTime(),
                                                                                                          JasperReportType.PDF,
                                                                                                          "Event Log Report",
                                                                                                          "",
                                                                                                          "d_logger_event_v",
                                                                                                          "");
                response.setContentType("application/octet-stream");                                                                                            
                response.setHeader("Content-Disposition", "attachment; filename=\"" +"event_log_report" + ".pdf\";");                
                OutputStream stream = response.getOutputStream();                 
                try 
                {
                    out.writeTo(stream);
                    stream.flush();                                                                                                                                       
                    stream.close(); 
                }
                catch (IOException e) 
                {
                    System.out.println("report close by the user");
                }
            }
            catch (Exception ex)
            {
                System.out.println("HelperServlet action: " + ex.toString());
                LG.write(ex.toString(), "HelperServlet logReport action: ", ex);                     
            }
            catch (OutOfMemoryError er)
            {
               System.out.println("HelperServlet action: " + er.toString());
               LG.write(er.toString(), "HelperServlet logReport action: ", null);  
            }  
            
            return;
        }
        
      //open user guide
         else if (action.equals("userGuide")) {

      			try {
      				String pdfFileName = "User_Guide.pdf";
      				//String contextPath = apropST.getJasperReportsDir();
      				String contextPath = props.getJasperReportsDir();
      				File pdfFile = new File(contextPath + "\\" + pdfFileName);
      				response.setContentType("application/pdf");
      				response.setContentLength((int) pdfFile.length());
      				response.setHeader("Expires", "0");
      				response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
      				response.setHeader("Content-Disposition","inline; filename=" +pdfFileName );
      				response.setHeader("Accept-Ranges", "bytes");
      				OutputStream responseOutputStream = response.getOutputStream();
      				byte[] buf = new byte[8192];
      				InputStream is = new FileInputStream(pdfFile);
      				int c = 0;
      				while ((c = is.read(buf, 0, buf.length)) > 0) {
      					responseOutputStream.write(buf, 0, c);
      					responseOutputStream.flush();
      				}
      				responseOutputStream.close();
      				is.close();
      			} catch (Exception e) {
      				System.out.println(e.getMessage() + "apropST= " + props);
      				request.setAttribute("msg", "Failed to display user guide");
      				request.getRequestDispatcher("Login.jsp").forward(request, response);
      			}
      			return;
      	}
	     else if(action.equals("getClob")) 
	     {
	         General gen =  new General(logPath); 
	         try 
	         {   
	         	String id = request.getParameter("clob_id");
	        	String content = gen.getClobContent(id);
	         	response.getWriter().print(content);
	         }
	         catch(Exception ex)
	         {
	             LG.write(ex.toString(), "HelperServlet   action=getClob", ex);
	             response.getWriter().print("");
	         }  
	         
	         return;
	     }
	     else if(action.equals("saveClob")) 
	     {
	         General gen =  new General(logPath); 
	         try 
	         {   
	         	String clobString = request.getParameter("clobString");
	         	String description = ComplyUtils.getNull(request.getParameter("description"));
	         	String oldClobId = ComplyUtils.getNull(request.getParameter("oldValueId"));
	         	
	        	String newId = gen.saveStringAsClob(clobString, description, oldClobId);
	         	response.getWriter().print(newId);
	         }
	         catch(Exception ex)
	         {
	             LG.write(ex.toString(), "HelperServlet   action=saveClob", ex);
	             response.getWriter().print("-1");
	         }  
	         
	         return;
	     }
	     else if(action.equals("getATUsersList"))
         {
	    	 General gen =  new General(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
	    	 try
             {
                 String list = new Gson().toJson(gen.getATUsersList());
                 response.setHeader("Content-Type", "application/json; charset=UTF-8");
                 response.getWriter().print(list);
                 
             }
             catch(Exception ex) 
             {
            	 response.getWriter().print("");
                 LG.write(ex.toString(), "HelperServlet: getATUsersList: ", ex);
             }
             
             return;
         }
	     else if(action.equals("getUomGroup"))
        {
        	try 
        	{
        		General gen =  new General(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
        		String selGroupId = request.getParameter("selGroupId");   
				String json = new Gson().toJson(gen.getUOMGroupList(selGroupId));
				response.setHeader("Content-Type", "application/json; charset=UTF-8");
				response.getWriter().print(json);
			} 
        	catch (Exception ex) {
				LG.write(ex.toString(), "HelperServlet: getUomGroup: ", ex);
                response.getWriter().print("-1");
			}
        	return;
        }
        else if(action.equals("getUom"))
        {
        	try 
        	{
        		General gen =  new General(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
        		String uomGroupId = ComplyUtils.getNull(request.getParameter("uomGroupId"));  
        		String selectedUom = request.getParameter("selectedUom");  
        		String json = new Gson().toJson(gen.getUOMArray(uomGroupId, selectedUom));
				response.setHeader("Content-Type", "application/json; charset=UTF-8");
				response.getWriter().print(json);
			} 
        	catch (Exception ex) {
				LG.write(ex.toString(), "HelperServlet: getUom: ", ex);
                response.getWriter().print("-1");
			}
        	return;
        }

    }
}
