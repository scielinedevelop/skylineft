package comply.SkyLine.servlets;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.maintenance.QCLABManagement;

import com.google.gson.Gson;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(
        name = "MNQCLabsUpdateServlet",
        urlPatterns = "/mnqclabsupdateservlet"
)
public class MNQCLabsUpdateServlet extends BasicServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private LogWriter LG = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        String action = request.getParameter("actionid");
        if(isSessionTimeout)
        {
            return;
        }
        response.setContentType(CONTENT_TYPE);
        HttpSession session = request.getSession();
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        
        int currUserID = ((User)session.getAttribute("USER")).USER_ID;
        QCLABManagement mng = new QCLABManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
        String selectedID = request.getParameter("SelectedID");
        if(selectedID == null || selectedID.equals("0"))
        {
            selectedID = "-1";
        }
        
        if(action != null && !action.equals("")) 
        {
            if(action.equals("getData")) 
            {
                String data = "";
                try 
                {
                    
                    data = new Gson().toJson(mng.getQCLabData(Integer.parseInt(selectedID)));
                    
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(data);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNQCLabsUpdateServlet: getData: ", ex);
                    response.getWriter().print(data);
                }
                return;
            }
            
            if(action.equals("saveData")) 
            {
                try 
                {
                    String qcName = request.getParameter("qcLabName");
                    int retVal = mng.saveData(Integer.parseInt(selectedID), qcName, "", "", currUserID, "");
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(retVal);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNQCLabsUpdateServlet: saveData: ", ex);
                    response.getWriter().print("-1");
                }
                return;
            }
        }
        
        request.setAttribute("CURR_ID",selectedID);
        request.getRequestDispatcher("Maintenance/QCLabUpdate.jsp").forward(request, response);
        
    }
}
