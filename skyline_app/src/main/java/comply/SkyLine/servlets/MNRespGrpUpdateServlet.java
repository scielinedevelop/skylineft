package comply.SkyLine.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.maintenance.RespGroupManagement;

@WebServlet(
        name = "MNRespGrpUpdateServlet",
        urlPatterns = "/mnrespgrpupdateservlet"
)
public class MNRespGrpUpdateServlet extends BasicServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private static final String  DISABLE_CONTROLS = "DISABLE_CONTROLS";
    private LogWriter LG = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        String action = request.getParameter("actionid");
        if(isSessionTimeout)
        {
            return;
        }
        response.setContentType(CONTENT_TYPE);
        HttpSession session = request.getSession();
        int currUserID = ((User)session.getAttribute("USER")).USER_ID;
        
        String selectedID = request.getParameter("SelectedID");
        if(selectedID == null || selectedID.equals("0"))
        {
            selectedID = "-1";
        }
        if(selectedID.equals("99999")) /* change ID of EVERYONE group back to "0" */
        {
          selectedID = "0";
        }
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        String availibleUsers ="";
        String selectedUsers="";
        
        if(action != null && !action.equals("")) 
        {
            if(action.equals("getGroupData")) 
            {
                String data = "";
                try 
                {
                    RespGroupManagement rpMng = new RespGroupManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
                    data = new Gson().toJson(rpMng.GetGroupData(Integer.parseInt(selectedID)));
                    
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(data);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNRespGrpUpdateServlet: getGroupData: ", ex);
                    response.getWriter().print(data);
                }
                return;
            }
            
            if(action.equals("SaveData")) 
            {
                try 
                {
                    String groupName = request.getParameter("GroupName");
                    String groupMembers = request.getParameter("GroupMembers");
                    
                    RespGroupManagement rpMng = new RespGroupManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
                    int retVal = rpMng.SaveData(Integer.parseInt(selectedID),groupName,groupMembers,currUserID,"");
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(retVal);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNRespGrpUpdateServlet: SaveData: ", ex);
                    response.getWriter().print("-1");
                }
                return;
            }
        }
        
        RespGroupManagement rspMng = new RespGroupManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
        try 
        {
            availibleUsers = new Gson().toJson(rspMng.GetAvailibleUsers(Integer.parseInt(selectedID)));
        }
        catch(Exception ex) 
        {
            availibleUsers = "0";
            LG.write(ex.toString(), "MNRespGrpUpdateServlet: GetAvailibleUsers: ", ex);
        }
        try 
        {
            selectedUsers = new Gson().toJson(rspMng.GetSelectedUsers(Integer.parseInt(selectedID)));
        }
        catch(Exception ex) 
        {
            selectedUsers = "0";
            LG.write(ex.toString(), "MNRespGrpUpdateServlet: GetSelectedUsers: ", ex);
        }
        
        request.setAttribute("COMBO_AVAILIBLE",availibleUsers);
        request.setAttribute("COMBO_SELECTED",selectedUsers);
        request.setAttribute("CURR_GROUP_ID",selectedID);
        
        request.getRequestDispatcher("Maintenance/RespGrpUpdate.jsp").forward(request, response);
    }
}
