package comply.SkyLine.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.maintenance.SiteManagement;

@WebServlet(
        name = "MNSiteUpdateServlet",
        urlPatterns = "/mnsiteupdateservlet"
)
public class MNSiteUpdateServlet extends BasicServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private static final String  DISABLE_CONTROLS = "DISABLE_CONTROLS";
    private LogWriter LG = null;
    //mnsiteupdateservlet
    public void init(ServletConfig config) throws ServletException 
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        String action = request.getParameter("actionid");
        if(isSessionTimeout)
        {
            return;
        }
        response.setContentType(CONTENT_TYPE);
        HttpSession session = request.getSession();
        int currUserID = ((User)session.getAttribute("USER")).USER_ID;
        
        String selectedID = request.getParameter("SelectedID");
        if(selectedID == null || selectedID.equals("0"))
        {
            selectedID = "-1";
        }
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        
        if(action != null && !action.equals("")) 
        {
            if(action.equals("getSiteData")) 
            {
                String data = "";
                try 
                {
                    SiteManagement sMng = new SiteManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
                    data = new Gson().toJson(sMng.GetSiteData(Integer.parseInt(selectedID)));
                    
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(data);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNSiteUpdateServlet: getSiteData: ", ex);
                    response.getWriter().print(data);
                }
                return;
            }
            
            if(action.equals("SaveData")) 
            {
                try 
                {
                    String siteName = request.getParameter("SiteName");
                    String displayName = request.getParameter("DisplayName");
                    SiteManagement sMng = new SiteManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
                    
                    int retVal = sMng.SaveData(Integer.parseInt(selectedID),siteName,displayName,currUserID,"");
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(retVal);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNSiteUpdateServlet: getSiteData: ", ex);
                    response.getWriter().print("-1");
                }
                return;
            }
        }
        
        request.setAttribute("CURR_ID",selectedID);
        request.getRequestDispatcher("Maintenance/SiteUpdate.jsp").forward(request, response);
    }
}