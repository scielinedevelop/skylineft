package comply.SkyLine.servlets;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.maintenance.UomGroupManagement;

import com.google.gson.Gson;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(
        name = "MNUomGroupUpdateServlet",
        urlPatterns = "/mnuomgroupupdateservlet"
)
public class MNUomGroupUpdateServlet extends BasicServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private LogWriter LG = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        String action = request.getParameter("actionid");
        if(isSessionTimeout)
        {
            return;
        }
        response.setContentType(CONTENT_TYPE);
        HttpSession session = request.getSession();
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        
        int currUserID = ((User)session.getAttribute("USER")).USER_ID;
        UomGroupManagement mng = new UomGroupManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
        String selectedID = new String(request.getParameter("SelectedID").getBytes("ISO-8859-1"), "UTF-8");//request.getParameter("SelectedID");
        if(selectedID == null || selectedID.equals("0"))
        {
            selectedID = "-1";
        }
        
        if(action != null && !action.equals("")) 
        {
            if(action.equals("getData")) 
            {
                String data = "";
                try 
                {
                    
                    data = new Gson().toJson(mng.getUomGroupData(selectedID));
                    
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(data);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNUomGroupUpdateServlet: getData: ", ex);
                    response.getWriter().print(data);
                }
                return;
            }
            
            if(action.equals("saveData")) 
            {
                try 
                {
                    String uomGroup = new String(request.getParameter("uomGroupName").getBytes("ISO-8859-1"), "UTF-8");
                    int active = Integer.parseInt(request.getParameter("isActive"));
                    
                    int retVal = mng.saveData(selectedID, uomGroup, active, currUserID,"");
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(retVal);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNUomGroupUpdateServlet: saveData: ", ex);
                    response.getWriter().print("-1");
                }
                return;
            }
        }
        
        request.setAttribute("CURR_ID",selectedID);
        request.getRequestDispatcher("Maintenance/UomGroupUpdate.jsp").forward(request, response);
        
    }
}
