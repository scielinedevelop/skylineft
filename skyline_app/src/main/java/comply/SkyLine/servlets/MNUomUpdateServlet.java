package comply.SkyLine.servlets;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.general.General;
import comply.SkyLine.bl.maintenance.UomData;
import comply.SkyLine.bl.maintenance.UomManagement;

import com.google.gson.Gson;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import org.json.JSONObject;

@WebServlet(
        name = "MNUomUpdateServlet",
        urlPatterns = "/mnuomupdateservlet"
)
public class MNUomUpdateServlet extends BasicServlet 
{
	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private LogWriter LG = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        String action = request.getParameter("actionid");
        if(isSessionTimeout)
        {
            return;
        }
        response.setContentType(CONTENT_TYPE);
        HttpSession session = request.getSession();
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        
        int currUserID = ((User)session.getAttribute("USER")).USER_ID;
        UomManagement mng = new UomManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
        String selectedID = new String(request.getParameter("SelectedID").getBytes("ISO-8859-1"), "UTF-8");//request.getParameter("SelectedID");
        if(selectedID == null || selectedID.equals("0"))
        {
            selectedID = "-1";
        }
        
        if(action != null && !action.equals("")) 
        {
            if(action.equals("getData")) 
            {
                String data = "";
                JSONObject jObj = null;
                try 
                {
                    jObj = new JSONObject();
                    String selGroupId = "-1";
                    if(!selectedID.equals("-1"))
                    {
                    	UomData o = mng.getUomData(selectedID);
                    	selGroupId = String.valueOf(o.uom_group_id);
                    	data = new Gson().toJson(o);
	                    jObj.put("dataForEdit", data);
                    }
                    General gen = new General(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
                    String uom_group_json = new Gson().toJson(gen.getUOMGroupList(selGroupId));
                    jObj.put("uom_group_list", uom_group_json);
                    
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(jObj);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNUomUpdateServlet: getData: ", ex);
                    response.getWriter().print(jObj);
                }
                return;
            }
            
            if(action.equals("saveData")) 
            {
                try 
                {
                    String uom = new String(request.getParameter("uomName").getBytes("ISO-8859-1"), "UTF-8");
                    String descr = new String(request.getParameter("uomDescr").getBytes("ISO-8859-1"), "UTF-8");
                    int uomGroupId = Integer.parseInt(request.getParameter("uomGroupId"));
                    String factor = new String(request.getParameter("factor").getBytes("ISO-8859-1"), "UTF-8");
                    int normal = Integer.parseInt(request.getParameter("isNormal"));
                    int active = Integer.parseInt(request.getParameter("isActive"));
                    
                    int retVal = mng.saveData(selectedID, uom, descr, currUserID,"", uomGroupId, factor, normal, active);
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(retVal);
                }
                catch(Exception ex) 
                {
                    LG.write(ex.toString(), "MNUomUpdateServlet: saveData: ", ex);
                    response.getWriter().print("-1");
                }
                return;
            }
            if(action.equals("checkIfNormal")) 
            {
            	String data = "";
            	try 
            	{
            		int uomGroupId = Integer.parseInt(request.getParameter("uomGroupId"));
            		data = new Gson().toJson(mng.checkIfNormal(uomGroupId));
            		response.setHeader("Content-Type", "application/json; charset=UTF-8");
            		response.getWriter().print(data);
            	}
            	catch(Exception ex) 
            	{
            		LG.write(ex.toString(), "MNUomUpdateServlet: checkIfNormal: ", ex);
            		response.getWriter().print("-1");
            	}
            	return;
            }
            if(action.equals("unselectLastNormal")) 
            {
            	try 
            	{
            		String uomForUnselectNormal = new String(request.getParameter("uomForUnselectNormal").getBytes("ISO-8859-1"), "UTF-8");
            		int retVal = mng.unselectLastNormal(uomForUnselectNormal);
            		response.setHeader("Content-Type", "application/json; charset=UTF-8");
            		response.getWriter().print(retVal);
            	}
            	catch(Exception ex) 
            	{
            		LG.write(ex.toString(), "MNUomUpdateServlet: unselectLastNormal: ", ex);
            		response.getWriter().print("-1");
            	}
            	return;
            }
        }
        
        request.setAttribute("CURR_ID",selectedID);
        request.getRequestDispatcher("Maintenance/UomUpdate.jsp").forward(request, response);
        
    }
}
