package comply.SkyLine.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.gson.Gson;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.maintenance.DepartmentManagement;
import comply.SkyLine.bl.maintenance.LangManagement;
import comply.SkyLine.bl.maintenance.QCLABManagement;
import comply.SkyLine.bl.maintenance.SiteManagement;
import comply.SkyLine.bl.maintenance.UserData;
import comply.SkyLine.bl.maintenance.UsersManagement;

@WebServlet(
        name = "MNUsersUpdateServlet",
        urlPatterns = "/mnusersupdateservlet"
)
public class MNUsersUpdateServlet extends BasicServlet {
    
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private LogWriter LG = null;
    
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        String action = request.getParameter("actionid");
        if(isSessionTimeout)
        {
            return;
        }
        
        response.setContentType(CONTENT_TYPE);
        HttpSession session = request.getSession();
        
        String selectedUserID = request.getParameter("SelectedID");
        if(selectedUserID == null || selectedUserID == "") 
        {
            selectedUserID = "0";
        }
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        String siteCombo = "";
        String roleCombo = "";
        String deptCombo = "";
        String langCombo = "";
        String labCombo = "";
        String wfCombo = "";
        
            if(action != null && !action.equals("")) 
            {
//                if(action.equals("getWFStates")) 
//                {
//                    JSONObject obj = null;
//                    
//                    try
//                    {
//                        String wfTypeID = request.getParameter("wfType");
//                        obj = new JSONObject();
//                        WFManagement wfMng = new WFManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
//                        if(wfMng.GetWFStates(Integer.parseInt(wfTypeID),Integer.parseInt(selectedUserID))) 
//                        {
//                            String combo = new Gson().toJson(wfMng.WFStateAll());
//                            obj.put("AllStates",combo);
//                            
//                            combo = new Gson().toJson(wfMng.WFStateSelected());
//                            obj.put("SelectedStates",combo);
//                        }
//                        
//                        response.setHeader("Content-Type", "application/json; charset=UTF-8");
//                        response.getWriter().print(obj);
//                    }
//                    catch(Exception ex) 
//                    {
//                        LG.write(ex.toString(), "MNUsersUpdateServlet   getWFStates:", ex);
//                        response.getWriter().print(obj);
//                    }
//                    return;
//                }
                
                if(action.equals("getUserData")) 
                {
                    String userData = "";
                    try
                    {
                        UsersManagement uMng = new UsersManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
                        userData = new Gson().toJson(uMng.GetUserData(selectedUserID));
                        
                        response.setHeader("Content-Type", "application/json; charset=UTF-8");
                        response.getWriter().print(userData);
                    }
                    catch(Exception ex) 
                    {
                        LG.write(ex.toString(), "MNUsersUpdateServlet   getUserData:", ex);
                        response.getWriter().print(userData);
                    }
                    
                    return;
                }
                
                if(action.equals("SaveData")) 
                {
                    try
                    {
                        UserData data = BuildUserData(request);
                        if(data == null) 
                        {
                            response.getWriter().print("-1");
                            return;
                        }
                        data.USER_ID = Integer.parseInt(selectedUserID);
                        data.changed_by = ((User)session.getAttribute("USER")).USER_ID;
                        
                        UsersManagement uMng = new UsersManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
                        int retVal = uMng.SaveUserData(data);
                        response.setHeader("Content-Type", "application/json; charset=UTF-8");
                        response.getWriter().print(retVal);
                        
                    }
                    catch(Exception ex) 
                    {
                        LG.write(ex.toString(), "MNUsersUpdateServlet   getUserData:", ex);
                        response.getWriter().print("-1");
                    }
                    return;
                    
                }
            }
            
            
        
        
        try 
        {
            SiteManagement siteMng = new SiteManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);            
            siteCombo = new Gson().toJson(siteMng.GetSiteComboList());            
        }
        catch(Exception ex) 
        {
            siteCombo = "0";
            LG.write(ex.toString(), "MNUsersUpdateServlet: GetSites: ", ex);
        }
        
        try 
        {
            UsersManagement uMng = new UsersManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
            roleCombo = new Gson().toJson(uMng.GetRoleList("skyline"));
        }
        catch(Exception ex) 
        {
            roleCombo = "0";
            LG.write(ex.toString(), "MNUsersUpdateServlet: GetRoles: ", ex);
        }
        
        try 
        {
            DepartmentManagement dMng = new DepartmentManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
            deptCombo = new Gson().toJson(dMng.GetDepartmentCombo());
        }
        catch(Exception ex) 
        {
            deptCombo = "0";
            LG.write(ex.toString(), "MNUsersUpdateServlet: GetRoles: ", ex);
        }
        
        try 
        {
            QCLABManagement labMng = new QCLABManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
            labCombo = new Gson().toJson(labMng.GetQCLabCombo());
        }
        catch(Exception ex) 
        {
            labCombo = "0";
            LG.write(ex.toString(), "MNUsersUpdateServlet: GetLabs: ", ex);
        }
        
        try 
        {
            LangManagement langMng = new LangManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
            langCombo = new Gson().toJson(langMng.GetLangCombo());
        }
        catch(Exception ex) 
        {
            langCombo = "0";
            LG.write(ex.toString(), "MNUsersUpdateServlet: GetLangs: ", ex);
        }
        
//        try 
//        {
//            WFManagement wfMng = new WFManagement(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath);
//            wfCombo = new Gson().toJson(wfMng.GetWFTypes());
//        }
//        catch(Exception ex) 
//        {
//            wfCombo = "0";
//            LG.write(ex.toString(), "MNUsersUpdateServlet: GetWFTypes: ", ex);
//        }
        
        request.setAttribute("SITE_COMBO",siteCombo);
        request.setAttribute("ROLE_COMBO",roleCombo);
        request.setAttribute("DEPT_COMBO",deptCombo);
        request.setAttribute("LAB_COMBO",labCombo);
        request.setAttribute("LANG_COMBO",langCombo);
        request.setAttribute("WF_COMBO",wfCombo);
        request.setAttribute("CURR_USER_ID",selectedUserID);
        
        request.getRequestDispatcher("Maintenance/UsersUpdate.jsp").forward(request, response);
        
    }
    
    public UserData BuildUserData(HttpServletRequest request) 
    {
        UserData data = new UserData();
        try
        {
            String userName = request.getParameter("UserName");
            String pass = request.getParameter("Pass");
            String siteID = request.getParameter("SiteID");
            String roleID = request.getParameter("RoleID");
            String firstName = request.getParameter("FirstName");
            String lastName = request.getParameter("LastName");
            String initials = request.getParameter("Initials");
            String title = request.getParameter("Title");
            String email = request.getParameter("Email");
            String deptID = request.getParameter("DeptID");
            String labID = request.getParameter("LabID");
            String ldap = request.getParameter("LdapName");
            String maint = request.getParameter("Maint");
            String locked = request.getParameter("Locked");
            String sampleApp = request.getParameter("SampleApp");
            String testApp = request.getParameter("TestApp");
            String deviations = request.getParameter("Deviations");
            String activityApp = request.getParameter("ActivityApp");
            String inventoryList = request.getParameter("InventoryList");
            String inventoryType = request.getParameter("inventoryType");
            String coa = request.getParameter("Coa");
            String inventoryUsage = request.getParameter("InventoryUsage");
            String newPass = request.getParameter("NewPass");
            String selectedStates = request.getParameter("SelectedStates");
            String removeStates = request.getParameter("RemoveStates");
            String langCode = request.getParameter("LangCode");
            
            data.USER_NAME = userName;
            data.PASSWORD = pass;
            data.SITE_ID = Integer.parseInt(siteID);
            data.ROLE = roleID;
            data.FIRST_NAME = firstName;
            data.LAST_NAME = lastName;
            data.INITALS = initials;
            data.TITLE = title;
            data.EMAIL = email;
            data.DEPT_ID = Integer.parseInt(deptID);
            data.LAB_ID = Integer.parseInt(labID);
            data.LDAP_NAME = ldap;
            data.LANG_CODE = langCode;
            
            
            data.MAINTENANCE = (maint.equals("true"))?1:0;
            data.LOCKED = (locked.equals("true"))?1:0;
            data.SAMPLE_APP = (sampleApp.equals("true"))?1:0;
            data.TEST_APP = (testApp.equals("true"))?1:0;
            data.DEVIATION = (deviations.equals("true"))?1:0;
            data.ACTIVITY_APP = (activityApp.equals("true"))?1:0;
            data.INVENTORY_LIST = (inventoryList.equals("true"))?1:0;
            data.INVENTORY_TYPE = (inventoryType.equals("true"))?1:0;
            data.COA = (coa.equals("true"))?1:0;
            data.INVENTORY_USAGE = (inventoryUsage.equals("true"))?1:0;
            data.NEW_PASS = newPass;
            data.ADD_STATES = selectedStates;
            data.REMOVE_STATES = removeStates;
            
        }
        catch(Exception ex) 
        {
            LG.write(ex.toString(), "MNUsersUpdateServlet: BuildUserData: ", ex);
            data = null;
        }
        return data;
        
    }
}
