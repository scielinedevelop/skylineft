package comply.SkyLine.servlets;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.ComplyUtils;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.maintenance.MaintenanceBase;
import comply.SkyLine.bl.maintenance.MaintenanceFactory;

import com.google.gson.Gson;

import jasper.biz.JasperDataSourceSupplier;
import jasper.biz.JasperReportType;
import jasper.biz.JasperReportGenerator;
//import jasper.biz.JasperReportType;
//import jasper.biz.JasperTemplateFactory;
import jasper.biz.JasperTemplateFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.Date;

import java.util.HashMap;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import org.json.JSONArray;

@WebServlet(
        name = "MaintenanceMainServlet",
        urlPatterns = "/maintenancemainservlet"
)
public class MaintenanceMainServlet extends BasicServlet  
{
	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private LogWriter LG = null;

    public void init(ServletConfig config) throws ServletException 
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        if(isSessionTimeout)
        {
            return;
        }
        response.setContentType(CONTENT_TYPE);
        String action = request.getParameter("actionid");
        HttpSession session = request.getSession();
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        String mType = request.getParameter("TabID");
        String viewName = request.getParameter("tableViewName");
        MaintenanceBase mainBL = MaintenanceFactory.GetMaintenanceBL(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath,mType);
        
        if(action != null)
        {
            if(action.equals("getUsersList"))
            {
                try
                {
                    String list = new Gson().toJson(mainBL.getUsersArray(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath));
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(list);
                    
                }
                catch(Exception ex) 
                {
                    
                    LG.write(ex.toString(), "MaintenanceMainServlet: getUsersList: ", ex);
                    
                }
                
                return;
            }
            if(action.equals("showReportByXml"))
            {
              String tableDisplayName = request.getParameter("tableDisplayName");
              String userFullName = request.getParameter("selUserFullName");  
              String userId = request.getParameter("selUserID");
              String fromDate = request.getParameter("from_Date"); 
              String toDate = request.getParameter("to_Date");
              String toDateDisplay = request.getParameter("toDateForDisplay");
              String maintenanceType = request.getParameter("maintenanceType");
              String xmlName = request.getParameter("tableATxml");
              String reportType = request.getParameter("reportType");
              String filter = "";
              String displayFilter = "";
              if(!userId.equals("0") && !userId.equals(""))
              {
                  filter += " and hidden_user_id = "+ userId+ "";
                  displayFilter += "User:  " + userFullName;
              }
              if(!fromDate.equals("") && !fromDate.equals("00/00/0000"))
              {
                  filter += " and to_date(change_date,'dd/MM/yyyy HH24:MI') > to_date('"+ fromDate +"','dd/MM/yyyy HH24:MI')  ";
                  displayFilter += "\n" + "From Date:  " + fromDate;
              }
              if(!toDate.equals("") && !toDate.equals("00/00/0000"))
              {
                 filter += " and to_date(change_date,'dd/MM/yyyy HH24:MI') < to_date('"+ toDate +"','dd/MM/yyyy HH24:MI')  ";
                 displayFilter += "     To Date:  " + toDateDisplay;
              }
              
              String jasperDir = props.getJasperReportsDir();
              String jasperTemplatesDir = props.getJasperReportsTemplateDir();
              String jasperTempDir = props.getJasperReportsTmpDir();
              
              try
              { 
                HashMap<String, String> hmReportReplacerList  = new  HashMap<String, String>();  
                hmReportReplacerList.put("@filter_replacer@", filter); 
                
                HashMap<String, Object> hmReportParameterList  = new  HashMap<String, Object>();         
                hmReportParameterList.put("SESSION_ID", session.getId() + (new Date()).getTime());        
                hmReportParameterList.put("DIR_JASPER_XML", jasperDir);     
                hmReportParameterList.put("DIR_JASPER_XML_TMP", jasperTempDir);
                
                if(reportType.equals("1")) /* build sub report by view */
                {
                  hmReportParameterList.put("SUB_REPORT_DATA_SOURCE",  new JasperDataSourceSupplier(connUtility.getUrl(), connUtility.getDBuser(), connUtility.getPassword()).createReportDataSource("select * from " + viewName,"JRMapArrayDataSource"));
                  JasperTemplateFactory jf = new JasperTemplateFactory(connUtility.getUrl(), connUtility.getDBuser(), connUtility.getPassword(), jasperDir, jasperTemplatesDir, jasperTempDir);
                  hmReportParameterList.put("SUB_REPORT",jf.geAtGroupedJasperReportCompiledDataInjection("rMaintSubReport" + (new Date()).getTime(),viewName,"hidden_object_id","PARENT_PARAM_ID",true));
                }
                else if(reportType.equals("2"))
                {
                  hmReportParameterList.put("DATA_SOURCE_RESGROUP",  new JasperDataSourceSupplier(connUtility.getUrl(), connUtility.getDBuser(), connUtility.getPassword(),false).createReportDataSource("select * from " + viewName + " where maintenance_type = '" + maintenanceType + "'","JRMapArrayDataSource"));
                  hmReportParameterList.put("SUB_REPORT_RESGROUP",(new jasper.biz.JasperReportGenerator()).getCompiled("resgroup" + session.getId() + (new Date()).getTime(),"rATMaintenanceSubRespGr.xml",jasperDir,jasperTempDir,null));
                }
                else if(reportType.equals("3"))
                {
                	//sub_reports
                	hmReportParameterList.put("SUB_REPORT_DATA_SOURCE", new JasperDataSourceSupplier(connUtility.getUrl(), connUtility.getDBuser(), connUtility.getPassword(),false).createReportDataSource("select * from p_location_sub_at_v","JRMapArrayDataSource"));
                	hmReportParameterList.put("SUB_REPORT",(new jasper.biz.JasperReportGenerator()).getCompiled("subLocations" + session.getId() + (new Date()).getTime(),"rATMaintenanceSubLocations.xml",jasperDir,jasperTempDir,null));
                }  
               
                //the call      
                JasperReportGenerator jrg = new JasperReportGenerator(connUtility.getUrl(), connUtility.getDBuser(), connUtility.getPassword());      
                
                ByteArrayOutputStream out  = jrg.getByteArrayOutputStream( session.getId() + (new Date()).getTime(),                                            
                                                                           xmlName,
                                                                           JasperReportType.PDF,         
                                                                            "Audit Trail for - " +tableDisplayName+ " Table", 
                                                                            displayFilter,                  
                                                                           hmReportReplacerList,      
                                                                           hmReportParameterList,     
                                                                           jasperDir,      
                                                                           jasperTempDir); 
                                               
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\"MaintenanceReport.pdf" + "\";");
                OutputStream stream = response.getOutputStream();
                out.writeTo(stream);
                stream.flush();
                stream.close();
                 
                 
              }
              catch (Exception e) 
              {
                LG.write(e.toString(), "MaintenanceMainServlet:showReportByXml: ", e);
              }
              return;
              
            }
            
            if(action.equals("showReportByView"))
            {
               String tableDisplayName = request.getParameter("tableDisplayName");
               String userFullName = request.getParameter("selUserFullName");  
               String userId = request.getParameter("selUserID");
               String fromDate = request.getParameter("from_Date"); 
               String toDate = request.getParameter("to_Date");
               String toDateDisplay = request.getParameter("toDateForDisplay");
               String filter = "";
               String displayFilter = "";
               if(!userId.equals("0") && !userId.equals(""))
               {
                   filter += " and hidden_user_id = "+ userId+ "";
                   displayFilter += "User:  " + userFullName;
               }
               if(!fromDate.equals("") && !fromDate.equals("00/00/0000"))
               {
                   filter += " and to_date(\"TIME STAMP\",'dd/MM/yyyy HH24:MI') > to_date('"+ fromDate +"','dd/MM/yyyy HH24:MI')  ";
                   displayFilter += "\n" + "From Date:  " + fromDate;
               }
              if(!toDate.equals("") && !toDate.equals("00/00/0000"))
              {
                  filter += " and to_date(\"TIME STAMP\",'dd/MM/yyyy HH24:MI') < to_date('"+ toDate +"','dd/MM/yyyy HH24:MI')  ";
                  displayFilter += "     To Date:  " + toDateDisplay;
              }
               
               
               String jasperDir = props.getJasperReportsDir();
               String jasperTemplatesDir = props.getJasperReportsTemplateDir();
               String jasperTempDir = props.getJasperReportsTmpDir();
               
             
               
               try {
                  
                   JasperTemplateFactory jtf = new JasperTemplateFactory (connUtility.getUrl(), connUtility.getDBuser(), connUtility.getPassword(), 
                                                                          jasperDir, jasperTemplatesDir,jasperTempDir);
                                                                                                                               
                    ByteArrayOutputStream out = jtf.getAtGroupedJasperReportStream(session.getId() + (new Date()).getTime(),
                                                                                  JasperReportType.PDF, 
                                                                                  "Audit Trail for - " +tableDisplayName+ " Table", 
                                                                                  displayFilter, viewName, 
                                                                                  filter);
                   
                   response.setContentType("application/octet-stream");
                   response.setHeader("Content-Disposition", "attachment; filename=\"MaintenanceReport.pdf" + "\";");
                   OutputStream stream = response.getOutputStream();
                   out.writeTo(stream);
                   stream.flush();
                   stream.close();
                   
               }
               catch (Exception e) 
               {
                 LG.write(e.toString(), "MaintenanceMainServlet:showReportByView: ", e);
               }
               return;
            }
        }
        JSONArray prop = getMaintTablesStructure(request, logPath);

        request.setAttribute("TABLES_STRUCTURE",prop);   
        request.setAttribute("DEFAULT_TAB_ID", ComplyUtils.getNull(request.getParameter("defaultTabID")));
        request.setAttribute("DEFAULT_TABLE_FILTER", ComplyUtils.getNull(request.getParameter("defaultTableFilter")));
        session.setAttribute("SELECTED_MENU", "maintenance");
        
        request.getRequestDispatcher("MaintenanceMain.jsp").forward(request, response);
    }
}
