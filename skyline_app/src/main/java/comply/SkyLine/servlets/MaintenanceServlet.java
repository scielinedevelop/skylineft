package comply.SkyLine.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import comply.SkyLine.bl.biz.AppProperties;
import comply.SkyLine.bl.biz.User;
import comply.SkyLine.bl.comtec.LogWriter;
import comply.SkyLine.bl.general.ConnUtility;
import comply.SkyLine.bl.general.DataTableParamModel;
import comply.SkyLine.bl.maintenance.MaintenanceBase;
import comply.SkyLine.bl.maintenance.MaintenanceFactory;

@WebServlet(
        name = "MaintenanceServlet",
        urlPatterns = "/maintenanceservlet"
)
public class MaintenanceServlet extends BasicServlet  
{
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private static final String  DISABLE_CONTROLS = "DISABLE_CONTROLS";
    private LogWriter LG = null;

    public void init(ServletConfig config) throws ServletException 
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        super.doPost(request, response);
        String action = request.getParameter("actionid");
        if(isSessionTimeout)
        {
            return;
        }
        response.setContentType(CONTENT_TYPE);
        HttpSession session = request.getSession();
        int currUserID = ((User)session.getAttribute("USER")).USER_ID;
        
        AppProperties props = (AppProperties)session.getAttribute("AppProp");
        ConnUtility connUtility = (ConnUtility)session.getAttribute("ConUtil");
        String logPath = props.getLogPath();
        LG = new LogWriter(logPath);
        String prop = null;
        final DataTableParamModel paramModel = DataTablesParamUtilityServlet.getParam(request);
        
        String mType = request.getParameter("TabID");
        String popupName = request.getParameter("PopupName");
        String tableName = request.getParameter("TableName");
        
        
        MaintenanceBase mainBL = MaintenanceFactory.GetMaintenanceBL(connUtility.getUrl(),connUtility.getDBuser(),connUtility.getPassword(), logPath,mType);
        JSONObject tableResult;
        
        if(action != null)
        {
            if(action.equals("GetTableData"))
            {
                try
                {
                    tableResult = mainBL.GetTableData(paramModel,"0","0");
                    response.setHeader("Content-Type", "application/json; charset=UTF-8");
                    response.getWriter().print(tableResult);
                    
                }
                catch(Exception ex) 
                {
                    tableResult = null;
                    LG.write(ex.toString(), "MaintenanceServlet: GetTableData: ", ex);
                    
                }
                
                return;
            }
            
            if(action.equals("DeleteItem")) 
            {
                try 
                {                     
                    String itemId = request.getParameter("ItemID"); 
                    int retVal = 0;
                    
                    if(mType.toLowerCase().equals("uom")) 
                    {                        
                        retVal = mainBL.deleteItemParams(currUserID, "", itemId);
                    }
                    else
                    {
                        retVal = mainBL.DeleteItem(Integer.parseInt(itemId),currUserID,"");
                    }
                    response.getWriter().print(retVal);
                }
                catch(Exception ex) 
                {
                    response.getWriter().print(-1);
                    LG.write(ex.toString(), "MaintenanceServlet: DeleteItem: ", ex);
                }
                return;
            }
            
        }
        
        try 
        {            
            prop = getTableProperties(request, tableName);
        }
        catch (Exception e) 
        {
            prop = "";
            LG.write(e.toString(), "MaintenanceServlet:action==null: getTableProperties: ", e);
        }
 
        request.setAttribute("TABLE_PROPERTIES",prop);
        request.setAttribute("TAB_ID",mType);
        request.setAttribute("POPUP_NAME",popupName);
        request.setAttribute("TABLE_NAME",tableName);  
        
        String tabPermiss = request.getParameter("currTabPermissions");
        SetControlsPermissions(request,session, tabPermiss);
        request.getRequestDispatcher("Maintenance/MaintenanceTables.jsp").forward(request, response);
    }
    
    private boolean SetControlsPermissions(HttpServletRequest request, HttpSession session, String tabPermissions)
    {
        
        String role = ((User)session.getAttribute("USER")).ROLE;     
        boolean canUpdate = false;
        
        if(!tabPermissions.equals(""))
        {
            if(tabPermissions.toLowerCase().contains(role.toLowerCase()))
            {                
                canUpdate = true;
            }
        }
        else if(role.toLowerCase().equals("a") || ((User)session.getAttribute("USER")).MAINTENANCE_TABLES == 1)
        {
            canUpdate = true;
        }
        
        if(!canUpdate)
        {
            request.setAttribute(DISABLE_CONTROLS, "disabled");
            request.setAttribute("CAN_UPDATE",0);
            return false;
        }
        else
        {
            request.setAttribute(DISABLE_CONTROLS, "");
            request.setAttribute("CAN_UPDATE",1);
            return true;
        }
    }
}
