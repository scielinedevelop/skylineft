package comply.SkyLine.servlets;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import comply.SkyLine.bl.biz.*;

@WebListener
public class SessionListener implements HttpSessionListener {
	
	private HttpSession session = null;
    private boolean isCleanup = false;
	
	@Override
    public void sessionCreated(HttpSessionEvent event) 
	{         
		session = event.getSession();   
		System.out.println("Session Created with session id " + session.getId());
       
		String value = AppPropSingleton.getInstance().getVersion();
		SimpleDateFormat formatter = new SimpleDateFormat ("dd/MM/yyyy");
		Date currentTime_1 = new Date();
		String dateString = formatter.format(currentTime_1);
		session.setAttribute("VERSION", "Skyline - " + dateString + " v." + value);   
    }
	
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
          
    	System.out.println("Session Destroyed, Session id:" + se.getSession().getId());
           
           session = se.getSession();
           
           // Removing attachment directory
           try 
           {
        	   String attachPath = (String)session.getAttribute("ATTACH_PATH");
			   deleteDir(attachPath, 0, true);
			   File attachDir = new File(attachPath);
			   attachDir.delete();
			   
			   //cleanning...
			   synchronized (this) 
			   {
			          if(!isCleanup)
			          {
			             isCleanup = true;
			          }
			          else
			          {
			              System.out.print("cleaning is made by other session");
			              return;
			          }
			   } 
			   System.out.print("start clean up");
			   isCleanup = false;
		} catch (Exception e) {
			isCleanup = false;
		}
           
    }
    
    // Delete  file / directory
    private void deleteDir(String path, int deleteTimeInterval, boolean deleteFolder)//should set isCleanup to false on exception
    {
        File deleteDir;
        File  tmpFile;
        File[] fileList; 
        
        deleteDir = new File(path);
        fileList = deleteDir.listFiles();
        
        for(int i=0 ; i<fileList.length ; i++)
        {
          tmpFile = fileList[i];
          if( ( (new Date()).getTime() - tmpFile.lastModified() > deleteTimeInterval || deleteTimeInterval ==0 ) && !tmpFile.isHidden() )   //tmpFile.isHidden() - for protect .svn hidden folders
          {
              if(tmpFile.isDirectory()) 
              {
                  deleteDir(tmpFile.getAbsolutePath(),0,true); //recursive call -> delete all (deleteTimeInterval = 0,  deleteFolder true) because parent directory pass the lastModified / isHidden conditions
              }
              else 
              {
                  tmpFile.delete();
              }
          }
        }
        
        if(deleteFolder)
        {
          deleteDir.delete();
        } 
    }
}
