<!DOCTYPE HTML>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>SieUpdate</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>
            <%=session.getAttribute("VERSION")%>
        </title>
        <link href="CSS/comply_theme/Skyline_9.css" rel="stylesheet"  type="text/css" />
        <link href="CSS/comply_theme/jquery-ui.custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="CSS/comply_theme/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="CSS/comply_theme/ColumnFilterWidgets.css" rel="stylesheet" type="text/css" />
        
        <script src="js/comply_js/jquery.js" type="text/javascript"></script>
        <script src="js/comply_js/jquery.dataTables.js" type="text/javascript"></script>  
        <script src="js/comply_js/jquery-ui.custom.js" type="text/javascript"></script>
        <script src="js/comply_js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
        <script src="js/comply_js/jquery-migrate.js" type="text/javascript"></script>
        <script src="js/comply_js/ColumnFilterWidgets.js" type="text/javascript"></script>
        <script src="js/comply_js/skyline.dataTables.js" type="text/javascript"></script> 
        <script src="js/comply_js/CommonFuncs.js" type="text/javascript"></script>
        <script src="js/comply_js/Lang_Selection.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            $(document).ready(function () 
            {
                $('button').button();
                LoadLabelsJson(); 
                $('#lblError').val("");
                LoadDeptData();
            });
            
            function LoadDeptData()
            {
                var deptID = $('#currId').val();
                if(deptID == "-1")
                {
                    return;
                }
                
                $.ajax
                ({
                    type:"POST",
                    contentType: "application/json; charset=utf-8",
                    url: "mndeptupdateservlet?actionid=getDeptData&SelectedID="+deptID,
                    dataType: "json",
                    success: function(data) 
                    {
                        if(data != null || data != "")
                        {
                            $('#DeptName').val(data.DEPT_NAME);
                            $('#address').val(data.ADDRESS);
                            $('#telNo').val(data.TEL_NO);
                        }
                    }
                });
            }
            
            function SaveData()
            {
                $('#lblError').html("");
                
                if($.trim($('#DeptName').val()) == "")
                {
                    DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                    $('#DeptName').focus();
                    return;
                }
                
                var deptID = $('#currId').val();
                var deptName = $('#DeptName').val();
                var address = $('#address').val();
                var telNo = $('#telNo').val();
                
                var saveParams = "actionid=SaveData&SelectedID="+deptID+"&DeptName="+encodeURIComponent(deptName)+"&Address="+encodeURIComponent(address)+"&TelNo="+encodeURIComponent(telNo);
                
                $.ajax
                ({
                    type:"POST",
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    url: "mndeptupdateservlet",
                    data:saveParams,
                    dataType: "json",
                    success: function(data) 
                    {
                        if(data == "-1")
                        {
                            DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                        }
                        else if(data == "-2")
                        {
                            DisplayError(replace("DisplayError_Msg_Short_Name_T_4"));
                        }
                        else
                        {
                            $("#mask", window.parent.document).hide();
                
                            $('#UpdateFormDiv', window.parent.document).hide();
                        
                            window.parent.RefreshTable();
                        }                
                    },
                    error: function(data)
                    {
                        DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                    }
                });
            }
            
            function CancelAction()
            {
                $("#mask", window.parent.document).hide();
                
                $('#UpdateFormDiv', window.parent.document).hide();
            }
            
            function DisplayError(errMsg)
            {
                $( "#tabs" ).tabs( "option", "active", 0 );
                var list = $('#lblError').append('<ul ></>').find('ul');
                list.append('<li class="errorMessage">'+ errMsg + '</>');
            }
        </script>
    </head>
    <body>
        <table width="100%" align="center" border="0">
            <tr>
                <td colspan="4">
                        <h2 class="InitiationTitle"  id="setTitle" align="center" lang_key="Department_Details"></h2>
                </td>                                
            </tr>
            <tr><td><br/></td></tr>
            <tr>
                <td>
                    <table>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3">
                                <span class="mandatoryFieldMark">*</span>
                                <label lang_key="Department_Name"></label>:
                            </td>
                            <td>
                                <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="DeptName"
                                                    name="DeptName" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3">                                                                 
                                <label lang_key="Address"></label>:
                            </td>
                            <td>
                                <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="address"
                                                    name="address" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3">                                                                 
                                <label lang_key="Tel_Number"></label>:
                            </td>
                            <td>
                                <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="telNo"
                                                    name="telNo" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" id="lblError" class="cssStaticDataMandatory" style="TEXT-ALIGN:left;">
                    
                </td>
            </tr>
            <tr><td><br/></td></tr>
            <tr>
                <td align="center">
                    <button id="btnSave" onclick="SaveData();"><span lang_key="Save"></span></button>
                    <button id="btnCancel" onclick="CancelAction()" ><span lang_key="Cancel"></span></button>                                       
                </td>
            </tr>
        </table>
        <form name="main" id="main" method="post" action="mndeptupdateservlet">
            <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>        
            <input type="hidden" name="canUpdate" id="canUpdate" value='<%=request.getAttribute("CAN_UPDATE") %>'>
            <input type="hidden" name="Mode" id="Mode" value='<%=request.getAttribute("MODE") %>' >
            <input type="hidden" name="currId" id="currId" value='<%=request.getAttribute("CURR_ID") %>'>
        </form>
    </body>
</html>