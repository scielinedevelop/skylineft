<!DOCTYPE HTML>

<%@ page contentType="text/html;charset=UTF-8"%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        <%=session.getAttribute("VERSION")%>
    </title>
    
    <%@ include file="../include/includeGeneralCSS.jsp"%> 
	<%@ include file="../include/includeGeneralJS.jsp"%>
	<%@ include file="../include/includeDataTableFiles.jsp"%>  

    <script type="text/javascript">
        var oTable;
        var tabsArr = ["AnalysisStatus","Method","StrgCondition","TimePoint","SPointType","Manufacturer","Vendor","LIMSubsProcess","ContainerType","Location","Activity", "UOMGroup"];
        var reservedArr = ["AnalysisStatus","TestStatus","InventoryStatus"];
        
        $(document).ready(function () {
                    $("button" ).button();
                    
                    if($('#disableControls').val() == "disabled")
                    {
                        $(":button").button('option', 'disabled', true);
                    }
                   var labelsArr = LoadLabelsJson(); 
                   var tabID = $("#tabID").val();
                   var data = "maintenanceservlet?actionid=GetTableData&TabID=" + tabID;
                   var propArray =  $.parseJSON($('#table_prop_json').val()); 
                   var additOptions = {"fnOnRowSelection":enableButtons, "fnOnRemoveRowSelection":disableButtons,
                                        "fnOnClick":fnOnClick_, "fnOnDblClick":fnOnDblClick_, 
                                        "fnOnDrawCallback":disableButtons, "fnCreatedRow":fnCreatedRow_};
                   fnDrawTable("DTResults", propArray, data, additOptions, labelsArr);
                   
                   if(tabID == "Login")
                   {                        
                        $('#btnNew').button('option', 'disabled', true);
                   }

                   initConfirmDialogDiv();
        });

        function enableButtons(aData, tableID)
        {
            if($('#canUpdate').val() == 1)
            {
                var tabID = $("#tabID").val();
                if(($.inArray($("#tabID").val(), reservedArr) != -1) && aData[3] == 1) //aData[3] == 1 - reserved/hidden
                {
                    disableButtons();
                    return;
                }
                else if(tabID == "Login")
                {
                    $('#btnDelete').button('option', 'disabled', true);                    
                }
                else
                {                    
                    $('#btnDelete').button('option', 'disabled', false);
                }
                $('#btnUpdate').button('option', 'disabled', false);
                
            }
        }
        
        function disableButtons()
        {
            $('#btnUpdate').button('option', 'disabled', true);
            $('#btnDelete').button('option', 'disabled', true);
        }
        
        
        function fnOnClick_(aData, tableID)
        {
            $('#SelectedID').val(aData[0]);   
            
            if($("#tabID").val() == 'RG' && aData[0] == 0)
            {
                $('#SelectedID').val(99999); // EVERYONE group
            }
            
            $('#itemInUse').val(aData[aData.length - 1]);            
        }
        
         function fnOnDblClick_(aData, tableID)
        {
                
                if($('#canUpdate').val() == 1)
                {
                    var tabID = $("#tabID").val();
                    if(($.inArray($("#tabID").val(), reservedArr) != -1) && aData[3] == 1) //aData[3] == 1 - reserved/hidden
                    {
                        return;
                    }
                    if($("#tabID").val() == "Location")
                    {
                            if($('#itemInUse').val() == 1)
                            {
                                openConfirmDialog({ message:"OpenConfirmDialog_Msg_Short_Name_T_21", onConfirm:OpenPopupForm, onConfirmParams:["edit"]});
                            }
                            else
                            {
                                OpenPopupForm("edit");
                            }
                    }
                    else
                    {
                        OpenPopupForm("edit") ;
                    }
                }
        }
        
        function fnCreatedRow_(nRow, aData, iDataIndex, tableID)
        {
            var tabID = $("#tabID").val();            
            if(($.inArray(tabID, reservedArr) != -1) && aData[3] == 1) //aData[3] == 1 - reserved/hidden
            {               
                $.each(aData, function (i, o) 
                 {
                    $("td:eq("+ i +")", nRow).css("color", "grey");
                 });
            }
            
            if(tabID == "TestGeneral" || tabID == "SPointType" || tabID == "TestCategory")
            { 
                 $.each(aData, function (i, o) 
                 {
                        var str = aData[i+1];      
                        if( i < 3)
                        {
                                //alert("i:" + i + "    " + str);
                                if(str != null || str != "")
                                {
                                    if( checkRtl(str) ) 
                                    {                               
                                        $("td:eq("+i+")", nRow).addClass("cssTableCellRTL");
                                    } 
                                    else 
                                    {
                                        $("td:eq("+i+")", nRow).addClass("cssTableCellLTR");
                                    }; 
                                }
                        }
                 });
                   
            }
        }
        
         function handleAjaxError( xhr, textStatus, error ) 
	{      
	   // $("#hiddenformMessage").hide();
            //$("#mask").hide();
            if (xhr.responseText.match(/TIME_IS_OUT/)) 
	    {   
		top.location.href = 'Login.jsp';
	    } 
	}
        
        function OpenPopupForm(mode)
        {
            var selectedID = 0;
            var used = "-1"; 
            if(mode == "edit")
            {
                if($.inArray($("#tabID").val(), tabsArr) != -1)// && $('#itemInUse').val() == 1)
                {
                    used = $('#itemInUse').val();
                }
                selectedID = $('#SelectedID').val();
            }
            var servletID = $('#PopupName').val();
            servletID = "redirect.htm?url="+servletID+"?SelectedID=" + encodeURIComponent(selectedID) + "&itemInUse=" + used;
          
            window.parent.document.getElementById('UpdateForm').src = servletID;
            
            //maintenanceaudittrailservlet
            $("#mask", window.parent.document).fadeTo(500, 0.25);
            
            $('#UpdateFormDiv', window.parent.document).show();
        }
        
        function RefreshTable()
        {           
           var table = $("#DTResults").dataTable();
           table.fnDraw(false); 
        }
        
        function DeleteItem() 
        {
            
            var selectedID = $('#SelectedID').val();
            
            if(selectedID == null || selectedID == "" || selectedID == "0")
            {
                displayAlertDialog("DispAlDial_Msg_Short_Name_T_150");
                return;
            }
            
            doContinue = false;
            openConfirmDialog({ message:"OpenConfirmDialog_Msg_Short_Name_T_18", onConfirm:DoDelete });
        }
        
        function DoDelete()
        {           
            if($.inArray($("#tabID").val(), tabsArr) != -1 && $('#itemInUse').val() == 1)
            {
                displayAlertDialog("DispAlDial_Msg_Short_Name_T_158");
                return;
            }
            if($("#tabID").val() == 'RG' && $('#SelectedID').val() == 99999) // EVERYBODY group
            {
                displayAlertDialog("DispAlDial_Msg_Short_Name_T_158");
                return;
            }
            
            var tabID = $('#tabID').val();            
            var selectedID = $('#SelectedID').val();
            $.ajax
            ({
                type:"POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "maintenanceservlet",
                data:"actionid=DeleteItem&ItemID=" + encodeURIComponent(selectedID)+"&TabID="+tabID,
                dataType: "json",
                success: function(data) 
                {
                    if(data == "1")
                    {
                        displayAlertDialog("DispAlDial_Msg_Short_Name_T_157");
                        RefreshTable();
                    }
                    else if(data == "-2")
                    {
                        displayAlertDialog("DispAlDial_Msg_Short_Name_T_160");
                    }
                    else if(data == "-3")
                    {
                        displayAlertDialog("DispAlDial_Msg_Short_Name_T_158");
                    }
                    else
                    {
                        displayAlertDialog("DispAlDial_Msg_Short_Name_T_159");
                    }
                                        
                },
                error: function(data)
                {
                    displayAlertDialog("DispAlDial_Msg_Short_Name_T_159");
                }
            });
        }
        
        function openAuditTrailForm()
        {            
            $('#btnATReportMain', window.parent.document).click();          
        }
       
    </script>
  </head>
  <body>
  <div id="mask" class="containment-wrapper"></div>
   <TABLE WIDTH="98%" align="center" BORDER="0" CELLSPACING="0" CELLPADDING="0" >
    <tr>
        <TD>
            <table BORDER=0 width=100% cellSpacing="1" cellPadding="1" DIR="ltr">
                <tr> <td><br/></td></tr>
                <tr>
                    <td>
                        <DIV>
                            <table id="DTResults" class="display" width="100%" align="center"></table>          
                        </DIV>
                    </td>
                </tr>
                <tr> <td><br/></td></tr>
                <tr>
                    <td align="center">
                        <button type="button" id="btnNew" onclick="OpenPopupForm('new');" ><span lang_key="New"></span></button>
                        <button type="button" id="btnUpdate" onclick="OpenPopupForm('edit')" disabled="disabled"><span lang_key="Update"></span></button>                       
                        <button type="button" id="btnDelete"  onclick="DeleteItem()" disabled="disabled"><span lang_key="Delete"></span></button>
                        <button type="button" id="btnATReport"  onclick="openAuditTrailForm()"><span lang_key="Audit_Trail"></span></button>
                    </td>
                </tr>                
            </table>            
        </TD>        
    </tr>
</table>

     <form name="main" id="main" method="post" action="maintenanceservlet">      
        
        <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>                
        <input type="hidden" name="table_prop_json" id="table_prop_json" value='<%= request.getAttribute("TABLE_PROPERTIES")%>'></input>  
        <input type="hidden" name="canUpdate" id="canUpdate" value='<%=request.getAttribute("CAN_UPDATE") %>'>
        <input type="hidden" name="disableControls" id="disableControls" value='<%=request.getAttribute("DISABLE_CONTROLS") %>'>        
        <input type="hidden" name="tabID" id="tabID" value='<%=request.getAttribute("TAB_ID") %>'>
        <input type="hidden" name="PopupName" id="PopupName" value='<%= request.getAttribute("POPUP_NAME") %>'>
        <input type="hidden" name="SelectedID" id="SelectedID" value='<%= request.getAttribute("SELECTED_ID") %>'>
        <input type="hidden" name="itemInUse" id="itemInUse" value="0">
        <input type="hidden" name="esign" id="esign" value='<%= request.getAttribute("ESIGN") %>'>
        <input type="hidden" name="TableName" id="TableName" value='<%= request.getAttribute("TABLE_NAME") %>'>
      
    </form>
  </body>
</html>