<!DOCTYPE HTML>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        <%=session.getAttribute("VERSION")%>
    </title>
    <link href="CSS/comply_theme/Skyline_9.css" rel="stylesheet"  type="text/css" />
    <link href="CSS/comply_theme/jquery-ui.custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="CSS/comply_theme/demo_table_jui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/comply_theme/ColumnFilterWidgets.css" rel="stylesheet" type="text/css" />
    
    <script src="js/comply_js/jquery.js" type="text/javascript"></script>
    <script src="js/comply_js/jquery.dataTables.js" type="text/javascript"></script>  
    <script src="js/comply_js/jquery-ui.custom.js" type="text/javascript"></script>
    <script src="js/comply_js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
    <script src="js/comply_js/jquery-migrate.js" type="text/javascript"></script>
    <script src="js/comply_js/ColumnFilterWidgets.js" type="text/javascript"></script>
    <script src="js/comply_js/skyline.dataTables.js" type="text/javascript"></script> 
    <script src="js/comply_js/CommonFuncs.js" type="text/javascript"></script>
    <script src="js/comply_js/Lang_Selection.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(document).ready(function () 
        {
            $('button').button();
            LoadLabelsJson(); 
            if($('#currGroupId').val() == 0) // EVERYONE group
            {
              $('button').button('option','disabled', true);
              $('#btnCancel').button('option','disabled', false);
            }
            $('#lblError').val("");
            SetComboControls();
            
            LoadGroupData();
            
            $('#btnAddUser').click(function () 
            {
                $('#ddlAllUsers option:selected').remove().appendTo('#ddlSelectedUsers');
            });

            $('#btnRemoveUser').click(function () 
            {
                $('#ddlSelectedUsers option:selected').remove().appendTo('#ddlAllUsers').removeAttr('selected');
            });
        });
        
        function LoadGroupData()
        {
            var groupID = $('#currGroupId').val();
            
            if(groupID == "-1")
            {
                return;
            }
            if(groupID == "0") // EVERYONE group
            {
                groupID = "99999"; 
            }
            $.ajax
            ({
                type:"POST",
                contentType: "application/json; charset=utf-8",
                url: "mnrespgrpupdateservlet?actionid=getGroupData&SelectedID="+groupID,
                dataType: "json",
                success: function(data) 
                {
                    if(data != null || data != "")
                    {
                        $('#groupName').val(data.GROUP_NAME);
                    }
                }
            });
        }
        
        function SetComboControls()
        {
            var data = $.parseJSON($('#json_availible').val());
            $(data).map(function ()   
            {
                    $("#ddlAllUsers").append($('<option>').val(this.id).text(this.text));
            });
            
            data = $.parseJSON($('#json_selected').val());
            $(data).map(function ()   
            {
                    $("#ddlSelectedUsers").append($('<option>').val(this.id).text(this.text));
            });
        }
        
        function CancelAction()
        {
             $("#mask", window.parent.document).hide();
            
            $('#UpdateFormDiv', window.parent.document).hide();
                                    
        }
        
        function DisplayError(errMsg)
        {
            $( "#tabs" ).tabs( "option", "active", 0 );
            var list = $('#lblError').append('<ul ></>').find('ul');
            list.append('<li class="errorMessage">'+ errMsg + '</>');
        }
        
        function SaveData()
        {
            $('#lblError').html("");
            
            var ctl = $.trim($('#groupName').val());
            if(ctl == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#groupName').focus();
                return;
            }
            
            var groupID = $('#currGroupId').val();
            if(groupID == '0') //everyone group
            {
                CancelAction();
                return;
            }
            
            var groupName = $('#groupName').val();
            var groupMembers = [];
            $("#ddlSelectedUsers > option").each(function() 
            {
                groupMembers.push(this.value)
            });
            
            var saveParams = "actionid=SaveData&SelectedID="+groupID+"&GroupName="+encodeURIComponent(groupName)+"&GroupMembers="+groupMembers;
            
            $.ajax
            ({
                type:"POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "mnrespgrpupdateservlet",
                data: saveParams,
                dataType: "json",
                success: function(data) 
                {
                    if(data == "-1")
                    {
                        DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                    }
                    else if(data == "-2")
                    {
                        DisplayError(replace("DisplayError_Msg_Short_Name_T_4"));
                    }
                    else
                    {
                        $("#mask", window.parent.document).hide();
            
                        $('#UpdateFormDiv', window.parent.document).hide();
                    
                        window.parent.RefreshTable();
                    }                
                },
                error: function(data)
                {
                    DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                }
            });
        }
        
    </script>
    </head>
    <body>
        <table width="100%" align="center" border="0">
            <tr>
                <td colspan="4">
                        <h2 class="InitiationTitle"  id="setTitle" align="center" lang_key="Responsibility_Group_Details"></h2>
                </td>                                
            </tr>
            <tr><td><br/></td></tr>
            <tr>
                <td align="center">
                    <table>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:center">
                            <span class="mandatoryFieldMark">*</span>
                                <label lang_key="Group_Name"></label>:
                                    &nbsp;&nbsp;
                                <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="groupName"
                                                    name="groupName" maxlength="20" style="width: 200px"> </input>
                            </td>
                        </tr>
                        <tr>
                            <td></br></td>
                        </tr>
                        <tr>
                            <td class="cssStaticData" style="text-align:center">
                                <label lang_key="Users"></label>
                            </td>
                            <td style="width: 50px">
                            <td class="cssStaticData" style="text-align:center">
                                <label lang_key="Group_Members"></label>
                            </td>
                        </tr>
                        <tr style="height: 10px">
                        </tr>
                        <tr>
                            <td>
                                <select multiple id="ddlAllUsers" style="width: 250px; height: 300px"></select>
                            </td>
                            <td style="width: 50px" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <button type="button" style="width: 40px;text-align:center" id="btnAddUser"> > </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button type="button" style="width: 40px;text-align:center" id="btnRemoveUser"> < </button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <select multiple id="ddlSelectedUsers" style="width: 250px; height: 300px"></select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" id="lblError" class="cssStaticDataMandatory" style="TEXT-ALIGN:left;">
                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <button id="btnSave" onclick="SaveData();"><span lang_key="Save"></span></button>
                    <button id="btnCancel" onclick="CancelAction()" ><span lang_key="Cancel"></span></button>                                       
                </td>
            </tr>
        </table>
        <form name="main" id="main" method="post" action="mnrespgrpupdateservlet">
            <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>        
            <input type="hidden" name="json_availible" id="json_availible" value='<%=request.getAttribute("COMBO_AVAILIBLE") %>'>
            <input type="hidden" name="json_selected" id="json_selected" value='<%=request.getAttribute("COMBO_SELECTED") %>'>            
            <input type="hidden" name="canUpdate" id="canUpdate" value='<%=request.getAttribute("CAN_UPDATE") %>'>
            <input type="hidden" name="Mode" id="Mode" value='<%=request.getAttribute("MODE") %>' >
            <input type="hidden" name="currGroupId" id="currGroupId" value='<%=request.getAttribute("CURR_GROUP_ID") %>'>
    </form>
    </body>
</html>