<!DOCTYPE HTML>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>
            <%=session.getAttribute("VERSION")%>
        </title>
        <link href="CSS/comply_theme/Skyline_9.css" rel="stylesheet"  type="text/css" />
        <link href="CSS/comply_theme/jquery-ui.custom.css" rel="stylesheet" type="text/css" media="all" />
        
        <script src="js/comply_js/jquery.js" type="text/javascript"></script> 
        <script src="js/comply_js/jquery-ui.custom.js" type="text/javascript"></script>
        <script src="js/comply_js/jquery-migrate.js" type="text/javascript"></script>
        <script src="js/comply_js/CommonFuncs.js" type="text/javascript"></script>
        <script src="js/comply_js/Lang_Selection.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            
            $(document).ready(function () 
            {
                $('button').button();
                $('#lblError').val("");
                LoadLabelsJson(); 
                LoadData();
            });
            
            function LoadData()
            {
                var uomGroup = $('#currId').val();
                if(uomGroup == "-1")
                {
                    return;
                }
                
                $.ajax
                ({
                    type:"POST",
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    url: "mnuomgroupupdateservlet?actionid=getData&SelectedID=" + encodeURIComponent(uomGroup),
                    dataType: "json",
                    success: function(data) 
                    {
                        if(data != null || data != "")
                        {
                            $('#uomGroupName').val(data.uom_group);
                            //$('#uomDescription').val(data.is_active);
                            $('#chbActive').prop('checked', (data.is_active == 1)?true:false);
                        }
                    }
                });
            }
            
            function CancelAction()
            {
                 $("#mask", window.parent.document).hide();
                
                $('#UpdateFormDiv', window.parent.document).hide();
            }
            
            function DisplayError(errMsg)
            {
                $( "#tabs" ).tabs( "option", "active", 0 );
                var list = $('#lblError').append('<ul ></>').find('ul');
                list.append('<li class="errorMessage">'+ errMsg + '</>');
            }
            
            function SaveData()
            {
                $('#lblError').html("");
                var id = $('#currId').val();
                
                var uomGroup = $.trim($('#uomGroupName').val());
                if(uomGroup == "")
                {
                    DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                    $('#uomGroupName').focus();
                    return;
                }
                
                //var descr = $('#uomDescription').val();
                var active = ($('#chbActive').is(':checked'))?"1":"0"; 
                
                var saveParams = "actionid=saveData&SelectedID="+encodeURIComponent(id)+ "&uomGroupName="+encodeURIComponent(uomGroup)+"&isActive="+active;
                
                $.ajax
                ({
                    type:"POST",
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    url: "mnuomgroupupdateservlet?"+saveParams,
                    dataType: "json",
                    success: function(data) 
                    {
                        if(data == "-1")
                        {
                            DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                        }
                        else if(data == "-2")
                        {
                            DisplayError(replace("DisplayError_Msg_Short_Name_T_4"));
                        }
                        else
                        {
                            $("#mask", window.parent.document).hide();
                
                            $('#UpdateFormDiv', window.parent.document).hide();
                        
                            window.parent.RefreshTable();
                        }                
                    },
                    error: function(data)
                    {
                        DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                    }
                });
            }
        </script>
    </head>
    <body>
    
        <table width="100%" align="center" border="0">
            <tr>
                <td colspan="4">
                        <h2 class="InitiationTitle" align="center" lang_key="Uom_Group_Details"></h2>
                </td>                                
            </tr>
            <tr><td><br/></td></tr>
            <tr>
                <td>
                    <table>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3">
                                <span class="mandatoryFieldMark">*</span>
                                <label lang_key="UOM_Group"></label>:
                            </td>
                            <td>
                                <input type="text" onblur="focusInput(event)" onfocus="focusInput(event)" id="uomGroupName"
                                                    name="uomGroupName" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:right">
                                <label lang_key="Active"></label>:
                            </td>
                            <td>
                                <input type="checkbox" checked="checked" id="chbActive" name="chbActive"> </input>
                            </td>                            
                        </tr>
                        <!-- <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" >                                
                                <label lang_key="Description"></label>:
                            </td>
                            <td>
                                <input type="text" onblur="focusInput(event)" onfocus="focusInput(event)" id="uomDescription"
                                                    name="uomDescription" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr> -->
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" id="lblError" class="cssStaticDataMandatory" style="TEXT-ALIGN:left;">
                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <button id="btnSave" onclick="SaveData();"><span lang_key="Save"></span></button>
                    <button id="btnCancel" onclick="CancelAction()" ><span lang_key="Cancel"></span></button>                                       
                </td>
            </tr>
        </table>
        <form name="main" id="main" method="post" action="mnuomgroupupdateservlet">
            <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>        
            <input type="hidden" name="canUpdate" id="canUpdate" value='<%=request.getAttribute("CAN_UPDATE") %>'>
            <input type="hidden" name="Mode" id="Mode" value='<%=request.getAttribute("MODE") %>' >
            <input type="hidden" name="currId" id="currId" value='<%=request.getAttribute("CURR_ID") %>'>
        </form>
    </body>
</html>