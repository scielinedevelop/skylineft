<!DOCTYPE HTML>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>
            <%=session.getAttribute("VERSION")%>
        </title>
        <link href="CSS/comply_theme/Skyline_9.css" rel="stylesheet"  type="text/css" />
        <link href="CSS/comply_theme/jquery-ui.custom.css" rel="stylesheet" type="text/css" media="all" />
        
        <script src="js/comply_js/jquery.js" type="text/javascript"></script> 
        <script src="js/comply_js/jquery-ui.custom.js" type="text/javascript"></script>
        <script src="js/comply_js/jquery-migrate.js" type="text/javascript"></script>
        <script src="js/comply_js/CommonFuncs.v2.js" type="text/javascript"></script>
        <script src="js/comply_js/Lang_Selection.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            
            $(document).ready(function () 
            {
                $('button').button();
                $('#lblError').val("");
                LoadLabelsJson(); 
                LoadData();
                initConfirmDialogDiv();
            });
            
            function LoadData()
            {
                var uom = $('#currId').val();
                /* if(uom == "-1")
                {
                    return;
                } */
                
                $.ajax
                ({
                    type:"POST",
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    url: "mnuomupdateservlet?actionid=getData&SelectedID=" + encodeURIComponent(uom),
                    dataType: "json",
                    success: function(data) 
                    {
                         
                        if(data.uom_group_list != null)
                        {
                            var selectedArr = -1;
                            if (data.dataForEdit != null && data.dataForEdit != undefined)
                            {
                                selectedArr = $.parseJSON(data.dataForEdit).uom_group_id;
                            }

                            customSelectPopulate($('#ddlUomGroup'), $.parseJSON(data.uom_group_list), selectedArr, {useSingleDefaultOption:true});   
                        }
                        if(data.dataForEdit != null && data.dataForEdit != "" && data.dataForEdit != undefined)
                        {
                            $('#uomName').val($.parseJSON(data.dataForEdit).uom_name);
                            $('#uomDescription').val($.parseJSON(data.dataForEdit).uom_description);
                            $('#ddlUomGroup').val($.parseJSON(data.dataForEdit).uom_group_id);
                            $('#txtFactor').val($.parseJSON(data.dataForEdit).factor);
                            $('#chbIsNormal').prop('checked', ($.parseJSON(data.dataForEdit).is_normal == 1)?true:false);
                            $('#chbActive').prop('checked', ($.parseJSON(data.dataForEdit).is_active == 1)?true:false);
                        } else
                        {
                        	$('#chbIsNormal').prop('checked', false);
                        }
                        
                    }
                });
            }
            
            function CancelAction()
            {
                 $("#mask", window.parent.document).hide();
                
                $('#UpdateFormDiv', window.parent.document).hide();
            }
            
            function DisplayError(errMsg)
            {
                $( "#tabs" ).tabs( "option", "active", 0 );
                var list = $('#lblError').append('<ul ></>').find('ul');
                list.append('<li class="errorMessage">'+ errMsg + '</>');
            }
            
            function SaveData()
            {
                $('#lblError').html("");
                var id = $('#currId').val();
                
                var uom = $.trim($('#uomName').val());
                if(uom == "")
                {
                    DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_2"));
                    $('#uomName').focus();
                    return;
                }
                
                var uomGroup = $('#ddlUomGroup option:selected').val();
                
                if(uomGroup == "0")
                {
                    DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_2"));
                    $('#ddlUomGroup').focus();
                    return;
                }
                
                var factor = $.trim($('#txtFactor').val());
                if(factor == "")
                {
                    DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_2"));
                    $('#txtFactor').focus();
                    return;
                }
                
                
                var normal = ($('#chbIsNormal').is(':checked'))?"1":"0"; 
                
                if (normal == 1) 
                {
	                var saveParams = "actionid=checkIfNormal&SelectedID="+encodeURIComponent(id) + "&uomGroupId="+encodeURIComponent(uomGroup);
	
					$.ajax
					({
					  type:"POST",
					  contentType: "application/x-www-form-urlencoded; charset=utf-8",
					  url: "mnuomupdateservlet?"+saveParams,
					  dataType: "json",
					  success: function(data) 
					  {
					      if(data == "0" || uom == data)
					      {
					    	  doSave (id, uom, uomGroup, factor, normal);
					      }
					      else 
					      {
					          openConfirmDialog({ message:"Only one Normal can be selected. The last Normal will be saved", 
					        	  				onConfirm:updateNormal, onConfirmParams:[data, id, uom, uomGroup, factor, normal]});
					      }
					  },
					  error: function(data)
					  {
					      DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_3"));
					  }
					});
                
                } else
                {
                	doSave (id, uom, uomGroup, factor, normal);
                }
                
            }
            
            function doSave (id, uom, uomGroup, factor, normal)
            {
            	var descr = $('#uomDescription').val();
                var active = ($('#chbActive').is(':checked'))?"1":"0"; 
            	
            	
            	var saveParams = "actionid=saveData&SelectedID="+encodeURIComponent(id)+ "&uomName="+encodeURIComponent(uom)+"&uomDescr="+encodeURIComponent(descr) +
				  "&uomGroupId="+encodeURIComponent(uomGroup)+"&factor="+encodeURIComponent(factor)+"&isNormal="+normal+"&isActive="+active;

				$.ajax
				({
				  type:"POST",
				  contentType: "application/x-www-form-urlencoded; charset=utf-8",
				  url: "mnuomupdateservlet?"+saveParams,
				  dataType: "json",
				  success: function(data) 
				  {
				      if(data == "-1")
				      {
				          DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_3"));
				      }
				      else if(data == "-2")
				      {
				          DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_4"));
				      }
				      else
				      {
				          $("#mask", window.parent.document).hide();
				
				          $('#UpdateFormDiv', window.parent.document).hide();
				      
				          window.parent.RefreshTable();
				      }                
				  },
				  error: function(data)
				  {
				      DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_3"));
				  }
				});
            	
            }
            
            
            function updateNormal (uomForUnselectNormal, id, uom, uomGroup, factor, normal)
            {
            	var saveParams = "actionid=unselectLastNormal&SelectedID="+encodeURIComponent(id) + "&uomForUnselectNormal="+encodeURIComponent(uomForUnselectNormal); //"&uomName="+encodeURIComponent(uom);

				$.ajax
				({
					  type:"POST",
					  contentType: "application/x-www-form-urlencoded; charset=utf-8",
					  url: "mnuomupdateservlet?"+saveParams,
					  dataType: "json",
					  success: function(data) 
					  {
						  if(data == "-1")
					      {
					          DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_3"));
					      }
					      else if(data == "-2")
					      {
					          DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_4"));
					      }
					      else 
					      {
					    	  doSave (id, uom, uomGroup, factor, normal);
					      }
					  },
					  error: function(data)
					  {
					      DisplayError(replaceCodeByText("DisplayError_Msg_Short_Name_T_3"));
					  }
				});
            	
            }
            
            
        </script>
    </head>
    <body>
    
        <table width="100%" align="center" border="0">
            <tr>
                <td colspan="4">
                        <h2 class="InitiationTitle" align="center" lang_key="Uom_Details"></h2>
                </td>                                
            </tr>
            <tr><td><br/></td></tr>
            <tr>
                <td>
                    <table>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3">
                                <span class="mandatoryFieldMark">*</span>
                                <label lang_key="Uom"></label>:
                            </td>
                            <td>
                                <input type="text" onblur="focusInput(event)" onfocus="focusInput(event)" id="uomName"
                                                    name="uomName" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" >                                
                                <label lang_key="Description"></label>:
                            </td>
                            <td>
                                <input type="text" onblur="focusInput(event)" onfocus="focusInput(event)" id="uomDescription"
                                                    name="uomDescription" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3">
                                <span class="mandatoryFieldMark">*</span>
                                <label lang_key="UOM_Group"></label>:
                            </td>
                            <td>
                                <select id="ddlUomGroup" name="ddlUomGroup" style="width: 150px"> 
                                    <option value="0">Choose</option>
                                </select>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" >   
                            	<span class="mandatoryFieldMark">*</span>                             
                                <label lang_key="Factor"></label>:
                            </td>
                            <td>
                                <input type="text" onblur="focusInput(event)" onfocus="focusInput(event)" id="txtFactor" onkeypress="event.returnValue=validateDecimal(this.value, event.keyCode);"
                                                    name="factor" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:right">
                                <label lang_key="Normal"></label>:
                            </td>
                            <td>
                                <input type="checkbox" checked="checked" id="chbIsNormal" name="chbIsNormal"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:right">
                                <label lang_key="Active"></label>:
                            </td>
                            <td>
                                <input type="checkbox" checked="checked" id="chbActive" name="chbActive"> </input>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" id="lblError" class="cssStaticDataMandatory" style="TEXT-ALIGN:left;">
                    
                </td>
            </tr>
            
            <tr>
                <td align="center">
                    <button id="btnSave" onclick="SaveData();"><span lang_key="Save"></span></button>
                    <button id="btnCancel" onclick="CancelAction()" ><span lang_key="Cancel"></span></button>                                       
                </td>
            </tr>
        </table>
        <form name="main" id="main" method="post" action="mnuomupdateservlet">
            <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>        
            <input type="hidden" name="canUpdate" id="canUpdate" value='<%=request.getAttribute("CAN_UPDATE") %>'>
            <input type="hidden" name="Mode" id="Mode" value='<%=request.getAttribute("MODE") %>' >
            <input type="hidden" name="currId" id="currId" value='<%=request.getAttribute("CURR_ID") %>'>
        </form>
    </body>
</html>