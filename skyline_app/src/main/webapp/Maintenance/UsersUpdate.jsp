<!DOCTYPE HTML>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        <%=session.getAttribute("VERSION")%>
    </title>
    <link href="CSS/comply_theme/Skyline_9.css" rel="stylesheet"  type="text/css" />
    <link href="CSS/comply_theme/jquery-ui.custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="CSS/comply_theme/demo_table_jui.css" rel="stylesheet" type="text/css" />
    <link href="CSS/comply_theme/ColumnFilterWidgets.css" rel="stylesheet" type="text/css" />
    
    <script src="js/comply_js/jquery.js" type="text/javascript"></script>
    <script src="js/comply_js/jquery.dataTables.js" type="text/javascript"></script>  
    <script src="js/comply_js/jquery-ui.custom.js" type="text/javascript"></script>
    <script src="js/comply_js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
    <script src="js/comply_js/jquery-migrate.js" type="text/javascript"></script>
    <script src="js/comply_js/ColumnFilterWidgets.js" type="text/javascript"></script>
    <script src="js/comply_js/skyline.dataTables.js" type="text/javascript"></script> 
    <script src="js/comply_js/CommonFuncs.js" type="text/javascript"></script>
    <script src="js/comply_js/Lang_Selection.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        
        
        var wfTypesSelected = {selected:[],availible:[]};
        
        var wfTypeschanges = {add:[],deleted:[]};
        
        
        function AddNewState(selectedVal)
        {
            for(var i = 0;i< wfTypeschanges.deleted.length;i++)
            {
                if(wfTypeschanges.deleted[i] == selectedVal)
                {
                    wfTypeschanges.deleted.splice(i,1);
                }
            }
            
            wfTypeschanges.add.push(selectedVal);
        }
        
        function RemoveNewState(selectedVal)
        {
            for(var i = 0;i< wfTypeschanges.add.length;i++)
            {
                if(wfTypeschanges.add[i] == selectedVal)
                {
                    wfTypeschanges.add.splice(i,1);
                }
            }
            
            wfTypeschanges.deleted.push(selectedVal);
        }
        
        $(function () 
        {
            $('#tabs').tabs();
        });
        
        $(document).ready(function () 
        {
            $('button').button();
            LoadLabelsJson(); 
            $('#lblError').val("");
            SetComboControls();
            
            $('#btnAddState').click(function () 
            {
                $("#ddlAllStates option:selected").each(function(){                    
                    AddNewState($(this).attr("value"));
                });
                
                $('#ddlAllStates option:selected').remove().appendTo('#ddlAllSelectedStates');
                
                RefreshSelections();
            });

            $('#btnRemoveState').click(function () 
            {
                $("#ddlAllSelectedStates option:selected").each(function(){                    
                    RemoveNewState($(this).attr("value"));
                });
                
                $('#ddlAllSelectedStates option:selected').remove().appendTo('#ddlAllStates').removeAttr('selected');
                RefreshSelections();
            });
            
            $( "#UserPass" ).keypress(function(event) 
            {
                validateLegalCharSelective(event.keyCode,"\'"+'\"',event); 
            });
            
            $( "#CONFIRM" ).keypress(function(event) 
            {
                validateLegalCharSelective(event.keyCode,"\'"+'\"',event); 
            });
            
            LoadUserData();
            
            
        });
        
        
        
        function RemoveSelected()
        {
            
        }
        function LoadUserData()
        {
            var userID = $('#currUserId').val();
            
            if(userID == "0")
            {
                return;
            }
            
            $.ajax
            ({
                type:"POST",
                contentType: "application/json; charset=utf-8",
                url: "mnusersupdateservlet?actionid=getUserData&SelectedID="+userID,
                dataType: "json",
                success: function(data) 
                {
                    $('#USER_NAME').val(data.USER_NAME);
                    $('#UserPass').val(data.PASSWORD);
                    $('#CONFIRM').val(data.PASSWORD);
                    $('#ddlSite').val(data.SITE_ID);
                    $('#ddlRole').val(data.ROLE);
                    $('#FirstName').val(data.FIRST_NAME);
                    $('#LastName').val(data.LAST_NAME);
                    $('#initials').val(data.INITALS);
                    $('#title').val(data.TITLE);
                    $('#email').val(data.EMAIL);
                    $('#ddlDept').val(data.DEPT_ID);
                    $('#ldap').val(data.LDAP_NAME);
                    $('#ddlLab').val(data.LAB_ID);
                    $('#ddlLang').val(data.LANG_CODE);
                    
                    if(data.MAINTENANCE == "1")
                    {
                        $('#chkMaintenance').prop('checked', true);
                    }
                    else
                    {
                        $('#chkMaintenance').prop('checked', false);
                    }
                    
                    if(data.LOCKED == "1")
                    {
                        $('#chkLocked').attr('checked', true);
                    }
                    else
                    {
                        $('#chkLocked').attr('checked', false);
                    }
                    
                    if(data.SAMPLE_APP == "1")
                    {
                        $('#chkSampleApp').prop('checked', true);
                    }
                    else
                    {
                        $('#chkSampleApp').prop('checked', false);
                    }
                    
                    if(data.TEST_APP == "1")
                    {
                        $('#chkTestApp').prop('checked', true);
                    }
                    else
                    {
                        $('#chkTestApp').prop('checked', false);
                    }
                    
                    if(data.DEVIATION == "1")
                    {
                        $('#chkDeviations').prop('checked', true);
                    }
                    else
                    {
                        $('#chkDeviations').prop('checked', false);
                    }
                    
                    if(data.ACTIVITY_APP == "1")
                    {
                        $('#chkActivityApp').prop('checked', true);
                    }
                    else
                    {
                        $('#chkActivityApp').prop('checked', false);
                    }
                    
                    if(data.INVENTORY_LIST == "1")
                    {
                        $('#chkInventoryList').prop('checked', true);
                    }
                    else
                    {
                        $('#chkInventoryList').prop('checked', false);
                    }
                    
                    if(data.COA == "1")
                    {
                        $('#chkCOA').prop('checked', true);
                    }
                    else
                    {
                        $('#chkCOA').prop('checked', false);
                    }
                    
                    if(data.INVENTORY_USAGE == "1")
                    {
                        $('#chkInventoryUsage').prop('checked', true);
                    }
                    else
                    {
                        $('#chkInventoryUsage').prop('checked', false);
                    }
                    $('#chkInventoryType').prop('checked', (data.INVENTORY_TYPE == "1")?true:false);
                }
            });
        }
        function SetComboControls()
        {
            var siteData =  $.parseJSON($('#json_site').val());
            $(siteData).map(function ()   
            {
                    $("#ddlSite").append($('<option>').val(this.id).text(this.text));
            });
            
            var roleData =  $.parseJSON($('#json_role').val());
            $(roleData).map(function ()   
            {
                    $("#ddlRole").append($('<option>').val(this.id).text(this.text));
            });
            
            var deptData =  $.parseJSON($('#json_dept').val());
            $(deptData).map(function ()   
            {
                    $("#ddlDept").append($('<option>').val(this.id).text(this.text));
            });
            
            var labData =  $.parseJSON($('#json_lab').val());
            $(labData).map(function ()   
            {
                    $("#ddlLab").append($('<option>').val(this.id).text(this.text));
            });
            
            var langData =  $.parseJSON($('#json_lang').val());
            $(langData).map(function ()   
            {
                    $("#ddlLang").append($('<option>').val(this.id).text(this.text));
            });
            
            
            var wfData =  $.parseJSON($('#json_wfType').val());
            $(wfData).map(function ()   
            {
                    $("#ddlWorkflow").append($('<option>').val(this.id).text(this.text));
            });
        }
        
        function CancelAction()
        {
             $("#mask", window.parent.document).hide();
            
            $('#UpdateFormDiv', window.parent.document).hide();
                                    
        }
        
        function SaveData()
        {
            $('#lblError').html("");
            
            if(!ValidateFields())
            {
                return;
            }
            
            var userName = $('#USER_NAME').val();
            var pass = $('#UserPass').val();
            var userID = $('#currUserId').val();
            
            var siteID = $('#ddlSite').val();
            var roleID = $('#ddlRole').val();
            var firstName = $('#FirstName').val();
            var lastName = $('#LastName').val();
            var initials = $('#initials').val();
            var title = $('#title').val();
            var email = $('#email').val();
            var deptID = $('#ddlDept').val();
            var labID = $('#ddlLab').val();
            var langCode = $('#ddlLang').val();
            var ldapName = $('#ldap').val();
            var maint = $('#chkMaintenance').is(':checked'); 
            var locked = $('#chkLocked').is(':checked'); 
            var sampleApp = $('#chkSampleApp').is(':checked'); 
            var testApp = $('#chkTestApp').is(':checked'); 
            var deviations = $('#chkDeviations').is(':checked');
            var activityApp = $('#chkActivityApp').is(':checked');            
            var inventoryList = $('#chkInventoryList').is(':checked');
            var inventoryType = $('#chkInventoryType').is(':checked');
            var coa = $('#chkCOA').is(':checked');
            var inventoryUsage = $('#chkInventoryUsage').is(':checked');
            var newPass = $('#chkNewPass').is(':checked');
            
            var saveParams = "actionid=SaveData&SelectedID="+userID+"&UserName="+encodeURIComponent(userName)+"&Pass="+encodeURIComponent(pass)+"&SiteID="+siteID+"&RoleID="+roleID+"&FirstName="+encodeURIComponent(firstName)+
            "&LastName="+encodeURIComponent(lastName)+"&Initials="+encodeURIComponent(initials)+"&Title="+encodeURIComponent(title)+"&Email="+encodeURIComponent(email)+"&DeptID="+deptID+"&LabID="+labID+"&LangCode="+langCode+
            "&LdapName="+encodeURIComponent(ldapName)+"&Maint="+maint+"&Locked="+locked+"&SampleApp="+sampleApp+"&TestApp="+testApp+
            "&Deviations="+deviations+"&ActivityApp="+activityApp+"&InventoryList="+inventoryList+"&inventoryType="+inventoryType+"&Coa="+coa+"&InventoryUsage="+inventoryUsage+"&NewPass="+newPass+
            "&SelectedStates="+wfTypeschanges.add+"&RemoveStates="+wfTypeschanges.deleted;
                        
            
            $.ajax
            ({
                type:"POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "mnusersupdateservlet",
                data: saveParams,
                dataType: "json",
                success: function(data) 
                {
                    if(data == "-1")
                    {
                        DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                    }
                    else if(data == "-2")
                    {
                        DisplayError(replace("DisplayError_Msg_Short_Name_T_13"));
                    }
                    else
                    {
                        $("#mask", window.parent.document).hide();
            
                        $('#UpdateFormDiv', window.parent.document).hide();
                        window.parent.RefreshTable();
                    }
                                        
                },
                error: function(data)
                {
                    DisplayError(replace("DisplayError_Msg_Short_Name_T_3"));
                }
            });
        }

        function ValidateFields()
        {            
            if(!fnValidateString({inStr:$('#UserPass').val(), fieldName:"Password", focusOn:"UserPass", illegals:"\'"+'\"'})
              || !fnValidateString({inStr:$('#CONFIRM').val(), fieldName:"Confirm", focusOn:"CONFIRM", illegals:"\'"+'\"'})) /* illegals: ' and " signs  */
            {                
                return false;
            }
            
            var ctl = $('#USER_NAME').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#USER_NAME').focus();
                return false;
            }
            
            ctl = $('#UserPass').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#UserPass').focus();
                return false;
            }
            
            ctl = $('#CONFIRM').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#CONFIRM').focus();
                return false;
            }
            
            ctl = $('#ddlSite').val();
            if(fnTrimString(ctl) == "" || fnTrimString(ctl) == "0")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#ddlSite').focus();
                return false;
            }
            
            ctl = $('#ddlRole').val();
            if(fnTrimString(ctl) == "" || fnTrimString(ctl) == "0")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#ddlRole').focus();
                return false;
            }
            
            ctl = $('#FirstName').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#FirstName').focus();
                return false;
            }
            
            ctl = $('#LastName').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#LastName').focus();
                return false;
            }
            
            ctl = $('#initials').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#initials').focus();
                return false;
            }
            
            ctl = $('#title').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#title').focus();
                return false;
            }
            
            ctl = $('#email').val();
            if( !validateEmailAddress(ctl) )
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_14"));
                $('#email').focus();
                return false;
            }
            
            ctl = $('#ddlDept').val();
            if(fnTrimString(ctl) == "" || fnTrimString(ctl) == "0")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#ddlDept').focus();
                return false;
            }
            
            ctl = $('#ddlLab').val();
            if(fnTrimString(ctl) == "" || fnTrimString(ctl) == "0")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#ddlLab').focus();
                return false;
            }
			
			ctl = $('#ddlLang').val();
            if(fnTrimString(ctl) == "" || fnTrimString(ctl) == "0")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#ddlLang').focus();
                return false;
            }
            
            //compare passwords
            ctl = $('#UserPass').val();
            var ctl1 = $('#CONFIRM').val();
            
            if(fnTrimString(ctl) != fnTrimString(ctl1))
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_15"));
                $('#CONFIRM').focus();
                return false;
            }
            
            return true;
        }
        
        function HasWFTypeExists(wfTypeID)
        {
            
            for(var i = 0;i< wfTypesSelected.availible.length;i++)
            {
                if(wfTypesSelected.availible[i].Type == wfTypeID)
                {
                    return true;
                }
            }
            for(var i = 0;i< wfTypesSelected.selected.length;i++)
            {
                if(wfTypesSelected.selected[i].Type == wfTypeID)
                {
                    return true;
                }
                
                return false;
            }
        }
        
        function RefreshSelections()
        {
            var wfTypeID = $('#ddlWorkflow option:selected').val();

            for(var i = wfTypesSelected.availible.length-1;i>=0 ;i--)
            {
                 if(wfTypesSelected.availible[i].Type == wfTypeID)
                 {
                    //indx.push(i);
                    wfTypesSelected.availible.splice(i,1);
                 }
            }
            

            for(var i = wfTypesSelected.selected.length -1;i>= 0;i--)
            {
                if(wfTypesSelected.selected[i].Type == wfTypeID)
                {                    
                    wfTypesSelected.selected.splice(i,1);
                }
            }
            
            
            $("#ddlAllStates > option").each(function() 
            {                
                wfTypesSelected.availible.push({"Type":wfTypeID,"Value":this.value,"Text":this.text});                
            });
            
            $("#ddlAllSelectedStates > option").each(function() 
            {
                wfTypesSelected.selected.push({"Type":wfTypeID,"Value":this.value,"Text":this.text});                
            });
        }
        
        function WorkflowTypeChange()
        {
            var curUserID = $('#currUserId').val();
            var selectedWfType = $('#ddlWorkflow option:selected').val();
            
            $('#ddlAllStates').empty();
            $('#ddlAllSelectedStates').empty();
                
            if(selectedWfType == "0")
            {
                return;
            }
            
            if(HasWFTypeExists(selectedWfType))
            {
                for(var i = 0;i< wfTypesSelected.availible.length;i++)
                {
                    if(wfTypesSelected.availible[i].Type == selectedWfType)
                    {
                        $("#ddlAllStates").append($('<option>').val(wfTypesSelected.availible[i].Value).text(wfTypesSelected.availible[i].Text));
                    }
                }
                
                for(var i = 0;i< wfTypesSelected.selected.length;i++)
                {
                    if(wfTypesSelected.selected[i].Type == selectedWfType)
                    {
                        $("#ddlAllSelectedStates").append($('<option>').val(wfTypesSelected.selected[i].Value).text(wfTypesSelected.selected[i].Text));
                    }
                }
                return;
            }
            
            $.ajax
            ({
                type:"POST",
                contentType: "application/json; charset=utf-8",
                url: "mnusersupdateservlet?actionid=getWFStates&wfType=" + selectedWfType + "&SelectedID="+curUserID,
                dataType: "json",
                success: function(data) 
                {
                    
                    if(data != null)
                    {
                        if(data.AllStates != null)
                        {
                            $($.parseJSON(data.AllStates)).map(function ()   
                            {
                                    $("#ddlAllStates").append($('<option>').val(this.id).text(this.text));
                                    wfTypesSelected.availible.push({"Type":selectedWfType,"Value":this.id,"Text":this.text});
                            }) 
                        }
                        
                        if(data.SelectedStates != null)
                        {
                            $($.parseJSON(data.SelectedStates)).map(function ()   
                            {
                                    $("#ddlAllSelectedStates").append($('<option>').val(this.id).text(this.text));
                                    wfTypesSelected.selected.push({"Type":selectedWfType,"Value":this.id,"Text":this.text});
                            }) 
                        }
                    }                    
                },
                error: function(data)
                {
                    
                }
            });
            
            
        }
        
        
        function DisplayError(errMsg)
        {
            $( "#tabs" ).tabs( "option", "active", 0 );
            var list = $('#lblError').append('<ul ></>').find('ul');
            list.append('<li class="errorMessage"">'+ errMsg + '</>');
        }
    </script>
  </head>
  <body>
    <table width="100%" align="center" border="0">
        <tr>
                <td colspan="4">
                        <h2 class="InitiationTitle"  id="setTitle" align="center" lang_key="User_Details"></h2>
                </td>                                
        </tr>
        <tr><td><br/></td></tr>
        <tr>
            <td>
                <div id="tabs"  style="height: 450px; width:100%;">
                    <ul style="text-transform: uppercase;">
                        <li><a href="#tabGen" lang_key="Details"></a></li>
                        <li><a href="#tabProp" lang_key="Properties"></a></li>
                        <!-- <li><a href="#tabStates" lang_key="States"></a></li>-->
                    </ul>
                    <div id="tabGen">
                        <table>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="User_Name">User Name</label>:
                                </td>
                                <td>
                                    <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="USER_NAME"
                                                    name="USER_NAME" maxlength="20" style="width: 200px" onkeypress="event.returnValue = validateLegalChar(event.keyCode);"></input>                                                                        
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Password">Password</label>:
                                </td>
                                <td>
                                    <input type="password" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" 
                                        id="UserPass" name="UserPass" maxlength="20" style="width: 200px"></input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Confirm">Confirm</label>:
                                </td>
                                <td>
                                    <input type="password" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" 
                                        id="CONFIRM" name="CONFIRM" maxlength="20" style="width: 200px"></input>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Site">Site</label>:
                                </td>
                                <td>
                                    <select id="ddlSite" style="width:208px;" name="ddlSite"> 
					<option value="0">Choose</option>
                                    </select>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Role">Role</label>:
                                </td>
                                <td>
                                    <select id="ddlRole" style="width:208px;" name="ddlRole"> 
					<option value="0">Choose</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="First_Name">First Name</label>:
                                </td>
                                <td>
                                    <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="FirstName"
                                                    name="FirstName" maxlength="20" style="width: 200px"> </input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Last_Name">Last Name</label>:
                                </td>
                                <td>
                                    <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="LastName"
                                                    name="LastName" maxlength="20" style="width: 200px"> </input>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Initials">Initials</label>:
                                </td>
                                <td>
                                    <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="initials"
                                                    name="initials" maxlength="20" style="width: 200px"> </input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Title">Title</label>:
                                </td>
                                <td>
                                    <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="title"
                                                    name="title" maxlength="20" style="width: 200px"> </input>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Email">Email</label>:
                                </td>
                                <td>
                                    <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="email"
                                                    name="email" maxlength="50" style="width: 200px"> </input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Department">Department</label>:
                                </td>
                                <td>
                                    <select id="ddlDept" style="width:208px;" name="ddlDept"> 
					<option value="0">Choose</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                   <label lang_key="Ldap_Name">LDAP Name</label>:
                                </td>
                                <td>
                                    <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="ldap"
                                                    name="ldap" maxlength="50" style="width: 200px"> </input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Lab">Lab</label>:
                                </td>
                                <td>
                                    <select id="ddlLab" style="width:208px;" name="ddlLab"> 
					<option value="0">Choose</option>
                                    </select>
                                </td>
                            </tr>
                            <tr class="TableRow"> 
                                <td class="cssStaticData" nowrap>
                                    <span class="mandatoryFieldMark">*</span>
                                    <label lang_key="Language">Language</label>:
                                </td>
                                <td>
                                    <select id="ddlLang" style="width:208px;" name="ddlLang"> 
					<option value="0">Choose</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" id="lblError" class="cssStaticDataMandatory" style="TEXT-ALIGN:left;">
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="tabProp">
                        <table border="0" width="90%">
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                   <label lang_key="Locked">Locked</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkLocked" name="chkLocked" ></input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Maintenance_Tables">Maintenance tables</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkMaintenance" name="chkMaintenance" ></input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Inventory_List"></label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkInventoryList" name="chkInventoryList" ></input>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Required_New_Password">Required new Password</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkNewPass" name="chkNewPass" ></input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Sample_Approval">Sample Approval</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkSampleApp" name="chkSampleApp" ></input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Inventory_Usage">Inventory Usage</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkInventoryUsage" name="chkInventoryUsage" ></input>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Test_Approval">Test Approval</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkTestApp" name="chkTestApp" ></input>
                                </td>
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Deviations">Deviations</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkDeviations" name="chkDeviations" ></input>
                                </td>
                                 <td class="cssStaticData" nowrap>
                                    <label lang_key="Inventory_Type"></label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkInventoryType" name="chkInventoryType" ></input>
                                </td>
                            </tr>
                             <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Activity_Approver">Activity Approver</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkActivityApp" name="chkActivityApp" ></input>
                                </td>
                            </tr>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap>
                                    <label lang_key="Coa">COA</label>:
                                </td>
                                <td>
                                    <input type="checkbox" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)"
                                                    id="chkCOA" name="chkCOA" ></input>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="tabStates" align="center" style="vertical-align:middle; display:none">
                         <table>
                            <tr class="TableRow">
                                <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:center">
                                    <label lang_key="Workflow">Workflow</label>:
                                    &nbsp;&nbsp;
                                    <select id="ddlWorkflow" style="width:208px;" name="ddlWorkflow" onchange="WorkflowTypeChange()"> 
					<option value="0">Choose</option>
                                    </select>
                                </td>                                
                            </tr>
                            <tr>
                                <td></br></td>
                            </tr>
                            <tr>
                                <td class="cssStaticData" style="text-align:center">
                                    <label lang_key="Availible_States">Availible States</label>
                                </td>
                                <td style="width: 50px">
                                <td class="cssStaticData" style="text-align:center">
                                    <label lang_key="Selecte_States">Selected States</label>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                            </tr>
                            <tr>
                                <td>
                                    <select multiple id="ddlAllStates" style="width: 250px; height: 300px"></select>
                                </td>
                                <td style="width: 50px">
                                    <table>
                                        <tr>
                                            <td>
                                                <button type="button" style="width: 40px;text-align:center" id="btnAddState"> > </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button type="button" style="width: 40px;text-align:center" id="btnRemoveState"> < </button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <select multiple id="ddlAllSelectedStates" style="width: 250px; height: 300px"></select>
                                </td>
                            </tr>
                         </table>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <button id="btnSave" onclick="SaveData();"><span lang_key="Save"></span></button>
                <button id="btnCancel" onclick="CancelAction()" ><span lang_key="Cancel"></span></button>                                       
            </td>
        </tr>
    </table>
    <form name="main" id="main" method="post" action="mnusersupdateservlet">
        <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>        
        <input type="hidden" name="json_site" id="json_site" value='<%=request.getAttribute("SITE_COMBO") %>'>
        <input type="hidden" name="json_role" id="json_role" value='<%=request.getAttribute("ROLE_COMBO") %>'>
        <input type="hidden" name="json_dept" id="json_dept" value='<%=request.getAttribute("DEPT_COMBO") %>'>
        <input type="hidden" name="json_lab" id="json_lab" value='<%=request.getAttribute("LAB_COMBO") %>'>
		<input type="hidden" name="json_lang" id="json_lang" value='<%=request.getAttribute("LANG_COMBO") %>'>
        <input type="hidden" name="json_wfType" id="json_wfType" value='<%=request.getAttribute("WF_COMBO") %>'>        
        <input type="hidden" name="canUpdate" id="canUpdate" value='<%=request.getAttribute("CAN_UPDATE") %>'>
        <input type="hidden" name="Mode" id="Mode" value='<%=request.getAttribute("MODE") %>' >
        <input type="hidden" name="currUserId" id="currUserId" value='<%=request.getAttribute("CURR_USER_ID") %>'>
    </form>
  </body>
</html>