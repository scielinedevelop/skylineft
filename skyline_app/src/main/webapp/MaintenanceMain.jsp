<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8"  import="comply.SkyLine.bl.biz.User" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><%=session.getAttribute("VERSION")%></title>
    
    <%@ include file="include/includeGeneralCSS.jsp"%> 
	<%@ include file="include/includeGeneralJS.jsp"%>
    
    <script src="js/comply_js/loading.js" type="text/javascript"></script>
    <script src="js/comply_js/splitter.js" type="text/javascript"></script>
    <script src="js/comply_js/jquery.cookie.js" type="text/javascript"></script>
    
    <%@ include file="include/includeFoundationJS.jsp"%>	
    
    <style type="text/css">
        
        #mask
        {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: #000;
                display: none;
                z-index: 101; /* ui-front (which includes splitter-bar) has z-index of 110 */ 
        }
        
        #UpdateFormDiv
         {
            /* top: 8%;
            left: 35%; */
            width: 400px;               
            height: 600px;
            z-index: 104;
         }
		 
         #EsignDiv
         {
            top: 25%;
            left: 35%;
            width: 400px;               
            height: 300px;
            z-index: 105;
         }
         
         #divAuditTrail
         {
            top: 25%;
            left: 30%;
            width: 650px;               
            height: 300px;
            z-index: 10001;
         }
         
        div.ui-datepicker
        {
            font-size:12px;
        }
        #spnCommentRequired
	{
		visibility:hidden;
	}
    </style>
    
    <script type="text/javascript">
      
      var tablesDef = [];
        
//        $(function () 
//        {
//            $("#UpdateFormDiv").draggable()
//           .resizable({
//               maxHeight: 700,
//               maxWidth: 1000,
//               minHeight: 530,
//               minWidth: 770
//           });
//        });
        
        $(function () 
        {
            $("#EsignDiv").draggable();                      
        });
        $(function()
        {    
            $( "#UpdateFormDiv" ).draggable({containment: ".containment-wrapper", scroll: false }); 
        });
        $(function()
        {    
            $( "#divAuditTrail" ).draggable({ handle: "h2" , containment: ".containment-wrapper", scroll: false }); 
        });
        
        /*
        $(function()
        {          
            
            $("#UpdateFormDiv").draggable
            ({
                containment: ".containment-wrapper",
                //cursor: "move",
                handle: function () {
                    //window.frames['UpdateForm'].contentDocument.document.getElementById('h2Draggable').style.backgroundColor="#000";
                    //window.frames[0].document.getElementById('h2Draggable');
                    var f = document.getElementById('UpdateForm');  
                    alert(f);
                    var doc = f.contentDocument || f.contentWindow.document;
                    var txt = doc.getElementById('h2Draggable');            
                    alert(txt);
                },
                //iframeFix: true,
                start: function () {
                    $("#UpdateForm").css('z-index', '-1');
                },
                stop: function () {
                    $("#UpdateForm").css('z-index', '10001');
                }
            });
        });*/

        $(document).ready(function ()  
        {
            $('button').button();	
            LoadLabelsJson(); 
            $('#lblError').html("");
            $("#mask").hide();
            $("#accordion").accordion();
            $(".simple").splitter({
                    type: "v",
                    outline: true, 
                    sizeLeft: 300,
                    minLeft: 10,
                    minRight: 10,
                    resizeToWidth: true,
                    dock: "left",
                    dockSpeed: 500,
                    cookie: "docksplitter",
                    sizeOfset: 500,
                    dockKey: 'Z',   // Alt-Shift-Z in FF/IE
                    accessKey: 'I'  // Alt-Shift-I in FF/IE   
             });
            
            $(".ComplyAccordion li").click(function(){
                $("#accordion li").removeClass("ReportItemSelected");
                $(this).addClass("ReportItemSelected");
            });
            
            $('.standardbutton[data-ajax="true"]').click(function(e) {
                e.preventDefault();
                
                var $this = $(this);
                alert($this.data('ajaxMethod').toUpperCase());
                $.ajax({
                    method: $this.data('ajaxMethod').toUpperCase(),
                    cache: false,
                    url: $this.attr('href'),
                    dataType: 'html',
                    success: function(resp) {
                    // this assumes that the data-ajax-mode is always "replace":
                        alert("Sucess");
                        $($this.data('ajaxUpdate')).html(resp);
                    },
                    error:function(resp) {
                        alert("error");
                        $($this.data('ajaxUpdate')).html(resp);
                    }
                });
            });
            
            $(function() 
            {                       
                var beforeShow = {'beforeShow':onATDatesChange};
                initDatePickerWithOptions("txtFromDate", beforeShow);
                initDatePickerWithOptions("txtToDate", beforeShow);
                $("#txtFromDate").val('00/00/0000');
                $("#txtToDate").val('00/00/0000');
            });
            
            if($('#defaultTabID').val() != "")
            {
            	$('a[id="'+$('#defaultTabID').val()+'"]').click();
            }
            
        });
        
        function handleAjaxError( xhr, textStatus, error ) 
		{      
	            if (xhr.responseText.match(/TIME_IS_OUT/)) 
		    {   
			top.location.href = 'Login.jsp';
		    } 
		}  
        
        function onATDatesChange (obj)
		{
			var min = null, max = null; 		
			
			if (obj.id === 'txtFromDate')
			{	// set max date if relevant						
				max = $('#txtToDate').datepicker('getDate');
	                        if($('#txtToDate').val() == '00/00/0000')
	                        {
	                            max = "";
	                        }
			}
			else // txtToDate
			{	// set min date if relevant
				min = $('#txtFromDate').datepicker('getDate');	
	                        if($('#txtFromDate').val() == '00/00/0000')
	                        {
	                            min = "";
	                        }
			}
			//alert(max + " : " + min);
			return {minDate: min, maxDate: max};
		}
        
        function ShowSelectedTab(tableURL, stabId)
		{                        
	            var tabID = $(stabId).attr('id');
	            
	            GetTabInfo(tabID);
	            $('#actionid').val("");
	            $('#TabID').val(tabID);
	            $('#UpdateForm').prop("src","dummy.html");
	            //document.getElementById('UpdateForm').src = "dummy.html";
	            
	            var servletName = $('#ServletName').val();            
	            document.getElementById('main').action = servletName;
	            document.getElementById('main').target = "maintenanceData";
	            document.getElementById('main').submit();
		}
        
        
        function GetTabInfo(tabID)
        {
            $('#tableATxml').val("");
            $('#tableViewName').val("");
            $('#maintenanceType').val("");
            var tablesDef = $('#tablesStrArray').val();
            //alert(tablesDef);
            var arrObj = $.parseJSON(tablesDef);
            
            $(arrObj).map(function () 
            {                
                if(tabID == this['Type'])
                {                    
                    
                    $('#ServletName').val(this['ServletName']);
                    $('#PopupName').val(this['PopupServlet']);
                    $('#TableName').val(this['TableName']);
                    $('#esign').val(this['esign']);
                    $('#UpdateFormDiv').css('width',this['width']);
                    $('#UpdateFormDiv').css('height',this['height']);
                    if(tabID == 'Material' || tabID == 'Vendor' || tabID == 'Method' || tabID == 'ResultCategory' || tabID == 'FormulaTemplateBuilder') //kd 25032020 workaround for fixed bug-7630
                    {
            			var w = (window.innerWidth - this['width'].match(/\d+/g))/2;
                    	$('#UpdateFormDiv').css('top','1%');
                    	$('#UpdateFormDiv').css('left', w+'px');
                    } 
                    else
                    {
                    	$('#UpdateFormDiv').css('top','8%');
                    	$('#UpdateFormDiv').css('left', '35%');
                    }
                    if(tabID == 'UOM') //kd 09062021 workaround for enlage UOM Details window
                    {
            			$('#UpdateFormDiv').css('height','400px');
                    } 
                    $('#tableDisplayName').val($('#'+tabID).text());    
                    
                    $('#currTabPermissions').val(this['permissions']);
                    
                    if(this['TableView'] != null)
                    {
                        $('#tableViewName').val(this['TableView']);
                    }
                    else if(this['auditTrailParams'] != null)
                    {
                       var obj = this['auditTrailParams'];
                       $('#reportType').val(obj.rType);
                       $('#tableATxml').val(obj.xml);
                       if(obj.view != null)
                       {
                          $('#tableViewName').val(obj.view);
                       }
                       if(obj.filter != null)
                       {
                          $('#maintenanceType').val(obj.filter);
                       }
                    }
                }
            });        
        }
        
         function RefreshTable()
        {            
            $('#maintenanceData')[0].contentWindow["RefreshTable"]();
        }
        
        function Sign()
        {
            $('#lblError').html("");
            
            var ctl = $('#userName').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#userName').focus();
                return;
            }
            
            ctl = $('#Password').val();
            if(fnTrimString(ctl) == "")
            {
                DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                $('#Password').focus();
                return;
            }
            
            var isComments = $('#MandatoryComments').val();
            if(isComments == "1")
            {
                ctl = $('#txtComments').val();
                if(fnTrimString(ctl) == "")
                {
                    DisplayError(replace("DisplayError_Msg_Short_Name_T_2"));
                    $('#txtComments').focus();
                    return;
                }
            }
            var userName = $('#userName').val();
            var password = $('#Password').val();
            var servletName = $('#ServletName').val();
            
            var urlAction = servletName + "?actionid=AutenticateUser&UserName="+userName+"&Pass="+password;
            
            $.ajax
            ({
                type:"POST",
                contentType: "application/json; charset=utf-8",
                url: urlAction,
                dataType: "json",
                success: function(data) 
                {
                    if(data != null || data != "")
                    {                        
                        if(data == "1")
                        {
                            var comments = $('#txtComments').val();
                            $('#maintenanceData')[0].contentWindow["DoAction"](comments);
                        }
                        else if(data == "-2")
                        {
                            DisplayError(replace("DisplayError_Msg_Short_Name_T_16"));
                        }
                        else
                        {
                            DisplayError(replace("DisplayError_Msg_Short_Name_T_17"));
                        }
                    }
                }
            });
            
            
        }
        
        function ClearFields()
        {
            $('#userName').val("");
            $('#Password').val("");
            $('#txtComments').val("");
        }
        
        function SetCommentsMandatory(isVisible)
        {
            if(isVisible)
            {
                $('#spnCommentRequired').css('visibility', 'visible');
                $('#MandatoryComments').val("1");
            }
            else
            {
                $('#spnCommentRequired').css('visibility', 'hidden');
                $('#MandatoryComments').val("0");
            }
        }
        
        function CancelAction()
        {
             $("#mask").hide();
                
            $('#EsignDiv').hide();
        }
        function DisplayError(errMsg)
        {
            $( "#tabs" ).tabs( "option", "active", 0 );
            var list = $('#lblError').append('<ul ></>').find('ul');
            list.append('<li style= "list-style:square;">'+ errMsg + '</>');
        }
        
        function openHiddenForm()
        {             
            $('#ddlUsers :not(:first)').remove().val(0);
            $("#txtFromDate").val('00/00/0000');
            $("#txtToDate").val('00/00/0000');
            
            $.ajax
            ({
                type:"POST",
                contentType: "application/json; charset=utf-8",
                url: "maintenancemainservlet?actionid=getUsersList&TabID=" + $('#TabID').val(),
                dataType: "json",
                success: function(data) 
                {
                    $(data).map(function()
                    {
                        $('#ddlUsers').append($('<option user_name="'+this.attributes[0]+'" user_full_name="'+this.text+'">').val(this.id).text(this.text+" ("+ this.attributes[0] + ")"));
                    });        
                    $("#mask").fadeTo(500, 0.25);
                    $("#divAuditTrail").show();
                },
                error: handleAjaxError
            });
                       
        }
        
        function showATReport()
        {                        
            $("#from_Date").val($("#txtFromDate").val());
            var toDate = $("#txtToDate").val();
            $('#toDateForDisplay').val(toDate);
            if(toDate != "00/00/0000" && toDate != '')
             {
                var date = $('#txtToDate').datepicker('getDate');
                var nextDayDate = new Date();
                nextDayDate.setDate(date.getDate() + 1);
                toDate = nextDayDate.getDate() + "/" + (nextDayDate.getMonth() + 1) + "/" + nextDayDate.getFullYear(); /* month + 1 because in datepicker month start with 0 */
             }
            $("#to_Date").val(toDate);
           
            if($("#ddlUsers option:selected").val() > 0)
            {
               $("#selUserFullName").val($("#ddlUsers option:selected").attr('user_full_name'));
               $('#selUserID').val($("#ddlUsers option:selected").val());
            }
            else
            {
                $("#selUserFullName").val("");
                $('#selUserID').val("0");
            }
            if($('#tableATxml').val() != "")
            {
              $('#actionid').val('showReportByXml');
            }
            else
            {
              $('#actionid').val('showReportByView');
            }
            
            
            main.action = "maintenancemainservlet";
            main.submit();
        }
        
        function closeHiddenForm()
        {
            $("#divAuditTrail").hide();
            $("#mask").hide();            
        }
        
    </script>
    
  </head>
  <body>
   <%
       String pageTitle = "";
       String pageHeader = "<label lang_key='Maintenance' ></label>";            
       String navigation = "";
       request.setAttribute("NAVIGATION",   navigation);
   %>
<div >
	<%@ include file="include/includeMainHeader.jsp" %>	
</div>
<div id="mask" class="containment-wrapper"></div>    
<table width="99%" align="center" border="0" cellspacing="0" cellpadding="0" >
 <tr>
    <td>        
        <TABLE WIDTH="100%" align="center" BORDER="0" CELLSPACING="1" CELLPADDING="1" >
            <tr>
                <td>
                    <div style="height:70px;">
                       <%@ include file="include/includePageHeader.jsp"%>
                    </div>
                </td>
            </tr>
            <TR>
                <td align="center" class="ui-widget-content ui-corner-all" >
                    <div id="simple" class="simple">
                        <div id="accordion">
                            <h3 lang_key="Admin"></h3>
                            <div class="ComplyAccordion" style="overflow:hidden;"><!--  kd 17052020 fixed bug-8103 added overflow:hidden -->
                                <ul>
                                    <li >
                                        <a class="tabref" id="Users" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)" lang_key="Users"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="RG" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Responsibility_Groups"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Site" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Sites"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Dept" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"   lang_key="Departments"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Login" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Login_Parameters"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Notif" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Notifications"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="NG" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Notifications_Groups"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="RunRename" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Run_Rename"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="RunDeletion" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Exclude_Runs"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="UpdateStartDate" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Update_Run_Start_Date"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="RunConnectors" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Run_Connectors"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="RunData" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Run_Data"></a>
                                    </li>
                                </ul>
                            </div>
                            <h3  lang_key="General"></h3>
                            <div class="ComplyAccordion">
                                <ul>
                                    <li>
                                        <a class="tabref" id="UOM" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Uom"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="UOMGroup" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="UOM_Group"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="QCLab" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Qc_Labs"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Country" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Destinations"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Reason" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Reasons"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="TermReason" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Termination_Reasons"></a> 
                                    </li>
                                    <li>
                                        <a class="tabref" id="EquipType" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Equipment_Types"></a> 
                                    </li>
                                    <li>
                                        <a class="tabref" id="CommentsCateg" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Comments_Categories"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="AnalysisStatus" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Analysis_Statuses"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Location" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Locations"></a>
                                    </li>                               
                                </ul>
                            </div>
                            <h3  lang_key="Process"></h3>
                            <div class="ComplyAccordion">
                                <ul>
                                    <li>
                                        <a class="tabref" id="SubstanceP" href="#settingContent"  onclick="ShowSelectedTab('maintenancesubstanceservlet', this)"  lang_key="Substances"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="ProductType" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Process_Names"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="SubstanceEM" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="EM_Types"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="ProductTypeSt" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Stability_Programs"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="ProductTypeEM" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Facilities"></a> 
                                    </li>
                                    <li>
                                        <a class="tabref" id="OperationModeP" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Operation_Modes"></a> 
                                    </li>
                                     <li>
                                        <a class="tabref" id="OperationModeEM" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Classes"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="SpecsCatalogNo" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)">Specs & Catalog No.</a>
                                    </li>
                                    <li> 
                                        <a class="tabref" id="StrgCondition" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Storage_Conditions"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="TimePoint" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Time_Points"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="SPointType" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Sample_Point_Types"></a>
                                    </li>
                                    <!-- <li>
                                        <a class="tabref" id="Activities" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Activities"></a>
                                    </li> -->
                                </ul>
                            </div>
                            <h3  lang_key="Parameters"></h3>
                            <div class="ComplyAccordion">
                                <ul>
                                    <li>
                                        <a class="tabref" id="Parameter" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Parameters"></a>
                                    </li>
                                </ul>
                            </div>
                            <h3  lang_key="Tests"></h3>
                            <div class="ComplyAccordion">
                               <ul>
                                    <li>
                                        <a class="tabref" id="TestGeneral" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Tests"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="ResultCategory" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Tests_Selection_List"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="TestCategory" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Tests_Categories"></a>
                                    </li>  
                                    <li>
                                        <a class="tabref" id="TestStatus" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Tests_Statuses"></a>
                                    </li>
                                    <li> 
                                        <a class="tabref" id="FormulaTemplateBuilder" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Formula_Template_Builder"></a>
                                    </li>
                                </ul>
                            </div>
                            <h3  lang_key="Sample_Management"></h3>
                            <div class="ComplyAccordion">
                                <ul>
                                    <li>
                                        <a class="tabref" id="SampleType" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Sample_Types"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="SampleStatus" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Sample_Statuses"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="ContainerType" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Container_Types"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="BatchNumber" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Batch_Number"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="StorageLocation" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Sample_Locations"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Analysis" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Analysis"></a>
                                    </li>                                  
                                </ul>
                            </div>
                            <h3  lang_key="Inventory"></h3>
                            <div class="ComplyAccordion">
                                <ul>
                                    <li>
                                        <a class="tabref" id="Manufacturer" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Manufacturer"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Vendor" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Vendor"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Material" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Materials"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="InventoryStatus" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Inventory_Status"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="StorageLocation" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Sample_Locations"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Method" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Method"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="Frequency" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="Frequency"></a>
                                    </li>
                                </ul>
                            </div>
                             <h3  lang_key="Interfaces"></h3>
                            <div class="ComplyAccordion">
                                <ul>                                    
                                    <li>
                                        <a class="tabref" id="LIMSubsProcess" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="LIMS_Substance_Process"></a>
                                    </li>
                                    <li>
                                        <a class="tabref" id="LIMSSPTest" href="#settingContent"  onclick="ShowSelectedTab('maintenanceservlet', this)"  lang_key="LIMS_SP_Tests"></a>
                                    </li>
                                </ul>
                            </div>                
                        </div>
                        <div id="settingContent">
                            <iframe id="maintenanceData" name="maintenanceData" width="100%" height="696" frameborder=0 src="dummy.html"></iframe>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <div id="UpdateFormDiv" class="cssBasicHiddenformDiv">
            <iframe id="UpdateForm" name="UpdateForm" width="100%" height="100%" frameborder=0 src="dummy.html"></iframe>
        </div>
        <div id="EsignDiv" class="cssBasicHiddenformDiv">
            <table width="100%" align="center" border="0">
                <tr>
                    <td colspan="4">
                            <h2 class="InitiationTitle" style="cursor:move;" id="setTitle" align="center" lang_key="Electronic_Signature"></h2>
                    </td>                                
                </tr>
                <tr><td><br/></td></tr>
                <tr>
                    <td>
                    <table>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:center">
                                <span class="mandatoryFieldMark">*</span>
                               <label lang_key="User_Name"></label>:
                            </td>
                            <td>
                                <input type="text" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="userName"
                                                    name="userName" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:center">
                            <span class="mandatoryFieldMark">*</span>
                                <label lang_key="Password"></label>:
                            </td>
                            <td>
                                <input type="password" class="InputStyle" onblur="focusInput(event)" onfocus="focusInput(event)" id="Password"
                                                    name="Password" maxlength="20" style="width: 200px"> </input>
                            </td>                            
                        </tr>
                        <tr class="TableRow">
                            <td class="cssStaticData" nowrap align="center" colspan="3" style="text-align:center">
                                <span class="mandatoryFieldMark" id="spnCommentRequired">*</span>
                                <label lang_key="Comments"></label>:
                            </td>
                            <td>
                                <TEXTAREA rows="3" cols="25" id="txtComments" name="txtComments"  ONBLUR="focusInput()" ONFOCUS="focusInput()"></TEXTAREA>
                            </td>                            
                        </tr>
                    </table>
                </td>
                </tr>
                <tr>
                    <td colspan="4" id="lblError" class="cssStaticDataMandatory" style="TEXT-ALIGN:left;">
                        
                    </td>
                </tr>
                 <tr><td><br/></td></tr>
                <tr>
                    <td align="center">
                        <button id="btnSave" onclick="Sign()"><span lang_key="Ok"></span></button>
                        <button id="btnCancel" onclick="CancelAction()" ><span lang_key="Cancel"></span></button>                                       
                    </td>
                </tr>
            </table>            
        </div>
    <div id="divAuditTrail" class="cssBasicHiddenformDiv">
      <table width="100%" align="center" border="0">
            <tr>
                <td colspan="4">
                        <h2 class="InitiationTitle" style="cursor:move;" align="center" lang_key="Audit_Trail_Details"></h2>
                </td>                                
            </tr>
            <tr><td><br/></td></tr>
            <tr>
                <td style="padding-left:40px;">
                    <table>
                        <tr>
                            <td class="cssStaticData" colspan="1" style="text-align: right; white-space: noWrap;">
                                <label lang_key="User"></label>:&nbsp;&nbsp;&nbsp;
                            </td>
                            <td colspan="1">
                                <select id="ddlUsers" >
                                    <option value="0">ALL</option>
                                </select>
                            </td>                            
                        </tr>                        
                        <tr><td><br/></td></tr>
                        <tr>
                            <td class='cssStaticData' style="text-align: right; white-space: noWrap;" align="left">
                                    <label lang_key="From_Date"></label>:&nbsp;&nbsp;&nbsp;
                            </td>
                            <td class='cssStaticData' style="text-align: left; white-space: noWrap;" align="left" colspan=1>
                                <div>
                                    <input readonly="readonly" type="text" style="width:100px;" id="txtFromDate" align="left">
                                </div>
                            </td>                                                        
                        </tr>
                        <tr><td><br/></td></tr>
                        <tr>
                            <td class='cssStaticData' style="text-align: right; white-space: noWrap;">
                                    <label lang_key="To_Date"></label>:&nbsp;&nbsp;&nbsp;
                            </td>
                            <td class='cssStaticData' style="text-align: left; white-space: noWrap;" colspan=1>
                                <div>
                                    <input readonly="readonly" type="text" style="width:100px;" id="txtToDate">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td><br/></td></tr>
            <tr><td><br/></td></tr>
            <tr>
                <td align="center">
                    <button id="btnSave" style="width:120px;" onclick="showATReport();"><span lang_key="Show_Report"></span></button>
                    <button id="btnCancel" onclick="closeHiddenForm()" ><span lang_key="Cancel"></span></button>                                       
                </td>
            </tr>
        </table>
  </div>
  <button type="button" id="btnATReportMain" style="display:none;"  onclick="openHiddenForm()"></button>
        <form name="main" id="main" method="post" action="maintenancemainservlet">             
            <!-- SKYLINE V.9 -->
            <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>
            <input type="hidden" name="TabID" id="TabID" value='<%= request.getAttribute("TABID") %>'>
            <input type="hidden" name="ServletName" id="ServletName" value='<%= request.getAttribute("SERVLET_NAME") %>'>
            <input type="hidden" name="PopupName" id="PopupName" value='<%= request.getAttribute("SERVLET_NAME") %>'>
            <input type="hidden" name="TableName" id="TableName" value='<%= request.getAttribute("TABLE_NAME") %>'>
            <input type="hidden" name="tablesStrArray" id="tablesStrArray" value='<%= request.getAttribute("TABLES_STRUCTURE") %>'>            
            <input type="hidden" name="esign" id="esign" value='<%= request.getAttribute("ESIGN") %>'>
            <input type="hidden" name="MandatoryComments" id="MandatoryComments" value=''>
            <input type="hidden" name="currTabPermissions" id="currTabPermissions" value=''>
            <input type="hidden" name="defaultTabID" id="defaultTabID" value='<%= request.getAttribute("DEFAULT_TAB_ID") %>'>
            <input type="hidden" name="defaultTableFilter" id="defaultTableFilter" value='<%= request.getAttribute("DEFAULT_TABLE_FILTER") %>'>
            
            <input type="hidden" name="tableDisplayName" id="tableDisplayName" value=''>
            <input type="hidden" name="tableViewName" id="tableViewName" value=''>
            <input type="hidden" name="maintenanceType" id="maintenanceType" value=''><!--only for tables with responsibility group -->
            <input type="hidden" name="tableATxml" id="tableATxml" value=''>
            <input type="hidden" name="reportType" id="reportType" value=''>
            <input type="hidden" name="selUserFullName" id="selUserFullName" value=''></input> 
            <input type="hidden" name="selUserID" id="selUserID" value='0'></input> 
            <input type="hidden" name="from_Date" id="from_Date" value=''></input>
            <input type="hidden" name="to_Date" id="to_Date" value=''></input>
            <input type="hidden" name="toDateForDisplay" id="toDateForDisplay" value=''></input>
    </form>
  </body>
</html>
