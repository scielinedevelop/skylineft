<script src="js/comply_js/jquery.dataTables-1.11.3.min.js" type="text/javascript"></script>
<script src="js/comply_js/dataTables.jqueryui.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>
<script src="js/comply_js/skyline.dataTables-v2.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>

<!--------------  imports for HTML5 export buttons ------------------------->	
<!-- for jquery.dataTables v1.11.3-->
<script src="js/comply_js/buttons/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="js/comply_js/buttons/jszip.min.js" defer type="text/javascript"></script>
<script src="js/comply_js/buttons/pdfmake.min.js" defer type="text/javascript"></script>
<script src="js/comply_js/buttons/vfs_fonts.js" defer type="text/javascript"></script>
<script src="js/comply_js/buttons/buttons.print.min.js" defer type="text/javascript"></script>
<script src="js/comply_js/buttons/buttons.html5.min.js" type="text/javascript"></script>