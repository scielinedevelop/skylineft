<!-- DROPZONE VERSION 5.9.2-->
<!-- <link href="CSS/comply_theme/dropzone/dropzone.css" rel="stylesheet"  type="text/css"> -->
<link href="CSS/comply_theme/dropzone/dropzone.min.css?<spring:message code="Env" text="" />" rel="stylesheet"  type="text/css">
<style type="text/css">
	.dropzone{border:2px dashed #b2b2b2}
    .dropzone .dz-preview .dz-remove{text-decoration:none;background-color:#83c5be;color:white;border-radius:5px;margin-top:10px}
    .dropzone .dz-preview .dz-remove:hover{text-decoration:none}
    .dropzone .dz-preview .dz-progress{margin-top:10px;-webkit-transition-delay:2s;transition-delay:2s;-webkit-transition-duration:2s;transition-duration:2s}
    .dropzone .dz-preview .dz-error-message{top:160px}
</style>
<!-- <script src="js/comply_js/dropzone/dropzone.js" type="text/javascript"></script>  -->
<script src="js/comply_js/dropzone/dropzone.min.js?<spring:message code="Env" text="" />" type="text/javascript"></script> 
<script src="js/comply_js/skyline.dropzone-api.js?<spring:message code="Env" text="" />" type="text/javascript"></script>