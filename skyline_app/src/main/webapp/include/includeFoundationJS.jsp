<link rel="icon" href="images/favicon.ico" /> <!-- add favorite icon to a browser tab -->

<script src="js/comply_js/foundation.min.js" ></script>
<script>
	$(document).ready(function()
	{
		/* var options = {
		  "closingTime": 50
		};
		//Initialize the dropdown menu with a overrided "closingTime"-> Amount of time to delay closing a submenu on a mouseleave event.
		new Foundation.DropdownMenu($('.main-navbar'), options); */
		
		//just override "closingTime" in DEFAULTS
		Foundation.DropdownMenu.defaults.closingTime = 50;
		
		//INIT foundation
    	$(document).foundation();
    });
</script>