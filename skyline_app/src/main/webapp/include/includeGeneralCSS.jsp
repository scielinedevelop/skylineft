<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- TODO: for improve font loading process try to compress woff/woff2 files -->
<!-- <link href="CSS/comply_theme/skyline_fonts.css" rel="stylesheet" type="text/css" media="all"/> -->

<link href="CSS/comply_theme/font-awesome.min.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/buttons.dataTables.min.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/dataTables.jqueryui.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/jquery-ui.custom.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/app.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all"/> 