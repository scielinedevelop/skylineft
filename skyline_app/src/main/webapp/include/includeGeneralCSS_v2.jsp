<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link href="CSS/comply_theme/font-awesome.min.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/chosen.1.8.7.css" rel="stylesheet"  type="text/css">
<link href="CSS/comply_theme/buttons.dataTables.min.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/dataTables.jqueryui.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/jquery-ui.custom.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/comply_theme/app_v2.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all"/> 