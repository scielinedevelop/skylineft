<script src="js/comply_js/jquery-2.2.4.min.js" type="text/javascript"></script> 
<!-- jquery-ui.custom.min.js verison v1.12.1 -->
<!-- <script src="js/comply_js/jquery-ui.custom.js" type="text/javascript"></script> -->
<script src="js/comply_js/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="js/comply_js/jquery-migrate-1.4.1.js" type="text/javascript"></script>
<!-- <script src="js/comply_js/jquery-migrate-1.4.1.min.js" type="text/javascript"></script> -->
<script src="js/comply_js/chosen.jquery.1.8.7.min.js" type="text/javascript"></script>
<script src="js/comply_js/moment-2.14.1.min.js" type="text/javascript"></script>
<script src="js/comply_js/CommonFuncs.v2.js?<spring:message code="Env" text="" />" type="text/javascript"></script>
<script src="js/comply_js/CommonDBFuncs.js?<spring:message code="Env" text="" />" type="text/javascript"></script>
<script src="js/comply_js/Lang_Selection.js?<spring:message code="Env" text="" />" type="text/javascript"></script>

<script>
  
  /*** code for prevent Violations like:
      "Added non-passive event listener to a scroll-blocking 'touchstart' event. Consider marking event handler as 'passive' to make the page more responsive"
  */
var isIE = isIE();
jQuery.event.special.touchstart = {
  setup: function( _, ns, handle ){
    if(!isIE) {
      if ( ns.includes("noPreventDefault") ) {
        this.addEventListener("touchstart", handle, { passive: false });
      } else {
        this.addEventListener("touchstart", handle, { passive: true });
      }
    }
  }
};
jQuery.event.special.touchmove = {
  setup: function( _, ns, handle ){
    if(!isIE) {
      if ( ns.includes("noPreventDefault") ) {
        this.addEventListener("touchmove", handle, { passive: false });
      } else {
        this.addEventListener("touchmove", handle, { passive: true });
      }
    }
  }
};
jQuery.event.special.wheel = {
  setup: function( _, ns, handle ){
    if(!isIE) {
      if ( ns.includes("noPreventDefault") ) {
        this.addEventListener("wheel", handle, { passive: false });
      } else {
        this.addEventListener("wheel", handle, { passive: true });
      }
    }
  }
};
</script> 