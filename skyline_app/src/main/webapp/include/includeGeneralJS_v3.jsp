<script src="js/comply_js/jquery-3.6.0.min.js" type="text/javascript"></script> 
<script src="js/comply_js/jquery-ui-1.13.0.min.js" type="text/javascript"></script>
<script src="js/comply_js/jquery-migrate-3.3.2.js" type="text/javascript"></script>
<!-- <script src="js/comply_js/jquery-migrate-3.3.2.min.js" type="text/javascript"></script> -->
<script src="js/comply_js/chosen.jquery.1.8.7.min.js" type="text/javascript"></script>
<script src="js/comply_js/moment-2.14.1.min.js" type="text/javascript"></script>

<script src="js/comply_js/CommonFuncs.v2.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>
<script src="js/comply_js/CommonDBFuncs.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>
<script src="js/comply_js/Lang_Selection.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>