<style type="text/css">
  .loading-div-background 
  {
    display:block;
    position:fixed;
    top:0;
    left:0;
    background-color: #f2f5f7;
    width:100%;
    height:100%;
    opacity: 0.95;
    filter: Alpha(opacity=95);
    z-index: 999;
  }
  .loading-div
  {
    text-align:center;
    position:relative;
    top: 30%;
    z-index: 1001;
  }
  h2 {
      color:#454545;
    }
</style>

<div id="loading_div_background" class="loading-div-background" >
    <div id="loading_div" class="loading-div">
      <img src="images/page-loader.gif" alt=""></img>
      <h2 style="margin: 8px 0;">Please wait...</h2>
    </div>
</div> 

<script type="text/javascript"> 
    window.onload = function() {
        setTimeout(function() {
            document.getElementById("loading_div_background").style.display = "none";
          }, 250);    
    };
</script>