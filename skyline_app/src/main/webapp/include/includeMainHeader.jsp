<%@ page contentType="text/html;charset=UTF-8"  import="comply.SkyLine.bl.biz.User" %>
<div class="top-bar-container" style="width:100%;">
<header class="top-bar" style="width: 100%;">

	<div class="top-bar-left">
		<div style="float:left;">
			<a id="homePageHeaderJspTempalte" href="menu.jsp">
				<img src="images/logo_scieline_no_bottom_text.png" border="0" title="Main Menu">
			</a>
		</div>
	</div>

	<div class="top-bar-right" style="margin-top: 2px;">
		<ul class="menu">
			<li id="tdNoLdap" class="menu-text" style="display:none">
				<span id="spnChangePassword" onclick="ChangePass();" style="cursor:pointer;">Change Password</span>
			</li>
			<li id="tdAdmin" class="menu-text" style="display:none">					
				<span id="spnLog" onclick="ViewLog();" style="cursor:pointer;">View Log</span>
			</li>
			<li id="tdAdminEvent" class="menu-text" style="display:none">					
				<span id="spnLog" onclick="ViewEventLog();" style="cursor:pointer;">View Event Log</span>
			</li>
			<li id="tdAdminConnection" class="menu-text" style="display:none">					
				<span id="spnLogConnection" onclick="ViewConnectionLog();" style="cursor:pointer;">View Connection Log</span>
			</li>
			<li class="menu-text">
				<span class="user-name">${userName}</span>
				<!-- <img src="images/menu_group.png" style="width: 9.3px;height: 5.2px;object-fit: contain;cursor: pointer;"/> -->
			</li>
			<li class="menu-text">
				<span class="logOutPipe">|</span>
			</li>
			<li class="menu-text">
				<span id="spnLogConnection" onclick="confirmMainMenuExitPageWarning('Login.jsp?exit=true')" style="cursor:pointer;">Log Out</span>
				<!-- <a href="#" ><span class="logOut"></span></a> -->
			</li>

		</ul>

	</div>
</header>
<div class="sub-header" style="width:100%;text-align: left;">
    <ul class="main-navbar dropdown menu" data-dropdown-menu >
    	<li style="padding-left:7px" class="maintenance">
    		<a class="main-navbar-icon" href='#' >Maintenance</a>
    		<ul class="menu is-dropdown-submenu">
    			<li ><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=maintenancemainservlet')" >Maintenance</a></li>
    			<li id="forms" style="display:none;"><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=skylineForm/demoFormBuilderMainInit.request')" >Forms</a></li>
    			<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=frmnotificationlistservlet')" >Notifications</a></li>
    		</ul>
    	</li>
    	 <li class="process-template">
    		<a class="main-navbar-icon" href='#' >Process Template</a>
    		<ul class="menu is-dropdown-submenu">
    			<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizproductlistservlet_')">Production</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizemproductlistservlet_')">Environmental Monitoring</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizstabproductlistservlet_')">Stability</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizprojectmanagementlistservlet_')">Project Management</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=notebookstandalonelistservlet')">Notebook</a></li>
				<li><a href="redirect.htm?url=catalog">Catalog</a></li>
				<li><a href="redirect.htm?url=request">Development Application</a></li>
				<li><a href="DynamicFieldsUpdate.jsp" >Dynamic Fields</a></li>
    		</ul>
    	</li>
    	<li class="data-entry">
    		<a class="main-navbar-icon" href='#' >Data Entry</a>
    		<ul class="menu is-dropdown-submenu">
    			<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=entrunstatusmainservlet')">Run Management</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=dataentrymainservlet_?Mode=P')">Data Entry</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=dataentrymainservlet_?Mode=EM')">EM Data Entry</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=dataentrymainservlet_?Mode=ST')">Stability Data Entry</a></li>  
	            <li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=dataentrymultiplerunsservlet')">Data Entry for Multiple Runs</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=dataentrysamplecommentsservlet_')">Sample Comments</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=entsampledrawndateservlet_')">Sample Drawn Date</a></li>                    
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=enttestresultsservlet_')">Test Result By Sample</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=testresultsbydateservlet_')">Test Results by Date</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=entnewrunservlet_')">Method Results</a></li>							
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=resultsforapprovalmainservlet_')">Results for Approval</a></li>
    		</ul>
    	</li>
   		<li class="sample-tracking">
    		<a class="main-navbar-icon" href='#' >Sample Tracking</a>
    		<ul class="menu is-dropdown-submenu">
    			<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizlabtestrequestservlet_')">Lab Test Request Form</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=sampletrackingservlet_')">Track Sample</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=analysisrequestmainservlet_')">Analysis Request</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=entuploadcsvfileservlet_')">Import Data</a></li>
    		</ul>
    	</li>   	
    	 <li class="inventory">
    		<a class="main-navbar-icon" href='#' >Inventory</a>
    		<ul class="menu is-dropdown-submenu">
    			<li><a href="#">Inventory Type</a>
    				<ul class="vertical menu nested">
		    			<li class="first_item"><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=inventorytypelistservlet_')">Inventory</a></li>
						<li class="last_item"><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=inventorytypelistservlet_?isInstrumentsType=1')">Instruments</a></li>
		    		</ul>
    			</li>  
            	<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=inventorylistservlet_')">Inventory List</a></li>
    		</ul>
    	</li>
    	<li class="reports">
    		<a class="main-navbar-icon" href='#' >Analysis & Reports</a>
    		<ul class="menu is-dropdown-submenu">
    			<li><a href="#" onclick="confirmMainMenuExitPageWarning('GraphMenu.jsp')">Custom Charts & Reports</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=formreportmenuservlet')">Reports</a></li>
				<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=coaservlet')">COA List</a></li>
				<li>
		    		<a href='#' >Trends</a>
		    		<ul class="vertical menu nested">
		    			<li class="first_item"><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizgraphtrendsservlet_')">Trends Analysis</a></li>
						<li><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizemspecificationlimitsservlet')">EM Specification Limits</a></li>	
						<li class="last_item"><a href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wiztrendsservlet_')">Control Limits</a></li>
		    		</ul>
		    	</li>
    		</ul>
    	</li>
    	<li class="search">
    		<a class="main-navbar-icon" href="#" onclick="confirmMainMenuExitPageWarning('redirect.htm?url=wizsearchrundataservlet_')" >Search</a>
    	</li>
    	<li class="user-guide" style="padding-right:0">
    		<a class="main-navbar-icon" href='#' onclick="showUserGuide();">User Guide</a>
    	</li>
    	
    </ul>
  </div>
</div>
<script type="text/javascript">  
	
    function showUserGuide(){
			$('#main').attr('action', 'helperservlet?actionid=userGuide');
			$('#main').attr('target','_blank');
			$('#main').submit();
	}
	
</script>
