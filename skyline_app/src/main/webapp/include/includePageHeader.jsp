<table width="100%" style="margin: 0px;" cellpadding="0" cellspacing="0" border="0">
    <tr >
        <td width="10%">
            <a id="headerShowBackIcon" href="" style="display:none;cursor:pointer;padding-left: 10px;">
            	<!-- <img src="images/back_image.png" title="Back" /> -->
            </a> 			
		</td>	
        <td width="80%" class="cssPageHeader page-header" style="padding-bottom:10px">
                <%=pageHeader%>
        </td>
        <td width="10%"></td>        
    </tr>
    <TR>
		<TD class="cssCurrentPath page-sub-header" colspan=3> 
            <%=pageTitle%>
      <div class="floatingButtonsPanelContainer" id="divFloatingButtonsPanelContainer" style="display: none;">
				<div class="floatingButtonsShowHideIcon" id="divFloatingButtonsShowHideIcon">
          <i class="fa fa-angle-right" aria-hidden="true" onclick="floatingButtonPanelToggleClick(this)"></i>
        </div>
				<div class="floatingButtonsPanel" style="display:none;">
					<div style="float: left;font-size: 14px;color: #006d77;">
						<span id="btnSaveFloating" onclick="pageGlobalSave()">Save</span>
            <span style="display:none;" class="section-toggle-collapse-btn" onclick="toggleAllSectionsCollapse(true)">Collapse All</span>
            <span style="display:none;" class="section-toggle-collapse-btn" onclick="toggleAllSectionsCollapse(false)">Expand All</span>
					</div>	
				</div>
			</div>	
		<TD>		
    </TR>
   <%--  <TR>            
		<TD style="text-align:left;" vAlign="Top" colspan=3>
			<%=request.getAttribute("NAVIGATION")%>
		<TD>
    </TR> --%>
</table>