<link href="CSS/comply_theme/summernote/summernote-lite.css?<spring:message code="Env" text="" />" rel="stylesheet" type="text/css" media="all" />
<style type="text/css">
	/* for summernote editor */ 
	.note-editor .note-editing-area {
	    background-color: white;
	}
	.close {
		line-height: 2;
	}
	.note-modal-footer {
		padding: 0;
		padding-right: 30px;
	}
	.note-editor.note-frame, .note-editor.note-airframe {
    	border: 1px solid #cec9c9;
	}
	.richtext-viewonly-mode .note-editor.note-frame {
		border:none;
	}
	.richtext-viewonly-mode .note-editor.note-frame .note-toolbar {
		display: none;
	}
	.richtext-viewonly-mode .note-editor.note-frame .note-statusbar {
		display: none;
	}
	.richtext-viewonly-mode .note-editor.note-frame .note-editing-area .note-editable[contenteditable=false] {
    	background-color: white;
	}
</style>
<!-- <script src="js/comply_js/summernote/summernote-lite.js" type="text/javascript"></script>  -->
<script src="js/comply_js/summernote/summernote-lite.min.js?<spring:message code="Env" text="" />" type="text/javascript"></script> 
<script src="js/comply_js/skyline.richtext-editor-api.js?<spring:message code="Env" text="" />" type="text/javascript"></script> 