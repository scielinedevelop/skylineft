/**
	check if the run name exists in the system.
	see isRunNameExist in java.
	note: this function also returns when -2 time out.
**/
function isRunExists(productId, runName, sourceRunName, stageIDList, stageSourceCode)
{ 
	 var toReturn = -1;
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=isRunExists&productId=" + productId + "&runName=" + runName + "&sourceRunName=" + sourceRunName +"&stageIDList=" + stageIDList + "&stageSourceCode=" + stageSourceCode,
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				toReturn = -2;
			}
			else
			{
				toReturn = data;
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function isAlive()
{ 
	 var toReturn = 1;
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=isAlive",
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				toReturn = 0;
			}
			else
			{
				toReturn = 1;
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function getTasksCount()
{ 
	 var toReturn = "0";
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getTasksCount",
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				commanDBAjaxError();
			}
			else
			{
				toReturn = data;
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function getConnectorRunsList()
{ 
	 var toReturn = "";
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getConnectorRunsList",
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				commanDBAjaxError();
			}
			else
			{
				toReturn = data;
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}
 
function commanDBAjaxError( xhr, textStatus, error ) 
{
	if (xhr.responseText.match(/TIME_IS_OUT/)) 
    {   
		top.location.href = 'Login.jsp';
		return;
    } 
	else
	{
		return -1;
	}
	
}

function fnDivCalc()
{
	var toReturn = '';
	$.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=divCalcRetObject",
		dataType: "json",
		success: function( data ) 
		{  
			if ( data instanceof Object )
			{
				toReturn = data;
			}
			else
			{
				if(data.match(/TIME_IS_OUT/))
				{
					commanDBAjaxError();
				}
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function saveInSession(key, val)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=saveInSession&key=" +  encodeURIComponent(key) + "&val=" + encodeURIComponent(val),
		dataType: "text",
		success: function( data ) 
		{  
			//do nothing
		},
		async: false
	});	
	
	return toReturn;
}

function getInSession(key)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getInSession&key=" +  encodeURIComponent(key),
		dataType: "text",
		success: function( data ) 
		{  
			if(data == null || data.match(/TIME_IS_OUT/))
			{
				toReturn = '';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false
	});	
	
	return toReturn;
}

/* used by Inventory List General for check if reference id with same value not exist for the same inventory type  */
function fnCheckInvRefID(invTypeID, invID, invRefID)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=checkInvRefID&invTypeID=" + invTypeID + "&invID=" + invID + "&invRefID=" + invRefID,
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				toReturn = 'TIME_IS_OUT';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function fnCheckIfLocationIsFull_ByNumberOfItems(originalItemID, numberOfNewItems, locID, onConfirmFunc, onConfirmFuncParams)
{
	return _validateLocationState('-1',originalItemID, numberOfNewItems, locID, onConfirmFunc, onConfirmFuncParams);
}

function fnCheckIfLocationIsFull(itemsIdToUpdate, locID, onConfirmFunc, onConfirmFuncParams)
{
	return _validateLocationState(itemsIdToUpdate,"", "", locID, onConfirmFunc, onConfirmFuncParams);
}

function _validateLocationState(itemsIdToUpdate, originalItemID, numberOfNewItems, locID, onConfirmFunc, onConfirmFuncParams)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=checkIfLocationIsFullOccupied&selLocationId=" + locID + "&itemIdToUpdate="+ itemsIdToUpdate + "&origItemID=" + originalItemID + "&numberOfItems="+numberOfNewItems,
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				toReturn = 'TIME_IS_OUT';
			}
			else
			{
				var locState = data;
				if(locState == '-1')
				{
					toReturn = '-1';
				}
				else if(locState == '-2')
				{
					displayAlertDialog('Error occurred on location validation');
				}
				else
				{
					if(locState == "force") {
						displayAlertDialog("DisplayError_Msg_Short_Name_T_20");
					}
					else if(locState == "full") {
						openConfirmDialog({ message:"OpenConfirmDialog_Msg_Short_Name_T_59", 
											onConfirm:onConfirmFunc, 
											onConfirmParams:onConfirmFuncParams
										});
					}
					else if(locState.indexOf("_multi") > -1) {
						var delimInx = locState.indexOf(';'); 
						var state = locState.substr(0,delimInx);	
						var freeSpace = locState.substr(delimInx+1);
						if(state == "full_multi") {
							openConfirmDialog({ message:"OpenConfirmDialog_Msg_Short_Name_V_104",replaceme:[{key:0,val:freeSpace}], 
								onConfirm:onConfirmFunc,
								onConfirmParams:onConfirmFuncParams
							});
						}
						else if(state == "force_multi") {//
							displayAlertDialog("DispAlDial_Msg_Short_Name_V_102",{replaceme:[{key:0,val:freeSpace}]});
						}
					}		    		
				}
			}
			
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function fnCheckIfTestHasDBCalc(currTestCode)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=checkIfTestHasDBCalc&currTestCode=" + encodeURIComponent(currTestCode),
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				toReturn = 'TIME_IS_OUT';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function getSystemParameter(paramName)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getSystemParameter&paramName=" +  encodeURIComponent(paramName),
		dataType: "text",
		success: function( data ) 
		{  
			if(data == null || data.match(/TIME_IS_OUT/))
			{
				toReturn = '';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false
	});	
	
	return toReturn;
}

function checkLinkedTestParams(listOfID)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getLinkedTestParams&listOfID=" +  encodeURIComponent(listOfID),
		dataType: "text",
		async:false,
		success: function( data ) 
		{  
			if(data == null || data.match(/TIME_IS_OUT/))
			{
				toReturn = '';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false
	});	
	
	return toReturn;
}


function getRespGrByUser()
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getRespGrList",
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				commanDBAjaxError();
			}
			else
			{
				toReturn = data;
			}
		},
		async: false
	});	
	
	return toReturn;
}

function getLastAndNextCalibrationDatesByFrequency(inventoryId, internalFreq, externalFreq)
{
	return _getLastAndNextCalibrationDates(inventoryId,'','',-1,internalFreq, externalFreq);
}

function getLastAndNextCalibrationDatesByDateAndType(inventoryId,calibrationDate,calibrationType,calibrationId)
{
	return _getLastAndNextCalibrationDates(inventoryId,calibrationDate,calibrationType,calibrationId,'', '');
}

function _getLastAndNextCalibrationDates(inventoryId,calibrationDate,calibrationType,calibrationId,internalFreq, externalFreq)
{ 
	 var toReturn = "";
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getLastAndNextCalibrationDates&inventoryId="+inventoryId+"&calibrationDate="+calibrationDate+"&calibrationType="+calibrationType+"&calibrationId="+calibrationId+
				"&internalFreq="+internalFreq+"&externalFreq="+externalFreq,
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				commanDBAjaxError();
			}
			else
			{
				if(data == -1)
				{
					console.log('error in _getLastAndNextCalibrationDates');
				}
				else
				{
					toReturn = data;
				}
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function getObiSessionNoWsdlClass()
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getObiSessionNoWsdlClass",
		dataType: "text",
		success: function( data ) 
		{  
			if(data == null || data.match(/TIME_IS_OUT/))
			{
				toReturn = '';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false
	});	
	return toReturn;
}

//kd 27052019 
function getUserLastSelScheme1()
{
	var toReturn = '';
	$.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getUserLastSelScheme",
		dataType: "text",
		success: function( data ) 
		{  
			if(data == null || data.match(/TIME_IS_OUT/))
			{
				toReturn = '';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false
	});	
	return toReturn;
}

//kd 28052019 
function setUserSchemeCurrent(val)
{
	var toReturn = '';
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=setUserSchemeCurrent&val=" +  encodeURIComponent(val),
		dataType: "text",
		success: function( data ) 
		{  
			//do nothing
			toReturn = data;
		},
		async: false
	});	
	
	return toReturn;
}

function fnSaveClobContent(content, description, oldValueId)
{
	var toReturn = "-1";
	 $.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=saveClob&clobString=" + encodeURIComponent(content) + "&description=" + description + "&oldValueId="+oldValueId,
		dataType: "text",
		success: function( data ) 
		{  
			if(data.match(/TIME_IS_OUT/))
			{
				toReturn = 'TIME_IS_OUT';
			}
			else
			{
				toReturn = data;
			}
		},
		async: false,
		error: commanDBAjaxError
	});	
	
	return toReturn;
}

function fnGetClobContent(id, callbackFunc)
{
	if(id == null || id == 0)
	{
		callbackFunc.apply(this, [""]);
	}
	$.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getClob&clob_id=" + id,
		dataType: "text",
		success: function( data ) 
		{  
			callbackFunc.apply(this, [data]);
		},
		error: commanDBAjaxError
	});	
}

function fnGetUsersForAuditTrail(callbackFunc)
{
	$.ajax
	({
		type:"POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getATUsersList",
		dataType: "json",
		success: function(data) 
		{
			if(callbackFunc)
			{
				callbackFunc.apply(this, [data]);
			}
		},
		error: handleAjaxError
	});
}

function fnGetUomGroup(selValue, callbackFunc) {
	sendAjaxRequest({servletName:"helperservlet",urlParams:"actionid=getUomGroup&selGroupId="+selValue,dataType:"json",
		onSuccess:function(returnedList) {
			if(callbackFunc)
			{
				callbackFunc.apply(this, [returnedList]);
			}
		}
	});
}

function fnGetUomByGroup(selParentVal, selValue, callbackFunc) {
	sendAjaxRequest({servletName:"helperservlet",urlParams:"actionid=getUom&uomGroupId="+selParentVal+"&selectedUom="+encodeURIComponent(selValue),dataType:"json",
		onSuccess:function(returnedList) {
			if(callbackFunc)
			{
				callbackFunc.apply(this, [returnedList]);
			}
		}
	});
}
