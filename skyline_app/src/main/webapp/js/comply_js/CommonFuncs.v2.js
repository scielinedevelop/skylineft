/**
 * CommonFuncs version created for Skyline v11 
 */

try
{
	//window.history.go(1);
}
catch(e){}

var currColor = '';
var ILLEGAL_CHARS = "\"\'";
var bOnChange = false;
var bChildWindowChanged = false; // var used for dialog close event

/*** prevent BACKSPACE key press propagation 
**   12012014 a.b 
***/
$(document).off('keydown').on('keydown', function (event) 
{
    if (event.keyCode === 8) 
    {
        var d = event.srcElement || event.target;
//        if ((!$(d).is('input:not([readonly],[disabled]), text') && !$(d).is('input:not([readonly],[disabled]), password') && !$(d).is('textarea:not([readonly],[disabled])'))
//            || ($(d).is('input:radio') || $(d).is('input:checkbox'))
//            )      
//        {
//            event.preventDefault();
//        }
        if (($(d).is('input') && (($(d).is('input:radio') || $(d).is('input:checkbox')) || !$(d).is('input:not([readonly],[disabled]), text') && !$(d).is('input:not([readonly],[disabled]), password')))
                || ($(d).is('textarea') && !$(d).is('textarea:not([readonly],[disabled])'))
                || ($(d).is('div') && !$(d).is('div[contenteditable="true"]'))
                || ($(d).is('p') && !$(d).is('p[contenteditable="true"]'))
            )      
        {
            event.preventDefault();
        }
        
    }
});
/*** prevent html  text selection, except for input fields and textarea
**   22022021 ab: code commented, cause problems with richtext editor(summernote plugin)
***/
// $(document).on('selectstart', function(event)
// {
//     var el = event.srcElement || event.target;
//     //if($(el).is(':not(input, textarea)')) // yp 13032016 add allowCopy attribute to allow copy on specific fields (fix bug 3405) -> 
//     if($(el).is(':not(input, textarea)[allowCopy!=1]') && !$(el).is('div[contenteditable="true"]') && !$(el).is('p[contenteditable="true"]'))
//     {
//         event.preventDefault();
//     }
// });

/** 
 * jQuery.expr[':'] is deprecated from version 3.0 use jQuery.expr.pseudos instead
 * customized jQuery :selector. 
  * as :selector are always extension it's mostly efficient to use them as in
  *					$(element).filter(':selector') 
  *				rather than - $(element:selector).
  * tutorials about expr[':'] extension can be found here: http://www.websanova.com/blog/jquery/12-awesome-jquery-selector-extensions#.U7563rGcDcY
  * which explains, for instance, why we're using m[3] and the function parameters (el, i, m)
**/
$.extend( $.expr.pseudos, {
	/* exclude elements whose direct parent answers to certain selector
	 * usage: $('select').filter(':excludeParent(.exClass)') will return all <select> whose direct parent is NOT of class exClass */
	excludeParent: 	function(el, i, m) {
						return $(el).parent(m[3]).length < 1;
					},
	/* inverted version of excludeParent: returns only elements whose direct parent answer to certain selector 
	 * usage: $('span').filter(':parentAttr(li)') will return only <span>s within <li>s */
	parentAttr:	function(el, i, m) {
					return $(el).parent(m[3]).length > 0;
				}, 
	/* as excludeParent but checks ALL parents above element. Currently used to check if element is disabled (through any of its parents) */
	excludeParents: function(el, i, m) {
						return $(el).parents(m[3]).length < 1;
					}
});

/*  converts plain text (e.g. result comment from db) into html assimilate-able string to show, for instance, in new alert pop-up 
	using MAP object to traverse the string only once 
	' is replaced with \\' otherwise jQuery formats &#39; (previously &apos;) back to ' within HTML */
	var MAP = { '<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": "\\'", 
	'\n': '<br/>'
};

function convertTextToHtml( text )
{ 
return text.replace(/["'<>\n]/g, 
				function(m) {							
					return MAP[m];
				});
}

// for multilanguage support DisplayError 
// *jsonData - from Lang_Selection.js
function replaceCodeByText(errMsg) 
{
	if ( typeof jsonData != "undefined" && errMsg != null )
	{
		 errMsg = jsonData[errMsg]; 
		 return errMsg;
	}
}

function slashDate( date )
{
	if( date.indexOf("/") > -1 || date.length != 8 )
	{
		return date;
	}
	
	return [date.slice(0, 2), date.slice(2, 4), date.slice(4)].join('/');
}

function isNumeric(input)
{
	var regexp = /^[0-9]+$/;
	if(regexp.test(input))
	{
		return true;
	}

	return false
}

function validateNumber(intKeyCode, e)
{
	var event = e ? e : window.event;
	var retVal = false;
	if ((intKeyCode < 48) || (intKeyCode > 57))
	{
		retVal = false;
	}
	else
	{
		retVal = true;
	}
	
	if(!retVal)
	{
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
	}
	else
	{
		return true;
	}
}

function validatePositiveNumber (intKeyCode, value, e)
{
	var event = e ? e : window.event;
	var retVal = false;
	if ((intKeyCode < 48) || (intKeyCode > 57) || (intKeyCode == 48 && value.length == 0))
		retVal = false;
	else
		retVal = true;
	
	if(!retVal)
	{
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
	}
	else
	{
		return true;
	}
}

function fnValidatePositiveNumber (inStr, inFieldName, isRequired, focusElemId)
{
	var id = (focusElemId == null) ? '' : focusElemId;
	var reg = /[^0-9]/;
	if ( inStr != null && (reg.test(inStr) || inStr.indexOf('0') == 0))
	{
		displayAlertDialog(inFieldName + " contains illegal characters or format", { focusOn:id, title:"Invalid Number" });
		return false;
	}
	
	if (isRequired && ( inStr == null || inStr == "" ))
	{	
	    displayAlertDialog(inFieldName + " is a required field", { focusOn:id, title:"Required Field Missing" });
		return false;
	}
	
	return true;
}

/* uses validateNumeric to alert for illegal chars or format in a numeric string
*	if illegal an alert pops up using the relevant field name and false is returned;
*	if legal returns true 
*	note: to use isRequired properly, inStr must be already trimmed 
*/
function fnValidateNumeric (inStr, inFieldName, isDecimal, isSigned, isRequired, focusElemId)
{
	var id = (focusElemId == null) ? '' : focusElemId;
	
	if ( inStr != null && inStr != "" && !validateNumeric(inStr, isDecimal, isSigned))
	{
		displayAlertDialog(inFieldName + " contains illegal characters or format", { focusOn:id, title:"Invalid Number" });
		return false;
	}
	
	if (isRequired && ( inStr == null || inStr == "" ))
	{	
		displayAlertDialog(inFieldName + " is a required field", { focusOn:id, title:"Required Field Missing" });
		return false;
	}
	
	return true;
}

/* validates a numeric whole field for illegal chars or format
*	isDecimal: if True allows for a single '.' 
*	isSigned: if True allows a '-' at the first index only
*	any illegal format or character returns false   
*/
function validateNumeric (inStr, isDecimal, isSigned)
{
	var reg = /[^0-9.-]/	
	if (!isSigned && inStr.indexOf('-') > -1)
		return false;
		
	if (!isDecimal && inStr.indexOf('.') > -1)
		return false;
	
	if (reg.test(inStr))
		return false;
		
	// check if string contains more than one point sign
	if (inStr.indexOf('.') != inStr.lastIndexOf('.'))
		return false;
	
	// check if string contains more than one minus sign
	if (inStr.lastIndexOf('-') > 0)
		return false;
	
	var strLength = inStr.length;
	if(strLength == 1 && (inStr == '.' || inStr == '-'))
		return false;
	
	// check if last sign in string is point
	if((strLength-1) == inStr.indexOf('.'))
		return false;
	
	return true;
}

function validateDecimal(value, key, e)
{	
	var event = e ? e : window.event;
	var retVal = false;
	if(key == 45 && value.length == 0) /*45='-'*/
	{
		retVal = true;	
	}
	
	if((key >= 48 && key <=57) || (key == 46 && value.indexOf(".") == -1))
	{
		retVal = true;
	}
	
	if(!retVal)
	{ // t.s. 170814; this code might be irrelevant. came before bug fix when (!retVal) didn't return retVal at the end (now retVal is ALWAYS returned)
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);		
	}
	
	return retVal;        
}

function validateDecimalNoSign(value, key, e)
{
	var event = e ? e : window.event;
        var retVal = false;
	if((key >= 48 && key <=57) || (key == 46 && value.indexOf(".") == -1))
	{
		retVal = true;	
	}
	if(!retVal)
            event.preventDefault ? event.preventDefault() : (event.returnValue = false);
        else
            return true;
}

function validateLegalChar(inKeyCode, e)
{
        var event = e ? e : window.event;
        var retVal = true;
        if (inKeyCode == 34 ||							// "
		(inKeyCode >= 38 && inKeyCode <= 39) ||		// & '
		 inKeyCode == 44 ||							// ,
		(inKeyCode >= 59 && inKeyCode <= 64) ||		// ; < = > ? @
		 inKeyCode == 92  ||						// \ 
		 inKeyCode == 124)							// |
		 retVal = false;
	if(!retVal)
            event.preventDefault ? event.preventDefault() : (event.returnValue = false);
        else
            return true;
}

function validateLegalString (inStr)
{
	var illegalChars = ['\"', '&', '\'', ',', ';', '<', '=', '>', '?', '@', '\\', '|'];
	if (inStr == null)
	{
		return true; // might be required, yet legal
	}
		
	for (var i in illegalChars)
	{
		if (inStr.indexOf(illegalChars[i]) > -1)
			return false;
	}
	return true;	
}

/* "overload" to validateLegalString() that allows for a pre-set list of illegal chars (inIllegals), 
 * or exceptions to illegal chars (inLegals).
 * inLegals is used only if inIllegals is null.
 * If neither was sent (only inStr used) the else clause will run and call validateLegalString() */
function validateLegalStringSelective (inStr, inIllegals, inLegals)
{	
	var currentIllegals = inIllegals; 
	if (inIllegals == null)
	{	// removes inLegals from default illegal chars list, or return the full list if inLegals is null
		currentIllegals = createIllegalCharString(inLegals);
	}
	
	if ( inStr != null && (inIllegals != null || inLegals != null) )
	{
		for (var i = 0; i < currentIllegals.length; i++)
		{ 
			if (inStr.indexOf(currentIllegals.charAt(i)) > -1)
			{ 
				return false;
			}
		}
	}
	else // both are null/undefined: execute basic validation
	{
		return validateLegalString(inStr);
	}
	
	return true;
}

function createIllegalCharString (inLegals)
{
	var defStr = '\"\'\\&,;<=>?@|';
	if (inLegals == null) // covers a case where value was not sent
	{
		return defStr;
	}
	var reg = new RegExp('[' + inLegals + ']', 'g');
	return (defStr).replace(reg, '');
}

/* Besides alerting if a required field is empty, it also allows for several values considered "empty", 
 * and for exceptions of the usual illegal chars (or a pre-set list sent to the function).
 * inStr - string to be evaluated. Should be sent trimmed already.
 * forbiddenValues - values separated by | (pipe) sign (e.g, "0|1|choose") that will alert that the field is required if inStr is any of those.
 * 		(Originally for indexes of DDL, or any value which is not "" but is still considered illegitimate/empty).  
 * illegals - A string of concatenated chars replacing the "usual" illegal chars.
 * legals - A string of concatenated chars, which are an exception from the usual list of illegal chars.
 * focusOn - The id of the element that should get focus after alert-dialog is close.
 * frameId - If focusOn field is within an iFrame and called from the parents send frame elementId by this parameter
 * note: @legals is considered ONLY if @illegals is null */ 
function fnValidateString (obj) /* inStr, fieldName, isRequired, forbiddenValues, illegals, legals, focusOn, frameId */
{	
	// set defaults if no values sent
	var _inFieldName = (obj.fieldName == null) ? '' : obj.fieldName,
		_isRequired = (obj.isRequired == null) ? false : obj.isRequired,
		_inForbiddenValues = (obj.forbiddenValues == null) ? '' : obj.forbiddenValues, 
		_inIllegals = obj.illegals,
		_inLegals = obj.legals,
		_hasFocus = (obj.focusOn == null) ? '' : obj.focusOn,
		_frameId = (obj.frameId == null) ? '' : obj.frameId,
		_inStr = obj.inStr; 
	var alertMsg = '';
	if ( !validateLegalStringSelective(_inStr, _inIllegals, _inLegals) )
	{		
        alertMsg = (_inFieldName + " contains illegal characters" + 
			       //(_inLegals != null ? "" : ("<br/>Please do not use " + (_inIllegals != null ? displayStringOfCharsWithSpace(_inIllegals) : "\" & \' , ; < = > ? @ \\ |" ))));
        			"<br/>Please do not use " + displayStringOfCharsWithSpace(_inIllegals, _inLegals));
		displayAlertDialog(alertMsg, { title:"Invalid Value", focusOn:_hasFocus, frameId:_frameId });
		return false;
	}
	
	if (_isRequired) 
	{		
		alertMsg = (_inFieldName == '') ? "" : (_inFieldName + " is a required field");
		
		if (_inStr == null || _inStr == "")
		{			
			displayAlertDialog(alertMsg, { title:"Required Field Missing", focusOn:_hasFocus, frameId:_frameId });
			return false;
		}
		
		if (("|" + _inForbiddenValues + "|").indexOf("|" + _inStr + "|") != -1)
		{
			displayAlertDialog(alertMsg, { title:"Required Field Missing", focusOn:_hasFocus, frameId:_frameId });
			return false;
		}
	}
    return true;
}
/*
  function separates string with spaces for comfort display
*/
function displayStringOfCharsWithSpace(illegals, legals)
{        
	var strWithSpace = " ";
	var tmpIllegals = illegals;
	
	if(illegals == null)
	{
		// removes inLegals from default illegal chars list, or return the full list if inLegals is null
		tmpIllegals = createIllegalCharString(legals);        	
	}
	for (var i = 0; i < tmpIllegals.length; i++)
	{
			strWithSpace += tmpIllegals.charAt(i) + " ";                    
	}
	return strWithSpace;
}

/*
 * The function creates and attaches div to be used as wait message pop up with background(mask)  
*/
function initWaitMessageDiv(obj)
{
	var bodyElem = $('body')[0]; 
	if (bodyElem == null)
		return;
	
	var _obj = null;
	if(arguments != null && typeof arguments[0] == 'object' )
	{
			_obj = arguments[0];
	}
	var _width =  (_obj != null && _obj.width != null) ? _obj.width :300;
	var _height =  (_obj != null && _obj.height != null) ? _obj.height :100;
	
	var _left_default = ($(document).width()/2) - (_width/2);
	var _top_default = ($(window).height()/2) - (_height);
	//alert($(window).height() + " : "+ $(document).height());
	var _left = (_obj != null && _obj.left != null) ? _obj.left :_left_default;
	var _top = (_obj != null && _obj.top != null) ? _obj.top : _top_default ;
	var closeOnClick = (_obj != null && _obj.closeOnClick != null)?_obj.closeOnClick:true;
		
	var firstDiv = document.createElement('div');
	$(firstDiv).attr({'id':'mask', 'class':'pageDefaultMask'}).append();
	bodyElem.appendChild(firstDiv);
	
	var secDiv = document.createElement('div');
	$(secDiv).attr({'id':'hiddenformMessage', 'class':'cssWaitMessageDiv'})
			.css({'top':_top,'left':_left, 'width':_width, 'height':_height});
	if(closeOnClick)
	{
		$(secDiv).on('click', hideWaitMessage);
	}
//        var spinDiv = document.createElement('div');
//        $(spinDiv).attr({'id':'hiddenformMessage', 'class':'loader'});
//        $(secDiv).append($(spinDiv));
	$(secDiv).append($('<h2>').attr({'id':'waitMessage','class':'cssUserMessage'}))
			 .append($('<img>').attr('src', 'images/circular_skyline_logo.gif'));	 	
	bodyElem.appendChild(secDiv);
}

function showWaitMessage(obj)
{
	var _obj = null;
	var _text = null;
	
	if( typeof arguments[0] == 'object' )
	{
			_obj = arguments[0];
	}
	else
	{
		if (typeof jsonData != "undefined") {
			_text = (arguments[0] != null && arguments[0].length != "") ? jsonData["Processing"] : jsonData["Please_Wait"]; // kd 1072016 for multilingual purpose
		}
		else
		{
			_text = (arguments[0] != null && arguments[0].length != "") ? arguments[0] : "Please wait..."; // kd 1072016 only this string was before adding if..else condition
		}
	}
	
	$('#waitMessage').text(_text);
	
	$("#mask").show();
	$("#hiddenformMessage").show();
}

function hideWaitMessage()
{
	$("#hiddenformMessage").hide();
	$("#mask").hide();
} 

/* beta: create and attach div to be used with $.dialog() for user audit-trail comments 
 * the function creates, attaches and initializes the comment pop-up with $.dialog()
 * it returns the JQuery object of the dialog, and it should be kept in a global variable (otherwise, direct call to its id should be used)
 * In order to use the comment-dialog openCommentDialogAndCommit() function should be used, 
 * as it provides a handler to commit on 'OK' */
function initUserCommentDiv()
{	
	var bodyElem = $('body')[0]; //document.getElementsByTagName('body')[0];
	if (bodyElem == null)
		return;
	
	var eDiv = document.createElement('div');
	$(eDiv).attr('id', 'divUserCommentDialog')
		   .attr('title', 'Comment')
		   .data('maxLength', '-1');
	
	var eTable = document.createElement('table');
	$(eTable).css({'text-align':'center', 'width':'100%'})
			.append('<tr><td><br/></td></tr>')
			.append($('<tr>')							
							.append($('<td>')
											.css('text-align','left')
											.append($('<textarea>').attr('rows', 11)
																	.attr('id', 'txaUserComment')
																	.css({'width':'300px', 'margin-left':'10px', 'margin-right':'10px'}))
							)
			);
	
	eDiv.appendChild(eTable); 	
	bodyElem.appendChild(eDiv);
	
	// set dialog properties
	$( '#divUserCommentDialog' ).dialog({
		autoOpen: false,
		height: 350,
		width: 360,
		modal: true,		
		buttons: {
			OK: function() { 
					var _max = $(this).data('maxLength'); 
					var _handler = $(this).data('okHandler');
					var _comment = $.trim( $('#txaUserComment').val() );
					var _mandatory = $(this).data('commentMandatory');
					
					if( _max > 0 && _comment.length > _max)
					{
						displayAlertDialog(DispAlDial_Msg_Short_Name_V0_44, {replaceme:[{key:0,val:_max}]}); // kd 'The comment length should be up to ' + _max + ' chars'
						return false;
					}
					
					if (_mandatory && _comment == '' )
					{
 						displayAlertDialog("DispAlDial_Msg_Short_Name_T_146", { title:'Required Field Missing' });						
						return false;
					}
					else if(_comment != '' && $(this).data('validateComment'))
					{
						if( !fnValidateString({inStr:_comment, fieldName:"Comment", legals:$(this).data('legalChars'),illegals:$(this).data('illegalChars') }) )
						{           
							return false;
						} 
					}
					
					$(this).data('comment', _comment);
					if( _handler != null && typeof(_handler) == 'function' )
					{
						var _params = $(this).data('okHandlerParams');
						if ( $(this).data('incComment') )
						{
							_params.push( $(this).data('comment') );
						}
						
						_handler.apply(null, _params);
					}
					$( this ).dialog( "close" );
				},
			Cancel: function() {
					var _handler = $(this).data('cancelHandler');
					
					if( _handler != null && typeof(_handler) == 'function' )
					{
						_handler.apply(null, $(this).data('cancelHandlerParams'));
					}
					$( this ).dialog( "close" );
				}
			}
	});	
	
	return $( '#divUserCommentDialog' );
}

/* This function MUST be called in order to attach a comment to a process because the handler in case of cancel/close
 * is sent along. The function initiate confirmation handler, max text length and opens the dialog.
 * @Params: objParams - an object containing all/some of the relevant parameters. Used to add cancel-button-handler. USED ALONE without other parameters
 *			maxLength - maximum characters for comment; negative or 0 to indicate no chars limit
 *			handler - the local function to be called if OK was clicked (and comment length is between 1 and maxLength)
 *			params - [optional] an array of parameters to pass the handler function.
 *			isRetry - [optional] boolean. true - keep existing comment (used in case of failure response from server-side), false (default) - clear text. */
function openCommentDialogAndCommit(maxLength, handler, params, isRetry, objParams)
{
	var oDialog = $( '#divUserCommentDialog' );
	// check if dialog refers to the correct DIV
	if(oDialog.length == 0)
		return;
	
	var _passParams = false, 
		_clearComment = true, 
		_okHandler = null, 
		_maxLength = -1,
		_okHandlerParams = [],
		_cancelHandler = null, 
		_cancelHandlerParams = [],
		_includeCommentAsParam = false;
		_validateComment = false;
		_legalChars = null;
		_illegalChars = null;
		_isMandatory = true;
		
	if(arguments.length == 1 && typeof arguments[0] == 'object') 
	{
		var obj = arguments[0];
		_maxLength = (obj.maxLength == null) ? -1 : obj.maxLength;
		_okHandler = (obj.okHandler == null) ? null : obj.okHandler; 
		_includeCommentAsParam = (obj.includeComment == null) ? false : obj.includeComment;
		_okHandlerParams = (obj.okHandlerParams == null) ? [] : obj.okHandlerParams;		
		_cancelHandler = (obj.cancelHandler == null) ? null : obj.cancelHandler; 
		_cancelHandlerParams = (obj.cancelHandlerParams == null) ? [] : obj.cancelHandlerParams;
		_clearComment = (obj.clear == null) ? true : obj.clear;
		_validateComment = (obj.validateComment == null) ? false : obj.validateComment;
		_legalChars = (obj.legalChars == null) ? _legalChars : obj.legalChars;
		_illegalChars = (obj.illegalChars == null) ? _illegalChars : obj.illegalChars;
		_isMandatory = (obj.isMandatory == null) ? _isMandatory : obj.isMandatory;
	}
	else 
	{ // more than only an object sent (so object should be omitted!)
		_maxLength = maxLength;
		_okHandler = handler;
		
		// check if 3rd parameter is an array (used as parameters for handler call)
		if( arguments.length >= 3 && (arguments[2] instanceof Array) )
		{ 
			_okHandlerParams = params;
			_passParams = true;
		}
		
		if( arguments.length == 2 || // isRetry is omitted
			(((arguments.length == 3 && _passParams == false) || arguments.length == 4 ) // isRetry parameter was sent (3rd/4th place)
			&& ( arguments[arguments.length-1] == null || !arguments[arguments.length-1] ))) // isRetry == null OR == false
		{
			_clearComment = true;
		}
	}
	
	if( _clearComment )
	{
		$('#txaUserComment').val('');
		oDialog.data('comment', '');
	}
	$('#txaUserComment').attr('onkeyup', 'imposeMaxLength(this,'+_maxLength+')')
                                           .attr('onpaste', 'imposeMaxLength(this,'+_maxLength+')');
        
	oDialog.data('maxLength', _maxLength)
		   .data('okHandler', _okHandler)
		   .data('okHandlerParams', _okHandlerParams)
		   .data('cancelHandler', _cancelHandler)
		   .data('cancelHandlerParams', _cancelHandlerParams)
		   .data('incComment', _includeCommentAsParam)
		   .data('validateComment', _validateComment)
		   .data('legalChars', _legalChars)
		   .data('commentMandatory', _isMandatory)
		   .data('illegalChars', _illegalChars)
		   .dialog('open');
		   
}

/* beta: create and attach div to be used with $.dialog() as alert */
var alertDialog; 
$(function ()
{	
	var bodyElem = $('body')[0]; //document.getElementsByTagName('body')[0];
	if (bodyElem == null)
		return;
	
	var eDiv = document.createElement('div');
	$(eDiv).attr('id', 'divAlertDialog')
		   .attr('title', 'Alert');		   
	
	var eInnerDiv = document.createElement('div');
	$(eInnerDiv).attr('id', 'divAlertText').css('padding','7px')
	
	eDiv.appendChild(eInnerDiv); 	
	bodyElem.appendChild(eDiv);
	
	// set dialog properties
	$( '#divAlertDialog' ).dialog({
		autoOpen: false,
		show: 0,		
		modal: true,		
		close: function( e, ui ) {
			var focusId = $(this).data('hasFocus');
			var iFrame = $(this).data('focusInFrame');
			
			if( focusId )
			{
				if( iFrame && iFrame != '' )
				{
					$('#' + iFrame).contents().find('#' + focusId).focus();
				}
				else
				{
					$('#' + focusId).focus();
				}
			}
			// clear data so next alert won't focus on the wrong element
			$(this).data('hasFocus', '');
			$(this).data('focusInFrame', '');
		}		
	});	
	
	$( '#divAlertDialog' ).parent().on('keydown', 'button', function(e) {	
		if( e.which == 13 )
		{ 			
			$( '#divAlertDialog' ).dialog('close');
			return false;
		}
	});
	
	alertDialog = $( '#divAlertDialog' );
});

function displayAlertDialog(message, title, obj)
{
	var _title = null, _obj = null,
	_message = "", // kd 18052016 added multilingual
	dict = {}; // kd 06072016 added for using with variables in message lables
	
	if( arguments.length === 3 )
	{
		_title = title;
		_obj = obj;
	}
	else if( arguments.length === 2 )
	{
		if( typeof arguments[1] == 'object' )
		{
			_obj = arguments[1];
		}
		else
		{
			_title = arguments[1];
		}
	}
	
	if( _obj != null )
	{
		$( '#divAlertDialog' ).data('hasFocus', (_obj.focusOn != null ? _obj.focusOn : ''));
		$( '#divAlertDialog' ).data('focusInFrame', (_obj.frameId != null ? _obj.frameId : ''));
		if( _title == null && _obj.title != null )
		{
			_title = _obj.title;
		}
	}
	if (typeof jsonData != "undefined") {
		_message = jsonData[message]; // kd 18052016
		if( _title != null ) // kd 28082016
		{
			var titleFromLang = jsonData[_title];
			_title = titleFromLang?titleFromLang:_title;
		}
	} 
	if ( _message != "" && _message != null )
		{
			// kd 06072016  
			if ( _obj != null &&  _obj.replaceme != null )
			{
				dict = _obj.replaceme;
				for ( i = 0; i < dict.length; i++ ) 
				{
					_message = _message.replace( "{" + dict[i].key + "}", dict[i].val);
				}
			}
			// kd end
			$( '#divAlertDialog div' ).html(_message);
		}
	else
		{
			$( '#divAlertDialog div' ).html(message);
		}
	$( '#divAlertDialog' ).dialog('option', 'title', (_title != null) ? _title : 'Alert').dialog('open');
}

function clearCommentDialog()
{
	$('#txaUserComment').val('');
	$('#divUserCommentDialog').data('comment', '');
}

function initConfirmDialogDiv()
{	
	var bodyElem = $('body')[0]; //document.getElementsByTagName('body')[0];
	if (bodyElem == null)
		return;
	
	var eDiv = document.createElement('div');
	$(eDiv).attr('id', 'divConfirmDialog')
		   .attr('title', 'Please Confirm');		   
	
	var eInnerDiv = document.createElement('div');
	$(eInnerDiv).attr('id', 'divConfirmMessage').css('padding','7px');
	
	eDiv.appendChild(eInnerDiv); 	
	bodyElem.appendChild(eDiv);
	
	// set dialog properties
	$( '#divConfirmDialog' ).dialog({
		autoOpen: false,
		show: 0,
		modal: true, 
		buttons: [{
            id  :   "btnConfirm",
            text    :   "Confirm",
            tabIndex:   -1,
            click   :   function() { 					
				var _handler = $(this).data('confirmHandler');					
					 
				$( this ).dialog( "close" );	
				 
				if( _handler != null && typeof(_handler) == 'function' )
				{
					_handler.apply(null, $(this).data('confirmHandlerParams'));
				}		
			} 
        },{
            id      :   "btnCancelConfirm",
            text    :   "Cancel", 
            tabIndex:   -1,
            click   :   function() {
				var _handler = $(this).data('cancelHandler');					
						
				$( this ).dialog( "close" );
				
				if( _handler != null && typeof(_handler) == 'function' )
				{
					_handler.apply(null, $(this).data('cancelHandlerParams'));
				}  
			}
        }]	
	});	
	
	return $( '#divConfirmDialog' );
}

function openConfirmDialog(obj)
{
	var oDialog = $( '#divConfirmDialog' );
	// check if dialog was instantiated
	if(oDialog.length == 0)
		return;
		
	var _message = (obj.message == null) ? "Are you sure?" : obj.message, 
		_okHandler = (obj.onConfirm == null) ? null : obj.onConfirm, 
		_okHandlerParams = (obj.onConfirmParams == null) ? [] : obj.onConfirmParams, 
		_cancelHandler = (obj.onCancel == null) ? null : obj.onCancel, 
		_cancelHandlerParams = (obj.onCancelParams == null) ? [] : obj.onCancelParams, 
		_title = (obj.title == null) ? "Please Confirm" : obj.title,
		_replaceme = (obj.replaceme == null) ? null : obj.replaceme, // kd 08082016 for using with variables in labels of message
		msg; // for use multilanguage
		
		// kd 08082016  
		if (typeof jsonData != "undefined") {
			msg = jsonData[_message]; 
			// kd 22112016
			if (_title != null) {
				tempVar = _title;
				_title = jsonData[_title];
				if (typeof(_title) == "undefined") {
					_title = tempVar;
				}
			} // kd end
		}
		if ( msg != "" && msg != null )
		{
			if ( _replaceme != null )
			{
				for ( i = 0; i < _replaceme.length; i++ ) 
				{
					msg = msg.replace( "{" + _replaceme[i].key + "}", _replaceme[i].val);					
				}
			}
			$( '#divConfirmDialog div' ).html(msg);
		}
		// kd end
		else
		{
			// set message
			$( '#divConfirmDialog div' ).html(_message);
		}
		
	// set parameters and open dialog
	oDialog.data('confirmHandler', _okHandler)
		   .data('confirmHandlerParams', _okHandlerParams)
		   .data('cancelHandler', _cancelHandler)
		   .data('cancelHandlerParams', _cancelHandlerParams)
		   .dialog('option', 'title', _title)
		   .dialog('open');
}

function initDatePickerWithOptionsByClass(cssClass, opt)
{
	var _opt = (opt)?opt:{};
	_opt.targetType = "class";
	initDatePickerWithOptions(cssClass, opt);
}

function initDatePickerWithOptions(target, options)
{	
	var opt = (options)?options:{};
	var _targetTypeDefault = 'id';
    var _showOn = (opt.showOn)? opt.showOn : "button";
    var _showBP = true; //show button panel
    var _isShowBP = (opt.showButtonPanel)? opt.showButtonPanel : _showBP;
    var _dateFormat = (opt.dateFormat)? opt.dateFormat : 'dd/mm/yy';
    var _disabled = (opt.disabled)? opt.disabled : false;
    var _defaultDate = (opt.defaultDate) ? opt.defaultDate : null;
    var _handler = (opt.onSelect)? opt.onSelect : onSelectDateDefault; 
	var _handlerBeforeShow = (opt.beforeShow)? opt.beforeShow : null; 
	var _isRemoveClear = (opt.removeClear) ? opt.removeClear : false; 
	var _altField = (opt.altField) ? opt.altField : null;
	var _altFormat = (opt.altFormat) ? opt.altFormat : ['dd-mm-yy','ddmmyy'];
	var _targetType = (opt.targetType)?opt.targetType:_targetTypeDefault;
	var _raiseFlagOnChange = (opt.raiseFlagOnChange)?opt.raiseFlagOnChange:false;
	var $datepickerElement = null;
	switch(_targetType) {
		case 'id':	$datepickerElement = $("#" + target);
					break;
		case 'class':	$datepickerElement = $("." + target);
					break;
		case 'element':	$datepickerElement = target;
					break;
	}

	if($datepickerElement == null)
	{
		console.error("Error on datepicker init function");
		return;
	}

	$datepickerElement.datepicker
    ({
        showOn: _showOn,
        showButtonPanel: _isShowBP,
        dateFormat: _dateFormat,
        buttonText: "",
        buttonImage: "images/calendar.png",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        disabled: _disabled,
        defaultDate: _defaultDate,
		altField: _altField,
		altFormat: _altFormat,
		constrainInput: false, // ef task 7078
        beforeShow: function(input) 
        {		
            if(_isShowBP)
			{
                overrideDefaultButtonPanel(input, _handler, _isRemoveClear);
			}
			if( _handlerBeforeShow != null)
			{
				return _handlerBeforeShow(input);
			}  
			if(opt.elementsInRange)	
			{
				return restrictDateRange(input, opt.elementsInRange)
			}
		  	
        },
        onSelect: function(dateText, inst) {
			_handler.apply(inst, [dateText]);
			if(_raiseFlagOnChange) raiseChangeFlag();
		},
        onChangeMonthYear:function( year, month, inst )
        { 
            if(_isShowBP)
                overrideDefaultButtonPanel(inst.input, _handler, _isRemoveClear);
        }
    }).on('change', function() { 
    	var input = this;
		var doContinue = validDateWithMomentJS(input);
		if(doContinue && opt.elementsInRange)
		{
			doContinue = validateDateRange(input, opt.elementsInRange);
		}
		if(doContinue)
		{
    		_handler.apply(input, [$(input).val()]);
			if(_raiseFlagOnChange) raiseChangeFlag();
		}
		
    });
	$datepickerElement.prop('readonly', false);
	if($datepickerElement.val() == "")
	{
		$datepickerElement.val('00/00/0000');
	}
}

function restrictDateRange(currElem, elementsInRange)
{
	var min = null, max = null; 		
			
	if (currElem.getAttribute('dateRange') === 'fromDate')
	{	
		var $to = elementsInRange.to;
		// set max date if relevant						
		max = ($to.val() == '00/00/0000')?"":$to.datepicker('getDate');
	}
	else //toDate
	{	
		var $from = elementsInRange.from;
		// set min date if relevant
		min = ($from.val() == '00/00/0000')?"":$from.datepicker('getDate');	
	}
	return {minDate: min, maxDate: max};
}

function validateDateRange(input, elementsInRange)
{
	var $to = elementsInRange.to;	
	var $from = elementsInRange.from;				
	var toDate = ($to.val() == '00/00/0000')?"":$to.datepicker('getDate');
	var fromDate = ($from.val() == '00/00/0000')?"":$from.datepicker('getDate');	
	if(fromDate == "" || toDate == "") return true;
	if(fromDate > toDate)
	{
		displayAlertDialog('Invalid Date range');
		$(input).val('00/00/0000');
		return false;
	}

	return true;
}

function validDateWithMomentJS(date)// ef task 7078
{	
	var dateFormat = 0;
	var firstFormat = moment(date.value,"DD/MM/YYYY",true);	
	var secondFormat  = moment(moment(date.value,"DD-MM-YYYY",true).format("DD/MM/YYYY"),"DD/MM/YYYY",true);
	if(firstFormat.isValid())
	{
		dateFormat = firstFormat;
	}
	else if(secondFormat.isValid())
	{
		dateFormat = secondFormat;
	}	
	if ((dateFormat == 0) || (dateFormat.format('YYYY')=="0000"))
	{
		displayAlertDialog('Invalid Date');
		$(date).val('00/00/0000');
		return 0;
	}

	if(parseInt(dateFormat.format('YYYY')  /1000) == 0)
	{
		if(parseInt(dateFormat.format('YYYY')  /100) != 0)
		{
			displayAlertDialog('Invalid Date');
			$(date).val('00/00/0000');
			return 0;
		}
		dateFormat.add(2000,'years')
		$(date).val(dateFormat.format('DD/MM/YYYY'));
	}
	
	return 1;
}

function overrideDefaultButtonPanel(input, handler, isRemoveClear)//input - HTMLInputElement Object
{
    setTimeout(function() 
    {
         var btnWidget = $( input ).datepicker( "widget" ); 
            /* override 'Today' button */ 
             btnWidget.find('.ui-datepicker-current').removeClass('ui-priority-secondary').addClass('ui-priority-primary')
             .text('Today')
             .click(function() 
             {				                
                $(input).datepicker('setDate', new Date()).datepicker('hide');
                $('.ui-datepicker-current-day').click();
             });
             
			if( isRemoveClear )
			{
				btnWidget.find('.ui-datepicker-close').remove();
			}
			else
			{            
				/* override 'Done' button */ 
				btnWidget.find('.ui-datepicker-close')
				.text('Clear')
				.click(function() 
				{
					$.datepicker._clearDate(input);
					$(input).datepicker( "hide" );     
					$(input).val("00/00/0000");               
				});
			}
     }, 1);
}

function onSelectDateDefault()
{	
    return;
}

function CorrectDateFormat(oTime)
{
	var time = fnTrimString(oTime.value);
	var isColonExist;

	if(time.indexOf(":") > -1)
	{
		isColonExist = true;
	}
	else
	{
		isColonExist = false;
	}

	
	if(time.length == 3 && !isColonExist)
	{
		time = "0" + time;
	}

	if(time.length == 4 && isColonExist)
	{
		time = "0" + time;
	}

	if(time.length == 4 && !isColonExist)
	{
		time = time.substr(0, 2) + ":" + time.substr(2);
	}    

	oTime.value = time;
}

function validateTime(timeString)
{
	
	var reTime =  /^([01]?[0-9]|[2][0-3])(:[0-5][0-9])?$/;
		
	if(!reTime.test(timeString))
	{
		return false;
	}
	else
	{
		return true;
	}

}    

/* This function is used for jQuery's datepicker, when the format is dd/mm/yyyy, and date field is not "readonly".
 * The datepicker accepts any legal date with slashes (e.g, d/m/yyyy, dd/n/yyyy, etc.) so we only have to insert slashes
 * in the most reasonable places and fill in with 0s. Still, we later have to validate the date (for example, 30/2/2012 is "legal" format, yet illegal date)
 * Practically, the count starts from the end, and assume 4-digit year. 
 * If day+month are given only 3 digits, the month will "receive" 2 if possible (i.e 10 - 12), otherwise the day receives 2 digits.
 * The function both updates the value of the date-field, and returns it - so we can use it directly */
function fnCorrectDateFormat(oDate)
{
	var date = fnTrimString( $(oDate).val() );
	var correctedDate = "", year, month, day;
	var isSlashExist = (date.indexOf("/") > -1) ? true:false;	
	var parsedDate = "";
	
	if (!isSlashExist)
	{			
		// we use slice() because IE doesn't support substr() with negative index. 	
		year = date.slice(-4, date.length); 
		if (date.length > 7) // assumed: dd/mm
		{
			month = date.slice(-6, -4); // as we sliced, date consists of only dd/mm
			day = date.substr(0, 2); 
		}
		else if (date.length > 6) // assumed: d?d/m?m
		{
			if ( date.charAt(date.length-6) === '0' || (date.charAt(date.length-6) === '1' && date.charAt(date.length-5) <= '2') ) // legitimate for a month
			{ // d/mm
				month = date.slice(-6, -4);	
				day = "0" + date.substr(0, 1);
			}
			else
			{ // dd/m
				month = "0" + date.slice(-5, -4);
				day = date.substr(0, 2);
			}
		}
		else // assumed: d/m
		{
			month = "0" + date.substr(1, 1);
			day = "0" + date.substr(0, 1);
		}
		
		correctedDate = day + "/" + month + "/" + year;
	}
	else // date already includes slashes
	{ 
		correctedDate = date;
	}
	
	try {
		parsedDate = $.datepicker.parseDate('dd/mm/yy', correctedDate); 
		$(oDate).datepicker('setDate', parsedDate);	
		correctedDate = $(oDate).val(); // as picker has corrected format
	} catch (e) { 
		// thrown when parser dims invalid date 
		parsedDate = "00/00/0000";
	}
	
	return {corrected: correctedDate, parsed: parsedDate}; // parsed= "00/00/0000" indicates illegal date
}

/*	gets input text element that suppose to hold time format, verifies its validity and tries to format it as HH24:MM format.
 *	returns an object with the above properties:
 *	isValid: true/false - whether the time is valid. 
 *	corrected: if time is valid, returns it in HH24:MM format, otherwise, return the original text from the field	
 *	note: if time is valid, the actual element text is reformatted (if needed) as corrected time.*/
function fnCorrectTimeFormat (oTime)
{
	var time = $.trim( $(oTime).val() );
	var correctedTime = time;
	var isTimeValid = true; 
	
	if (time == null)
	{
		return {corrected: null, isValid: false};
	}
	
	var isColonExist = (time.indexOf(":") > -1) ? true:false;
   
	if( time.length == 3 && !isColonExist || 
		time.length == 4 && isColonExist )
	{
		time = "0" + time; 
	}
	
	if(time.length == 4 && !isColonExist)
	{
		time = time.substr(0, 2) + ":" + time.substr(2);
	}
	else if (time.length != 5)
	{
		isTimeValid = false;
	}

	if ( isTimeValid )
	{
		var timeRegex = /(([01][0-9])|([2][0-3])):[0-5][0-9]/g; 
		if ( !timeRegex.test(time) )
		{
			isTimeValid = false;
		}
		
		if ( isTimeValid )
		{		
			$(oTime).val(time);
			correctedTime = time;
		}
	}
	
	return {corrected: correctedTime, isValid: isTimeValid};
}

function fnTrimString (stringToTrim) 
{
	if (stringToTrim != null)
		return stringToTrim.replace(/^\s+|\s+$/g,"");
		
	return '';
}

function checkDirection (str, objId)
{   
	setElementDirection(document.getElementById(objId), str);
}

function setElementDirection(el, value)
{
	if(value != null || value != "")
	{
		if( checkRtl(value) ) 
		{                         
			$(el).removeClass("cssTableCellLTR").addClass("cssTableCellRTL");
		} 
		else 
		{
			$(el).removeClass("cssTableCellRTL").addClass("cssTableCellLTR");
		}; 
	}
}

function getDirectionClass(value)
{
	var dirClass = "";
	if(value != null || value != "")
	{
		if( checkRtl(value) ) 
		{                         
			dirClass = "cssTableCellRTL";
		} 
		else 
		{
			dirClass = "cssTableCellLTR";
		}; 
	}
	return dirClass;
}

function checkRtl( character ) 
{
	var ltrChars            = 'A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02B8\u0300-\u0590\u0800-\u1FFF'+'\u2C00-\uFB1C\uFDFE-\uFE6F\uFEFD-\uFFFF';
	var rtlChars            = '\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC';
	//var ltrDirCheckRe       = new RegExp('^[^'+rtlChars+']*['+ltrChars+']');
	var rtlDirCheckRe       = new RegExp('^[^'+ltrChars+']*['+rtlChars+']');

	return rtlDirCheckRe.test(character);
};

function focusInput(currEvent)
{       
	
	var event = window.event || currEvent;
	var e = event.target || event.srcElement;
	//alert(window.event + "  :  " + e);
	if (e == null)
	{
		return;
	}
	//alert(event.type);
	if (event.type == 'focus')
	{
		currColor = e.style.borderColor;
		e.style.border = '1px solid';
		e.style.borderColor = '#5897fb';
	}
	else if(event.type == 'blur') //for support to IE11(cause Event Object may return other event.types)
	{
		e.style.borderColor = currColor;
		e.style.border = '#aaa 1px solid';
	}
	
}

function imposeMaxLength(limitField,  limitNum) 
{
    if ($(limitField).val().length > limitNum)
    {
        var text = $(limitField).val().substring(0, limitNum);
        $(limitField).val(text);
    }
}

/* used on paste(also from Edit menu, or mouse click) */
function imposePasteMaxLength(Object, MaxLen)
{ 
	setTimeout(function () {
	 $(Object).val($(Object).val().substr(0, MaxLen));
	  }, 0);
}

function exitPageWarning(obj)
{
	var _message = "Click <b>Confirm</b> to exit without saving your changes",
		_title = "Warning",
		_isChanged = bOnChange,
		_onConfirm = null;
	
	if( obj != null && typeof obj == 'object' )
	{
		_message = (obj.message == null) ? _message : obj.message;
		_title = (obj.title == null) ? _title : obj.title;
		_isChanged = (obj.isChanged == null) ? _isChanged : obj.isChanged;
		_onConfirm = (obj.onConfirm == null || typeof obj.onConfirm != 'function') ? null : obj.onConfirm;
	}
	
	if( _isChanged )
	{
		if( $('#divConfirmDialog').length == 0 )
		{
			initConfirmDialogDiv();
		}
		
		openConfirmDialog({ title:_title, message:_message, onConfirm:_onConfirm });
	}
	else if( _onConfirm )
	{
		_onConfirm();
	}
}

function ignoreComment(obj)
{
	var _isChanged = bOnChange,
		_onConfirm = null;
	
	if( obj != null && typeof obj == 'object' )
	{
		_isChanged = (obj.isChanged == null) ? _isChanged : obj.isChanged;
		_onConfirm = (obj.onConfirm == null || typeof obj.onConfirm != 'function') ? null : obj.onConfirm;
	}
	
	if( _isChanged )
	{
		
		_onConfirm();
		
	}
}

function confirmExitPageWarning(obj)
{
	exitPageWarning({ isChanged:bOnChange, onConfirm:obj.onConfirmFunc});
}

function ignoreCommentIfNoChanges(obj)
{
	ignoreComment({ isChanged:bOnChange, onConfirm:obj.onConfirmFunc});
}

function confirmMainMenuExitPageWarning(href)
{
	exitPageWarning({ isChanged:bOnChange, 
					onConfirm:function()
					{
						window.location.href=href;
        				return true;
					}
	});
}

function raiseChangeFlag()
{
	bOnChange = true;
	parent.bChildWindowChanged = true;
} 

function declineChangeFlag()
{
	bOnChange = false;
	parent.bChildWindowChanged = false;
}

function setParametrOfChange()
{
   if(document.getElementById("actionid"))
   {
        if(document.getElementById("actionid").value == "edit")
            bOnChange = true;
   }
   else
   {
       if($('#action_id').val() == "edit")
            bOnChange = true;
    }
}

// Check if user using IE
function isIE()
{
	var ua = window.navigator.userAgent;
	
	var msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
	   // Edge (IE 12+) => return version number
	   return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}

	// other browser
	return false;
}

// Define modalDialog to show: for IE or Chrome
function isOriginShowModalDialog() 
{
	if(isIE())
	{
		return true;
	}
	else
	{
		return false;
	}
}
//Define showModalDialog for none IE browsers
function showModalDialogForChrome()
{	
	window.showModalDialog = window.showModalDialog || function(url, arg, opt, objOnClose,obj) {
		url = url || ''; //URL of a dialog
		arg = arg || null; //arguments to a dialog	       
		opt = opt || 'dialogWidth:300px;dialogHeight:100px'; //options: dialogTop;dialogLeft;dialogWidth;dialogHeight or CSS styles	       
		var caller = showModalDialog.caller.toString();	        
		if (parent.parent.document.getElementById('dialog1')!= null ) {	// IF Dialog opened in an exist Dialog	  
			parent.parent.document.getElementById('dialog-close').style.visibility = "hidden";
		}
		var dialog = parent.document.body.appendChild(document.createElement('dialog'));
		dialog.setAttribute("id", "dialog1");
		dialog.setAttribute('style', opt.replace(/dialog/gi, ''));
		var myHtml='<a href="#" id="dialog-close" style="position: absolute; top: 0; right: 4px; font-size: 20px; color: #000; text-decoration: none; outline: none;">&times;</a><iframe id="dialog-body" src="' + url + '" style="border: 0; width: 100%; height: 100%;"></iframe>';
	  
		dialog.innerHTML = myHtml;
		parent.document.getElementById('dialog-body').contentWindow.dialogArguments = arg;
		dialog.style.border="solid 4px #006d77";	  
	   
	   parent.document.getElementById('dialog-close').addEventListener('click', function(e) {
			e.preventDefault();	
			dialog.close();
		});	
		dialog.showModal();	        
	   
		dialog.addEventListener('close', function() {
			 if (parent.parent.document.getElementById('dialog1')!= null ) {	// IF Dialog opened in an exist Dialog	  		  
				 parent.parent.document.getElementById('dialog-close').style.visibility = "visible";
			 }
			var returnValue = parent.document.getElementById('dialog-body').contentWindow.returnValue;
			parent.document.body.removeChild(dialog);
			if(objOnClose != null)//Callback function
				objOnClose.onClose.apply(this, [returnValue,obj]);
			else return returnValue;
		});	     
	};
}
 //Closing dialog for Chrome
function closeDialog()
{	
	//var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
	var isChrome = ((navigator.userAgent.toLowerCase().indexOf('chrome') > -1) &&(navigator.vendor.toLowerCase().indexOf("google") > -1)); //kd 120819 Old row (previos row, commented) not worked on server: window.chrome.runtime = undefined
	if ( isChrome && parent.parent.document.getElementById('dialog1')) 
	{
		parent.parent.document.getElementById('dialog1').close();
	}
}

/* allows to "parse" nulls as ints, parsing the defVal instead. Useful with comparison when one of the fields might be omitted
 * inStr - string to parse (might be null or NaN)
 * defVal - default value if inStr is unparsable; the function defaults defVal to 0 if it's unparsable
 * defOnNaN - if true a NaN is treated as null; otherwise the function returns NaN as the original parseInt */
function parseIntExtended (inStr, defVal, defOnNaN)
{
	if (defVal == null || isNaN(defVal))
	{
		defVal = 0;
	}
	
	if (inStr == null || inStr.length == 0)
	{
		inStr = defVal;
	}
	
	if (defOnNaN && isNaN(inStr))
	{
		inStr = defVal;
	}
	
	return parseInt(inStr);
}

function logOff()
{
	top.location.href = 'Login.jsp';
}
    
/* return index of TAB by tab id */
function getTabIndexById(id)
{
	var index = $('#tabs a[id="'+id+'"]').parent().index();
	return index;
}

/* return ID of active tab */
function getActiveTabId()
{
	var tabIndex = $("#tabs").tabs('option', 'active');        
	var activeTabID = $("#tabs ul>li a").eq(tabIndex).attr('id'); 
	return activeTabID;
}

/*
 * Function designed to simulate standard checkbox behavior for element that replace standard browser checkbox
 * Example use: 
 * 		- for set checkbox as checked/unchecked call should be: $(element).checkbox('checked',true|false|1|0)
 * 		- for get checkbox state (checked/unchecked):
 * 			- in number equivalent call : $(element).checkbox('val')
 * 			- in boolean equivalent call : $(element).checkbox('checked') or $(element).checkbox('prop')
 */
(function ($) {
	$.fn.checkbox = function (oper, val) {
		var $element = $(this);
		var arg_len = arguments.length;

		if (arg_len > 0) {
			try {
				if (oper === 'val' || oper === 'prop' || oper === 'checked') {
					var isChecked = $element.hasClass('checked');
					if (arg_len === 1) //get value
					{
						return (oper === 'val') ? (isChecked ? 1 : 0): isChecked;
					}
					else if (arg_len === 2) //set value
					{
						// convert entry value to be treated as boolean 
						var strVal = val.toString();
						val = (strVal == "true" || strVal == "1") ? true : false;

						if (val && !isChecked) $element.addClass('checked');
						if (!val && isChecked) $element.removeClass('checked');
					}
				}
				else if(oper === 'disabled') {
					var isDisabled = $element.hasClass('disabled');
					if (arg_len === 1) //get value
					{
						return isDisabled?true:false; 
					}
					else if (arg_len === 2) //set value
					{
						// convert entry value to be treated as boolean 
						var strVal = val.toString();
						val = (strVal == "true") ? true : false;

						if (val && !isDisabled) $element.addClass('disabled');
						if (!val && isDisabled) $element.removeClass('disabled');
					}
				}
			} catch (e) {
				console.error("checkbox(): ", e);
			}
		}

	}
})(jQuery)

// handle click event of custom 'checkbox' element 
function checkboxToggleCheck(elem) {
	$(elem).toggleClass("checked");
}

function initFloatingButtonPanel() {
	var $buttonsContainer = $('#divFloatingButtonsPanelContainer');
	$buttonsContainer.css('display', 'inline-block');
	$('.floatingButtonsPanel').css('display', 'inline-block');

	setFloatingButtonPanelPosition();
	window.addEventListener('resize', function(event){
		setFloatingButtonPanelPosition();
	});

	var uiTabs = $("#tabs ul.ui-tabs-nav > li a");
	if(uiTabs.length > 0)
	{
		// initFloatingButtonsPanel - on TABs click
		uiTabs.on("click", function() {
			setFloatingButtonPanelPosition();
		})
	}
}

function setFloatingButtonPanelPosition()
{
	var $window = $(window);
	var $buttonsContainer = $('#divFloatingButtonsPanelContainer');
	var $leftPanel = $('#divFloatingButtonsShowHideIcon');
	
	if($buttonsContainer.css('display') != 'none') {
		if($leftPanel.find('.fa-angle-right').length > 0)
		{
			var $rightPanel = $('.floatingButtonsPanel');
			var panelLeftPos = window.outerWidth - ($leftPanel.width()+$rightPanel.outerWidth()) - (window.outerWidth - $window.width())  + $window.scrollLeft();
			// console.log("panelLeftPos: "+ panelLeftPos + " left+right: "+$leftPanel.width() + "/" + $rightPanel.outerWidth() + " container width: " + $buttonsContainer.width());
			$buttonsContainer.offset({'left':panelLeftPos});
		}
		else
		{
			var panelLeftPos = $window.width() - $leftPanel.width()  + $window.scrollLeft();
			$buttonsContainer.offset({'left':panelLeftPos});
		}
	}


}

function floatingButtonPanelToggleClick(elem) {
	var $window = $(window);
	var $buttonsContainer = $('#divFloatingButtonsPanelContainer');
	var $rightPanel = $('.floatingButtonsPanel');
	if ($(elem).hasClass('fa-angle-left')) // show buttons
	{
		$(elem).removeClass('fa-angle-left').addClass('fa-angle-right');
		var $leftPanel = $('#divFloatingButtonsShowHideIcon');
		$rightPanel.css('display', 'inline-block');

		var panelLeftPos = window.outerWidth - ($leftPanel.width()+$rightPanel.outerWidth()) - (window.outerWidth - $window.width()) + $window.scrollLeft();
		// console.log("panelLeftPos: "+ panelLeftPos + " left+right: "+$leftPanel.width() + "/" + $rightPanel.outerWidth()+ " container width: " + $buttonsContainer.width());
		$buttonsContainer.offset({ 'left': panelLeftPos });
	}
	else // hide buttons
	{
		$(elem).removeClass('fa-angle-right').addClass('fa-angle-left');

		$rightPanel.css('display', 'none');
		var panelLeftPos = $window.width() - $(elem).parent().width() + $window.scrollLeft();
		$buttonsContainer.offset({ 'left': panelLeftPos });
	}

}

function destroyFloatingButtonPanel()
{
	var $buttonsContainer = $('#divFloatingButtonsPanelContainer');
	$buttonsContainer.css('display', 'none');
	$('.floatingButtonsPanel').css('display', 'none');
}

function initFloatingTabsPanel()
{
	$('.floatingTabsPanel,.floatingTabsShowHideIcon').on('mouseenter',function(){
		floatingTabsPanelToggleClick($(this).parent());
	})
	.on('mouseleave',function(){
		floatingTabsPanelToggleClick($(this).parent());
	});
}

function floatingTabsPanelToggleClick(elem) {
	var $window = $(window);
	
	var $tabsPanel = $('.floatingTabsPanel');

	if ($tabsPanel.hasClass('tabs-preview')) // show tabs
	{
		$('.floatingTabsShowHideIcon').css('display', 'none');
		$('.floatingTabsPanel').removeClass('tabs-preview').addClass('tabs-full-view');
		var panelLeftPos = $window.scrollLeft();
		var $container = $('#divFloatingTabsPanelContainer');
		$container.offset({ 'left': panelLeftPos });
	}
	else // hide tabs
	{
		$('.floatingTabsShowHideIcon').css('display', 'block');
		$('.floatingTabsPanel').removeClass('tabs-full-view').addClass('tabs-preview');
		var $container = $('#divFloatingTabsPanelContainer');
		var panelLeftPos = $(elem).width() - $container.width() + $window.scrollLeft();
		$container.offset({ 'left': panelLeftPos });
	}

	// *** code for toggling with right/left arrow ***
	// var $icon = $(elem).find('i');
	// if ($icon.hasClass('fa-angle-right')) // show buttons
	// {
	// 	$icon.removeClass('fa-angle-right').addClass('fa-angle-left');

	// 	$('.floatingTabsPanel').css('display', 'block');
	// 	var panelLeftPos = $window.scrollLeft();
	// 	var $container = $('#divFloatingTabsPanelContainer');
	// 	$container.offset({ 'left': panelLeftPos });
	// }
	// else // hide buttons
	// {
	// 	$icon.removeClass('fa-angle-left').addClass('fa-angle-right');

	// 	$('.floatingTabsPanel').css('display', 'none');
	// 	var $container = $('#divFloatingTabsPanelContainer');
	// 	var panelLeftPos = $(elem).width() - $container.width() + $window.scrollLeft();
	// 	$container.offset({ 'left': panelLeftPos });
	// }

}

/* 
	handle click listener on collapse/expand icon 
*/
function toggleSectionCollapse(elem) {
	var $span = $(elem);
	var title = ($span.hasClass("collapse-icon"))?'Expand':'Collapse';
	$span.toggleClass("collapse-icon expand-icon")
			.attr('title', title)
			.closest(".section-parent")
			.find(".section-toggle-content")
			.toggle();
}

function toggleAllStaticSectionsCollapse(doCollapse)
{
	toggleAllSectionsCollapseByClass(doCollapse,'static-section');
}

function toggleAllNotebookSectionsCollapse(doCollapse)
{
	toggleAllSectionsCollapseByClass(doCollapse,'notebook-section');
}

function toggleAllSectionsCollapseByClass(doCollapse, className) {

	$('.'+className).each(function(inx, el){
		var $section = $(el);
		$span = $section.find('.span-collapse-expand');
		var isSectionCollapsed = !$span.hasClass('collapse-icon');
		if((doCollapse && !isSectionCollapsed)
				||
			(!doCollapse && isSectionCollapsed)
		)
		{
			$span.trigger('click');
		}
	});
}

function isSectionCollapsed($section) {
	return $section.find('.span-collapse-expand')
					.hasClass('collapse-icon') ? false : true;
}

// handle click listener on maximize/minimize icon 
function toggleSectionFullScreen(elem) {
	var $icon = $(elem);
	if ($icon.hasClass("maximize-icon")) {
		$icon.removeClass("maximize-icon").addClass("minimize-icon");
		$icon.attr('title', 'Minimize');
	}
	else {
		$icon.removeClass("minimize-icon").addClass("maximize-icon");
		$icon.attr('title', 'Maximize');
	}
	$icon.closest(".section-parent").toggleClass("full-screen-section");
}

function fnCreateTableRowActionIcons(configArr)
{
	var returnHtml = "";
	var config = (configArr == null)?[]:configArr;
	var tableRowIcons = {edit:"edit-icon",new:"new-icon",copy:"copy-icon",remove:"remove-icon",add_attachment:"add-attachment-icon"};
	for(var i=0; i<config.length;i++)
	{
		var _o = config[i];
		var disabled = (_o.disabled)?"disabled":"";
		if(_o.iconType && tableRowIcons.hasOwnProperty(_o.iconType))
		{
			var tooltip = (_o.tooltip)?'title="'+_o.tooltip+'"':'';
			var onClickAction = 'onClickAction="'+_o.iconType+'"';
			returnHtml += '<span class="app-icon ' + tableRowIcons[_o.iconType]+' '+disabled+'" '+tooltip+' '+onClickAction+'></span>';
		}
	}
	return returnHtml;
}

function fnGetTableRowIconActionNameByEvent(event)
{
	// console.log(event.target.nodeName);
	if(event && event.target.nodeName && event.target.nodeName.toLowerCase() === "span" && event.target.hasAttribute('onClickAction'))
	{
		return event.target.getAttribute('onClickAction');
	}
	return null;
}
    
function createModalDialog(obj)
{
	if(isAlive() == 0)
	{
		logOff();
		return;
	}
	var $dialogElem, dialogId;
	var defaultIframeId = "iframeDialogDiv";
	var heightDiff;
	// if current dialog fields are tracked for changes, use this prop
	var trailDialogChanges = (obj.trailDialogChanges)?obj.trailDialogChanges:false;
	var dialogIsIframe = (obj.isIframe)?obj.isIframe:false;
	// define if dialog should be attached to parent window (in case dialog opened from Tab), relevant to iframes only
	var attachIframeToParent = (dialogIsIframe && obj.attachToParent)?obj.attachToParent:false;
	if(attachIframeToParent)
	{
		dialogId = defaultIframeId; // set constant id only when dialog created in parent window
		// $dialogElem = $('#' + dialogId, window.parent.document);
	}
	else {
		dialogId = (dialogIsIframe)?defaultIframeId:obj.dialogId;
		$dialogElem = $('#' + dialogId);
	}
	if(!$dialogElem || $dialogElem.length == 0 || !isDialogInstanceExists($dialogElem))
	{
		var $dialogDiv;
		var toDestroy = (obj.isDestroyDialog)?obj.isDestroyDialog:false;
		if(dialogIsIframe) {
			// open iframe inside dialog
			var $div = $('<div id="'+dialogId+'" style="overflow-y:hidden;position: relative;">');
			var $iframe = $('<iframe id="iframeDialog" style="border: 0px;width:100%;height:100%;position: relative;" src="about:blank"></iframe>');
			$div.append($iframe);		
			if(attachIframeToParent)
			{
				$dialogDiv = $(parent.document.body.appendChild($div[0]));
			}
			else {
				$dialogDiv = $(document.body.appendChild($div[0]));
			}
		}
		else {
			$dialogDiv = $dialogElem;
		}
		$dialogElem = $dialogDiv.dialog({
						autoOpen : false,
						modal : true,
						height : obj.height,
						width : obj.width,
						title: (obj.title)?obj.title:'',
						resizable: (obj.isResizable)?true:false,
						tabIndex:   -1,
						buttons:(obj.buttons)?obj.buttons:{},
						resizeStop: function( event, ui ) {
							if(dialogIsIframe) 
							{
								//workaround to fix 'iframeDialogDiv' width and height issues when resized
								$(event.target).css({'width':'100%','height':($(event.target).parent().height()-heightDiff) + 'px'});
							}
						},
						open: function (event, ui) {
							if(trailDialogChanges)
							{
								// override dialog close button, to confirm closing if there are changes in dialog fields
								bChildWindowChanged = false;
								$(event.target).parent().find('.ui-dialog-titlebar-close').off().on('click', function(el)
								{
									// console.log("isChanged", bChildWindowChanged);
									exitPageWarning({ isChanged:bChildWindowChanged, onConfirm:function()
									{
										bChildWindowChanged = false;
										$dialogElem.dialog('close');
									}});
								});
							}
							//heightDiff = dialog config height - iframeDialogDiv start height
							heightDiff = obj.height - $(event.target).outerHeight();
						},
						close : function() {
							// console.log("parent close func");
							if(dialogIsIframe)
							{
								var iframeContents = $dialogElem.find('iframe').contents();
								var returnValue = iframeContents.find('#iframeReturnValue').val();							
								console.log("toReturn",returnValue);

								if(returnValue == 'TIME_OUT')
								{
									logOff();
									return;
								}
								// force destroy iframe dialog instance
								destroyModalDialog($dialogElem);

								//Callback function
								if(obj.onClose != null) {
									// console.log("onClose function", obj.onClose);
									var _closeParams = (obj.onCloseParams == null) ? [] : obj.onCloseParams;
									obj.onClose.apply(this, [returnValue, _closeParams]);
								}
							}
							else {
								if(toDestroy)
								{
									destroyModalDialog($dialogElem);
								}
							}
						}
					});
	}
	return $dialogElem;
}

function destroyModalDialog($dialogElem)
{
	$dialogElem.dialog( "destroy" );
	$dialogElem.remove();
	// console.log("dialog destroyed");
}

/**
 * Close jquery dialog windows, support content that build with hidden 'DIV' or 'IFRAME'
 * @param {*} targetElem, clicked element(button), used in case of hidden div only
 */
function closeModalDialog(targetElem)
{
	if(targetElem) {
		$(targetElem).closest('.ui-dialog-content').parent().find('.ui-dialog-titlebar-close').trigger('click');	
	}
	else {
		var dialogDiv = parent.document.getElementById('iframeDialogDiv');
		if($(dialogDiv).length > 0)
		{
			$(dialogDiv).parent().find('.ui-dialog-titlebar-close').trigger('click');	
		}
		else {
			// in case dialog opened by showModalDialogForChrome()
			closeDialog();
		}
	}
}

function isDialogInstanceExists($dialogElem) {
	return !(typeof $dialogElem.dialog( "instance" ) == "undefined");
}

function clearModalDialog($dialogElem)
{
	$dialogElem.find('iframe#iframeDialog').attr('src', 'about:blank');
}

function createAttachmentDialog(obj)
{
	var pageParamsObj = obj.pageParamsObj?obj.pageParamsObj:{};
	var defaultPageURL = "UploadServlet?currentJSP=frmUploadAttachment.jsp&paramsObj=" + encodeURIComponent(JSON.stringify(pageParamsObj));
	var pageURL = obj.pageURL?obj.pageURL:defaultPageURL;
	var $dialogElem = createModalDialog({isIframe:true,attachToParent:obj.attachToParent, 
											title:"Attachment",
                                            width:590, height:370, 
											onClose: obj.onClose
                                        });
    $dialogElem.find('iframe').attr('src', pageURL);
    $dialogElem.dialog('open'); 
}

function isJsonObject(obj) {
    try {
        JSON.parse(JSON.stringify(obj));
    } catch (e) {
        return false;
    }
		return true;
	}

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function parseStringToJson(jsonString)
{
	var obj = {};	
	try
	{	
		obj = JSON.parse(jsonString);
	}
	catch(e)
	{
		console.log("error in parsing string to json: ",jsonString);
		console.error(e);
		return jsonString;
	}
	return obj;
}

function createAuditTrailPreScreen(opt) 
{
	if($('#divAuditTrailPreScreen').length == 0)
	{
		var html = '<div id="divAuditTrailPreScreen" class="html-popup-layout" style="padding: 4% 2% 2% 4%;overflow: visible;"> '
					+ ' <span class="ui-helper-hidden-accessible"><input type="text"/></span> <!-- this span prevent from focus on first element -->'
					+ '	<div class="layout-row"> '
					+ '		<div class="layout-cell field-label" style="margin-right: 82px;"><label>User</label></div> '
					+ '		<div class="layout-cell" style="width:80%"> '
					+ '			<select class="chosen-multiple select-user" multiple="multiple" onchange="customMultiSelectChangeHandler(this);"></select> '
					+ '		</div>'
					+ '	</div> '
					+ '	<div class="layout-row"> '
					+ '		<div class="layout-cell field-label" style="margin-right: 44px;"><label>From Date</label></div> '
					+ '		<div class="layout-cell"> '
					+ '			<input readonly="readonly" class="datepicker datepicker-from" dateRange="fromDate" type="text" style="width:90px;"> '
					+ '		</div> '
					+ '		<div class="layout-cell field-label" style="margin-left: 25px;margin-right: 10px;"><label>To Date</label></div> '
					+ '		<div class="layout-cell"> '
					+ '			<input readonly="readonly" class="datepicker datepicker-to" dateRange="toDate" type="text" style="width:90px;"> '
					+ '		</div>'
					+ '	</div> '
					+ ' <div class="layout-row audit-trail-custom-html"> '
					+ opt.customHtml
					+ '	</div> '
					+ '	<div class="layout-row center" style="margin-top:30px;margin-bottom: 0;"> '
					+ '		<button class="table-secondary-btn show-at-report-btn" disabled="disabled" style="width:120px;">Show Report</button> '
					+ '		<button class="table-secondary-btn" onclick="closeModalDialog(event.target)" >Cancel</button>'
					+ '	</div> '
					+ '</div>';
		
		var $atdiv = $($.parseHTML(html));
		$('body').append($atdiv);
		$atdiv.find('button').button();
		$atdiv.find('.show-at-report-btn').on('click', opt.showReportFunc.bind(this,$atdiv,(opt.showReportFuncParams)?opt.showReportFuncParams:[]));
		if(opt.customHtmlFunc)
		{
			opt.customHtmlFunc.call(this, $atdiv);
		}

		var fromEl = $atdiv.find('.datepicker[dateRange="fromDate"]');
		var toEl = $atdiv.find('.datepicker[dateRange="toDate"]');
		var options = {targetType:'element', elementsInRange:{from:fromEl,to:toEl}};
		initDatePickerWithOptions(fromEl, options);
		initDatePickerWithOptions(toEl, options);
		var $usersElem = $atdiv.find('.chosen-multiple');
		$usersElem.chosen({placeholder_text_multiple:"All", width:"80%"});
		fnGetUsersForAuditTrail(function (content){
			customSelectPopulate($usersElem, content, [], {useMultiDefaultOption:true})
		});
	}
	var $dialogElem = createModalDialog({dialogId:"divAuditTrailPreScreen", isDestroyDialog:true, title:labelsArr["Audit_Trail_Details"], width: 740, height:380});	
	$dialogElem.dialog('open');
}

function customSelectPopulate($currElem, parsedData, selectedVal, opt)
{
    var tempHtml = "";
	var selectedArr = (selectedVal)?(Array.isArray(selectedVal)?selectedVal:[selectedVal]):[];
    // selectedArr = (selectedArr)?selectedArr:[];
    // clear list
    $currElem.html('');

	// console.time("for");
	var len = parsedData.length, i = 0;
	for(i;i<len;i++)
	{
		var item = parsedData[i];
		var selected = '';
		var optionAttr = '';
        if($.inArray(item.id, selectedArr) != -1)
        {
            selected = 'selected';
        }
		if(item.hasOwnProperty("attributesMap"))
		{
			for(key in item.attributesMap) {
				optionAttr += key + "=" + "\""+item.attributesMap[key]+"\" ";
			}
		}
        tempHtml += '<option '+optionAttr+' value="'+item.id+'" '+selected+'>'+item.text+'</>';
        selected = '';
	}
	// console.timeEnd("for");

    if(tempHtml != "" && opt)
    {
        if(opt.useMultiDefaultOption)
		{
			$currElem.html('<option value="">All</option>');
		}
		else if(opt.useSingleDefaultOption){
			$currElem.html('<option value="-1">Choose</option>');
		}
    }
    $currElem.append(tempHtml).trigger('chosen:updated');
	return $currElem;
}

/* used with multi selection, when 'All' should be one of the option in selection list */
function customMultiSelectChangeHandler(el)
{
	if($(el).find('option:first-child').prop('selected'))
	{
		$(el).val('').find('option:first-child').prop('selected', false).end().trigger('chosen:updated');
        // $(el).val('').trigger('chosen:updated');
        // $($(el).data('chosen').container[0]).find('.chosen-choices > li:eq(0)').addClass('default-result').find('a.search-choice-close').remove();
	}
}

function ajax_populateSelectByJsonList(settings)
{
	sendAjaxRequest({servletName:settings.servletName,urlParams:settings.urlParams,dataType:"json",
					onSuccess:function(returnedList) {
						customSelectPopulate(settings.element, returnedList, settings.selectedVal, settings.selectOptions);
						if(settings.callbackFunc)
						{
							settings.callbackFunc.apply(this, settings.callbackFuncParams?[settings.callbackFuncParams]:[]);
						}
					}
	});
}

function sendAjaxRequest(settings)
{
	var _contentType = "application/x-www-form-urlencoded; charset=utf-8";
	var _returnDataType = "text";
	if(settings)
	{
		_contentType = settings.contentType?settings.contentType:_contentType;
		_returnDataType = settings.dataType?settings.dataType:_returnDataType;
	}

	$.ajax
	({ 
		type: "POST",
		contentType: _contentType,
		url: settings.servletName,
		data: settings.urlParams,
		dataType: _returnDataType,
		beforeSend:settings.beforeSendFunc,
		success: function( data, textStatus, jqXHR ) 
		{  		
			if(settings.onSuccess)
			{
				settings.onSuccess.apply(this, [data, textStatus, jqXHR])
			}
		},
		error: handleAjaxError
	});	
}


function handleAjaxError( xhr, textStatus, error ) 
{      
	if (xhr.responseText.match(/TIME_IS_OUT/)) 
	{   
		top.location.href = 'Login.jsp';
	} 
}

function isOptionSelected($select)
{
	return $select.prop('selectedIndex') > 0;
}

function getSignOptions(selValue)
{
	return '<option value="0" '+((selValue=='=')?"selected":"")+'>=</option>'+
			'<option value="1" '+((selValue=='<')?"selected":"")+'><</option>'+
			'<option value="2" '+((selValue=='>')?"selected":"")+'>></option>'+
			'<option value="3" '+((selValue=='<=')?"selected":"")+'><=</option>'+
			'<option value="4" '+((selValue=='>=')?"selected":"")+'>>=</option>';
}

// scroll to the top of the document
function goToPageTop() {
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}