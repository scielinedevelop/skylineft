var UNIQUE_CHART_NAME = "";

function initChartSeriesDesignDialog(obj)
 {	
    	var bodyElem = $('body')[0];
    	if (bodyElem == null)
    		return;
    	
    	var mainDiv = document.createElement('div');
    	$(mainDiv).attr('id', 'divChartSeriesDesignDialog')
    		   .data('maxLength', '-1');
    	    	
    	var iDiv = document.createElement('div');
    	$(iDiv).attr('id', 'divDesignDialogQueryName')
    			.css({'margin-top':'40px','display':'block'})
    			.append($('<label>').attr({'class':'cssStaticData'}).html('Query Name:'))
    			.append($('<input>')
    					 .attr({'type':'text', 'maxlength':'50','onkeypress':'validateLegalChar(event.keyCode, event)'})
    				     .css('width','220px'));
    	
    	var sDiv = document.createElement('div');
    	$(sDiv).attr('id', 'divDesignDialogSelectQueryName')
    			.css({'margin-top':'40px','display':'block'})
    			.append($('<label>').attr({'class':'cssStaticData'}).html('Select Query:'))
    			.append($('<select>').css({'width':'220px'}));
    	
    	mainDiv.appendChild(iDiv);
    	mainDiv.appendChild(sDiv);
    	bodyElem.appendChild(mainDiv);
    	
    	var curr_object_type = (obj != null && obj.objType != null)?obj.objType:'series';
    	
    	// set dialog properties
    	$( '#divChartSeriesDesignDialog' ).dialog({
    		autoOpen: false,
    		height: 230,
    		width: 360,
    		modal: true,		
    		buttons: {
    			OK: function() { 
    					if($('div#divDesignDialogQueryName').css('display') != 'none')
    					{
    						var $input = $('div#divDesignDialogQueryName >input');
    						var _max = $input.attr('maxLength'); 
    						var _text = $.trim($input.val());
    						if( _max > 0 && _text.length > _max)
        					{
        						displayAlertDialog(DispAlDial_Msg_Short_Name_V0_91, {replaceme:[{key:0,val:_max}]});
        						return false;
        					}
        					if( !fnValidateString({inStr:_text, fieldName:"Query Name", isRequired:true}) )
        					{           
        						return false;
        					} 
    						
        					_saveQuery(_text, curr_object_type);
    					}
    					else
    					{
    						var _selectVal = $('div#divDesignDialogSelectQueryName >select option:selected').val();
    						if( !fnValidateString({inStr:_selectVal, fieldName:"Select Query Name", isRequired:true, forbiddenValues:"0"}) )
        					{           
        						return false;
        					} 
    						_getByQueryName(curr_object_type, _selectVal);
    					}
    				},
    				Cancel: function() {
    					$( this ).dialog( "close" );
    				}
    			}
    	});	
    	
    	return $( '#divChartSeriesDesignDialog' );
}

function _openQueryDialog(obj)
{
	var oDialog = $( '#divChartSeriesDesignDialog' );
	var dialogTitle = "";
	var doContinue = true;
	// check if dialog refers to the correct DIV
	if(oDialog.length == 0)
		return;
	
	UNIQUE_CHART_NAME = obj.uniqueChartName;
	
	if(obj.action == "save")
	{
		dialogTitle = "Save Query";
		$('div#divDesignDialogSelectQueryName').css('display','none');
		$('div#divDesignDialogQueryName').css('display','block');
	}
	else//open
	{
		dialogTitle = "Open Query";
		$('div#divDesignDialogQueryName').css('display','none');
		$('div#divDesignDialogSelectQueryName').css('display','block');
		doContinue = false;
		
		$.ajax
        ({
        	type:"POST",
		    contentType: "application/x-www-form-urlencoded; charset=utf-8",
		    url: "helperservlet",
			data: "actionid=getChartSeriesQueryNames&objectType=" + obj.objType + "&uniqueChartName=" + UNIQUE_CHART_NAME,
		    dataType: "json",
            success: function(data) 
            {  
            	var currObj = $('div#divDesignDialogSelectQueryName >select');
                currObj.html('<option value="0">Choose</>');
                
            	if(data != null)
                {
                    var list = "";
                    $(data).map(function ()   
                    {
                            list += '<option value="'+this.id+'">'+this.text+'</>';
                    });
                    if(list != "")
                    {
                        currObj.append(list);
                    }
                    //open dialog
                    oDialog.dialog('open');
                } 
            },
            error:handleAjaxError
        });   
	}
	
	$('div#divChartSeriesDesignDialog').parent().children('div:first').find('span').html(dialogTitle);
	if(doContinue)
	{
		oDialog.dialog('open');
	}
}

function _saveQuery(text, objectType)
{
	showWaitMessage();
	var objectData = prepareSeriesQueryData();// method from Series screen, returns full settings object
	$.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=saveChartSeriesDesign&objectType=" + objectType + "&objectName=" + encodeURIComponent(text) + 
											"&objectData=" + encodeURIComponent(objectData) + "&uniqueChartName=" + UNIQUE_CHART_NAME,
		dataType: "text",
		success: function( data ) 
		{  
			hideWaitMessage();
			if(data == "-2")
			{
				displayAlertDialog("Query with the same name already exists.");
				return false;
			}
			else if (data == "-1")
			{
				displayAlertDialog("Save failed");
				return false;
			}
			else
			{
				$( '#divChartSeriesDesignDialog' ).dialog('close');
			}
		},
		error: handleAjaxError
	});	
}

function _getByQueryName(objectType, name)
{
	
	$.ajax
	({ 
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=utf-8",
		url: "helperservlet",
		data: "actionid=getChartSeriesDesign&objectType=" + objectType + "&objectName=" + encodeURIComponent(name) + "&uniqueChartName=" + UNIQUE_CHART_NAME,
		dataType: "text",
		success: function( data ) 
		{  
			populateSeriesSettingsFromQuery(data);
			$( '#divChartSeriesDesignDialog' ).dialog('close');
			hideWaitMessage();
		},
		error: handleAjaxError
	});	
}










