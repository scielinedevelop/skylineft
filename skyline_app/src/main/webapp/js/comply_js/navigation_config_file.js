/*
	Steps for implement navigate function:
	1)(in this document) add record*(if not exist) to _objectsPool: key, value(object with servlet data as: servlet name, parameters for enter to this servlet, 
																			database object(if needed) for get parameters data);
		* this record used as target/source object(screen)
	2)(in this document) add record to  _relationsPool that contain relations to _objectsPool.
	SOURCE page:
	3)(in java/jsp page) make next changes in the required field like:
						- (cmID) relate _relationsPool key to this field;
						- (class) add class "hasmenu";
						- (cmParamData) add parameters data for target screen as array ( JSONArray param ).
		In case, target screen use database procedure for get parameters data, cmParamData used for this procedure signature.
		Example in table record: instead "runName" before, after change this field as navigation field we have -> "<div class="hasmenu"  cmID="cm_de_io_to_item_general"  cmParamData="params_as_JSONArray" ></div> <span>field_value</span>";
		Example in html: "<div><span class="hasmenu" cmID="cm_de_material_to_item_general" cmParamData="params_as_JSONArray"></span></div>";
	4)(in jsp page) implement function _getNavigationBackDataArr() for set back parameters with data array.
	5)(in jsp page) import next files:  
					- for implement context menu - 'jquery.ui-contextmenu.js';
					- for use Navigation possibility 'navigation_config_file.js'.
	
	TARGET page:
	6)import 'navigation_config_file.js'.
	7)(in jsp page) For Back to source: call function _navigateBackTo(backData), where backData - is parameters array getting from request.
*/

var _objectsPool = {};
var _relationsPool = {};
var _navigationForm = null;
var _can_navigate = true;

$(document).contextmenu(
{
		delegate: ".hasmenu",
		preventContextMenuForPopup: true,
		menu: [],
		select: function(event, ui) 
		{
			var _targetID = ui.target.attr('cmID');
			var _relObject = _relationsPool[_targetID];	
			var currTargetObj = _objectsPool[ui.cmd];
			var backToObject = _objectsPool[_relObject[0].backToScreen];
			var additDataObj = ui.item.data();
			
			if(ui.target.attr('cmCanNavigate') !== undefined && ui.target.attr('cmCanNavigate') == "false")
			{
				_can_navigate = false;
			}
			if(_can_navigate)
			{
				_navigateTo(currTargetObj, backToObject, ui.target.attr('cmParamData'),additDataObj);
			}
			else
			{
				displayAlertDialog(ui.target.attr('cmErrorMsg'));
				_can_navigate = true;
			}
		},
		beforeOpen: function(event, ui) 
		{
				var _targetID = ui.target.attr('cmID');
				var _relObject = _relationsPool[_targetID];
				var targetsList = _relObject[0].targetsList;
				var titleArr = [];
				var currObj = [];
				for(var i=0; i < targetsList.length;i++)
				{
					currObj = targetsList[i];
					var targetTab = (currObj.target_tab)?currObj.target_tab:"";
					titleArr.push({title:currObj.display_name, cmd:currObj.obj_key, data:{tab:targetTab}});
				}
				$(document).contextmenu("replaceMenu", titleArr);
		}
});

function _createForm()
{
	//alert($($('form', window.parent.document)[0]).attr('id'));
	//_navigationForm = $('form', window.parent.document)[0];
                
	var bodyElem = $('body', window.parent.document)[0]; 
	
	if (bodyElem == null)
		return;
	var form = document.createElement('form');
	$(form).attr({'id':'navigationForm', 'name':'navigationForm', 'method':'post', 'action':''}).append();
    bodyElem.appendChild(form);
	_navigationForm = $('#navigationForm', window.parent.document);
}

function _navigateTo(targetFullObj, backFullObj, paramData, additDataObj)
{	
	var _dbObjName = targetFullObj[0].dbObjectName;
	var _enterScrParams = targetFullObj[0].screenEnterParams;
	var do_continue = false;
	_createForm();
	if(_dbObjName == null || _dbObjName == '')
	{
		var _paramDataArr = $.parseJSON(paramData);
		for(var i=0;i < _enterScrParams.length;i++)
		{
				//alert(_enterScrParams[i] + " : " + _paramDataArr[i]);
			$(_navigationForm).append('<input  type="hidden" id="'+_enterScrParams[i]+'" name="'+_enterScrParams[i]+'" value="'+_paramDataArr[i]+'"/>');
		}
		do_continue = true;
	}
	else
	{	
		$.ajax
		({ 
			type: "POST",
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
			url: "helperservlet",
			data: "actionid=getNavigationData&dbObjName=" + _dbObjName + "&paramsDataArr=" + paramData,
			dataType: "text",
			success: function( data ) 
			{  
				//alert(data);
				if(data.match(/TIME_IS_OUT/))
				{
					top.location.href = 'Login.jsp';
				}
				else
				{
					
					var _enterParamsData = $.parseJSON(data);
					//alert(_enterParamsData);
					if(_enterParamsData.length > 0)
					{
						for(var i=0;i < _enterScrParams.length;i++)
						{
							//alert(_enterParamsData[i]);
							// override target tab value
							if(_enterScrParams[i] == "txtTabID" && additDataObj.hasOwnProperty('tab'))
							{
								$(_navigationForm).append('<input  type="hidden" id="txtTabID" name="txtTabID" value="'+additDataObj.tab+'"/>');
							}
							else {
								$(_navigationForm).append('<input  type="hidden" id="'+_enterScrParams[i]+'" name="'+_enterScrParams[i]+'" value="'+_enterParamsData[i]+'"/>');
							}
						}
						
						do_continue = true;
					}
				}
			},
			async: false
		});	
	}
	if(do_continue && backFullObj.length > 0)
	{
		var _obj = _setBackObject(backFullObj);
		$(_navigationForm).append('<input  type="hidden" id="navigationBackScreenObject" name="navigationBackScreenObject" value='+_obj+' >');								
	}
	if(do_continue)
	{
		$(_navigationForm).attr('action', targetFullObj[0].obj_id);
		$(_navigationForm).submit();
	}
}

/* call method from source page */
function _setBackObject(backFullObj)
{
	var dataArr = [];
	if (typeof window.parent._getNavigationBackDataArr === "function")
	{
		dataArr = window.parent._getNavigationBackDataArr();
	}
	
	var paramsObj = backFullObj[0].screenEnterParams;
	var backObj = [];
	backObj.push({'backAction':backFullObj[0].obj_id, 'params':paramsObj, 'paramsData':dataArr});
	return JSON.stringify(backObj);
}

/* should be called from target page for BACK to source page */
function _navigateBackTo(backData)
{
	_createForm();	
	var _backObj = $.parseJSON(backData);
	if(_backObj.length > 0)
	{
		for(var i=0;i < _backObj[0].params.length;i++)
		{			
			//alert(_backObj[0].params[i] + "  ||  " + _backObj[0].paramsData[i]);
			$(_navigationForm).append('<input  type="hidden" id="'+_backObj[0].params[i]+'" name="'+_backObj[0].params[i]+'" value='+_backObj[0].paramsData[i]+'>');
		}
		$(_navigationForm).attr('action', _backObj[0].backAction);
		$(_navigationForm).submit();
	}
}

 _objectsPool = {
					'inventory_item_mng':[{'obj_id':'inventoryitemmainservlet_','dbObjectName':'nvg_sp_get_inventory_by_id',
											'screenEnterParams':['txtTabID','txtCanEdit','txtSiteID','txtInventoryTypeID','txtInventoryID','txtInUse','invListBackObj']}],
					'data_entry_mng':[{'obj_id':'dataentrymainservlet_', 'dbObjectName':'',
											'screenEnterParams':['txtTabID','Mode','txtStageID','txtProductID','txtRunID','txtProjectID','txtExperimentID','selTableRowID']}],
					'dashboard_wf_tasks':[{'obj_id':'dashboardworkflowtasksservlet', 'dbObjectName':'', 'screenEnterParams':['streamType']}]
				};
			
 _relationsPool = {
					'cm_de_material_to_item_general':[{'targetsList':[{'obj_key':'inventory_item_mng','display_name':'Inventory item general tab'}],'backToScreen':'data_entry_mng'}],
					'cm_de_io_to_item_general':[{'targetsList':[{'obj_key':'inventory_item_mng','display_name':'Inventory item general tab'}],'backToScreen':'data_entry_mng'}],
					'cm_inv_component_to_item_general':[{'targetsList':[{'obj_key':'inventory_item_mng','display_name':'Component item general tab'}],'backToScreen':'inventory_item_mng'}],
					'cm_inv_usages_to_de_test':[{'targetsList':[{'obj_key':'data_entry_mng','display_name':'Data Entry'}],'backToScreen':'inventory_item_mng'}],
					'cm_dash_wf_tasks_to_de':[{'targetsList':[{'obj_key':'data_entry_mng','display_name':'Data Entry'}],'backToScreen':'dashboard_wf_tasks'}],
					'cm_inv_handling_to_item_general':[{'targetsList':[{'obj_key':'inventory_item_mng','display_name':'Inventory item general tab'}],'backToScreen':'inventory_item_mng'}],
					'cm_inv_mvm_to_item_sequential_barcode':[{'targetsList':[{'obj_key':'inventory_item_mng','display_name':'Inventory item sequential barcodes tab','target_tab':'aSequentialBarcodes'}],'backToScreen':'inventory_item_mng'}]
			      };	

