var _handlerObject = {};
var _globalRemovedColumnsHolder = {};
var globalTableNewRowId = -1;

function drawTable(currTableID, propArray, source, additOptionsArr, labelsArr)
{
    var _handlerArray = (additOptionsArr != null)?additOptionsArr:{};
    _handlerObject[currTableID] = _handlerArray;
    var _colNamesArr = (labelsArr)?labelsArr:{};
    var $currTableElem = $('#' + currTableID);

    /******  Default table properties  *******/
    var _serverSide = false;                
    var _processing = true;  
	var _paging = true;                  
    var _paginationType = "full_numbers";  
    var _order = []; // use order defined in database
    var _dom = "lBfrtip";      
    var _autoWidth = true;
    var _footer = true;
    var _columnFilter = true;
    var _pageLength = 10;
    var _lengthMenu = [5,10,25,50,100];
	var _removeColumns = true;
	var _isTableEditable = false;
	var _useDTButtons = true;
    /*****************************************/
    
    /******  Default column properties  *******/
    var _visible = true;
    var _searchable = true;
    var _orderable = true;
	var _removable = true;
    var _width = null;
    var _className = "";
    var _title = ""; 
    var _type = null;
    /***********************************/
    
    var propertiesArrLength = propArray.length;
    var propertiesArray = (propertiesArrLength == 0)?[{}]:propArray;
	/* table configuration array */
	var tablePropertiesArray = (propertiesArrLength == 0)?[{}]:propArray[0];
    
    _serverSide = (tablePropertiesArray.serverSide != null)? tablePropertiesArray.serverSide : _serverSide;  
    _footer = (tablePropertiesArray.footer != null)? tablePropertiesArray.footer : _footer; 
    _columnFilter = (tablePropertiesArray.columnFilter != null)? tablePropertiesArray.columnFilter : _columnFilter;
	_isTableEditable = (tablePropertiesArray.editable != null)? tablePropertiesArray.editable : _isTableEditable; 
	_removeColumns = (tablePropertiesArray.removeColumns != null)? tablePropertiesArray.removeColumns : _removeColumns;
    
    /** set array of column properties **/   
    var colDefinArr = [];   
    var colFilterTypeArr = [];
	var colProp;
    for (var i=1; i < propertiesArrLength; i++ ) 
    {          
		colProp = propertiesArray[i];
		var isColVisible = (colProp.visible != null)? colProp.visible : _visible;
		var colTitle =  _setColumnTitle(colProp, _colNamesArr, isColVisible, currTableID); 
		var unqTitle =  (colProp.uniqueTitle)? colProp.uniqueTitle : _setUniqueTitle(colTitle);
		var isRemovable = _removeColumns?((colProp.removable != null)? colProp.removable : _removable):false;
		var className = (colProp.className != null)? colProp.className : _className;
		if(_removeColumns && !isRemovable) className += " removing-disabled";

    	   colDefinArr.push({                
	            "targets": [i-1],
	            "visible": isColVisible,
	            "searchable": (colProp.searchable != null)? colProp.searchable : _searchable,
	            "orderable": (colProp.orderable != null)? colProp.orderable : _orderable, 
	            "className": className,
	            "width": (colProp.width != null)? colProp.width : _width,
	            "title": colTitle,
	            /** "orderDataType" - Allows sorting to occur on user editable elements such as form inputs */
	            "orderDataType": (colProp.orderDataType != null)? colProp.orderDataType : '',
	            /** "type" - The type allows you to specify how the data for this column will be sorted.*/
	            "type": (colProp.type)? colProp.type : _type,
				"removable":isRemovable,
				"uniqueTitle":unqTitle,
				"columnId":(colProp.columnId)? colProp.columnId : unqTitle,
				"columnHtmlType":(colProp.columnHtmlType)? colProp.columnHtmlType : null,
				/** predefined buttons/icons for actions on row, like: delete, edit, copy, attach*/
				"rowActionButtons":(colProp.rowActionButtons)? colProp.rowActionButtons : null,
	            "createdCell": function (cell, cellData, rowData, rowIndex, colIndex) 
	            {
					if(_handlerArray.createdCell != null) {
	                    _handlerArray.createdCell.apply(this, [cell, cellData, rowData, rowIndex, colIndex, currTableID]);
					}
					else {
						_createdCell(cell, cellData, rowData, rowIndex, colIndex, this);
					}
	            }
	        }); 
        
	        if(_columnFilter)
	        {   
	            if(colProp.filterType != null)
	            {
	                if(colProp.filterType == "none")
	                {
	                    colFilterTypeArr.push(null);
	                }
	                else
	                {
	                    colFilterTypeArr.push({type: "text"});
	                }
	            }
	            else
	            {
	                if(isColVisible)
	                {
	                	colFilterTypeArr.push({type: "text"});
	                }
	                else {
	                	colFilterTypeArr.push(null);
	                }
	            }
	        }
			
    }    
    //console.log(colFilterTypeArr);
	/*******************************************/

	if(_isTableEditable)
	{
		$currTableElem.addClass('editable-table');
	}

	/** add table footer **/
	if(_footer)
	{
		var footer = '<tfoot><tr>';
		for(var i=0; i < colDefinArr.length; i++)
		{
			var _columnFilterElement = '';
			if(_columnFilter && colFilterTypeArr[i] != null)
			{
				var _fType = colFilterTypeArr[i].type;
				if(_fType == "text")
				{
//	        			add a text input to footer cell
					_columnFilterElement = '<input type="text" class="filter-data-column" data-index="'+i+'" />';
				}
			}
			footer += '<th>'+_columnFilterElement+'</th>';
		}
		footer += '</></>';
		$currTableElem.append(footer);
	}
	else {
		_columnFilter = false;
	}
	/*********************************************/
	
	    
	    /** TABLE initializing **/
	    var dtTable = $currTableElem.DataTable({
	    	"serverSide": _serverSide,
            "ajax": (_serverSide) ? source : null,
	    	"data":(_serverSide) ? null : source,
	    	"order":(tablePropertiesArray.order != null)? tablePropertiesArray.order : _order,
			"destroy":(tablePropertiesArray.destroy)? tablePropertiesArray.destroy : false,
			"paging": (tablePropertiesArray.paging != null)? tablePropertiesArray.paging : _paging,
	    	"pagingType": (tablePropertiesArray.paginationType != null)? tablePropertiesArray.paginationType : _paginationType,
	        "dom":(tablePropertiesArray.dom != null)? tablePropertiesArray.dom : _dom,
			"buttons":_useDTButtons?_initDtButtons(tablePropertiesArray, currTableID):[],
	        "autoWidth":(tablePropertiesArray.autoWidth != null)? tablePropertiesArray.autoWidth : _autoWidth,
	        "processing":(tablePropertiesArray.processing != null)? tablePropertiesArray.processing : _processing,
	        "language": (tablePropertiesArray.language != null)? tablePropertiesArray.language : _setLanguage(currTableID),
	        "lengthMenu":(tablePropertiesArray.lengthMenu != null)? tablePropertiesArray.lengthMenu : _lengthMenu,
	        "pageLength":(tablePropertiesArray.pageLength != null)? tablePropertiesArray.pageLength : _pageLength,
			/** scrollY - should receive a number or boolean, example: "300px"/300 */
			"scrollY": (tablePropertiesArray.scrollY)? tablePropertiesArray.scrollY : '',
            "scrollX": (tablePropertiesArray.scrollX)? tablePropertiesArray.scrollX : false,
            "scrollCollapse": (tablePropertiesArray.scrollCollapse)? tablePropertiesArray.scrollCollapse : false,
			"fixedColumns":(tablePropertiesArray.fixedColumns)? tablePropertiesArray.fixedColumns : false,
	        "columnDefs":colDefinArr,
	        "createdRow": function ( row, data, index ) {
	        	// add unique row id to each row
				$(row).attr('id','dtrow'+data[0]);
	        	if(_handlerArray.createdRow != null)
                    _handlerArray.createdRow.apply(this, [row, data, index, currTableID]);
	        },
	        "drawCallback": function( settings ) {
	        	if(_handlerArray.drawCallback != null) {
                    _handlerArray.drawCallback.apply(this, [settings, currTableID]);
				}
				if(_isTableEditable)
				{
					$currTableElem.find('.editable-cell-chosen-select').each(function() 
	    			{
						var chosen_config = {
					    	width:"100%"
						};
						$(this).chosen(chosen_config);
					})
				}
	        }
			,"initComplete": function(settings, json) {
				if(_removeColumns) {
				_onInitComplete(this, currTableID);
			}
			}
	    });
	    
	    _rowClicked(currTableID);
	    
	    if(_columnFilter)
	    {
			_applyDatatableColumnsFilter(dtTable);
	    }

		if(_useDTButtons) {
		//override configuration buttons set to dropdown button
		var $wrapper = $('#' + currTableID + '_wrapper');
		var confBtn = $wrapper.find('.configuration-buttons-set');
		if(confBtn) {
			confBtn.removeClass('dt-button').addClass('dropdown dropdown-button');
			confBtn.append('<span class="dropdown-span cog-icon" aria-hidden="true"></span>\
							<div class="dropdown-content"></div>');
			var $dtButtons = $wrapper.find('.dt-buttons');
			confBtn.find('.dropdown-content').append($dtButtons.find('.dt-button:not(.dt-action-buttons)'));
		}
		}
	    return dtTable;
}

function _createdCell(cell, cellData, rowData, rowIndex, colIndex, $this)
{
	// var dtTable = dt_ext_getDtTableById(currTableID);
	// var column_settings = dtTable.context[0].aoColumns[colIndex];
	var column_settings = $this.api().context[0].aoColumns[colIndex];

	var cellContent = "";
	if(column_settings.columnHtmlType)
	{
		var htmlType = column_settings.columnHtmlType;
		var colId = column_settings.columnId;
		var attrs = 'columnId="'+colId+'" ';
		if(htmlType == "text")
		{
			cellContent = '<div class="editable-cell-parent">\
							<input class="editable-cell css-input" '+attrs+' type="text" class="" value="">\
						</div>';
		}
		else if (htmlType == "select")
		{
			cellContent = '<div class="editable-cell-parent">\
							<select class="editable-cell editable-cell-chosen-select css-select" '+attrs+' data-placeholder="Choose" type="text" value=""></select>\
						</div>';
		}
	}
	else if(column_settings.rowActionButtons)
	{
		cellContent = _createTableRowActionIcons(column_settings.rowActionButtons);
		$(cell).addClass("table-column-with-icons");
	}
	else {
		cellContent = '<div class="viewable-cell-parent">'+cellData+'</div>';
		// cellContent = cellData;
	}
	$(cell).html(cellContent);
}

function _createTableRowActionIcons(configArr)
{
	var returnHtml = "";
	var config = (configArr == null)?[]:configArr;
	var tableRowIcons = {edit:"edit-icon",new:"new-icon",copy:"copy-icon",remove:"remove-icon",add_attachment:"add-attachment-icon"};
	for(var i=0; i<config.length;i++)
	{
		var _o = config[i];
		var disabled = (_o.disabled)?"disabled":"";
		if(_o.iconType && tableRowIcons.hasOwnProperty(_o.iconType))
		{
			var tooltip = (_o.tooltip)?'title="'+_o.tooltip+'"':'';
			var onClickAction = 'onClickAction="'+_o.iconType+'"';
			returnHtml += '<span class="app-icon ' + tableRowIcons[_o.iconType]+' '+disabled+'" '+tooltip+' '+onClickAction+'></span>';
		}
	}
	return returnHtml;
}

function _getTableRowIconActionNameByEvent(event)
{
	// console.log(event.target.nodeName);
	if(event && event.target.nodeName && event.target.nodeName.toLowerCase() === "span" && event.target.hasAttribute('onClickAction'))
	{
		return event.target.getAttribute('onClickAction');
	}
	return null;
}

function _onInitComplete(ctx, tableID)
{
	var dtColumnsApi = ctx.api().columns();
	dtColumnsApi.iterator('column', function ( settings, columnIndex) 
	{
		var column = this.column(columnIndex);
		var column_settings = settings.aoColumns[ columnIndex ];
		
		/** set attribute 'uniqueTitle' for the table header */
		$(column.header()).attr('uniqueTitle', column_settings.uniqueTitle);
		/****/

		/**
		 * set remove column icon
		 */
		if (column_settings.removable) 
        {
            var headerObj = column.header();
			var unqTitle = column_settings.uniqueTitle;
			
        	$(headerObj).find('div')
        		.append('<span class="close-icon" title="Remove column" onclick="_removeColumn(\''+tableID+'\',\''+unqTitle+'\', '+columnIndex+');event.stopImmediatePropagation();"></span>');
        }
		
	} );
				
}

function _rowClicked(tableID, isClick, isDblClick)
{
	var _handlerArray = _handlerObject[tableID];
	var timer = 0;
    var delay = 0; //to prevent from 'click' event when row is double-clicked need to set 'delay' value to 300;
    var preventClick = false;
    
    $('#' + tableID +' tbody').on('click', 'tr', function(event)
    { 
    		    	
		var $this = $(this);
    	var dtTable = $('#'+tableID).DataTable();
    	var isRowSelected = false;
        
    	//prevent select row when there is no data
    	if(dtTable.page.info().recordsDisplay > 0) 
        {    		
    		var currRowData = dtTable.row(this).data();

			if ($this.hasClass('selected')) 
            {                                 
            	$this.removeClass('selected'); 
            } 
            else 
            {
            	dtTable.$('tr.selected').removeClass('selected'); 
                $this.addClass('selected');
                isRowSelected = true;
            }
    		timer = setTimeout(function() 
    		{
	    		if(!preventClick)
		    	{
	    			if(_handlerArray.onRowActionBtnClick != null) {
	    			var act = _getTableRowIconActionNameByEvent(event);
            		if(act)
            		{
						if(!isRowSelected)$(event.currentTarget).addClass('selected');
						_handlerArray.onRowActionBtnClick.apply(this, [act, dtTable, currRowData, event]);    
					}
	    			else if(_handlerArray.onRowClick != null) {
		                _handlerArray.onRowClick.apply(this, [currRowData, tableID, isRowSelected, event]);                        
					}
		    	}
					else if(_handlerArray.onRowClick != null) {
						_handlerArray.onRowClick.apply(this, [currRowData, tableID, isRowSelected, event]);                        
					}
		    	}
    		}, delay);
        }
    	
    	preventClick = false;
    })
    .off('dblclick') //method removes event handlers that were attached and prevent from fire double click more than one time 
    .on('dblclick', 'tr', function (e) 
    {
    	var $this = $(this);
    	var dtTable = $('#'+tableID).DataTable();
    	if(dtTable.page.info().recordsDisplay > 0) 
        {
    		// console.log("dblclick");
			var currRowData = dtTable.row(this).data();
    		preventClick = true;
			clearTimeout(timer);
    		$this.addClass('selected');            
        	if(_handlerArray.onRowDblClick != null)
                _handlerArray.onRowDblClick.apply(this, [currRowData, tableID]); 
        }
    });
}

function _addTableRow(dtTable)
{
	var colsLen = dtTable.columns().nodes().length;
	var newRowArr = [];
	for(var i=0;i<colsLen;i++)
	{
		if(i==0)
		{
			newRowArr[i] = globalTableNewRowId--;
		}
		else {
			newRowArr[i] = "";
		}
	}
	dtTable.row.add(newRowArr).draw( false );
}

//Apply search on column
function _applyDatatableIndividualColumnFilter(column) 
{
	$(column.footer()).find('input').on('keyup change clear', function () {
        
        var inputValue = this.value.trim();
		if ( column.search() !== inputValue ) {
            column.search(inputValue).draw();
        }
	});
}

//Apply search on all columns
function _applyDatatableColumnsFilter(dtTable) 
{
	var dtColumnsApi = dtTable.columns();
	$(dtColumnsApi.footer()).find('input').on( 'keyup change clear', function (e)  {
		var _this = this;
		var index = $(_this).attr('data-index');
		// var column = dtColumnsApi.column( $(_this).parent().index()+':visible');
		var column = dtColumnsApi.column(index);
		var inputValue = _this.value.trim();
		if ( column.search() !== inputValue ) {
            column.search(inputValue).draw();
        }
	});
}

function _setLanguage(tableID)
{       
    return {"search": "Global Search:"};
}

/** 
 *  function remove all spaces from original title
 */
function _setUniqueTitle(colTitle)
{
	return colTitle.replace(/\s+/g, "_");
}

function _setColumnTitle(prop, colNamesArr, isVisible, tableID)
{
	var colName = prop.title;
	
	if(isVisible) {
		
		if(prop.is_checkbox)
		{        		
			colName = '<input title="'+prop.tooltip+'" id="'+prop.id+'" type="checkbox">';
		}
		else if(prop.is_image)
		{
			colName = '<img src="images/'+prop.title+'" style="cursor: default;">';
		}
		else if(prop.is_icon)
		{
			colName = '<i class="'+prop.title+'" aria-hidden="true" style="cursor: default;"></i>';
		}
		else if(colName != null)
		{                                
			colName = (Object.keys(colNamesArr).length == 0)?colName:(colNamesArr.hasOwnProperty(colName)?colNamesArr[colName]:colName);
		}
			
	}
	return colName;
}

function _initDtButtons(prop, currTableID)
{
	var buttonsArrToReturn = [];
	var addExportButtons = (prop.exportButtons==null)?true:prop.exportButtons;
	var addColumnConfigButton = (prop.removeColumns != null)?prop.removeColumns:true;
	
	if(addExportButtons)
	{
		buttonsArrToReturn = _setExportButtons(currTableID, prop.exportButtonsConfig);	
	}
	if(addColumnConfigButton)
	{
		buttonsArrToReturn.push({
			text:"Columns",
			className: 'columns-config-buttons',
			action: function ( e, dt, node, config ) {
				_resetColumns(currTableID);
			}
		});
	}
	if(addExportButtons || addColumnConfigButton)
	{
		buttonsArrToReturn.push({
			className: 'configuration-buttons-set',
		});
	}
	if(prop.customButtons)
	{
		var customBtns = prop.customButtons;
		var defaultClassName = 'table-primary-btn dt-action-buttons';
		for(var i=0;i<customBtns.length;i++)
		{
			var btnConfig = customBtns[i];
			var btnAction = btnConfig.action?btnConfig.action:null;
			
			buttonsArrToReturn.push({
				text: btnConfig.text?btnConfig.text:'',
				className: btnConfig.className?defaultClassName+' '+btnConfig.className:defaultClassName,
				customButtonConfig:btnConfig, // contains all custom configuration
				action: function ( e, dt, node, config ) {
					// console.log("dt button action")
					// console.log(e);
					// console.log(dt);
					// console.log(node);
					// console.log(config);

					/* action that should be done before main action */
					if (btnConfig.beforeAction && btnConfig.beforeAction instanceof Function) {
						btnConfig.beforeAction.apply(this, [e, dt, node, config, btnConfig.beforeActionCallParams?btnConfig.beforeActionCallParams:null]);
					}
					/* action on button click */
					if(btnAction == 'addrow')
					{
						_addTableRow(dt);
					}
					else if (btnAction instanceof Function) {
						btnAction.apply(this, [e, dt, node, config, btnConfig.actionCallParams?btnConfig.actionCallParams:null]);
					}
					/* action that should be done after main action */
					if (btnConfig.afterAction && btnConfig.afterAction instanceof Function) {
						btnConfig.afterAction.apply(this, [e, dt, node, config, btnConfig.afterActionCallParams?btnConfig.afterActionCallParams:null]);
					}
				}
			})
		}
	}
	return buttonsArrToReturn;
}

function _setExportButtons(tableID,exportButtonsProp)
{
	var _handlerArray = _handlerObject[tableID];
	
	return dt_ext_setExportButtons(tableID,exportButtonsProp,_handlerArray);
}

function dt_ext_setExportButtons(tableID, exportButtonsConfig, handlers)
{
	var _handlerArray = (handlers)?handlers:{};
	var config = exportButtonsConfig?exportButtonsConfig:{};
	var buttonsArrToReturn = [];
	var exclude = config.exclude?config.exclude:"";
	var displayAs = config.displayAs?config.displayAs:'';

	if(exclude.indexOf("pdf") == -1)
	{
		var pdfPropObj = config["pdf"]?config["pdf"]:{};
		var _pdfTitle = (pdfPropObj["title"] != null)?pdfPropObj["title"]:"Data export";
		var _orientation = (pdfPropObj["orientation"] != null)?pdfPropObj["orientation"]:"portrait";
		var _pageSize = (pdfPropObj["pageSize"] != null)?pdfPropObj["pageSize"]:"A4";
		
		buttonsArrToReturn.push({
			extend:    'pdfHtml5',
			text:      displayAs=='icon'?'<i class="fa fa-file-pdf-o"></i>':'PDF',
			titleAttr: 'PDF',
			orientation: _orientation,
			pageSize:_pageSize,
			title: _pdfTitle,
			exportOptions: {
				columns: ':visible:not(.no-print)'
			},
			customize: function (doc) {            	
				_customizePDFExportFile(tableID, doc, pdfPropObj, _handlerArray);
			}
		});
	}
	if(exclude.indexOf("excel") == -1)
	{
		buttonsArrToReturn.push({
			extend:    'excelHtml5',
			text:      displayAs=='icon'?'<i class="fa fa-file-excel-o"></i>':"Excel",
			titleAttr: 'Excel',
			title:"Data export",
			exportOptions: {
				columns: ':visible'
			}
		});
	}
	if(exclude.indexOf("print") == -1)
	{
		buttonsArrToReturn.push({
			extend:'print',
			exportOptions: {
				columns: ':visible'
			}
		});
	}
	
	return buttonsArrToReturn;
}

function _customizePDFExportFile(tableID, doc, config, handlerArray)
{	
	doc.pageMargins = [10,10,10,30];//[left, top, right, bottom]
	
	// Create a footer object with 3 columns
	// Left side: current page and total pages, user name
	// Right side: report creation date
	doc['footer']=(function(page, pages) {
		return {
			columns: [
				{
					alignment: 'left',
					text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
				},
				{
					alignment: (doc.pageOrientation=='landscape')?'center':'left',
					text: config["username"]?['User Name: ',{text:config["username"]}]:[]
				},
				{
					alignment: (doc.pageOrientation=='landscape')?'right':'left',
					text: ['Local Date & Time: ', { text: moment().format('DD/MMM/YYYY hh:mm A').toString() }]
				}
			],
			margin: [10,10,2,0]
		}
	});

	if(handlerArray.customizePDFExportFile != null)
		handlerArray.customizePDFExportFile.apply(this, [tableID, doc]);
}

function dt_ext_getDtTableById(currTableId)
{
	var dtTable = null;
	var $currTableElem = $('#'+currTableId);
	var isDataTableExists = dt_ext_isDataTable($currTableElem[0]); 
	if(isDataTableExists)
	{
		dtTable = $currTableElem.DataTable();
	}
	return dtTable;
}

function dt_ext_clearDataTableFilterFields(dtTable, dtID)
{
	$('#'+dtID+' tfoot input').val('').change();
	dtTable.search("").draw(true);
}

function dt_ext_goToDTPageByRowInd(dtTable, rowInd)
{
	var _pageSettings = dtTable.page.info();
    var _rowsPerPage = _pageSettings.length;
    var _pageInd = Math.floor(rowInd / _rowsPerPage);
    dtTable.page(_pageInd).draw('page');
}

function dt_ext_clearRowSelection(dtTable)
{
	dtTable.$('tr.selected').removeClass('selected'); 
}

function dt_ext_isDataTable(tableElem)
{
	return $.fn.DataTable.isDataTable(tableElem);
}

// for serverSide=false ONLY
function dt_ext_updateTableData(dtTable, newDataArray)
{
	dtTable.clear();
	dtTable.rows.add(newDataArray);
	dtTable.draw();
}

function dt_ext_destroyTable(currTableElem) 
 {
     if(dt_ext_isDataTable(currTableElem[0]))
     {
         currTableElem.DataTable().destroy();
         currTableElem.empty();
     }
 }

function dt_ext_deleteTableRowById(dtTable, tableRowId)
{
	var rowIdsArr = Array.isArray(tableRowId)?tableRowId:[tableRowId];
	for(var i=0;i<rowIdsArr.length;i++)
	{
		dtTable.row('#dtrow'+rowIdsArr[i]).remove();
	}
	dtTable.draw();
}

function dt_ext_getTableRowNodeById(dtTable, tableRowId)
{
	return dtTable.row('#dtrow'+tableRowId).node();
}

function dt_ext_getDataFromEditableTable(currTableId)
{
    var doContinue = true;
    var fullDataObj = {};
    try{
        var dtTable = dt_ext_getDtTableById(currTableId);
        if(dtTable == null) return;
        
        dtTable.rows().nodes().each( function (row, index, rowApi) 
        {
            if(!doContinue)return;
            
            var rowData = rowApi.data()[index];
            var rowId = rowData[0];
            var $editableCells = $(row).find('.editable-cell');
            var rowDataObj = {};
            $editableCells.each(function(inx,el){
                var $elem = $(el);
                var colId = $elem.attr('columnId');
                var elemTag = $elem.prop('tagName').toLowerCase();
                var colVal = "";
                if(elemTag == 'select')
                {
                    colVal = $elem.find('option:selected').val();
					colVal = colVal?colVal:0;
                }
                else if(elemTag == 'input')
                {
                    colVal = $elem.val().trim();
                }
                rowDataObj[colId] = colVal;
            });

            if(doContinue)
            {
                fullDataObj[rowId] = rowDataObj;
            }
        });
        console.log("getDataFromEditableTable fullDataObj: ", fullDataObj);
    }
    catch (e) {
        console.log("error in getDataFromEditableTable()", e);
        console.log("fullDataObj ", fullDataObj);
        fullDataObj = {};
    }
    return fullDataObj;
}

function _removeColumn(tableID, unqTitle, colIndex) 
{   
	var table = $('#' + tableID).DataTable();

	_get_set_removedColumnsHolder(tableID, unqTitle);

	var currColumn = table.column(colIndex);
	var input = $(currColumn.footer()).find('input.filter-data-column');
    $(input).val('');
    input.trigger('keyup'); 
    currColumn.visible(false);
}

function _resetColumns(tableID)
{
	var dtTable = $('#' + tableID).DataTable();
	var _$header, _uTitle, _column, _headerText, colIsVis;
	var colArray = [];
	var removedArr = _globalRemovedColumnsHolder[tableID];
	var applyResetObj = {"permanentHiddenColsNameArr":[]};

	for (var i = 0; i < dtTable.columns().header().length; i++) {
		_column = dtTable.column(i);
		colIsVis = _column.visible();
		_$header = $(_column.header());
		_uTitle = _$header.attr('uniqueTitle').trim();
		_headerText = _$header.text().trim();
		var _isColRemoveEnabled = _$header.hasClass('removing-disabled')?false:true;

		if(_isColRemoveEnabled)
		{
			var _title = _uTitle;
			if(_headerText != _uTitle ){
				_title = _uTitle+";"+_headerText;
			}
			colArray.push({"title":_title, "isColRemoveEnabled":_isColRemoveEnabled, "colIndex":i});
		}
		else {
			if(!colIsVis)
			{
				applyResetObj["permanentHiddenColsNameArr"].push(_uTitle);
			}
		}
	}

	var mainContainer = "<div id='divDTColumnsDef' class='columns-definition-layout'>";
	var ulAll = "<ul class='checkboxAll'>";
	var ulElem = "<ul id='columnsDefContainer'>";
	var id = 0, checked_counter = 0;
		
	for (var j = 0; j < colArray.length; j++) {
		if (colArray[j] != undefined) 
		{
			var obj = colArray[j];
			var isColRemoveEnabled = obj.isColRemoveEnabled;
			var name = obj.title;
			var val = name;
			if(name.indexOf(';') != '-1'){
				val = name.split(';')[0];
				name = name.split(';')[1];
			}
			if(id == 0)
			{
				ulAll += "<span id='chbAllNone' class='app-icon checkbox-icon' onclick='checkboxToggleCheck(this)'></span><label>All</label>"
				ulAll += "</ul>";
				mainContainer += ulAll;
			}
			id++;			    
			var checked = "", disabled = "", 
				reorderDisabledClass = "", 
				colIndex="colIndex="+obj.colIndex+"";
			
			if (!removedArr || removedArr.indexOf(val) == '-1') {
				checked = "checked";
				checked_counter++;
			}
			
			if(!isColRemoveEnabled) {
				disabled = "disabled";
			}
			
			ulElem += "<li class='ui-state-default "+reorderDisabledClass+"'>";
			ulElem += "<span uniqueTitle='"+val+"' "+colIndex+" class='app-icon checkbox-icon "+checked+" "+disabled+"' onclick='checkboxToggleCheck(this)'></span>"
			ulElem += "<label>"+name+"</label>";
			ulElem += "</li>";	
		}
	}
	ulElem += "</ul>";
	mainContainer += ulElem + "</div>";

	var $container = $($.parseHTML(mainContainer));
	$('body').append($container);
	
	var checkbox_counter = $( "#columnsDefContainer" ).find('span.checkbox-icon').length;
	if(checkbox_counter > 0 && checkbox_counter == checked_counter) 
	{
		$('#chbAllNone').checkbox('checked',true);
	}
	
	$( function() {
		$( "#columnsDefContainer" ).sortable({
			items: "li:not(.ui-state-disabled,.ui-state-default)"
		});
	});
	
	$('#columnsDefContainer span.checkbox-icon').on('click',function(){
		var isChecked = $(this).checkbox('checked');
		if(isChecked) {				
			var unchecked_counter = $( "#columnsDefContainer" ).find('span.checkbox-icon:not(.checked)').length;
			if(checkbox_counter > 0 && unchecked_counter == 0) 
			{
				$('#chbAllNone').checkbox('checked',true);
			}
		}
		else {
			$('#chbAllNone').checkbox('checked',false);
		}
	});
	
	$('#chbAllNone').on('click',function(){
		var chk = false;
		if ($('#chbAllNone').checkbox('checked')) {
			chk = true;
		}
		$('#columnsDefContainer span.checkbox-icon:not(:disabled)').each(function(){
			var $el = $(this);
			$el.checkbox("checked", chk);
		});
	});

	var $dialogElem = createModalDialog({dialogId:"divDTColumnsDef", isDestroyDialog:true, title:"Column Definition", width: 440, height:380
										,buttons:{
											"Apply": function() {
												_resetColumnsApply(tableID, applyResetObj);
												$( this ).dialog( "close" );
											},
											Cancel: function() {
												$( this ).dialog( "close" );
											}
										}
					});	
	$dialogElem.dialog('open');
}

function _resetColumnsApply(tableID, obj)
{
	var removedColsTitleArr = obj.permanentHiddenColsNameArr;

	// showWaitMessage();
	$('#columnsDefContainer span.checkbox-icon').each(function(){
		var $el = $(this);
		if(!$el.checkbox("checked"))
		{
			removedColsTitleArr.push($el.attr('uniqueTitle'));
		}
	});
	_get_set_removedColumnsHolder(tableID, removedColsTitleArr);

	var dtTable = $('#' + tableID).DataTable();
	var _uTitle, _column, _column_settings, _colIsVis;
	for(var i=0; i < dtTable.columns().header().length; i++)
	{	
		_column = dtTable.column(i);
		_column_settings = _column.context[0].aoColumns[i];
		_colIsVis = _column.visible();
		_uTitle = _column_settings.uniqueTitle;
		
		if(removedColsTitleArr.indexOf(_uTitle) > -1)
		{
			_column.visible(false, false); //false means -> defer recalculate column layout for better perfomance
		}
		else if(!colIsVis && _column_settings.removable) {
			_column.visible(true, false); //false means -> defer recalculate column layout for better perfomance
		}
	}
	dtTable.columns.adjust().draw( false ); // adjust column sizing and redraw (recalculate all columns layout after loop)
	// hideWaitMessage();
}

function _get_set_removedColumnsHolder(tableID, unqTitle)
{
	if(Object.keys(_globalRemovedColumnsHolder).length == 0 || !_globalRemovedColumnsHolder.hasOwnProperty(tableID))
	{
		_globalRemovedColumnsHolder[tableID] = [];
	}
	if(unqTitle)
	{
		if(Array.isArray(unqTitle))
		{
			_globalRemovedColumnsHolder[tableID] = unqTitle;
		}
		else {
			_globalRemovedColumnsHolder[tableID].push(unqTitle);
		}
	}
	return _globalRemovedColumnsHolder[tableID];
}

	//Override default implementation for date sorting
	$.extend($.fn.dataTableExt.oSort, 
	{        
		"date_custom-pre": function (a) 
		{
			var x = 0;
			if ( $.trim(a) != "")
			{
				if(a.indexOf(':') != -1)
				{
				  var tmp = a.split(' ');
				  var datea = tmp[0].split('/');
				  var timea = tmp[1].split(':');
				  x = (datea[2] + datea[1] + datea[0] + timea[0] + timea[1]) * 1;
				}
				else
				{
					var datea = a.split('/');
					x = (datea[2] + datea[1] + datea[0]) * 1;
				}
			 }
			 else
			 {
			   x = 10000000;
			 }
			return x;           
		},
		"date_custom-asc": function (a, b) {
			return ((a < b) ? -1 : ((a > b) ? 1 : 0));
		},
		"date_custom-desc": function (a, b) {
			return ((a < b) ? 1 : ((a > b) ? -1 : 0));
		}
	});

$.fn.dataTable.ext.order['dom-custom-sort'] = function  ( settings, col )
{
	var _domObjType = "";
	return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) 
	{
		if(_domObjType == "")
		{
			var $input = $(td).find('input');
			var $textarea = $(td).find('textarea');
			var $select = $(td).find('select');
			
			if($select.length > 0)
			{
				_domObjType = 'select';
			}
			else if($textarea.length > 0)
			{
				_domObjType = 'textarea';
			}
			else if($input.length > 0)
			{
				_domObjType = $input.attr('sorttype');
			}
			else
			{
				_domObjType = 'none';
			}
		}
		
		var _val = "";
		if(_domObjType == 'string')
		{
			_val = $('input', td).val();
		}
		else if(_domObjType == 'numeric')
		{
			var valText = $('input', td).val();
			_val = parseFloat((valText == null || valText.length == 0)?0:valText.trim());
		}
		else if(_domObjType == 'date') /*note:column is sorted as 'date' only if column type defined as 'date'*/
		{
			_val = $('input', td).val();
		}
		else if(_domObjType == 'select')
		{
			_val = $('select >option:selected', td).text();
		}
		else if(_domObjType == 'textarea')
		{
			_val = $('textarea', td).text();
		}
		else
		{
			_val = $(td).html();
		}
		return _val;
	} );
}

	/* Create an array with the values of all the input boxes in a column */
$.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val();
    } );
}
 
/* Create an array with the values of all the input boxes in a column, parsed as numbers */
$.fn.dataTable.ext.order['dom-text-numeric'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val() * 1;
    } );
}
 
/* Create an array with the values of all the select options in a column */
$.fn.dataTable.ext.order['dom-select'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('select > option:selected', td).text();
    } );
}
 
/* Create an array with the values of all the checkboxes in a column */
$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).prop('checked') ? '1' : '0';
    } );
}