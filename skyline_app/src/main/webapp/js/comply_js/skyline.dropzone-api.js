Dropzone.autoDiscover = false;

var dropzoneOptions = {
    url: "UploadServlet",
    // previewsContainer: ".custom-preview-container",
    // addRemoveLinks: true,
    // dictRemoveFile: "Remove File",
    // autoProcessQueue: false,
    timeout: 300000, // in milliseconds (5 min)
    paramName: 'uploadFile',
    uniqueOrder:0, // Define unique number for dropzone instance in case there are more than one dropzone instances on page.
    clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files. Should be overriden when there are more than one dropzone instances on page.
    init:function(){
        var self = this;
        // config
        self.options.addRemoveLinks = true;
        self.options.dictRemoveFile = "Delete";
        self.on('success', function(file, resp) {
            console.log(file);
            console.log(resp);
            if (resp.match(/TIME_IS_OUT/)) 
            {   
                logOff();
                return;
            } 
            if(resp == '-1')
            {
                console.log("error in file upload");
            }
            else {
                var previewElem = $(file.previewElement);
                previewElem.attr({'file_id': resp,'file_name':file.name});
                dzDropzonePreviewOnAfterChange(self);
            }

        });
        self.on('error', function(file, errorMessage, xhr){
            console.log(file);
            console.log(errorMessage);
            console.log(xhr);
        });
        
        // On removing file
        self.on("removedfile", function (file) {
            // console.log(file);
            var previewElem = $(file.previewElement);
            var fileId = previewElem.attr('file_id');
            dzDropzonePreviewOnAfterChange(self);
            $.ajax
            ({ 
                type: "POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "UploadServlet",
                data: "actionid=deleteAttachment&attachId=" + fileId,
                dataType: "text",
                success: function( data ) 
                {  
                    if(data == "1")
                    {
                        console.log("File removed", fileId);
                    }
                    else
                        console.error("Error when removing file", previewElem.attr('file_name'));
                },
                error: handleAjaxError
            });	
        });
    }
};

function dzGetDropzoneInitOptions(dzUnqId)
{
    if(dzUnqId)
    {
        dropzoneOptions.clickable = ".fileinput-button-"+dzUnqId;
        dropzoneOptions.uniqueOrder = dzUnqId;
    }
    return dropzoneOptions;
}

function dzGetDropzoneInstance(el)
{
    return el.get(0).dropzone;
}

function dzGetJustUploadedFilesData($dropzoneContainer)
{   
    // var dzInst = dzGetDropzoneInstance($dropzoneContainer.find('.dropzone'));
    // console.log(dzInst.files);
    // console.log(dzInst.getAcceptedFiles());

    return $dropzoneContainer.find('.dz-preview').map(function(){
        return {fileId:$(this).attr('file_id'), fileName:$(this).attr('file_name')};
    }).get();
}

function dzGetFilesDataFromTable($dropzoneContainer)
{
    return $dropzoneContainer.find('.file-row').map(function(){
        return {fileId:$(this).attr('file_id'), fileName:$(this).attr('file_name')};
    }).get();
}

// function dzGetFilesIdFromTable($dropzoneContainer)
// {
//     return $dropzoneContainer.find('.file-row').map(function(){ return $(this).attr('file_id'); }).get();
// }

function dzCreateFilesViewRows($rowsContainer, dataArr, isViewMode)
{
    var rows = "";
    var $rows = $rowsContainer.find('.file-row');
    var existsRowsCount = $rows.length;

    $.each(dataArr, function(i,item) {
        var inx = i+existsRowsCount;
        var firstRow  = (inx == 0)?"first":"";
        var _odd = (inx%2 == 0)?"odd":"even";
        var btnDwnl = isViewMode?'':'<span class="app-icon download-icon" title="Attachment download" onclick="note_getAttachment('+item.fileId+', \'download\')"></span>';
        var btnRmv = isViewMode?'':'<span class="app-icon remove-icon" onclick="dzRemoveTableRow(this)"></span>'
        var text = '<span class="text">'+item.fileName+'</span>'
        rows += '<div class="file-row '+_odd+' '+firstRow+'" file_id="'+item.fileId+'" file_name="'+item.fileName+'">'+btnDwnl+btnRmv+text+'</div>';
    });

    return $rowsContainer.append(rows);
}

function dzRemoveTableRow(clickedElem)
{
    var $elemParent = $(clickedElem).parent('.file-row');
    var $table = $elemParent.parent('.table-files');
    // remove row
    $elemParent.remove();
    // update row classes
    var $rows = $table.find('.file-row');
    $rows.each(function(inx, el) {
        var $row = $(el)
        var firstRow  = (inx == 0)?$row.hasClass('first')?"":"first":"";
        var _odd_even = (inx%2 == 0)?"odd":"even";
        $row.removeClass('even').removeClass('odd').addClass(_odd_even).addClass(firstRow);
    });
}

function dzClearPreviewContainer($dropzoneContainer)
{
    var inst = dzGetDropzoneInstance($dropzoneContainer.find('.dropzone'));
    inst.files.forEach(function(file) { 
        file.previewElement.remove(); 
      });
      
    $dropzoneContainer.find('.dropzone').removeClass('dz-started');
    dzDropzonePreviewOnAfterChange(inst);
}

function dzDestroyDropzone($dropzoneContainer)
{
    dzGetDropzoneInstance($dropzoneContainer.find('.dropzone')).destroy();
}

function dzDropzonePreviewOnAfterChange(dropzoneInst)
{
    if($(dropzoneInst.previewsContainer).find('.dz-preview').length == 0)
    {
        $('.upload-files-warning-span-'+dropzoneInst.options.uniqueOrder).css('display','none');
    }
    else {
        $('.upload-files-warning-span-'+dropzoneInst.options.uniqueOrder).css('display','inline-block');
    }
}



