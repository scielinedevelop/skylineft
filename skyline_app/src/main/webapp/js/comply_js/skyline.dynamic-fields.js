function initDynamicFields(initObj) {
    $.dynamicFieldsAPI().init(initObj);
}

(function($){

    var dynamicFieldsAPI = function()
    {
        var globalDynamicFieldsMainContainerElem = $('<div class="dynamic-fields-layout" style="width:99%;margin-left:0.5%;float: left;"></div>');
        var globalDialogManageDynamicFieldsId = "dialogManageDynamicFields";
        var dynamicPageViewList = [
            {id:"page", text:"Page"},
            {id:"section", text:"Section"}
        ]
        var dynamicFieldTypes = {
            TEXT_FIELD:"text-field-type",
            NUMBER_FIELD:"number-field-type",
            RICHTEXT_FIELD:"richtext-field-type",
            DROPDOWN_FIELD:"dropdown-field-type",
            RADIO_FIELD:"radio-field-type",
            CHECKBOX_FIELD:"checkbox-field-type",
            DATE_FIELD:"date-field-type",
            GROUP_TITLE_FIELD:"group-title-field-type"
        }
        var dynamicFieldsList = [
            {id:"text", type:dynamicFieldTypes.TEXT_FIELD, text:"Text"},
            {id:"number", type:dynamicFieldTypes.NUMBER_FIELD, text:"Number"},
            {id:"richtext", type:dynamicFieldTypes.RICHTEXT_FIELD, text:"Richtext"},
            {id:"dropdown", type:dynamicFieldTypes.DROPDOWN_FIELD, text:"Dropdown"},
            {id:"radio", type:dynamicFieldTypes.RADIO_FIELD, text:"Radio"},
            {id:"checkbox", type:dynamicFieldTypes.CHECKBOX_FIELD, text:"Checkbox"},
            {id:"date", type:dynamicFieldTypes.DATE_FIELD, text:"Date"},
            {id:"groupTitle", type:dynamicFieldTypes.GROUP_TITLE_FIELD, text:"Title"}
        ];

        function dnm_init(obj)
        {
            var initHtml = '<div class="layout-row">'
            +'      <div class="field-label" style="margin-right:10px"><label>Create fields in </label></div>'
            +'      <div style="width:200px;margin-right:10px">'
            +'          <select id="ddlFieldsContainer"><option value="page">Page</option><option value="pageSection">Section</option></select>'
            +'      </div>'
            +'      <div id="divSectionCount" style="display:none;margin-right:10px">'
            +'          <input type="text" style="width:40px">'
            +'      </div>'
            +'      <div >'
            +'          <button type="button" class="table-secondary-btn">Apply</button>'
            +'      </div>'
            +'  </div>'
            // var elemOptions = '';
            // $(dynamicPageViewList).map(function () {
            //     elemOptions += '<option value="'+this.id+'">'+this.text+'</option>';
            // });
            var html =  '<div class="dynamic-page-view-layout" style="width:99%;margin-left:0.5%;float: left;margin-bottom:15px;">'+initHtml+'</div>';
            var $html = $($.parseHTML(html));

            $html.find('.table-secondary-btn')
                .on('click',function(){dnm_buildInitialPage($('#ddlFieldsContainer').val())})
                .button();
            $html.find('#ddlFieldsContainer').on('change', function(){
                $('#divSectionCount').css('display',(this.value=='page'?'none':'block'));
            });
            $('#dynamicFieldsPlaceholder').append($html);
        }

        function dnm_buildInitialPage(containerType)
        {
            var rowsCount = (containerType == "pageSection")?3:10;
            var rowsHtml = "";
            
            for(var i=0;i<rowsCount;i++)
            {
                var addClass = i==0?"row-first":"";
                rowsHtml += '<div class="row '+addClass+'">';

                for(var j=0;j<2;j++) {
                    var addClass = j==0?"col-first":"";
                    var colHtml = '<div class="col col-6 '+addClass+'"><div class="rect-area-icons" ><span class="app-icon plus-icon"></span></div></div>';
                    rowsHtml += colHtml;
                }
                rowsHtml += '</div>';
            }
            if(containerType == "page")
            {
                globalDynamicFieldsMainContainerElem.empty().append(rowsHtml);
            }
            if(containerType == "pageSection")
            {
                globalDynamicFieldsMainContainerElem.empty();
                var sectionsCount = $('#divSectionCount').find('input').val().trim();
                sectionsCount = sectionsCount?sectionsCount:1;
                for(var i=0;i<sectionsCount;i++)
                {
                    dnm_createPageSection({content:rowsHtml});
                }
            }
            
            // globalDynamicFieldsMainContainerElem.disableSelection();
            globalDynamicFieldsMainContainerElem.find('.plus-icon').on('click', function(e){
                dnm_manageField(this);
            });
            $('#dynamicFieldsPlaceholder').append(globalDynamicFieldsMainContainerElem);
        }

        function dnm_createPageSection(options) 
        {
            var html = '<div class="section-parent ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">'+
                        '<div class="section-header">'+
                            '<div style="width:20px;float:left;">'+
                                '<span class="app-icon span-collapse-expand collapse-icon" title="Collapse" ></span>'+
                            '</div>'+			    	
                            '<div style="width:30%;float:left;" >' +
                            '<p class="header-contenteditable header-edit-mode" contenteditable="true" '+
                                    'data-placeholder=" Type section header here ..."></p>' +
                            '</div>'+
                            '<div style="float:right;">'+
                                '<span class="app-icon remove-icon" title="Remove section"></span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="section-content section-toggle-content" style="float: left;width: 100%;">'+
                            options.content + 
                        '</div>'+
                    '</div>';
            var $newSection = $($.parseHTML(html));
            $newSection.find('.header-edit-mode').on('keydown', function(event) {dnm_onSectionHeaderChange(event)});
            $newSection.find('.span-collapse-expand').on('click', function() {toggleSectionCollapse(this);});
            $newSection.find('.remove-icon').on('click', function() {dnm_removeSection(this);});
            globalDynamicFieldsMainContainerElem.append($newSection);
            return $newSection;
        }

        function dnm_onSectionHeaderChange(e)
        {
            if (e.keyCode === 13) {
                e.preventDefault();
            }
        }

        function dnm_removeSection(icon) 
        {
            var currSection = $(icon).closest('.section-parent');
            if (currSection.find('.richtext-editor').length > 0)
            {
                var $editor = currSection.find('.richtext-editor');
                $editor.richtext('destroy');
            }
            currSection.remove();
        }

        function dnm_manageField(el)
        {
            var html = '<div id="'+globalDialogManageDynamicFieldsId+'" class="html-popup-layout fields-manager" >'
                        +'  <div class="layout-row">'
                        +'      <div class="layout-cell cell-label field-label"><label>Name</label></div>'
                        +'      <div class="layout-cell cell-element"  >'
                        +'          <input type="text" id="txtFieldName" style="width:100%">'
                        +'      </div>'
                        +'  </div>'
                        +'  <div class="layout-row">'
                        +'      <div class="layout-cell cell-label field-label"><label>Field Type</label></div>'
                        +'      <div class="layout-cell cell-element"  style="width:200px">'
                        +'          <select id="ddlFieldType" class="chosen_single"></select>'
                        +'      </div>'
                        +'  </div>'
                        +'  <div class="layout-row">'
                        +'      <div class="layout-cell cell-label field-label"><label>Required</label></div>'
                        +'      <div class="layout-cell cell-element">'
                        +'          <span id="chbFieldRequired" class="app-icon checkbox-icon" onclick="checkboxToggleCheck(this);"></span>'
                        +'      </div>'
                        +'  </div>'
                // <div class="layout-row" style="display: none;">
                //     <div class="layout-cell cell-label field-label"><label>Range</label></div>
                //     <div class="layout-cell cell-element"  >
                //         <span id="chbFieldDateRange" class='app-icon checkbox-icon' onclick="checkboxToggleCheck(this);"></span>
                //     </div>
                // </div>
                        +'  <div id="divDropdownOptions" class="layout-row" style="display: none;">'
                        +'      <div class="layout-cell cell-label field-label"><label>Options</label></div>'
                        +'      <div class="layout-cell cell-element"  >'
                        +'          <div id="sortableDynamicOptions" style="float: left;width: 100%;"></div>'
                        +'          <div><span id="btnAddOption" style="padding-right: 5px;" class="app-icon plus-icon">Add Option</span></div>'
                        +'      </div>'
                        +'  </div>'
                        +'</div>';
            
            var $html = $($.parseHTML(html));
		    $('body').append($html);

            var elemOptions = '';
            $(dynamicFieldsList).map(function () {
                elemOptions += '<option fieldType="' + this.type + '" value="'+this.id+'">'+this.text+'</option>';
            });
            $html.find('#ddlFieldType')
                    .html(elemOptions)
                    .on('change', function(){dnm_onFieldTypeChange(this.value)});

            $html.find('#btnAddOption').on('click', function(){dnm_addOption()});
            $html.find('#sortableDynamicOptions').sortable();

            var $currRectArea = $(el).closest('.col');
            var $dialogElem = createModalDialog({dialogId:globalDialogManageDynamicFieldsId, isDestroyDialog:true, title:"Add new field", 
                                width: 550, height:450,
                                buttons:{
                                            "Attach": function() {
                                                dnm_appendField($currRectArea);
                                            },
                                            Cancel: function() {
                                                $( this ).dialog( "close" );
                                            }
                                        }
                            });	
                $dialogElem.dialog('open');
        }

        function dnm_appendField($rectArea)
        {
            var form = $('#'+globalDialogManageDynamicFieldsId);
            var obj = {};
            var fieldTypeId = form.find('#ddlFieldType option:selected').val();
            if(fieldTypeId == 'dropdown')
            {
                obj = {dropdown_options:$('#sortableDynamicOptions').find('.sortable-dynamic-options').map(function(){return $(this).val();}).get()};
            }
            var obj = {
                fieldLabel:form.find('#txtFieldName').val().trim(),
                fieldTypeId:fieldTypeId,
                fieldType:form.find('#ddlFieldType option:selected').text(),
                fieldRequired:form.find('#chbFieldRequired').checkbox('val'),
                optionalData:obj
            }
            
            var htmlElem = dnm_getFieldHtmlByType(obj);
            $rectArea.append(htmlElem);
            if(fieldTypeId == 'dropdown')
            {
                $('.chosen_single').chosen({width:'100%'});
            }
            dnm_initDragAndDrop();
            $("#"+globalDialogManageDynamicFieldsId).dialog('close');
        }

        function dnm_getFieldHtmlByType(obj)
        {
            var optionalData = obj.optionalData;
            var requiredHtml = obj.fieldRequired?'<span class="mandatoryFieldMark">*</span>':'';
            var html = '<div class="sortable-handler">'
                        +'<div style="float: left;width: 20%;" class="field-label">'
                        +   requiredHtml
                        +'  <label>'+obj.fieldLabel+'</label>'
                        +'</div>';
            if(obj.fieldTypeId == 'text')
            {
                html += '<div style="float: left;width: 50%;">'
                        +'  <input type="text" style="width:100%">'
                        +'</div>';
            }
            else if(obj.fieldTypeId == 'checkbox')
            {
                html += '<div style="float: left;width: 50%;">'
                        +'  <span class="app-icon checkbox-icon" onclick="checkboxToggleCheck(this);"></span>'
                        +'</div>';
            }
            else if(obj.fieldTypeId == 'dropdown')
            {
                var elemOptions = '<option value="-1">Choose</option>';
                for(var i=0;i<optionalData.dropdown_options.length;i++) {
                    var val = optionalData.dropdown_options[i];
                    elemOptions += '<option value="'+val+'">'+val+'</option>';
                };
                html += '<div style="float: left;width: 50%;">'
                        +'  <select class="chosen_single">'+elemOptions+'</select>'
                        +'</div>';
            }
            html += '</div>';
            return html;
        }

        function dnm_onFieldTypeChange(type)
        {
            if(type == 'dropdown')
            {
                $('#divDropdownOptions').css('display','');
            }
            else {
                $('#divDropdownOptions').css('display','none');
            }
        }

        function dnm_addOption()
        {
            var html = '<div style="float: left;width: 70%;margin-bottom: 10px;">'
                        +'  <span class="app-icon fa fa-arrows" style="font-size: 16px;"></span>'
                        +'  <input class="sortable-dynamic-options" type="text" style="width:75%">'
                        +'  <span class="app-icon remove-icon" style="font-size: 19px;"></span>'
                        +'</div>';
            $('#sortableDynamicOptions').append(html);
        }

        function dnm_initDragAndDrop()
        {
            var droppableParent;
                
            $('.sortable-handler').draggable({
                revert: 'invalid',
                revertDuration: 200,
                start: function () {
                    droppableParent = $(this).parent();
                
                    $(this).addClass('being-dragged');
                },
                stop: function () {
                    $(this).removeClass('being-dragged');
                }
            });
                
            $('.col').off().droppable({
                hoverClass: 'drop-hover',
                drop: function (event, ui) {
                    var draggable = $(ui.draggable[0]),
                        draggableOffset = draggable.offset(),
                        container = $(event.target),
                        containerOffset = container.offset();
                    
                    $('.sortable-handler', event.target).appendTo(droppableParent).css({opacity: 0}).animate({opacity: 1}, 200);
                    
                    // draggable.appendTo(container).css({left: draggableOffset.left - containerOffset.left, top: draggableOffset.top - containerOffset.top}).animate({left: 0}, 200);
                    draggable.appendTo(container).css({left: draggableOffset.left - containerOffset.left, top: '30%'}).animate({left: 0}, 200);
                }
            });
        }

        return {
            "init":function(obj) {
                dnm_init(obj);
            }
        }
    }

    $.dynamicFieldsAPI = dynamicFieldsAPI;
})(jQuery)