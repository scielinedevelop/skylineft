function initNotebook(initObj) {
    $.notebookAPI().init(initObj);
}

function saveNotebook(saveParams)
{
    $.notebookAPI().save(saveParams);
}

function printNotebook()
{
    $.notebookAPI().print();
}

function showNotebookAT()
{
    $.notebookAPI().print('history');
}

function resizeNotebookTables(options)
{
    $.notebookAPI().resizeTable(options);
}

(function ($)
{
    this.notebookSectionTypes = {
        TEST_RESULT_SECTION: "test-result-section",
        PARAMETER_SECTION: "parameter-section",
        MATERIAL_SECTION: "material-section",
        RICHTEXT_SECTION: "richtext-section",
        RICHTEXT_VIEW_SECTION: "richtext-view-section",
        FILE_SECTION: "file-section",
        SPREADSHEET_SECTION: "spreadsheet-section"
    }
    this.notebookTableNames = {
        TEST_RESULT_TABLENAME: "notebookTestResultsTable",
        PARAMETER_TABLENAME: "notebookParametersTable",
        MATERIAL_TABLENAME: "notebookMaterialsTable"
    }

    this.notebookImplementationTypes = {
        IS_RUN:"RUN",
        IS_STAGE:"STAGE",
        IS_STDL:"STANDALONE"
    }
    this.globalNotebookElementId = 'notebookSortableSections';

    var globalNotebookParentId = 0;
    var globalNotebookImplType;
    var globalNotebookIsInitialized = false;
    var globalNotebookSectionsCounter = 1;
    var globalNotebookIsDisabled = false;
    var globalNotebookIsInViewMode = false;
    var globalNotebokTableBlocksSaveState = {};
    var globalSectionsToPrint = [];
    var notebookSectionsList = [];

    var notebookAPI = function () {
    
        function note_initNotebook(obj) {
        
            var reload = true;
            var isViewMode = false;
            globalNotebookImplType = obj.parentType?obj.parentType:'';
            globalSectionsToPrint = (obj.sectionsToPrint)?obj.sectionsToPrint.split(','):[];
            
            if((globalNotebookImplType == notebookImplementationTypes.IS_RUN) 
                && (obj.forceViewMode || (!obj.forceEditMode && note_defineNotebookInViewMode(obj.parentId, obj.parentType)))
            )
            {
                isViewMode = true;
            }
        
            var displayModeChanged = ((globalNotebookImplType == notebookImplementationTypes.IS_RUN) && globalNotebookIsInViewMode != isViewMode)?true:false;
            globalNotebookIsInViewMode = isViewMode;
        
            if(globalNotebookIsInitialized) 
            {    
                // reload notebook only in case parent or displayMode were changed
                reload = ((globalNotebookParentId != obj.parentId) || displayModeChanged)?true:false;
                if(!reload && !globalNotebookIsInViewMode)
                {
                    note_synchronizeSections();
                }
            }
        
            // redefine global vars
            globalNotebookParentId = obj.parentId?obj.parentId:-1;
            globalNotebookIsDisabled = obj.isDisabled?obj.isDisabled:false;
            
            if(!globalNotebookIsInitialized) 
            {
                // should be run only once during first initialization
                note_buildNotebookMainHtml();
            }
            if(globalNotebookIsDisabled || globalNotebookIsInViewMode)
            {
                $('#notebookSelectionList').css('display','none');
                destroyFloatingButtonPanel();
            }
            else {
                $('#notebookSelectionList').css('display','block');
                initFloatingButtonPanel();
            }
        
            globalNotebookIsInitialized = true;
        
            if(isAlive() == 0)
            {
                logOff();
                return;
            }
            if(reload) {
                note_clearNotebookSections();
                note_loadNotebookConfig();
            }
        
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
            {
                // call to notebook parent function
                if(typeof notebookFloatBtn_toggleDisplayMode === 'function')
                {
                    notebookFloatBtn_toggleDisplayMode(isViewMode);
                }
            }
        }
        
        function note_createSectionByType(type)
        {
            if(globalNotebookIsInViewMode)
            {
                return note_createViewableSectionByType(type);
            }
            else {
                return note_createEditableSectionByType(type);
            }
        }
        
        function note_createViewableSectionByType(type)
        {
            var html = '';
            var newSection = '';
            var funcAfterSectionCreation = null;
            
            if(type == notebookSectionTypes.RICHTEXT_SECTION || type == notebookSectionTypes.RICHTEXT_VIEW_SECTION)
            {
                html = '<div style="float:left;width:100%" class="richtext-viewonly-mode"><div  class="richtext-editor" ></div></div>';
                funcAfterSectionCreation = function() {
                        newSection.find('.richtext-editor').richtext({viewonly:true});
                }
            }	
            else if(type == notebookSectionTypes.FILE_SECTION)
            {
                html = '<div class="notebook-files-preview-container"></div>' +'<div class="table-files"></div>';
            }
            else if(type == notebookSectionTypes.SPREADSHEET_SECTION)
            {
                html = '<iframe class="iFrameSpreadsheet" width="100%" height="400" frameborder=0 src="about:blank"></iframe>';
                funcAfterSectionCreation = function() {
                    newSection.find('.section-content').css('padding','0');
                }
            }
            else if(type == notebookSectionTypes.TEST_RESULT_SECTION || type == notebookSectionTypes.PARAMETER_SECTION || type == notebookSectionTypes.MATERIAL_SECTION) {
                var impl = noteImplAPI()._getViewableSectionByType(type);
                html = impl.html;
                globalNotebookSectionsCounter = impl.sectionCounter;
            }
        
            newSection = note_createSection({
                content: html,
                sectionType: type,
                preventFromDrag:true
            });
            if(funcAfterSectionCreation)
            {
                funcAfterSectionCreation();
            }
            return newSection;
        }
        
        function note_createEditableSectionByType(type)
        {
            var newSection = "", html = "", tableUnqId = "", fileSectionNum = "";
            var updatedOpt = {};
            var funcAfterSectionCreation = null;
            if(type == notebookSectionTypes.RICHTEXT_SECTION)
            {
                html = ((globalNotebookImplType == notebookImplementationTypes.IS_RUN)?'':'<div style="float:left;width:100%;margin-bottom: 10px;">'+
                                '<span class="app-icon checkbox-icon checkbox-viewonly" onclick="checkboxToggleCheck(this)"></span>' + 
                                '<span style="margin-left:10px;">View Only</span></div>')+
                        '<div style="float:left;width:100%"><div  class="richtext-editor" ></div></div>';
                funcAfterSectionCreation = function() {
                    newSection.find('.richtext-editor').richtext();
                }
            }
            else if(type == notebookSectionTypes.RICHTEXT_VIEW_SECTION)
            {
                html = '<div style="float:left;width:100%" class="richtext-viewonly-mode"><div  class="richtext-editor" ></div></div>';
                funcAfterSectionCreation = function() {
                    newSection.find('.richtext-editor').richtext({viewonly:true});
                }
            }	
            else if(type == notebookSectionTypes.FILE_SECTION)
            {
                fileSectionNum = globalNotebookSectionsCounter++;
                html = '<div style="margin-bottom: 5px;">' +
                            '<button type="button" class="table-primary-btn new-button fileinput-button-'+fileSectionNum+'"><span>Add files...</span></button>'+
                            '<span class="upload-files-warning-span-'+fileSectionNum+'" style="margin-left: 10px;display:none;">The documents are temporary and need to be saved</span>'+
                        '</div>' +
                        '<form class="dropzone">'+
                            '<input type="hidden" id="actionid" name="actionid" value="upload" /> '+
                            '<div class="fallback">'+    
                            '<input name="uploadFile" type="file" />'+
                            '</div>'+
                        '</form>'+
                        '<div class="table-files"></div>';
                funcAfterSectionCreation = function() {
                    newSection.find('.table-primary-btn').button();
                    newSection.find('.dropzone').dropzone(dzGetDropzoneInitOptions(fileSectionNum));
                }
            }
            else if(type == notebookSectionTypes.SPREADSHEET_SECTION)
            {
                updatedOpt = {preventFromDrag:true};
                html = '<iframe class="iFrameSpreadsheet" width="100%" height="400" frameborder=0 src="about:blank"></iframe>';
                funcAfterSectionCreation = function() {
                    newSection.find('.section-header').find('.maximize-icon').css('display','');
                    newSection.find('.section-content').css('padding','0');
                }
            }
            else if(type == notebookSectionTypes.TEST_RESULT_SECTION || type == notebookSectionTypes.PARAMETER_SECTION || type == notebookSectionTypes.MATERIAL_SECTION) {
                var impl = noteImplAPI()._getEditableSectionByType(type);
                html = impl.html;
                globalNotebookSectionsCounter = impl.sectionCounter;
                funcAfterSectionCreation = impl.funcAfterSectionCreation;
            }
        
            var options = $.extend({
                content: html,
                sectionType: type,
                preventFromDrag:false
            }, updatedOpt );
            newSection = note_createSection(options);
            if(funcAfterSectionCreation)
            {
                funcAfterSectionCreation(newSection);
            }
            return newSection;
        }
            
        function note_createSection(options) 
        {
            var disabledClass = globalNotebookIsDisabled?"disabled":"";
            var html = '';
            var preventFromDragClass = (options.preventFromDrag)?"prevent-drag-section":"";
            if(globalNotebookIsInViewMode)
            {
                html = '<div class="section-parent notebook-section" sectionType="'+options.sectionType+'">'+
                            '<div class="section-header section-preview '+preventFromDragClass+'">'+
                                '<div style="width:30%;float:left;" >'+ 
                                    '<p class="header-contenteditable header-view-mode"></p>' +
                                '</div>'+
                            '</div>'+
                            '<div class="section-content section-preview" style="float: left;width: 100%;">'+
                                options.content + 
                            '</div>'+
                        '</div>';
            }
            else {
                html = '<div class="section-parent notebook-section ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" sectionType="'+options.sectionType+'">'+
                            '<div class="section-header '+preventFromDragClass+'">'+
                                '<div style="width:20px;float:left;">'+
                                    '<span class="app-icon span-collapse-expand collapse-icon" title="Collapse" ></span>'+
                                '</div>'+			    	
                                '<div style="width:30%;float:left;" >' +
                                '<p class="header-contenteditable header-edit-mode '+disabledClass+'" contenteditable="true" '+
                                        'data-placeholder=" Type section header here ..."></p>' +
                                '</div>'+
                                '<div style="float:right;">'+
                                    '<span class="app-icon maximize-icon" title="Maximize" onclick="toggleSectionFullScreen(this)" style="display:none;margin-right: 5px;"></span>'+
                                    '<span class="app-icon remove-icon '+disabledClass+'" title="Remove section"></span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="section-content section-toggle-content '+disabledClass+'" style="float: left;width: 100%;">'+
                                options.content + 
                            '</div>'+
                        '</div>';
            }
            var $newSection = $($.parseHTML(html));
            $newSection.find('.header-edit-mode').on('keydown', function(event) {note_onSectionHeaderChange(event)});
            $newSection.find('.span-collapse-expand').on('click', function() {toggleSectionCollapse(this);note_onSectionStateChange(this);});
            $newSection.find('.remove-icon').on('click', function() {note_removeSection(this);});

            $('#'+globalNotebookElementId).append($newSection);
            return $newSection;
        }
        
        function note_onSectionStateChange(el)
        {
            var $section = $(el).closest(".section-parent");
            var sectionType = $section.attr('sectionType');
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN &&  sectionType == notebookSectionTypes.TEST_RESULT_SECTION && $section.attr('sectionCollapsedOnLoad'))
            {
                $section.removeAttr('sectionCollapsedOnLoad');
                note_loadNotebookTableBySection($section);
            }
            else if(globalNotebookImplType == notebookImplementationTypes.IS_RUN &&  sectionType == notebookSectionTypes.SPREADSHEET_SECTION && $section.attr('sectionCollapsedOnLoad'))
            {
                $section.removeAttr('sectionCollapsedOnLoad');
                var $iframe = $section.find('.iFrameSpreadsheet');
                var src = $iframe.attr('srcToLoad');
                $iframe.attr('src',src);
                $iframe.removeAttr('srcToLoad');
            }
        }

        function note_loadNotebookTableBySection($section, filter)
        {
            noteImplAPI()._loadNotebookTableBySection($section, filter);
        }
        
        function note_onSectionHeaderChange(e)
        {
            if (e.keyCode === 13) {
                e.preventDefault();
            }
        }
            
        function note_addNewSection()
        {
            var $elem = $('#ddlSections');
            var selOption = $elem.find(':selected');
            if(selOption.val() == '0')
            {
                displayAlertDialog("Please, choose the section");
                return;
            }
            var type = selOption.attr('sectionType');
            $elem.val('0').trigger('chosen:updated');
        
            if(type == notebookSectionTypes.TEST_RESULT_SECTION || type == notebookSectionTypes.PARAMETER_SECTION)
            {
                var tables = $('#'+globalNotebookElementId).find('.notebook-section[sectiontype="'+type+'"]');
                if(tables.length > 0)
                {
                    displayAlertDialog(selOption.text() + " table section already exists");
                    return;
                }
            }
            var $newSection = note_createSectionByType(type); 
            note_loadSectionData($newSection, type);
        }
        
        function note_loadSectionData($section, type, savedData)
        {
            if(globalNotebookIsInViewMode)
            {
                note_loadViewableSectionData($section,type,savedData);
            }
            else {
                note_loadEditableSectionData($section,type,savedData);
            }
        }
        
        function note_loadViewableSectionData($section, type, savedData)
        {
            if(type == notebookSectionTypes.TEST_RESULT_SECTION || type == notebookSectionTypes.PARAMETER_SECTION || type == notebookSectionTypes.MATERIAL_SECTION)
            {
                note_loadNotebookTableBySection($section, savedData);
            }
            else if(type == notebookSectionTypes.RICHTEXT_SECTION || type == notebookSectionTypes.RICHTEXT_VIEW_SECTION)
            {
                if(savedData)
                {
                    var $editor = $section.find('.richtext-editor');
                    loadRichtextContentById($editor, savedData.value);
                }
            }
            else if(type == notebookSectionTypes.FILE_SECTION)
            {
                var previewContainer = $section.find('.notebook-files-preview-container');
                var iframeHtml = "";
                var displayInTableArr = [];
                var displayInIframeArr = [];
                $.each(savedData.value, function(i,item) 
                {
                    if(note_isFileSupportedToView(item.fileName))
                    {
                        displayInIframeArr.push(item);
                        iframeHtml += '<iframe name="notebook_Iframe_'+item.fileId+'" style="width: 45%; height: 50vh; display: inline-block; margin-left: 10px;" src="about:blank"></iframe>';
                    }
                    else {
                        displayInTableArr.push(item);
                    }
                });
                previewContainer.html(iframeHtml);
                $.each(displayInIframeArr, function(i,item) {
                    note_getAttachment(item.fileId, "showInline");
                });
                dzCreateFilesViewRows($section.find('.table-files'), displayInTableArr, true);
            }
            else if(type == notebookSectionTypes.SPREADSHEET_SECTION)
            {
                var $ssFrame = $section.find('.iFrameSpreadsheet');
                var src = "spreadsheetservlet";
                if(savedData)
                {
                    var config = {isViewMode:globalNotebookIsInViewMode};
                    src += "?spreadsheetId="+savedData.value + "&configObject=" + encodeURIComponent(JSON.stringify(config));
                    console.log("spreadsheet config", JSON.stringify(config));
                }
                $ssFrame.attr('src',src);
            }
        }
        
        function note_loadEditableSectionData($section, type, savedData)
        {
            if(type == notebookSectionTypes.RICHTEXT_SECTION)
            {
                if(savedData)
                {
                    var $editor = $section.find('.richtext-editor');        
                    if(!globalNotebookImplType == notebookImplementationTypes.IS_RUN) {
                        $section.find('.checkbox-viewonly').checkbox('val',savedData.isViewonly);
                    }
                    loadRichtextContentById($editor, savedData.value);
                }
            }
            else if(type == notebookSectionTypes.RICHTEXT_VIEW_SECTION)
            {
                if(savedData)
                {
                    var $editor = $section.find('.richtext-editor');
                    loadRichtextContentById($editor, savedData.value);
                }
            }
            else if(type == notebookSectionTypes.SPREADSHEET_SECTION)
            {
                var $ssFrame = $section.find('.iFrameSpreadsheet');
                var src = "spreadsheetservlet";
                if(savedData)
                {
                    var config = {isViewMode:globalNotebookIsDisabled};
                    src += "?spreadsheetId="+savedData.value + "&configObject=" + encodeURIComponent(JSON.stringify(config));
                    console.log("spreadsheet config", JSON.stringify(config));
                    if(isSectionCollapsed($section))
                    {
                        $ssFrame.attr('srcToLoad',src);
                        src = 'about:blank';
                    }
                }
                $ssFrame.attr('src',src);
            }
            else if(type == notebookSectionTypes.FILE_SECTION)
            {
                if(savedData)
                {
                    dzCreateFilesViewRows($section.find('.table-files'), savedData.value, false);
                }
            }
            else if(type == notebookSectionTypes.TEST_RESULT_SECTION || type == notebookSectionTypes.PARAMETER_SECTION || type == notebookSectionTypes.MATERIAL_SECTION) {
                noteImplAPI()._loadEditableSectionData($section, type, savedData);
            }
        }
        
        function note_gatherSectionsData()
        {
            var fullObj = {};
            // get all sections
            var $sections = $('.notebook-section');
            $sections.each(function(inx,el) {
                var $section = $(el);
                var type = $section.attr('sectionType');
                var data = note_getSectionContentValue($section.find('.section-content'), type);
                var sectionId = $section.attr('sectionId');
        
                fullObj[inx+1] = {
                    collapsed:isSectionCollapsed($section),
                    title:$section.find('.header-contenteditable').html(),
                    type:type,
                    data:data,
                    sectionId:(!sectionId || sectionId == "")?-1:sectionId
                };
            });
            console.log("sections to save:", JSON.stringify(fullObj));
            return fullObj;
        }
        
        function note_getSectionContentValue($content, type)
        {   
            if(type == notebookSectionTypes.RICHTEXT_SECTION)
            {
                var $editor = $content.find('.richtext-editor');
                var id = saveRichtextContent($editor);
                var chkElem = $content.find('.checkbox-viewonly');
                var _isViewonly = (chkElem.length > 0)?chkElem.checkbox('val'):0;
                return {
                        isViewonly: _isViewonly,
                        value: id    
                    };
            }
            else if(type == notebookSectionTypes.RICHTEXT_VIEW_SECTION)
            {
                var $editor = $content.find('.richtext-editor');
                return {
                    isViewonly: 1,
                    value: $editor.attr("content_id")    
                };
            }
            else if(type == notebookSectionTypes.SPREADSHEET_SECTION)
            {
                var $ssFrame = $content.find('.iFrameSpreadsheet');
                var id = $ssFrame[0].contentWindow.saveSpreadsheetContent();
                // $ssFrame.attr('spreadsheet_id',id);
                return {
                        value: id    
                    };
            }
            else if(type == notebookSectionTypes.FILE_SECTION)
            {
                return {
                    value: dzGetFilesDataFromTable($content).concat(dzGetJustUploadedFilesData($content))
                };
            }
            else if(type == notebookSectionTypes.TEST_RESULT_SECTION || type == notebookSectionTypes.PARAMETER_SECTION || type == notebookSectionTypes.MATERIAL_SECTION) {
                return noteImplAPI()._getSectionContentValue($content, type);
            }
        }
        
        function note_getSelectedValuesFromDDL ($ddl, returnType)
        {
            var $selected = $ddl.find(':selected');
            if(returnType && returnType == 'id_val')
            {
                return $selected.map(function(){return {id:$(this).val(), val:$(this).text()}}).get();
            }
            else {
                return $selected.map(function(){return  $(this).val();}).get();
            }    
        }
        
        function note_populateSections(data)
        {
            console.log("notebook config data: ",data);
            if(data == null || data.length == 0) return;
        
            var sectionsObj = JSON.parse(data);
            for(key in sectionsObj)
            {
                var sectionData = sectionsObj[key];
                var sectionType = sectionData["type"];
                var contentData = sectionData["data"];
                var sectionId = sectionData["sectionId"];
                // for notebook print
                if(globalSectionsToPrint.length > 0 && ($.inArray(sectionId, globalSectionsToPrint) == -1))
                {
                    continue;
                }
                if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
                {
                    if(sectionType == notebookSectionTypes.RICHTEXT_SECTION && contentData["isViewonly"] == "1")
                    {
                        sectionType = notebookSectionTypes.RICHTEXT_VIEW_SECTION;
                    }
                }
                var $section = note_createSectionByType(sectionType);
                if($section)
                {
                    $section.find('.header-contenteditable').html(sectionData["title"]);
                    $section.attr('sectionId',sectionId);
        
                    if(!globalNotebookIsInViewMode && sectionData["collapsed"]) {
                        var $span = $section.find('.span-collapse-expand');
                        toggleSectionCollapse($span[0]);
                        $section.attr('sectionCollapsedOnLoad','1');
                    }
                    note_loadSectionData($section, sectionType, contentData);
                }
            }
        }
        
        function note_onDDLChanged(elem, selectedArr, sectionType)
        {
            var $elem = $(elem);
            var $content = $elem.closest('.section-content');
            if(sectionType == notebookSectionTypes.TEST_RESULT_SECTION)
            {
                var spIdList = note_getSelectedValuesFromDDL($content.find('.select-sample-points')).join();
                $.ajax
                ({ 
                    type: "POST",
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    url: "notebookservlet",
                    data: "action_id=getTestsData&parentId="+globalNotebookParentId+"&parentType="+globalNotebookImplType+"&spIdList="+((spIdList=="")?"-1":spIdList),
                    dataType: "json",
                    success: function( data ) 
                    {  			
                        var $el = $content.find('.select-tests');
                        note_updateTableFilterList($el, data.test, note_convertTestFromObjectToString(selectedArr));
                        if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
                        {
                            note_toggleDisplayTableButtons($content.find('[tableName="'+notebookTableNames.TEST_RESULT_TABLENAME+'"]'), true, '.apply-button');
                        }
                    },
                    error: handleAjaxError
                });	
            }
        }

        function note_toggleDisplayTableButtons ($currTableElem, isShow, buttonIdentifier)
        {
            noteImplAPI()._toggleDisplayTableButtons($currTableElem, isShow, buttonIdentifier);
        }
        
        function note_convertTestFromObjectToString (testArr)
        {
            var toReturn = [];
            for(var i=0;i<testArr.length;i++)
            {
                var delim = '@_unqdelim_@'; // defined in db view
                var obj = testArr[i];
                if(isJsonObject(obj)) {
                    toReturn[i] = obj.testName+delim+obj.testCode;
                }
            }
            return toReturn;
        }
        
        function note_updateTableFilterList($currElem, data, selectedArr)
        {
            customSelectPopulate($currElem, JSON.parse(data), selectedArr, {useMultiDefaultOption:true});
        }
        
        function note_parseTableFilter (filterArr)
        {
            var returnArr = [];
            for(var i=0;i<filterArr.length;i++)
            {
                returnArr[i] = filterArr[i].id;
            }
            return returnArr;
        }
        
        function note_removeSection(icon) 
        {
            var currSection = $(icon).closest('.notebook-section');
            var sectionType = currSection.attr('sectiontype')
            if (sectionType == notebookSectionTypes.RICHTEXT_SECTION)
            {
                var $editor = currSection.find('.richtext-editor');
                $editor.richtext('destroy');
            }
            else if(sectionType == notebookSectionTypes.FILE_SECTION)
            {
                dzDestroyDropzone(currSection);
            }
            else if(sectionType == notebookSectionTypes.PARAMETER_SECTION || sectionType == notebookSectionTypes.MATERIAL_SECTION) {
                noteImplAPI()._onSectionRemove(currSection);
            }
            currSection.remove();
        }
        
        function note_loadNotebookConfig()
        {
            console.log("LOAD NOTEBOOK CONFIGURATION");
            console.log("parentID", globalNotebookParentId);
            $.ajax
            ({ 
                type: "POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "notebookservlet",
                data: "action_id=getNotebookConfigIdByParent&parentId="+globalNotebookParentId + "&parentType="+globalNotebookImplType,
                dataType: "text",
                success: function( data ) 
                {  
                    if(data == "-1")
                    {
                        console.log("error in get notebook configuration id");
                        note_get_set_notebookConfigId("-1");
                    }
                    else if(data == "0")
                    {
                        console.log("no data found");
                        note_get_set_notebookConfigId("-1");
                    }
                    else
                    {
                        console.log("config id", data);
                        note_get_set_notebookConfigId(data);
                        fnGetClobContent(data, note_populateSections);
                    }
                },
                error: handleAjaxError
            });    
        }
        
        function note_saveNotebook(params)
        {
            showWaitMessage("Saving, Please wait...");
            globalNotebokTableBlocksSaveState = {};
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
            {
                var obj = noteImplAPI()._saveTableData(-1, notebookTableNames.TEST_RESULT_TABLENAME);
                obj = noteImplAPI()._saveTableData(-1, notebookTableNames.PARAMETER_TABLENAME);
                if(obj.saveState) globalNotebokTableBlocksSaveState = obj.saveState;
            }
            else if(globalNotebookImplType == notebookImplementationTypes.IS_STDL)
            {
                noteImplAPI()._saveTableData(-1, notebookTableNames.PARAMETER_TABLENAME);
            }
        
            note_saveNotebookConfig(params);
        }
        
        function note_saveNotebookConfig(params)
        {
            var sectionsDataObj = note_gatherSectionsData();
            var notebookOldConfigId = note_get_set_notebookConfigId();
            // prevent from save empty notebook, in case there was not previous savings 
            if(notebookOldConfigId == "" && Object.keys(sectionsDataObj).length == 0) {
                hideWaitMessage();
                console.log("no data to save");
                return false;
            }
            
            $.ajax
            ({ 
                type: "POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "notebookservlet",
                data: "action_id=saveNotebookConfig&notebookConfigObj=" + encodeURIComponent(JSON.stringify(sectionsDataObj))+ "&oldConfigId="+ notebookOldConfigId + "&parentId="+globalNotebookParentId + "&parentType="+globalNotebookImplType,
                dataType: "text",
                success: function( clobId ) 
                {  
                    var doContinue = true;
                    if(clobId == -1)
                    {
                        displayAlertDialog("Save failed");
                        doContinue = false;
                    }
                    if(doContinue)
                    {
                        note_get_set_notebookConfigId(clobId);
                        if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
                        {
                            console.log("globalNotebokTableBlocksSaveState", JSON.stringify(globalNotebokTableBlocksSaveState));
                            for(key in globalNotebokTableBlocksSaveState)
                            {
                                if(globalNotebokTableBlocksSaveState[key] == "0")
                                {
                                    doContinue = false;
                                }
                            }
                            globalNotebokTableBlocksSaveState = {};
                        }
                        if(!globalNotebookImplType == notebookImplementationTypes.IS_RUN)
                        {
                            note_postSaveHandler();
                        }
                        if(doContinue && params && params.callbackFunc)
                        {
                            params.callbackFunc.apply(this, params.callbackFuncParams?[params.callbackFuncParams]:[]);
                        }
                    }
                    hideWaitMessage();
                },
                error: handleAjaxError
            });	
        }
        
        function note_postSaveHandler()
        {
            var $fileSections = $('.notebook-section').filter('[sectiontype="file-section"]');
            $fileSections.each(function(inx,el) {
                var $content = $(el).find('.section-content');
                var dataArr = dzGetJustUploadedFilesData($content);
                dzCreateFilesViewRows($content.find('.table-files'), dataArr, false);
                dzClearPreviewContainer($content);
            });
        }
        
        function note_getAttachment(attachID, getType)
        {
            var form = $('#notebookForm');
            form.find('#actionid').val("showAttachment"); 
            form.find('#txtAttachID').val(attachID);
            form.find('#attachmentDisplayType').val(getType);
            var target = "";
            if(getType == "showInline")
            {
                target = "notebook_Iframe_"+attachID+"";
            }
            $('#notebookForm').attr({'action':'UploadServlet','target':target}); 
            $('#notebookForm').submit(); 
        }
        
        function note_synchronizeSections (customSynchArr)
        {
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN || globalNotebookImplType == notebookImplementationTypes.IS_STAGE)
            {
                // synchronize tables filters
                var defaultSynchArr = [notebookSectionTypes.TEST_RESULT_SECTION,notebookSectionTypes.PARAMETER_SECTION,notebookSectionTypes.MATERIAL_SECTION];
                var syncArr = (customSynchArr)?(Array.isArray(customSynchArr)?customSynchArr:[customSynchArr]):defaultSynchArr;
                var $sections = $('.notebook-section');
                $sections.each(function(inx,el) {
                    var $section = $(el);
                    var type = $section.attr('sectionType');
                    if($.inArray(type, syncArr) != -1)
                    {
                        var values = note_getSectionContentValue($section.find('.section-content'), type);
                        note_loadSectionData($section, type, values);
                    }
                });
            }
        }
        
        function note_buildNotebookMainHtml()
        {
            notebookSectionsList = noteImplAPI().notebookSectionsList;
            var notebookInitHtml =  '<div id="'+globalNotebookElementId+'"></div>'+
                                    '<div id="notebookSelectionList">'+ 
                                    '    <div style="float:left;width: 300px;">'+
                                    '        <select  id="ddlSections"  class="chosen_single"></select>'+
                                    '   </div>'+
                                    '   <div style="float:left;margin-top:3px;margin-left: 5px;">'+
                                    '       <span class="app-icon plus-icon add-section" title="Add"></span>'+
                                    '   </div>	'+						  	
                                    '</div>'+
                                    '<form name="notebookForm" id="notebookForm" method="post" action="" target="" style="display:none;">'+
                                    '   <input type="hidden" name="actionid" id="actionid" value=""></input>'+
                                    '   <input type="hidden" name="txtAttachID" id="txtAttachID" value=""></input>'+
                                    '   <input type="hidden" name="attachmentDisplayType" id="attachmentDisplayType" value=""></input>'+
                                    '</form>';
            var $noteParent = $('#notebookSectionsPlaceholder');
            $noteParent.html(notebookInitHtml);
            var $sectionListElem = $noteParent.find("#ddlSections");
            var elemOptions = '<option sectionType="" value="0">Choose Section</option>';
            $(notebookSectionsList).map(function () {
                elemOptions += '<option sectionType="' + this.type + '" value="'+this.id+'">'+this.text+'</option>';
            });
            $sectionListElem.append(elemOptions).chosen({ width: "100%" });
        
            $( "#"+globalNotebookElementId ).sortable({
                placeholder: "ui-state-highlight",
                handle: ".section-header",
                cancel: ".prevent-drag-section,.app-icon,.header-edit-mode"
            });

            $noteParent.find('.add-section').on('click', function() {note_addNewSection()});
        }
        
        function note_clearNotebookSections()
        {
            globalNotebookSectionsCounter = 1;
            $('.notebook-section').remove();
        }
        
        // check if file is supported by browser to be opened inline for view
        function note_isFileSupportedToView(fileName)
        {   
            var browserSupportedExt = ["pdf","txt","jpg","jpeg","png","bmp","gif"];
            var fileNameEndsWith = fileName.substring(fileName.lastIndexOf("."));
            var extension = fileNameEndsWith.substring(1,fileNameEndsWith.length);
            if($.inArray(extension.toLowerCase(), browserSupportedExt) != -1)
                return true;
            else
                return false;
        }
        
        // in case notebook has at least one section init one in view mode;
        function note_defineNotebookInViewMode(parentId, parentType)
        {
            var toReturn = false;
            $.ajax
            ({ 
                type: "POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "notebookservlet",
                data: "action_id=checkNotebookIsEmpty&parentId="+parentId + "&parentType="+parentType,
                dataType: "text",
                async: false,
                success: function( isEmpty ) 
                {  
                    // if notebook has data init in view mode;
                    if(isEmpty == "0") // empty == false
                    {
                        toReturn = true;
                    }
                    else  // empty == true
                    {
                        toReturn = false;
                    }
                },
                error: handleAjaxError
            });   
            return toReturn; 
        }
        
        function note_get_set_notebookConfigId(id) {
            var retVal = "";
            if(id) {
                $('#'+globalNotebookElementId).attr('notebook_content_id', id);
                retVal = id;
            }
            else {
                retVal = $('#'+globalNotebookElementId).attr('notebook_content_id');
                retVal = retVal?retVal:"";
            }
            return retVal;
        }
        
        function note_printNotebook()
        {
            var $sections = $('.notebook-section');
            var indexMap = {};
            var availableSections = "";
            $sections.each(function(inx,el) {
                var $section = $(el);
                var type = $section.attr('sectionType');
                var title = $section.find('.header-contenteditable').html();
                if(title.trim() == "")
                {
                    var defaultTitle = note_getSectionDefaultTitle(type);
                    var index;
                    if(indexMap.hasOwnProperty(type))
                    {
                        index = indexMap[type]+1;
                    }
                    else {
                        index = 1;
                    }
                    indexMap[type] = index;
                    title = defaultTitle + " " + index;
                }
                var sectionId = $section.attr('sectionId');
        
                availableSections += '<div class="layout-cell cell-element" style="margin-bottom: 13px;">'+
                                    '   <span sectionId="'+sectionId+'" style="width:15px;" class="app-icon checkbox-icon section-checkbox"></span>'+
                                    '   <label class="field-label" style="margin-left: 3px;margin-right: 40px;">'+title+'</label>'+
                                    '</div>';
            });
        
            var html = '<div id="divPrintNotebookPreScreen" class="html-page-layout" style="min-width:auto;width:100%;padding: 0% 2% 2% 4%;float:none;"> '
                        + '	<div class="layout-row center"> '
                        + '		<h3>Choose section</h3>'
                        + '	</div> '
                        + ' <div class="layout-row notebook-print-custom-html"> ';
            if($sections.length > 1)
            {
                html +=  ' <span id="chbATall" sectionName="chATall" class="app-icon checkbox-icon all-checkbox" ></span>'
                        + ' <label class="field-label" style="margin-left: 3px;margin-right: 40px; font-weight:bold;">Check All</label> </div>'
                        + '<div class="layout-cell cell-element" style="margin-bottom: 13px;">'
                        +  availableSections;
            }
            else {
                html += availableSections
            }
            
            html += '	</div> '
                    + '	<div class="layout-row center" style="margin-top:10px;margin-bottom: 0;"> '
                    + '		<button class="table-secondary-btn show-print-report-btn" style="width:120px;" disabled="disabled">Print Preview</button> '
                    + '		<button class="table-secondary-btn" onclick="closeModalDialog(event.target)" >Cancel</button>'
                    + '	</div> '
                    +'</div>';
        
            var $html = $($.parseHTML(html));
            $('body').append($html);
            $html.find('button').button();
            $html.find('.show-print-report-btn').on('click', function() {notePrint_showPrint()});
            $html.find('.section-checkbox').on('click', function() {checkboxToggleCheck(this);notePrint_onCheckboxClick(this);});
            $html.find('.all-checkbox').on('click', function() {checkboxToggleCheck(this);notePrint_onCheckboxClick(this);});
        
            var $dialogElem = createModalDialog({dialogId:"divPrintNotebookPreScreen", isDestroyDialog:true, title:"Print Notebook", width: 540, height:300});	
            $dialogElem.dialog('open');
        }
        
        function notePrint_showPrint()
        {
            var sectionsToPrint = $('#divPrintNotebookPreScreen').find('.notebook-print-custom-html').find('.section-checkbox.checked').map(function() {
                return $(this).attr('sectionId');
            }).get().join();
            
            var $dialogElem = createModalDialog({isIframe:true, title:'Preview', width: $(window).width(), height:$(window).height()});
            $dialogElem.find('iframe#iframeDialog').attr('src', 'NotebookPrintPreview.jsp?parentId='+globalNotebookParentId+'&sectionsToPrint='+sectionsToPrint);
            $dialogElem.dialog('open');    
        }
        
        function notePrint_onCheckboxClick(elem) 
        {
            var $parent = $('#divPrintNotebookPreScreen');
            var isDisabled = true;
            if($(elem).hasClass('all-checkbox'))
            {
                var isChecked =  $(elem).checkbox('checked');
                isDisabled = !isChecked;
                $parent.find('.section-checkbox').each(function () {
                    $(this).checkbox('checked', isChecked);
                });
            }
            else {
                var cnt = $parent.find('.notebook-print-custom-html').find('.section-checkbox.checked').length;
                isDisabled = (cnt>0)?false:true;
            }
            $parent.find('.show-print-report-btn').button('option','disabled',isDisabled);
        }
        
        function note_getSectionDefaultTitle(sectionType)
        {
            for(var i=0;i<notebookSectionsList.length;i++)
            {
                var $this = notebookSectionsList[i];
                if($this.type == sectionType)
                {
                    return $this.text;
                }
            }
            return "";
        }
        
        function note_showNotebookAT()
        {
            var availableSections = '<div class="layout-cell field-label" style="margin-right: 10px;"><label>Choose section:</label></div><div style="float:left;width:100%;margin-top:10px;">';
            availableSections += '<div class="layout-cell cell-element" style="width:15px;">' + 
                                '<span id="chbATall" sectionName="chATall" class="app-icon checkbox-icon all-checkbox" ></span></div>'+
                                '<div class="layout-cell field-label" style="margin-left: 3px;margin-right: 10px;font-weight:bold;"><label>Check All</label></div>';

            $(notebookSectionsList).map(function () {
                availableSections += '<div class="layout-cell cell-element" style="width:15px;">' +
                                    '   <span id="chbAT'+this.id+'" sectionName="'+this.id+'"  class="app-icon checkbox-icon section-checkbox" ></span></div>' +
                                    '<div class="layout-cell field-label" style="margin-left: 3px;margin-right: 10px;"><label class="field-label">'+this.text+'</label></div>';
            }); //onclick="checkboxToggleCheck(this);noteAT_onCheckboxClick();"
            var customFunc = function(form) {
                form.find('.section-checkbox').on('click', function() {checkboxToggleCheck(this);noteAT_onCheckboxClick(this);});
                form.find('.all-checkbox').on('click', function() {checkboxToggleCheck(this);noteAT_onCheckboxClick(this);});
            }
            availableSections += '</div>'
            createAuditTrailPreScreen({customHtml:availableSections, customHtmlFunc:customFunc, showReportFunc:noteAT_showAuditTrail});
        }
        
        function noteAT_onCheckboxClick(elem)
        {
            var $parent = $('#divAuditTrailPreScreen');
            var isDisabled = true;
            if($(elem).hasClass('all-checkbox'))
            {
                var isChecked =  $(elem).checkbox('checked');
                isDisabled = !isChecked;
                $parent.find('.section-checkbox').each(function () {
                    $(this).checkbox('checked', isChecked);
                });
            }
            else {
                var cnt = $parent.find('.audit-trail-custom-html').find('.section-checkbox.checked').length;
                isDisabled = (cnt>0)?false:true;
            }
            $parent.find('.show-at-report-btn').button('option','disabled',isDisabled);
        }
        

        function noteAT_showAuditTrail($parent)
        {
            var fromDate = $parent.find('.datepicker-from').val();
            var toDate = $parent.find('.datepicker-to').val();
            var users = note_getSelectedValuesFromDDL($parent.find('.select-user')).join();
            var sections = $parent.find('.audit-trail-custom-html').find('.section-checkbox.checked').map(function() {
                return $(this).attr('sectionName');
            }).get().join();
        
            var dataObj = {
                fromDate:fromDate,
                toDate:toDate,
                // toDateDisplay:toDateDisplay,
                users:users,
                sections:sections
            }
        
            console.log(JSON.stringify(dataObj));
        
            $.fileDownload('notebookservlet', {
                httpMethod: "POST", 
                data: "action_id=notebookAT&parentId="+globalNotebookParentId + "&filter="+encodeURIComponent(JSON.stringify(dataObj)),
                prepareCallback: function() {                                              
                                            showWaitMessage("Please wait while preparing the report...");
                }, 
                successCallback: function (url) {	 
                                            hideWaitMessage();
                },
                failCallback: function (html, url) { 
                                            hideWaitMessage();
                    displayAlertDialog("DispAlDial_Msg_Short_Name_T_2", { title:"TitleInMess_Msg_Short_Name_T_2" } );
                }
            }); 
        }

        function noteImplAPI()
        {
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN || globalNotebookImplType == notebookImplementationTypes.IS_STAGE) {
                return notebookRunImplementationAPI({"parentId":globalNotebookParentId,
                                                "isViewMode":globalNotebookIsInViewMode,
                                                "tableBlocksSaveState":globalNotebokTableBlocksSaveState,
                                                "implType":globalNotebookImplType,
                                                "sectionsCounter":globalNotebookSectionsCounter
                                            });
            }
            else if(globalNotebookImplType == notebookImplementationTypes.IS_STDL){
                return $.notebookStdlImplementationAPI({"parentId":globalNotebookParentId,
                                                      "isViewMode":globalNotebookIsInViewMode,
                                                      "sectionsCounter":globalNotebookSectionsCounter
                                                    });
            }
        }  

        return {
            "init":function(obj){
                note_initNotebook(obj);
            },
            "save": function(params) {
                note_saveNotebook(params);
            },
            "resizeTable": function(options) {
                noteImplAPI()._resizeTables(options);
            },
            "print": function(printAs) {
                if(printAs && printAs == 'history') {
                    note_showNotebookAT();
                }
                else {
                    note_printNotebook();
                }
            }
            ,"_getSelectedValuesFromDDL":function($ddl, returnType){
                return note_getSelectedValuesFromDDL($ddl, returnType);
            }
            ,"_synchronizeSections":function(customSynchArr) {
                note_synchronizeSections(customSynchArr);
            }
            ,"_convertTestFromObjectToString":function(testArr) {
                return note_convertTestFromObjectToString(testArr);
            }
            ,"_parseTableFilter":function(filterArr) {
                return note_parseTableFilter (filterArr);
            }
            ,"_onDDLChanged":function(elem, selectedArr, sectionType) {
                note_onDDLChanged(elem, selectedArr, sectionType);
            }
            ,"_updateTableFilterList":function ($currElem, data, selectedArr) {
                note_updateTableFilterList($currElem, data, selectedArr);
            }
        };
    }

    $.notebookAPI = notebookAPI;

})(jQuery)