
var notebookRunImplementationAPI = function(args) {

    var globalNotebookParentId = args.parentId;
    var globalNotebookIsInViewMode = args.isViewMode;
    var globalNotebokTableBlocksSaveState = args.tableBlocksSaveState;
    var globalNotebookImplType = args.implType;
    var globalNotebookSectionsCounter = args.sectionsCounter;
    
    var notebookSectionsList = [
        { id: "test", type: notebookSectionTypes.TEST_RESULT_SECTION, text: "Test Results" },
        { id: "parameter", type: notebookSectionTypes.PARAMETER_SECTION, text: "Parameters" },
        { id: "material", type: notebookSectionTypes.MATERIAL_SECTION, text: "Materials" },
        { id: "richtext", type: notebookSectionTypes.RICHTEXT_SECTION, text: "Richtext" },
        { id: "file", type: notebookSectionTypes.FILE_SECTION, text: "File" },
        { id: "spreadsheet", type: notebookSectionTypes.SPREADSHEET_SECTION, text: "SpreadSheet" }
    ];

    function noteDT_getEditableSectionByType(type)
    {
        var html = "", tableUnqId = "",  funcAfterSectionCreation = null;
        if(type == notebookSectionTypes.TEST_RESULT_SECTION)
        {
            html = '<div style="margin-right: 50px;float: left;">'+
                    '   <span class="field-label" style="margin-right: 10px;">Sample Points</span>'+
                    '   <select class="chosen-multiple select-sample-points" multiple="multiple"></select>'+
                    '</div>' +
                    '<div style="margin-right: 20px;float: left;"><span class="field-label" style="margin-right: 10px;">Tests</span>'+
                    '<select class="chosen-multiple select-tests" multiple="multiple" onchange="customMultiSelectChangeHandler(this);"></select>'+
                    '</div>';
            
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
            {
                tableUnqId = 'noteTestResultsTable_'+globalNotebookSectionsCounter++ +'';
                var tableContainerWidth = ($(window).width() - 130) + "px";	
                html += '<div style="margin-left: 20px;float: left;">'+
                        '   <button type="button" style="display:none;" class="table-secondary-btn apply-button test-results-apply-btn">Apply</button>'+
                        '</div>'+
                        '<div class="table-buttons" style="float: right;display:none;">' +
                        '   <button type="button" class="table-secondary-btn save-button test-result-save-btn">Save</button>'+
                        '   <button type="button" class="table-secondary-btn copy-button test-result-copy-btn">Copy</button>'+
                        '   <button type="button" class="table-secondary-btn edit-button test-result-edit-btn">Edit</button>'+
                        '   <button type="button" class="table-secondary-btn calculator-button test-result-calculator-btn">Calculate</button>'+
                        '</div>'+
                        '<div class="notebookTableParentDiv" style="margin-top:10px;width:'+tableContainerWidth+';min-width:1200px;float:left;">'+ 
                            '<table id="'+tableUnqId+'" class="display notebook-table notebook-test-results-table" tableName="'+notebookTableNames.TEST_RESULT_TABLENAME+'" style="width:100%"></table>'+
                        '</div>';
            }
            funcAfterSectionCreation = function(newSection) {
                    if(globalNotebookImplType == notebookImplementationTypes.IS_RUN) { 
                        newSection.find('.table-secondary-btn').button();
                        newSection.find('.test-results-apply-btn').on('click', function() {noteDT_loadNotebookTable(tableUnqId)});
                        newSection.find('.test-result-edit-btn').on('click', function() {noteDT_editTestResultsAndDates(tableUnqId)});
                        newSection.find('.test-result-copy-btn').on('click', function() {noteDT_copyTestResultsAndDates(tableUnqId)});
                        newSection.find('.test-result-calculator-btn').on('click', function() {noteDT_calculateTestResults(tableUnqId)});
                        newSection.find('.test-result-save-btn').on('click', function() {noteDT_saveTableData(tableUnqId, notebookTableNames.TEST_RESULT_TABLENAME)});
                    }
                    newSection.find('.select-sample-points').on('change', function() {customMultiSelectChangeHandler(this);note_onDDLChanged(this, [], type)});
                    newSection.find('.chosen-multiple').chosen({placeholder_text_multiple:"All", width:"300px"});
            }
        }
        else if(type == notebookSectionTypes.PARAMETER_SECTION)
        {
            html = '<div style="float: left;"><span class="field-label" style="margin-right: 10px;">Parameters</span>'+
                        '<select class="chosen-multiple select-parameters" multiple="multiple" onchange="customMultiSelectChangeHandler(this);"></select></div>';
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
            {
                tableUnqId = 'noteParametersTable_'+globalNotebookSectionsCounter++ +'';
                html += '<div style="margin-left: 20px;float: left;"><button type="button" class="table-secondary-btn parameters-apply-btn">Apply</button></div>'+
                        '<div class="table-buttons" style="float: right;display:none;">' +
                        '   <button type="button" class="table-secondary-btn new-button parameters-new-btn">New</button>'+
                        '   <button type="button" class="table-secondary-btn save-button parameters-save-btn">Save</button>'+
                        '</div>'+
                        '<div style="margin-top:10px;width:100%;float:left;">'+
                            '<table id="'+tableUnqId+'" class="display notebook-table notebook-parameters-table" tableName="'+notebookTableNames.PARAMETER_TABLENAME+'" style="width:100%"></table>'+
                        '</div>';
            }
            funcAfterSectionCreation = function(newSection) {
                    if(globalNotebookImplType == notebookImplementationTypes.IS_RUN) { 
                        newSection.find('.table-secondary-btn').button();
                        newSection.find('.parameters-apply-btn').on('click', function() {noteDT_loadNotebookTable(tableUnqId)});
                        newSection.find('.parameters-new-btn').on('click', function() {noteDT_openTableNewWindow(tableUnqId, notebookTableNames.PARAMETER_TABLENAME)});
                        newSection.find('.parameters-save-btn').on('click', function() {noteDT_saveTableData(tableUnqId, notebookTableNames.PARAMETER_TABLENAME)});
                    }
                    newSection.find('.chosen-multiple').chosen({placeholder_text_multiple:"All", width:"400px"});
            }
        }
        else if(type == notebookSectionTypes.MATERIAL_SECTION)
        {
            html = '<div style="float: left;"><span class="field-label" style="margin-right: 10px;">Materials</span>'+
                        '<select class="chosen-multiple select-materials" multiple="multiple" onchange="customMultiSelectChangeHandler(this);"></select></div>';
            if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
            {
                tableUnqId = 'noteMaterialsTable_'+globalNotebookSectionsCounter++ +'';
                html += '<div style="margin-left: 20px;float: left;"><button type="button" class="table-secondary-btn materials-apply-btn">Apply</button></div>'+
                        '<div class="table-buttons" style="float: right;display:none;"><button type="button" class="table-secondary-btn new-button materials-new-btn">New</button></div>'+
                        '<div style="margin-top:10px;width:100%;float:left;">'+
                            '<table id="'+tableUnqId+'" class="display notebook-table notebook-materials-table" tableName="'+notebookTableNames.MATERIAL_TABLENAME+'" style="width:100%"></table>'+
                        '</div>';
            }
            funcAfterSectionCreation = function(newSection) {
                if(globalNotebookImplType == notebookImplementationTypes.IS_RUN) { 
                    newSection.find('.table-secondary-btn').button();
                    newSection.find('.materials-apply-btn').on('click', function() {noteDT_loadNotebookTable(tableUnqId)});
                    newSection.find('.materials-new-btn').on('click', function() {noteDT_openTableNewWindow(tableUnqId, notebookTableNames.MATERIAL_TABLENAME)});
                }
                newSection.find('.chosen-multiple').chosen({placeholder_text_multiple:"All", width:"400px"});
            }
        }
        return {html:html,sectionCounter:globalNotebookSectionsCounter,funcAfterSectionCreation:funcAfterSectionCreation};
    }

    function noteDT_getViewableSectionByType(type)
    {
        var html = '';
        if(type == notebookSectionTypes.TEST_RESULT_SECTION)
        {
            var tableUnqId = 'noteTestResultsTable_'+globalNotebookSectionsCounter++ +'';
            var tableContainerWidth = ($(window).width() - 130) + "px";	
            html = '<div class="notebookTableParentDiv" style="margin-top:10px;width:'+tableContainerWidth+';min-width:1200px;float:left;">'+
                        '<table id="'+tableUnqId+'" class="display notebook-table notebook-test-results-table" tableName="'+notebookTableNames.TEST_RESULT_TABLENAME+'" style="width:100%"></table>'+
                    '</div>';  
        }
        else if(type == notebookSectionTypes.PARAMETER_SECTION)
        {
            var tableUnqId = 'noteParametersTable_'+globalNotebookSectionsCounter++ +'';
            html = '<div style="margin-top:10px;width:100%;float:left;">'+
                        '<table id="'+tableUnqId+'" class="display notebook-table notebook-parameters-table" tableName="'+notebookTableNames.PARAMETER_TABLENAME+'" style="width:100%"></table>'+
                    '</div>';
        }
        else if(type == notebookSectionTypes.MATERIAL_SECTION)
        {
            var tableUnqId = 'noteMaterialsTable_'+globalNotebookSectionsCounter++ +'';
            html = '<div style="margin-top:10px;width:100%;float:left;">'+
                        '<table id="'+tableUnqId+'" class="display notebook-table notebook-materials-table" tableName="'+notebookTableNames.MATERIAL_TABLENAME+'" style="width:100%"></table>'+
                    '</div>';
        }
        return {html:html,sectionCounter:globalNotebookSectionsCounter};
    }

    function noteDT_loadEditableSectionData($section, type, savedData)
    {
        var actionId = "";
        var val = [];
        if(type == notebookSectionTypes.TEST_RESULT_SECTION)
        {
            actionId = "getSPTestsSectionData";
            val = (savedData?note_parseTableFilter(savedData["filterBy"]["filterValSP"]):[]);
        }
        else if(type == notebookSectionTypes.PARAMETER_SECTION)
        {
            actionId = "getParametersSectionData";
            val = (savedData?note_parseTableFilter(savedData["filterBy"]["filterVal"]):[]);
        }
        else if(type == notebookSectionTypes.MATERIAL_SECTION)
        {
            actionId = "getMaterialsSectionData";
            val = (savedData?note_parseTableFilter(savedData["filterBy"]["filterVal"]):[]);
        }
        if(actionId != "")
        {
            if((globalNotebookImplType == notebookImplementationTypes.IS_RUN && savedData) 
                    && 
                ((type == notebookSectionTypes.TEST_RESULT_SECTION && !isSectionCollapsed($section)) // apply table inside hidden section(collapsed) cause ui problem when table is built with fixed columns plugin
                    || type == notebookSectionTypes.PARAMETER_SECTION 
                    || type == notebookSectionTypes.MATERIAL_SECTION)
                )
            {
                //simulate 'apply' button click: load table with already saved filter
                noteDT_loadNotebookTableBySection($section, savedData);
            }
            $.ajax
            ({ 
                type: "POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "notebookservlet",
                data: "action_id="+actionId+"&parentId="+globalNotebookParentId + "&parentType="+globalNotebookImplType,
                dataType: "json",
                success: function( data ) 
                {  			
                    if(data.sp != null)
                    {
                        var spElem = $section.find('.select-sample-points');
                        note_updateTableFilterList(spElem, data.sp, val);
                        note_onDDLChanged(spElem,(savedData?savedData["filterBy"]["filterValTest"]:[]), type);
                    }
                    if(data.parameter != null)
                    {
                        note_updateTableFilterList($section.find('.select-parameters'), data.parameter, val);
                        if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
                        {
                            noteDT_toggleDisplayTableButtons($section.find('[tableName="'+notebookTableNames.PARAMETER_TABLENAME+'"]'), true, '.apply-button');
                        }
                    }
                    if(data.material != null)
                    {
                        note_updateTableFilterList($section.find('.select-materials'), data.material, val);
                        if(globalNotebookImplType == notebookImplementationTypes.IS_RUN)
                        {
                            noteDT_toggleDisplayTableButtons($section.find('[tableName="'+notebookTableNames.MATERIAL_TABLENAME+'"]'), true, '.apply-button');
                        }
                    }
                },
                error: handleAjaxError
            });	
        }
    } 

    function noteDT_loadNotebookTableBySection ($section, filter)
    {
        var tableId = $section.find('.notebook-table').attr('id');
        noteDT_loadNotebookTable(tableId, filter);
    }

    function noteDT_loadNotebookTable (currTableID, filter)
    {    
        var $currTableElem = $('#'+currTableID);
        var $content = $currTableElem.closest('.notebook-section-content');
        var tableName = $currTableElem.attr('tableName');
        var dataSrc = "";
        var loadBySavedFilter = filter?true:false;
        if(tableName == notebookTableNames.TEST_RESULT_TABLENAME)
        {
            var spFilterList = loadBySavedFilter?note_parseTableFilter(filter["filterBy"]["filterValSP"]) : note_getSelectedValuesFromDDL($content.find('.select-sample-points')).join();
            var testFilterList = loadBySavedFilter?note_convertTestFromObjectToString(filter["filterBy"]["filterValTest"]) :note_getSelectedValuesFromDDL($content.find('.select-tests')).join();
            dataSrc = "action_id=getTestResultsTable&parentId="+globalNotebookParentId + "&editTable=" + ($currTableElem.hasClass('edit-mode')?"1":"0") + 
                        "&isViewOnly=" + (globalNotebookIsInViewMode?"1":"0") + 
                        "&spFilterList="+spFilterList + "&testFilterList="+encodeURIComponent(testFilterList);
        }
        else if(tableName == notebookTableNames.PARAMETER_TABLENAME)
        {
            var filterList = loadBySavedFilter?note_parseTableFilter(filter["filterBy"]["filterVal"]) : note_getSelectedValuesFromDDL($content.find('.select-parameters')).join();
            dataSrc = "action_id=getParametersTableData&parentId="+globalNotebookParentId + "&isViewOnly=" + (globalNotebookIsInViewMode?"1":"0") + "&filterList="+filterList;
        }
        else if(tableName == notebookTableNames.MATERIAL_TABLENAME)
        {
            var filterList = loadBySavedFilter?note_parseTableFilter(filter["filterBy"]["filterVal"]) : note_getSelectedValuesFromDDL($content.find('.select-materials')).join();
            dataSrc = "action_id=getMaterialsTableData&parentId="+globalNotebookParentId + "&filterList="+filterList;
        }

        if(dataSrc == "")return;

        $.ajax
        ({
            type:"POST",
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            url: "notebookservlet",
            data: dataSrc,
            dataType: "json",
            success: function(data) 
            {  
                if(tableName == notebookTableNames.TEST_RESULT_TABLENAME)
                {
                    noteDT_initTestResultsTable($currTableElem, data);
                }
                else {
                    var dtTable = noteDT_getDataTable(currTableID, true);
                    dt_ext_updateTableData(dtTable, data);              
                }
            },
            error:handleAjaxError
        });  
    }

    function noteDT_getSectionContentValue($content, type)
    {   
        if(type == notebookSectionTypes.TEST_RESULT_SECTION)
        {
            var selTests = note_getSelectedValuesFromDDL($content.find('.select-tests'),'id_val');
            var selTestsObj = [];
            for(var i=0;i<selTests.length;i++)
            {
                var _obj = selTests[i];
                var delim = '@_unqdelim_@';
                var arr = _obj.id.split(delim);
                selTestsObj.push({
                    testName: arr[0],
                    testCode: arr[1],
                    val:_obj.val
                })
            }
            
            return {
                filterBy: {    
                    filterValSP:note_getSelectedValuesFromDDL($content.find('.select-sample-points'),'id_val'), 
                    filterValTest: selTestsObj
                }
            };
        }
        else if(type == notebookSectionTypes.PARAMETER_SECTION)
        {
            return {
                filterBy: {
                    filterVal: note_getSelectedValuesFromDDL($content.find('.select-parameters'),'id_val')
                }
            }
        }
        else if(type == notebookSectionTypes.MATERIAL_SECTION)
        {
            return {
                filterBy: {
                    filterVal: note_getSelectedValuesFromDDL($content.find('.select-materials'),'id_val')
                }
            };
        }
    } 

    function note_getSelectedValuesFromDDL($ddl, returnType)
    {
        return $.notebookAPI()._getSelectedValuesFromDDL($ddl,returnType);
    }

    function note_parseTableFilter (filterArr)
    {
        return $.notebookAPI()._parseTableFilter(filterArr);
    }

    function note_convertTestFromObjectToString (filterArr)
    {
        return $.notebookAPI()._convertTestFromObjectToString(filterArr);
    }

    function note_onDDLChanged(elem, selectedArr, sectionType)
    {
        $.notebookAPI()._onDDLChanged(elem, selectedArr, sectionType);
    }

    function note_updateTableFilterList($currElem, data, selectedArr)
    {
        $.notebookAPI()._updateTableFilterList($currElem, data, selectedArr);
    }

    function noteDT_tableCreatedCell(cell, cellData, rowData, rowIndex, colIndex, tableID)
    {
        var $table = $('#'+tableID);
        var tableName = $table.attr('tableName');
        if(tableName == notebookTableNames.PARAMETER_TABLENAME)
        {
            var hdnParams = rowData[1];
            var disabled = hdnParams["disabled"]; // 'disabled' if parameter is calculated

            if(colIndex == 5)
            {      
                // var disabled = (rowData[15] == '' && $('#canUpdate').val() == 1)?'':'disabled';
                var _attr = "out_of_limits="+hdnParams["outOfLimits"]+"  test_name='"+rowData[2]+"'";
                switch(hdnParams["cellType"])
                {
                    case "text_validate": 
                        $(cell).html('<input type="text" '+_attr+' onchange="checkLimits(this, '+hdnParams["limits"]+')" '+disabled+'  onkeypress="validateDecimal(this.value, event.keyCode, event)" style="width:99%;">');
                        break;
                    case "text":
                        $(cell).html('<input type="text" '+_attr+' '+disabled+' style="width:99%;" onkeypress="validateLegalChar(event.keyCode, event)" maxlength="50">');
                        break;                    
                    case "passfail_list":
                        $(cell).html('<select '+_attr+' style="width:100%; text-align: left;" '+disabled+'><option value="0">Choose</><option value="Pass">Pass</><option value="Fail">Fail</></> ');
                        break;
                    case "date":                      
                        $(cell).html('<input '+_attr+' type="text" style="width:100px;text-align:center" class="datepicker" '+disabled+' align="left"  readonly="readonly">');
                        initDatePickerWithOptions($(cell).find('.datepicker'), {targetType:'element','disabled':disabled});
                        break;
                    case "category_list":
                        var selectList =  '<select '+_attr+' style="width:100%; text-align: left;" '+disabled+' cell_parameter_value>';
                        $(JSON.parse(cellData)).map(function ()   
                        {
                            selectList = selectList + '<option value='+this.id+'>'+this.text+'</>';
                        }) 
                        $(cell).html(selectList);
                        break;
                    default:
                        var actualData = (hdnParams["inputType"] != 'D') ? cellData : slashDate(cellData);
                        $(cell).html(actualData);
                        break;
                }			
            }
            else if(colIndex == 4)
            {
                if(hdnParams["cellType"] == "text_validate")
                {
                    var ddl =  '<select style="width:100%; text-align: left;" '+disabled+' cell_parameter_sign>'+
                                '<option value="0" '+((cellData=='=')?"selected":"")+'>=</option>'+
                                '<option value="1" '+((cellData=='<')?"selected":"")+'><</option>'+
                                '<option value="2" '+((cellData=='>')?"selected":"")+'>></option>'+
                                '<option value="3" '+((cellData=='<=')?"selected":"")+'><=</option>'+
                                '<option value="4" '+((cellData=='>=')?"selected":"")+'>>=</option>'+
                                '</select>';
                    $(cell).html(ddl);
                }
            }
            else if(colIndex == 7)
            {
                var rowIcons = [
                    {iconType:"edit", tooltip:"Update"},
                    {iconType:"remove", tooltip:"Delete", disabled:false}
                ];  
                $(cell).addClass("table-column-with-icons").html(fnCreateTableRowActionIcons(rowIcons));
            }
        }
        else if(tableName == notebookTableNames.MATERIAL_TABLENAME)
        {
            if(colIndex == 7)
            {
                var rowIcons = [
                    {iconType:"edit", tooltip:"Update"},
                    {iconType:"remove", tooltip:"Delete", disabled:false}
                ];  
                $(cell).addClass("table-column-with-icons").html(fnCreateTableRowActionIcons(rowIcons));
            }
        }
    }

    function noteDT_tableRowClicked(currRowData, tableId, isRowSelected, event)
    {
        var tableName = $('#'+tableId).attr('tableName');
        var sendParamsObj = {};
            if(tableName == notebookTableNames.PARAMETER_TABLENAME) 
            {
                var hdnParams = currRowData[1];    
                sendParamsObj = {
                    actionId: 'edit',
                    disableControls: hdnParams["disabled"],
                    rowId: currRowData[0],
                    parameterId: hdnParams["parameterId"]
                }   
            }
            else if(tableName == notebookTableNames.MATERIAL_TABLENAME) 
            {
                var hdnParams = currRowData[1];    
                sendParamsObj = {
                    actionId: 'edit',
                    disableControls: '',
                    rowId: currRowData[0],
                    hasSavedResult: hdnParams["hasSavedResult"]
                }   
            }
            var act = fnGetTableRowIconActionNameByEvent(event);
            if(act)
            {
                if(!isRowSelected)$(event.currentTarget).addClass('selected');    
                switch(act) {
                    case "edit": noteDT_openTableUpdateWindow(tableId, tableName, sendParamsObj);
                        break;
                    case "remove": noteDT_beforeDeleteTableRow(tableId, tableName, currRowData[0]);
                        break;
                    default: "";
                }
                
            }
    }

    function noteDT_tableRowDblClicked(currRowData, tableId)
    {
        var tableName = $('#'+tableId).attr('tableName');
        var sendParamsObj = {};
            if(tableName == notebookTableNames.PARAMETER_TABLENAME) 
            {
                var hdnParams = currRowData[1];    
                sendParamsObj = {
                    actionId: 'edit',
                    disableControls: hdnParams["disabled"],
                    rowId: currRowData[0],
                    parameterId: hdnParams["parameterId"]
                }  
            }
            else if(tableName == notebookTableNames.MATERIAL_TABLENAME) 
            {
                var hdnParams = currRowData[1];    
                sendParamsObj = {
                    actionId: 'edit',
                    disableControls: '',
                    rowId: currRowData[0],
                    hasSavedResult: hdnParams["hasSavedResult"]
                }   
            }
            noteDT_openTableUpdateWindow(tableId, tableName, sendParamsObj); 
    }

    function noteDT_openTableNewWindow(tableId, tableName)
    {
        var sendParamsObj = {actionId: 'new',rowId: '0', disableControls: ''};
        noteDT_openTableUpdateWindow(tableId, tableName, sendParamsObj);
    }

    function noteDT_openTableUpdateWindow(tableId, tableName, sendParamsObj)
    {
        var title = "";
        var width = "auto";
        var height = "auto";
        var actionType = sendParamsObj["actionId"];
        var isUpdate = (actionType.toLowerCase() == 'new')?false:true;
        var tableSection = "";
        if(tableName == notebookTableNames.PARAMETER_TABLENAME) 
        {
            tableSection = notebookSectionTypes.PARAMETER_SECTION;
            title = (isUpdate?"Edit":"New") + " " + labelsArr["Parameter_Result"];
            width = 800;
            height = 500;
            var src = "dataentryparamupdateservlet_?actionid=" + actionType + 
                        "&currResultId="+sendParamsObj["rowId"] + 
                        "&currParamId="+(!isUpdate?'0':sendParamsObj["parameterId"]) + 
                        "&disableControls="+sendParamsObj["disableControls"] + 
                        "&currStageId="+$('#comboStage option:selected').val() + 
                        "&currRunId="+ globalNotebookParentId + 
                        "&txtESign=0"+"&hideTitle=1";
        }
        else if(tableName == notebookTableNames.MATERIAL_TABLENAME) 
        {
            tableSection = notebookSectionTypes.MATERIAL_SECTION;
            title = (isUpdate?"Edit":"New")+ " " + labelsArr["Material_Result"];
            width = 900;
            height = 650;
            var src = "dataentryrawmaterialupdateservlet_?actionid=" + actionType + 
                        "&materialResultID="+sendParamsObj["rowId"] + 
                        "&disableControls="+sendParamsObj["disableControls"] + 
                        "&currProdId="+$('#comboProduct option:selected').val() + 
                        "&currStageId="+$('#comboStage option:selected').val() + 
                        "&currRunId="+ globalNotebookParentId + 
                        "&isSaved="+ (!isUpdate?'0':sendParamsObj["hasSavedResult"]) + 
                        "&currStartDateMM=" + $('#currStartDateMM').val() + 
                        "&currStartDateTimeMM=" + $('#currStartDateTimeMM').val() +
                        "&hideTitle=1";
        }

        var $dialogElem = createModalDialog({attachToParent:true, isIframe:true, isDestroyDialog:false, 
                                                width:width, height:height,
                                                onClose:noteDT_onCloseTableUpdateWindow, onCloseParams:[tableId,tableSection]
                                            });
            $dialogElem.find('iframe').attr('src', src);
            $dialogElem.dialog( "option", "title", title );
            $dialogElem.dialog('open'); 
    }

    function noteDT_onCloseTableUpdateWindow(returnValue, onCloseParams)
    {
        console.log("returnValue", returnValue);
        console.log("onCloseParams", onCloseParams);
        if(returnValue != "-1")
        {
            var tableId = onCloseParams[0];
            var tableSection = onCloseParams[1];
            noteDT_reloadTableSection(tableId, tableSection);
        }
    }

    function noteDT_beforeDeleteTableRow(tableId, tableName, rowIdToDelete)
    {
        var _message = "";
        if(tableName == notebookTableNames.PARAMETER_TABLENAME)
        {
            _message = "OpenConfirmDialog_Msg_Short_Name_T_4";
        }
        else if(tableName == notebookTableNames.MATERIAL_TABLENAME)
        {
            _message = "OpenConfirmDialog_Msg_Short_Name_T_5";
        }
        openConfirmDialog({ message:_message,  
                            onConfirm:function() 
                            {
                                openCommentDialogAndCommit(500, noteDT_deleteTableRow, [tableId,tableName, rowIdToDelete]);
                            }
        });
    }

    function noteDT_deleteTableRow(tableId, tableName, rowIdToDelete)
    {
        var comment = (commentObject.data("comment") == null)? "" : commentObject.data("comment");
        var dataSrc = "";
        var tableSection = "";
        if(tableName == notebookTableNames.PARAMETER_TABLENAME)
        {
            tableSection = notebookSectionTypes.PARAMETER_SECTION;
            dataSrc = "dataentryparamservlet_?actionid=deleteParameter&resultId=" + rowIdToDelete + 
                        "&auditTrailComments=" + encodeURIComponent(comment) + "&preventReturnTableData=1";
        }
        else if(tableName == notebookTableNames.MATERIAL_TABLENAME)
        {
            tableSection = notebookSectionTypes.MATERIAL_SECTION;
            dataSrc = "dataentryrawmaterialsservlet_?actionid=deleteRawMaterial&mResultId=" + rowIdToDelete + 
                    "&auditTrailComments=" + encodeURIComponent(comment) + "&preventReturnTableData=1";
        }
                
        $.ajax
        ({
            type:"POST",
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            url: dataSrc,
            dataType: "text",
            success: function(data) 
            {  
                if( data == "-1" )
                {
                    parent.showAlertMessage("ShowAlertMessage_Msg_Short_Name_T_30", { title:"TitleInMess_Msg_Short_Name_T_2" });
                    return;
                }
                else // data != "-1"
                {
                    noteDT_reloadTableSection(tableId, tableSection);
                }
            },
            error:handleAjaxError
        });  
    }

    function noteDT_reloadTableSection(tableId, sectionType)
    {
        noteDT_loadNotebookTable(tableId);
        $.notebookAPI()._synchronizeSections(sectionType);
    }

    function noteDT_getDataTable(currTableId, isCreate)
    {
        var dtTable = null;
        var $currTableElem = $('#'+currTableId);
        var tableName = $currTableElem.attr('tableName');
        var createTableIfNotExists = (isCreate == null)?false:isCreate;
        var isDataTableExists = dt_ext_isDataTable($currTableElem[0]); 

        if(isDataTableExists)
        {
            dtTable = $currTableElem.DataTable();
        }
        else if(createTableIfNotExists)
        {
            var tableAttr = [];
            var additOptions = {};
            if(tableName == notebookTableNames.PARAMETER_TABLENAME)
            {
                tableAttr = [{"paging":false,"dom":"t","footer":false}
                                        ,{"title":"result_id","visible":false,"searchable":false}
                                        ,{"title":"hidden_object","visible":false,"searchable":false}
                                        ,{"title":"Parameter_Name"}
                                        ,{"title":"Parameter_Code"}
                                        ,{"title":"Sign", "width": "100px"}
                                        ,{"title":"Value", "width": "250px"}
                                        ,{"title":"Uom"}    
                                        ,{"title":"", "searchable":false, "orderable":false, "visible": !globalNotebookIsInViewMode} 
                                    ];
                if(!globalNotebookIsInViewMode)
                    additOptions = {"createdCell":noteDT_tableCreatedCell,"onRowClick":noteDT_tableRowClicked,"onRowDblClick":noteDT_tableRowDblClicked};
            }
            else if(tableName == notebookTableNames.MATERIAL_TABLENAME)
            {
                tableAttr = [{"paging":false,"dom":"t","footer":false}
                                        ,{"title":"result_id","visible":false,"searchable":false}
                                        ,{"title":"hidden_object","visible":false,"searchable":false}
                                        ,{"title":"Material_Name"}
                                        ,{"title":"Batch_Number"}
                                        ,{"title":"Quantity", "width": "150px"}
                                        ,{"title":"Uom", "width": "100px"} 
                                        ,{"title":"Comment"}     
                                        ,{"title":"", "searchable":false, "orderable":false, "visible": !globalNotebookIsInViewMode} 
                                    ];
                if(!globalNotebookIsInViewMode)
                    additOptions = {"createdCell":noteDT_tableCreatedCell,"onRowClick":noteDT_tableRowClicked,"onRowDblClick":noteDT_tableRowDblClicked};
            }
            dtTable = drawTable(currTableId,tableAttr,[],additOptions,labelsArr);

            noteDT_toggleDisplayTableButtons($currTableElem, true);
        }

        return dtTable;
    }

    function noteDT_saveTableData(tableId, tableName)
    {
        var tableIdArr = [];
        var isGlobalSave = (tableId == "-1")?true:false;
        var toReturn = {};
        if(tableName == notebookTableNames.TEST_RESULT_TABLENAME)
        {
            if(tableId == "-1")
            {
                // find by table id, because of datatable build with FixedColumns
                var $tables = $('#'+globalNotebookElementId).find(".notebook-test-results-table[id^='noteTestResultsTable_']");
                if($tables.length > 0)
                {
                    globalNotebokTableBlocksSaveState[tableName] = 0;
                }

                $tables.each(function(inx,el) {
                    tableIdArr[inx] = $(el).attr('id');
                });
                // console.log("test results tables ids", tableIdArr);
            }
            else {
                tableIdArr[0] = tableId;
            }
            if(tableIdArr.length > 0)
            {
                toReturn = noteDT_saveTestResultsAndDates(tableIdArr, isGlobalSave);
            }
        }
        else if(tableName == notebookTableNames.PARAMETER_TABLENAME) 
        {
            if(tableId == "-1")
            {           
                var $tables = $('#'+globalNotebookElementId).find(".notebook-parameters-table");
                if($tables.length > 0)
                {
                    globalNotebokTableBlocksSaveState[tableName] = 0;
                }

                $tables.each(function(inx,el) {
                    tableIdArr[inx] = $(el).attr('id');
                });
                // console.log("parameters tables ids", tableIdArr);
            }
            else {
                tableIdArr[0] = tableId;
            }
            if(tableIdArr.length > 0)
            {
                toReturn = noteDT_saveParameterTableValues(tableIdArr,isGlobalSave);
            }
        }

        return {"saveState":globalNotebokTableBlocksSaveState};
    }


    function noteDT_destroyTable(currTableElem) 
    {
        if(dt_ext_isDataTable(currTableElem[0]))
        {
            currTableElem.DataTable().destroy();
            currTableElem.empty();
            // console.log("table destroyed", currTableElem[0]);
        }
    }

    function noteDT_toggleDisplayTableButtons ($currTableElem, isShow, buttonIdentifier)
    {
        buttonIdentifier = (buttonIdentifier == null)?".table-buttons":buttonIdentifier;
        var $section = $currTableElem.closest('.notebook-section-content');
        $section.find(buttonIdentifier).css('display', isShow?'block':'none');
    }

    function noteDT_resizeTables (options)
    {
        var newWidth = options.width;
        // Redraw the fixed columns based on new table size.
        var $tablesToResize = $('#'+globalNotebookElementId).find(".notebook-test-results-table[id^='noteTestResultsTable_']");
        $tablesToResize.each(function(inx,el) {
            var parentDiv = el.closest(".notebookTableParentDiv");
            console.log(newWidth, $(window).width(), window.outerWidth);
            $(parentDiv).css('width', (newWidth - 130) + "px");
            var dtTable = noteDT_getDataTable($(el).attr('id'));
            if(dtTable != null) {
                dtTable.columns.adjust().fixedColumns().relayout(); 
            }
        });  
    }

    /////////////////////////// TEST RESULTS ///////////////////////////////////////

    function noteDT_initTestResultsTable($currTableElem, fullData)
    {
        // destroy table if exist
        noteDT_destroyTable($currTableElem);

        // init table
        var header = fullData.header;
        var colDefinArr = []; 
        var displayCopyColumn = !globalNotebookIsInViewMode;
        for (var i=0; i < header.length; i++ ) 
        {       
            colDefinArr.push({                
                "targets": [i],    
                "searchable": false,
                "orderable": false,
                "visible":(i==1)?displayCopyColumn:true,
                "className": (i>1)?"center":"testColClass",
                "title": header[i]                   
            });
        }
        if(colDefinArr.length > 2)
        {
            var footer = '<tfoot><tr>';
            for(var i=0; i < colDefinArr.length; i++)
            {
                footer += '<th>'+colDefinArr[i].title+'</th>';
            }
            footer += '</></>';
            $currTableElem.append(footer);
        }
        $currTableElem.DataTable
        ({
                "data":fullData.data,
                "columnDefs":colDefinArr,
                "order": [], // use order defined in database
                "processing": false,
                "scrollY": 300,
                "scrollX": true,
                "scrollCollapse": true,
                "fixedColumns": true,
                "paging": false,
                "dom": "t",
                "bDestroy": true,
                // "autoWidth": false,
                "drawCallback":function(settings)
                {
                    $currTableElem.find('.datepicker').each(function() 
                    {
                        initDatePickerWithOptions('datepicker',{targetType:"class"});
                    });
                }
        });  
        noteDT_toggleDisplayTableButtons($currTableElem, (fullData.data.length > 0)?true:false);
    }

    function noteDT_editTestResultsAndDates(currTableId)
    {
        openCommentDialogAndCommit({ maxLength:200, okHandler:noteDT_confirmEditTestResultsAndDates, okHandlerParams:[currTableId] });
    }

    function noteDT_confirmEditTestResultsAndDates(currTableId)
    {
        var $currTableElem = $('#'+currTableId);
        if(!$currTableElem.hasClass('edit-mode')) $currTableElem.addClass("edit-mode");
        $currTableElem.closest('.notebook-section-content').find('.test-result-edit-btn').button('option', 'disabled', true);
        noteDT_loadNotebookTable(currTableId);
    }

    function noteDT_copyTestResultsAndDates(curTableId)
    {
        var table = $('#'+curTableId).DataTable();	

        table.rows().eq(0).each( function ( rowIndex ) 
        {
            var row = table.row( rowIndex );	
            var rowNode = row.node();
    
            if(rowIndex == 0)
            {
                var div = $(rowNode).find('div.drawnDateParentDiv');  
                var input_copy = $(div[0]).find('input[type=text]');
                var date_copy_val = $(input_copy[0]).val();
                var time_copy_val = $.trim($(input_copy[1]).val());
                
                if(time_copy_val.length != 5 || !validateTime(time_copy_val))
                {                            
                    showAlertMessage("ShowAlertMessage_Msg_Short_Name_T_19");
                    return false;
                }

                for(var k=1; k<div.length; k++)
                {
                    var input = $(div[k]).find('input[type=text]');
                    // var date_ = $(input[0]).val();
                    // var time_ = $.trim($(input[1]).val());
                    var old_date = $(input[0]).attr('old_value');
                    var old_time = $(input[1]).attr('old_value');
                    if(input.length > 0 && old_date == "00/00/0000" && old_time == "00:00")
                    {
                        $(input[0]).val(date_copy_val);
                        $(input[1]).val(time_copy_val);
                    }
                }
            }
            else
            {
                var copy_obj, copy_obj_val;
                var input = $(rowNode).find('input[type=text]');
                var textArea = $(rowNode).find('textarea');
                var select = $(rowNode).find('select');
                if(input.length > 0) copy_obj = input;
                else if(textArea.length > 0) copy_obj = textArea;
                else if(select.length > 0) copy_obj = select;
                
                var copy_obj_val = $.trim($(copy_obj[0]).val());                
                if(copy_obj_val.length > 0)
                {
                    for(var j=1; j < copy_obj.length; j++)
                    {
                        var currObj = $(copy_obj[j]);
                        if(!$(currObj).prop('disabled'))
                            currObj.val(copy_obj_val).trigger('change');
                    }
                }     
            }
        });
    }

    function noteDT_calculateTestResults(curTableId)
    {

        $.ajax
        ({
            type:"POST",
            contentType: "application/json; charset=utf-8",
            url: "dataentrytestsservlet_?actionid=CalcRun" + "&curRunId=" +globalNotebookParentId,
            dataType: "json",
            success: function(data) 
            {    
                if(data.errorMsg != null && data.errorMsg != "")
                {
                    parent.showAlertMessage(data.errorMsg);
                }
                else
                {
                    noteDT_loadNotebookTable(curTableId);
                }
            },
            
            error:handleAjaxError
        }); 
        
    }

    function noteDT_saveTestResultsAndDates(tableIdArr, isGlobalSave)
    {
        var fullDataObj = {};
        var tablesCount = tableIdArr.length;
        // variables for error msg data
        var e_curRowIndex = -1,
            e_curTableId = "",
            e_failedData = "";
        try 
        {
            var doContinue = true;
            var spDatesObj = {};
            var spDatesList = "";
            var resultIDList = "";

            for(var j=0; j < tablesCount;j++)
            {
                if(!doContinue)return;

                var curTableId = tableIdArr[j];
                e_curTableId = curTableId;
                var dtTable = noteDT_getDataTable(curTableId);
                if(dtTable == null) continue;
                
                dtTable.rows().eq(0).each( function ( rowIndex ) 
                {
                    if(!doContinue)return;

                    e_curRowIndex = rowIndex+1;
                    var row = dtTable.row( rowIndex );	
                    var rowNode = row.node();
                    var rowData = row.data();
                    e_failedData = rowData;
            
                    if(rowIndex == 0)
                    {
                        var div = $(rowNode).find('div.drawnDateParentDiv');  
                        // start from 1, exclude copy field
                        for(var k=1; k<div.length; k++)
                        {
                            var input = $(div[k]).find('input[type=text]');
                            var date_ = $(input[0]).val();
                            var time_ = $.trim($(input[1]).val());
                            var old_date = $(input[0]).attr('old_value');
                            var old_time = $(input[1]).attr('old_value');
                            
                            if(time_.length != 5 || !validateTime(time_))
                            {                            
                                showAlertMessage("ShowAlertMessage_Msg_Short_Name_T_19");
                                doContinue = false;
                                return false;
                            }
                            if(time_ != '00:00' && date_ == '00/00/0000')
                            {
                                showAlertMessage("ShowAlertMessage_Msg_Short_Name_T_20");
                                doContinue = false;
                                return false;
                            }
                            //update only date time that changed (avoid audit trail duplications) but! ignore empty dates(don't let the user to be with test value without sp actual date)..
                            if(date_ != '00/00/0000' && (date_ != old_date || time_ != old_time))
                            {
                                var parts = date_.split("/");
                                var dateWithoutSlash = parts[0]+''+parts[1]+''+parts[2];
                                spDatesObj[$(div[k]).attr('run_sp_id')] = {
                                    date: dateWithoutSlash,
                                    time: time_
                                }
                            }  
                        }
                    }
                    else
                    {
                        var input = $(rowNode).find('input[type=text]');
                        var textArea = $(rowNode).find('textarea');
                        var select = $(rowNode).find('select');
                        var objlist;
                        if(input.length > 0) objlist = input;
                        else if(textArea.length > 0) objlist = textArea;
                        else if(select.length > 0) objlist = select;
                        
                        // start from 1, exclude copy field
                        for(var j=1; j < objlist.length; j++)
                        {
                                var currObj = $(objlist[j]);
                                var val = $.trim(currObj.val());
                                if(!$(currObj).prop('disabled') && val != $.trim(currObj.attr('old_value')))
                                {
                                    //alert(currObj.val() + ":" + currObj.attr('old_value'));
                                    var inputType = currObj.attr('input_type');
                                    var isLinkedTest = currObj.attr('has_linked_tests_params');
                                    var resultId = currObj.attr('result_id');
                                    numeric = "";
                                    alpha = ""; 
                                    if(inputType == "N")
                                    {                            
                                        if(!fnValidateNumeric(val, rowData[0], true, true, false))
                                        {
                                            doContinue = false;
                                            return false;
                                        }  
                                        numeric = val;
                                    }
                                    else if(inputType == "T")
                                    {                            
                                        if(!fnValidateString({inStr:val, fieldName:rowData[0]}))
                                        {
                                            doContinue = false;
                                            return false;
                                        }
                                        alpha = val;
                                    }
                                    else if(inputType == "B")
                                    {
                                        alpha = val;
                                    }
                                    else if(inputType == "L")
                                    {
                                        numeric = val;
                                        alpha = currObj.find('option:selected').text();
                                    }
                                    // if curr test has linked tests/params defined in template add this test to check list
                                    if(isLinkedTest == '1')
                                    {
                                        resultIDList += resultId + ',';
                                    } 
                                    fullDataObj[resultId] = {
                                        numeric_value:numeric,
                                        alpha_value:alpha,
                                        out_of_limits:currObj.attr('out_of_limits')
                                    };
                                    console.log(curTableId, JSON.stringify(fullDataObj));
                                }
                        }
                        
                    }
                });
            }
        }
        catch (e) {
            console.log("error in noteDT_saveTestResultsAndDates()", e);
            console.log("fullDataObj ", fullDataObj);
            console.log("There is problem with "+e_curTableId+" table data on row " + e_curRowIndex + ".\n Failed on data: " + e_failedData+".");
            doContinue = false;
            fullDataObj = {};
        }
        // console.log("noteDT_saveTestResultsAndDates fullDataObj", fullDataObj);
        // console.log("noteDT_saveTestResultsAndDates doContinue", doContinue);
        if(doContinue)
        {
            globalNotebokTableBlocksSaveState[notebookTableNames.TEST_RESULT_TABLENAME] = 1;
            if(Object.keys(fullDataObj).length > 0 || Object.keys(spDatesObj).length > 0)
            {
                for(key in spDatesObj)
                {
                    var value = spDatesObj[key];
                    spDatesList += key + ',' + value.date + " " + value.time + ";";  /* run_sp_id, date,  time */
                }
                // console.log("spDatesList: ",spDatesList);
                var comment = (commentObject.data("comment") == null)? "":commentObject.data("comment");
                /* If list of results (test with linked tests/params) isn't empty, 
                        do check via DB if those linked tests/params have values.
                    In case if haven't values user need to confirm to continue save process or not.
                */
                if(Object.keys(fullDataObj).length > 0 && resultIDList.length > 0)
                {
                    resultIDList = resultIDList.substr(0,resultIDList.length-1);
                    var msg = checkLinkedTestParams(resultIDList);
                    if(msg.length > 0)
                    {
                        // doContinue = false;
                        openConfirmDialog({ message:msg, onConfirm:noteDT_confirmSaveTestResultsAndDates, onConfirmParams:[tableIdArr, JSON.stringify(fullDataObj),comment,spDatesList],
                                            onCancel:function()
                                            {
                                                globalNotebokTableBlocksSaveState[notebookTableNames.TEST_RESULT_TABLENAME] = 0;
                                                // for(var j=0; j<tableIdArr.length;j++)
                                                // {
                                                //     var tableId = tableIdArr[j];
                                                //     var $currTableElem = $('#'+tableId);
                                                //     $currTableElem.closest('.notebook-section-content').find('.test-result-edit-btn').button('option', 'disabled', false);
                                                // }
                                            }
                                        });  
                    }
                }
                else {
                    noteDT_confirmSaveTestResultsAndDates(tableIdArr, JSON.stringify(fullDataObj),comment,spDatesList);       
                }
            }
            else {
                if(!isGlobalSave)
                {
                    displayAlertDialog("ShowAlertMessage_Msg_Short_Name_T_35");//No tests or date has been updated
                }
            }
        }
    }

    function noteDT_confirmSaveTestResultsAndDates(tableIdArr, values, comment, spDatesList)
    {
        console.log("func arg:", arguments);
        // var isSuccessed = true;
        $.ajax
        ({
                type:"POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "notebookservlet?action_id=saveResultsAndDrawnDate" + "&txtCommentsResult=" + encodeURIComponent(comment) + 
                        "&resultsValuesList=" + encodeURIComponent(values) + "&spDatesList=" + encodeURIComponent(spDatesList) + "&testType=T",
                dataType: "text",
                success: function(retVal) 
                {  
                    if(retVal == "0")
                    {
                        
                        for(var j=0; j<tableIdArr.length;j++)
                        {
                            var tableId = tableIdArr[j];
                            var $currTableElem = $('#'+tableId);
                            $currTableElem.removeClass("edit-mode");
                            $currTableElem.closest('.notebook-section-content').find('.test-result-edit-btn').button('option', 'disabled', false);
                            noteDT_loadNotebookTable(tableId);
                        }
                    }
                    else {
                        displayAlertDialog("Test Results is failed to save");
                    }
                },
                error:handleAjaxError
        });
    }

    /////////////////////////// PARAMETERS ///////////////////////////////////////

    function noteDT_saveParameterTableValues(tableIdArr, isGlobalSave)
    {
        var fullDataObj = {};
        // variables for error msg data
        var e_curRowIndex = -1,
            e_curTableId = "",
            e_failedData = "";
        try 
        {
            var doContinue = true;

            for(var j=0; j<tableIdArr.length;j++)
            {
                if(!doContinue)return;

                var curTableId = tableIdArr[j];
                e_curTableId = curTableId;	
                var dtTable = noteDT_getDataTable(curTableId);
                if(dtTable == null) continue;
        
                dtTable.rows().eq(0).each( function ( index ) 
                {
                    if(!doContinue)return;

                    e_curRowIndex = index+1;
                    var rowHasValues = false;
                    var row = dtTable.row( index );	 
                    var rowData = row.data();
                    e_failedData = rowData;
                    var rowId = rowData[0];
                    var inputType = rowData[1]["inputType"];
                    var resultNumeric = "",
                        resultAlpha = "" ,
                        resultSign = "="
                        resultOOL = "";
                    for(var i=0;i<rowData.length;i++)
                    {	    	
                        var cell = dtTable.cell({row: index, column: i});	 
                        var node = cell.node();
                        
                        var $select = $(node).find('select');
                        var $input = $(node).find('input');
                        var inputVal = ($input.length > 0)?$input.val().trim():"";

                        if($select.length > 0)
                        {
                            if($select[0].hasAttribute('cell_parameter_sign'))
                            {
                                resultSign = $select.find('option:selected').text();
                                // rowHasValues = true;
                            }
                            else if(inputType == "L" && $select.find('option:selected').val() != 0) {
                                resultNumeric = $select.find('option:selected').val().trim();
                                resultAlpha = $select.find('option:selected').text().trim();
                                resultOOL = $select.attr('out_of_limits');
                                rowHasValues = true;
                            }
                        }
                        else if($input.length > 0)
                        {
                            if(inputType == 'D' && inputVal != "00/00/0000")
                            {
                                var parts = inputVal.split("/");
                                var dateWithoutSlash = parts.join('');
                                resultAlpha = dateWithoutSlash;
                                resultOOL = $input.attr('out_of_limits');
                                rowHasValues = true;
                            }
                            else if(inputType == 'T' && inputVal.length > 0)
                            {
                                if(!fnValidateString({inStr:inputVal, fieldName:rowData[2]}))
                                {
                                    doContinue = false;
                                    return false;
                                }
                                resultOOL = $input.attr('out_of_limits');
                                resultAlpha = inputVal;
                                rowHasValues = true;
                            }
                            else if(inputType == 'N' && inputVal.length > 0)
                            {
                                if(!fnValidateNumeric(inputVal, rowData[2], true, true, false))
                                {
                                    doContinue = false;
                                    return false;
                                }  
                                resultOOL = $input.attr('out_of_limits');
                                resultNumeric = inputVal;
                                rowHasValues = true;
                            }
                        }
                    }
                    if(doContinue && rowHasValues)
                    {
                        fullDataObj[rowId] = {
                            result_sign:resultSign,
                            numeric_value:resultNumeric,
                            alpha_value:resultAlpha,
                            out_of_limits:resultOOL
                        };
                        // console.log(JSON.stringify(fullDataObj));
                    }
                } );
            }
            console.log("noteDT_saveParameterTableValues fullDataObj");
            console.log(fullDataObj);
        } 
        catch (e) {
            console.log("error in noteDT_saveParameterTableValues()", e);
            console.log("fullDataObj ", fullDataObj);
            console.log("There is problem with "+e_curTableId+" table data on row " + e_curRowIndex + ".\n Failed on data: " + e_failedData+".");
            fullDataObj = {};
            doContinue = false;
        }
        if(doContinue)
        {
            globalNotebokTableBlocksSaveState[notebookTableNames.PARAMETER_TABLENAME] = 1;
            if(Object.keys(fullDataObj).length > 0)
            {
                $.ajax
                ({
                    type:"POST",
                    contentType: "application/x-www-form-urlencoded; charset=utf-8",
                    url: "notebookservlet?action_id=saveResultsAndDrawnDate" + "&resultsValuesList=" + encodeURIComponent(JSON.stringify(fullDataObj)) + "&testType=P",
                    dataType: "text",
                    success: function(data) 
                    {                          
                        if(data == "0")
                        {
                            for(var j=0; j<tableIdArr.length;j++)
                            {
                                var tableId = tableIdArr[j];
                                noteDT_loadNotebookTable(tableId);
                            }
                        }
                    }
                }); 
            }
            else {
                if(!isGlobalSave)
                {
                    displayAlertDialog("ShowAlertMessage_Msg_Short_Name_T_25");
                }
            }
        }
    }

    function checkLimits(object, physMax, physMin, warnMax, warnMin)
    {

        var val = $.trim($(object).val());
        // console.log("checkLimits arg: ");
        // console.log("val, physMax, physMin, warnMax, warnMin", val, physMax, physMin, warnMax, warnMin);
        if(!(physMax == 0  && physMin  == 0) &&  (val > physMax ||  val < physMin) )
        {
            var warn_msg = "";
            if(warnMin == 0 && warnMax == 0)
            {
                warn_msg = "No warning limits.";
            }
            else
            {
                warn_msg = "Warning limits: "+warnMin+" - "+warnMax+"";
            }
            showAlertMessage("The value of "+$(object).attr('test_name')+" is not valid and it will not be saved:<br/> "+warn_msg+" <br/> Physical limits: "+physMin+" - "+physMax+" ");                    
            $(object).val("");
        }
        else if(!(warnMax == 0  && warnMin  == 0) &&   (  val > warnMax ||  val < warnMin ))
        {
            $(object).attr('out_of_limits', "1");
        }
        else
            $(object).attr('out_of_limits', "0"); 

    }

    return {
            "notebookSectionsList":notebookSectionsList
            ,"_getEditableSectionByType":function(type) {
                return noteDT_getEditableSectionByType(type);
            }
            ,"_getViewableSectionByType":function(type) {
                return noteDT_getViewableSectionByType(type);
            }
            ,"_loadNotebookTableBySection":function($section, filter) {
                noteDT_loadNotebookTableBySection ($section, filter);
            }
            ,"_toggleDisplayTableButtons":function($currTableElem, isShow, buttonIdentifier) {
                noteDT_toggleDisplayTableButtons ($currTableElem, isShow, buttonIdentifier);
            }
            ,"_resizeTables":function(options) {
                noteDT_resizeTables (options);
            }
            ,"_saveTableData":function(tableId, tableName) {
                return noteDT_saveTableData(tableId, tableName);
            }
            ,"_loadEditableSectionData":function ($section, type, savedData) {
                noteDT_loadEditableSectionData($section, type, savedData);
            }
            ,"_getSectionContentValue":function($content,type)
            {
                return noteDT_getSectionContentValue($content,type);
            }
    };
}