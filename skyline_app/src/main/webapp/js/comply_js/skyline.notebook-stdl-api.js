(function ($)
{
    var globalTableRowId = -1;
    var tableParameterNameListHolder = [];
    var tableUomGroupListHolder = [];
    var tableUomListHolder = [];

    var notebookStdlImplementationAPI = function(args) 
    {

        var globalNotebookParentId = args.parentId;
        var globalNotebookIsInViewMode = args.isViewMode;
        // var globalNotebookImplType = args.implType;
        var globalNotebookSectionsCounter = args.sectionsCounter;
        var defaultParameterInputType = 'T';
        
        var notebookSectionsList = [
            { id: "parameter", type: notebookSectionTypes.PARAMETER_SECTION, text: "Parameters" },
            // { id: "material", type: notebookSectionTypes.MATERIAL_SECTION, text: "Materials" },
            { id: "richtext", type: notebookSectionTypes.RICHTEXT_SECTION, text: "Richtext" },
            { id: "file", type: notebookSectionTypes.FILE_SECTION, text: "File" },
            { id: "spreadsheet", type: notebookSectionTypes.SPREADSHEET_SECTION, text: "SpreadSheet" }
        ];

        function noteDT_getEditableSectionByType(type)
        {
            var html = "", tableUnqId = "",  funcAfterSectionCreation = null;
            if(type == notebookSectionTypes.PARAMETER_SECTION)
            {
                tableUnqId = 'noteParametersTable_'+globalNotebookSectionsCounter++ +'';
                html = //'<div style="float: left;"><span class="field-label" style="margin-right: 10px;">Parameters</span></div>'+
                        '<div style="margin-left: 20px;float: left;">'+
                        '   <button type="button" class="table-secondary-btn new-button">Add row</button>'+
                        '</div>'+
                        '<div style="margin-top:10px;width:100%;float:left;">'+
                            '<table id="'+tableUnqId+'" class="display notebook-table notebook-parameters-table" tableName="'+notebookTableNames.PARAMETER_TABLENAME+'" style="width:100%"></table>'+
                        '</div>';
                funcAfterSectionCreation = function(newSection) {
                    // noteDT_loadNotebookTable (tableUnqId);
                    newSection.find('.table-secondary-btn').button();
                    newSection.find('.new-button').on('click', function() {noteDT_addTableRow(tableUnqId)});
                }
            }
            return {html:html,sectionCounter:globalNotebookSectionsCounter,funcAfterSectionCreation:funcAfterSectionCreation};
        }

        function noteDT_loadEditableSectionData ($section, type, savedData)
        {    
            noteDT_loadNotebookTableBySection ($section);
        }

        function noteDT_loadNotebookTableBySection ($section)
        {
            var tableId = $section.find('.notebook-table').attr('id');
            noteDT_loadNotebookTable(tableId);
        }

        function noteDT_loadNotebookTable (currTableID)
        {    
            var $currTableElem = $('#'+currTableID);
            // var $content = $currTableElem.closest('.notebook-section-content');
            var tableName = $currTableElem.attr('tableName');
            var dataSrc = "";
            if(tableName == notebookTableNames.PARAMETER_TABLENAME)
            {
                dataSrc = "action_id=getStdlParametersTable&parentId="+globalNotebookParentId;
            }
            
            if(dataSrc == "")return;

            $.ajax
            ({
                type:"POST",
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                url: "notebookservlet",
                data: dataSrc,
                dataType: "json",
                success: function(data) 
                {  
                    var dtTable = noteDT_getDataTable(currTableID, true);
                    dt_ext_updateTableData(dtTable, data);
                },
                error:handleAjaxError
            });  
        }

        function noteDT_addTableRow(currTableID)
        {
            var dtTable = noteDT_getDataTable(currTableID);
            dtTable.row.add( [
                globalTableRowId--
                ,""
                ,""
                ,""
                ,""
                ,""
                ,""
                ,""
                ,""
                ,""
                ,""
            ] ).draw( false );
        }

        function noteDT_getDataTable(currTableId, isCreate)
        {
            var dtTable = null;
            var $currTableElem = $('#'+currTableId);
            var createTableIfNotExists = isCreate;
            var isDataTableExists = dt_ext_isDataTable($currTableElem[0]); 

            if(isDataTableExists)
            {
                dtTable = $currTableElem.DataTable();
            }
            else if(createTableIfNotExists)
            {
                var tableAttr = [];
                var additOptions = {};
                var tableName = $currTableElem.attr('tableName');
                if(tableName == notebookTableNames.PARAMETER_TABLENAME)
                {
                    tableAttr = [{"paging":false,"dom":"t","footer":false}
                                    ,{"title":"row_id","visible":false,"searchable":false}
                                    ,{"title":"hidden_object","visible":false,"searchable":false}
                                    ,{"title":"Parameter_Name"}
                                    ,{"title":"Parameter_Code"}
                                    ,{"title":"Sign", "width": "100px"}
                                    ,{"title":"Value", "width": "250px"}
                                    ,{"title":"UOM_Group"}
                                    ,{"title":"Uom"}  
                                    ,{"title":"User"} 
                                    ,{"title":"Date"}   
                                    ,{"title":"", "searchable":false, "orderable":false, "visible": !globalNotebookIsInViewMode} 
                                ];
                    if(!globalNotebookIsInViewMode)
                        additOptions = {"createdCell":noteDT_tableCreatedCell,"onRowClick":noteDT_tableRowClicked,"drawCallback":noteDT_tableDrawCallback};
                }
                
                dtTable = drawTable(currTableId,tableAttr,[],additOptions,labelsArr);
            }

            return dtTable;
        }

        function noteDT_tableCreatedCell(cell, cellData, rowData, rowIndex, colIndex, tableID)
        {
            var $table = $('#'+tableID);
            var tableName = $table.attr('tableName');
            if(tableName == notebookTableNames.PARAMETER_TABLENAME)
            {
                var hdnParams = rowData[1];
                var disabled = "";
                var parsedList = [];
                var selValue = "-1";
                if([2,6,7].indexOf(colIndex) != -1)
                {
                    if(cellData != "")
                    {
                        parsedList = parseStringToJson("["+cellData+"]");
                        selValue = parsedList[0].id;
                    } 
                }
                switch(colIndex) 
                {
                    case 2:
                        select =  '<select class="chosen_single select-parameter-name mandatory-field" fieldlabelname="Parameter Name"  selectedVal="'+selValue+'" style="width:100%; text-align: left;"></select>';
                        $(cell).html(noteDT_updateList($(select), parsedList, selValue)[0]);
                        $(cell).find('.select-parameter-name').on('change', function() {noteDT_onParameterNameChange(this, $(cell));});
                        break;
                    case 4:
                        if(hdnParams["inputType"] != "N")
                        {
                            disabled = 'disabled';
                        }
                        var ddl =  '<select style="width:100%; text-align: left;" '+disabled+' class="select-parameter-sign">'+getSignOptions(cellData)+'</select>';
                        $(cell).html(ddl);
                        break;
                    case 5:
                        
                        var html = '<div class="parameter-value"></div>';
                        $(cell).html(html);
                        noteDT_createParameterValueCellByType($(cell).find('.parameter-value'),{inputType:hdnParams["inputType"],data:cellData,disabled:disabled, selectionListId:hdnParams["selectionListId"]});
                        		
                        break;
                    case 6: // UOM GROUP     
                        select =  '<select class="chosen_single select-uom-group" selectedVal="'+selValue+'" style="width:100%; text-align: left;"></select>';
                        $(cell).html(noteDT_updateList($(select), parsedList, selValue)[0]);
                        $(cell).find('.select-uom-group').on('change', function() {noteDT_onUOMGroupChange(this.value,-1,$(cell));});
                        break;
                    case 7: // UOM     
                        var parentValId = hdnParams["uomGroupId"];
                        select =  '<select class="chosen_single select-uom mandatory-field" fieldlabelname="UOM" selectedVal="'+selValue+'" selectedParentVal="'+parentValId+'" style="width:100%; text-align: left;"></select>';
                        $(cell).html(noteDT_updateList($(select), parsedList, selValue)[0]);
                        break;
                    case 10:
                        var rowIcons = [
                            {iconType:"remove", tooltip:"Delete", disabled:false}
                        ];  
                        $(cell).addClass("table-column-with-icons").html(fnCreateTableRowActionIcons(rowIcons));
                        break;
                    default: $(cell).html(cellData);
                    break;
                }
            }
        }

        function noteDT_createParameterValueCellByType($parentElem, obj)
        {
            var funcAfterFieldCreation;
            var cellHtml = "";
            var cellType = obj.inputType?obj.inputType:defaultParameterInputType;
            var cellData = obj.data?obj.data:"";
            var disabled = obj.disabled?obj.disabled:"";
            switch(cellType)
            {
                case "N": 
                    cellHtml = '<input type="text" '+ disabled+' value="'+ cellData+'" onkeypress="validateDecimal(this.value, event.keyCode, event)" style="width:99%;">';
                    break;
                case "T":
                    cellHtml = '<input type="text" '+ disabled+' value="'+ cellData+'" style="width:99%;" onkeypress="validateLegalChar(event.keyCode, event)" maxlength="50">';
                    break;                    
                case "B":
                    cellHtml = '<select style="width:100%; text-align: left;" '+ disabled+' class="chosen_single"><option value="0">Choose</><option value="Pass">Pass</><option value="Fail">Fail</></> ';
                    break;
                case "D":                      
                    cellHtml = '<input type="text" value="'+slashDate( cellData)+'" style="width:100px;text-align:center" class="datepicker" '+ disabled+' align="left"  readonly="readonly">';
                    funcAfterFieldCreation = function() {
                        initDatePickerWithOptions($parentElem.find('.datepicker'), {targetType:'element','disabled': disabled});
                    }
                    break;
                case "L":
                    var parsedList = [];
                    var selValue = "-1", selTextVal = "";
                    var parentValId = obj.selectionListId?obj.selectionListId:-1;
                    if( cellData != "")
                    {
                        parsedList = parseStringToJson("["+ cellData+"]");
                        selValue = parsedList[0].id;
                        selTextVal = parsedList[0].text;
                    }     
                    var select =  '<select  style="width:100%; text-align: left;" '+ disabled+' selectedVal="'+selValue+'" selectedTextVal="'+selTextVal+'" selectedParentVal="'+parentValId+'" class="chosen_single select-parameter-value-list">';
                    cellHtml = noteDT_updateList($(select), parsedList, selValue)[0];
                    funcAfterFieldCreation = function() {
                        $parentElem.find('.chosen_single').chosen({ width: "100%",placeholder_text_single:"Choose"})
                                .on('chosen:showing_dropdown',function (evt, params) {
                                    noteDT_tableInitialListLoad($(this));
                                    $(this).off('chosen:showing_dropdown');
                                });
                    } 
                    break;
                default:
                    cellHtml =  cellData;
                    break;
            }	
            $parentElem.html(cellHtml);
            if(funcAfterFieldCreation)
            {
                funcAfterFieldCreation();
            }
            return $parentElem;	
        }

        function noteDT_tableRowClicked(currRowData, tableId, isRowSelected, event)
        {
            // var id = $('#'+tableId).DataTable().row( this ).id();
 
            //  alert( 'Clicked row id '+id );
            var tableName = $('#'+tableId).attr('tableName');
            var act = fnGetTableRowIconActionNameByEvent(event);
            if(act)
            {
                if(!isRowSelected)$(event.currentTarget).addClass('selected');    
                switch(act) {
                    case "remove": noteDT_beforeDeleteTableRow(tableId, tableName, currRowData[0]);
                        break;
                    default: "";
                }
                
            }
        }

        function noteDT_tableDrawCallback(settings, tableId)
        {
            var $table = $('table[id="'+tableId+'"]');
            $table.find('.datepicker').each(function() 
            {
                initDatePickerWithOptions('datepicker',{targetType:"class"});
            });
            $table.find('.chosen_single').each(function() 
            {
                var $this = $(this);
                if($this.next('div.chosen-container').length == 0)
                {
                    $this.chosen({ width: "100%",placeholder_text_single:"Choose"})
                        .on('chosen:showing_dropdown',function (evt, params) {
                            noteDT_tableInitialListLoad($(this));
                            $this.off('chosen:showing_dropdown');
                        });

                    if($this.hasClass('select-parameter-name')) {
                        $this.on('chosen:showing_dropdown',function (evt, params) {
                            noteDT_overrideChosenSelectedValue($this);          
                        });
                    }
                }
            });
        }

        function noteDT_overrideChosenSelectedValue($element)
        {
            $element.next('div.chosen-container').find('.chosen-single').find('span').html($element.find("option:selected").attr('parameter_name'));  
        }

        function noteDT_tableInitialListLoad($element)
        {
            // console.log("noteDT_tableInitialListLoad", $element[0]);
            if($element.hasClass('select-parameter-name')) {
                var selValue = $element.attr('selectedVal');
                if(tableParameterNameListHolder.length == 0) {
                    sendAjaxRequest({servletName:"notebookservlet",urlParams:"action_id=getParameterNames&selParamNameId="+selValue,dataType:"json",
                            beforeSendFunc:function() {
                              $element.html("<option>Loading...</option>").trigger('chosen:updated');  
                            },
                            onSuccess:function(returnedList) {
                                noteDT_updateList($element, returnedList, selValue);
                                noteDT_overrideChosenSelectedValue($element);
                                tableParameterNameListHolder = returnedList;
                            }
                    });
                }
                else {
                    noteDT_updateList($element, tableParameterNameListHolder, selValue);
                }
            }
            else if($element.hasClass('select-uom-group')) {
                var selValue = $element.attr('selectedVal');
                if(tableUomGroupListHolder.length == 0) {

                    fnGetUomGroup(selValue, function(returnedList) {
                        noteDT_updateList($element, returnedList, selValue);
                        tableUomGroupListHolder = returnedList;
                    });
                }
                else {
                    noteDT_updateList($element, tableUomGroupListHolder, selValue);
                }
            }
            else if($element.hasClass('select-uom')) {
                var selValue = $element.attr('selectedVal');
                var selParentVal = $element.attr('selectedParentVal');
                if(selParentVal == "-1")
                {
                    if(tableUomListHolder.length == 0) {
                        fnGetUomByGroup(selParentVal, selValue, function(returnedList) {
                            noteDT_updateList($element, returnedList, selValue);
                            tableUomListHolder = returnedList;
                        });
                    }
                    else {
                        noteDT_updateList($element, tableUomListHolder, selValue);
                    }
                }
                else {
                    fnGetUomByGroup(selParentVal, selValue, function(returnedList) {
                        noteDT_updateList($element, returnedList, selValue);
                    });
                }
            }
            else if($element.hasClass('select-parameter-value-list')) {
                var selectedTextVal = $element.attr('selectedTextVal');
                var selValue = $element.attr('selectedVal');
                var selParentVal = $element.attr('selectedParentVal');
                ajax_populateSelectByJsonList({element:$element,servletName:'notebookservlet', 
                                                urlParams:"action_id=getParameteSelListItems&selListId="+selParentVal+"&selItemValue="+selectedTextVal,
                                                selectedVal:selValue,selectOptions:{useSingleDefaultOption:true}});
            }
        }


        function noteDT_onUOMGroupChange(selValue, uomVal, $td)
        {
            var $uomElement = $td.next().find('.select-uom');
            fnGetUomByGroup(selValue, uomVal, function(returnedList) {
                noteDT_updateList($uomElement, returnedList, uomVal);
            });
            $uomElement.off('chosen:showing_dropdown');
        }

        function noteDT_onParameterNameChange(element, $td)
        {    
            var $tr = $td.closest('tr');
            var selOption = $(element).find("option:selected");
            //parameter name
            // $(element).next('.chosen-container').find('.chosen-single').find('span').html(selOption.attr('parameter_name'));
            noteDT_overrideChosenSelectedValue($(element));
            //parameter code
            var code = selOption.attr('parameter_code');
            $td.next().html(code?code:"");
            //value
            noteDT_createParameterValueCellByType($tr.find('.parameter-value'),{inputType:selOption.attr('input_type'),selectionListId:selOption.attr('result_category_id')});
            //uom_group_id
            var uomGroupId = selOption.attr('uom_group_id')?selOption.attr('uom_group_id'):-1;
            var $uomGroupCell = $tr.find('.select-uom-group');
            if(tableUomGroupListHolder.length == 0) {

                fnGetUomGroup(uomGroupId, function(returnedList) {
                    noteDT_updateList($uomGroupCell, returnedList, uomGroupId);
                    tableUomGroupListHolder = returnedList;
                });
                $uomGroupCell.off('chosen:showing_dropdown');
            }
            else {
                $uomGroupCell.val(uomGroupId).trigger('chosen:updated');
            }
            //uom
            noteDT_onUOMGroupChange(uomGroupId, selOption.attr('uom')?selOption.attr('uom'):-1, $uomGroupCell.closest('td'));
        }

        function noteDT_saveTableData(tableId, tableName)
        {
            var tableIdArr = [];
            // var isGlobalSave = (tableId == "-1")?true:false;
            if(tableName == notebookTableNames.PARAMETER_TABLENAME) 
            {
                if(tableId == "-1")
                {           
                    var $tables = $('#'+globalNotebookElementId).find(".notebook-parameters-table");
                    
                    $tables.each(function(inx,el) {
                        tableIdArr[inx] = $(el).attr('id');
                    });
                    // console.log("parameters tables ids", tableIdArr);
                }
                else {
                    tableIdArr[0] = tableId;
                }
                if(tableIdArr.length > 0)
                {
                    noteDT_saveParameterTableValues(tableIdArr);
                }
            }
        }

        function noteDT_saveParameterTableValues(tableIdArr)
        {
            var doContinue = true;
            var fullDataObj = {};
            try{
                for(var j=0; j<tableIdArr.length;j++)
                {
                    if(!doContinue)return;
                    var currTableId = tableIdArr[j];
                    var dtTable = noteDT_getDataTable(currTableId);
                    if(dtTable == null) continue;
                    if(!noteDT_tableCheckMandatoryField(dtTable))
                    {
                        return false; 
                    }
                    dtTable.rows().nodes().each( function (row, index, rowApi) 
                    {
                        if(!doContinue)return;
                        var resultNumeric = "",
                            resultAlpha = "" ,
                            resultSign = "=";

                        var rowData = rowApi.data()[index];
                        var rowId = rowData[0];
                        var paramSelOption = $(row).find('.select-parameter-name').find('option:selected');
                        var paramNameId = paramSelOption.val();
                        var paramName = paramSelOption.text();
                        var uomGroup = $(row).find('.select-uom-group').find('option:selected').val();
                        var uom = $(row).find('.select-uom').find('option:selected').val();
                        resultSign = $(row).find('.select-parameter-sign').find('option:selected').text();
                        var valueField = $(row).find('.parameter-value').children();
                        if(valueField.length > 0)
                        {
                            var fieldTag = valueField.prop('tagName').toLowerCase();
                            
                            var inputType = rowData[1]["inputType"];
                            if(fieldTag == 'select')
                            {
                                resultNumeric = valueField.find('option:selected').val();
                                resultAlpha = valueField.find('option:selected').text();
                            }
                            else if(fieldTag == 'input')
                            {
                                var inputVal = valueField.val().trim();
                                if(inputType == 'D' && inputVal != "00/00/0000")
                                {
                                    var parts = inputVal.split("/");
                                    var dateWithoutSlash = parts.join('');
                                    resultAlpha = dateWithoutSlash;
                                }
                                else if(inputType == 'T' && inputVal.length > 0)
                                {
                                    if(!fnValidateString({inStr:inputVal, fieldName:"Value for "+paramName}))
                                    {
                                        doContinue = false;
                                        return false;
                                    }
                                    resultAlpha = inputVal;
                                }
                                else if(inputType == 'N' && inputVal.length > 0)
                                {
                                    if(!fnValidateNumeric(inputVal, "Value for "+paramName, true, true, false))
                                    {
                                        doContinue = false;
                                        return false;
                                    }  
                                    resultNumeric = inputVal;
                                }
                            }
                        }
                        if(doContinue)
                        {
                            fullDataObj[rowId] = {
                                parameter_name_id:paramNameId,
                                uom_group_id:(uomGroup=="-1")?"":uomGroup,
                                uom:(uom=="-1")?"":uom,
                                result_sign:resultSign,
                                numeric_value:resultNumeric,
                                alpha_value:resultAlpha
                            };
                            console.log(JSON.stringify(fullDataObj));
                        }
                    });
                }
                console.log("noteDT_saveParameterTableValues fullDataArr", fullDataObj);
                if(doContinue)
                {
                    if(Object.keys(fullDataObj).length > 0)
                    {
                        sendAjaxRequest({servletName: "notebookservlet", urlParams:"action_id=saveStdlParameters&parentId="+globalNotebookParentId+"&valuesArr="+encodeURIComponent(JSON.stringify(fullDataObj)),
                                    onSuccess:function(retVal) {
                                        if(retVal == "-1")
                                        {
                                            displayAlertDialog("Save failed");
                                        }
                                        else {
                                            for(var j=0; j<tableIdArr.length;j++)
                                            {
                                                noteDT_loadNotebookTable(tableIdArr[j]);
                                            }
                                        }
                                    }       
                        });
                    }
                    else {
                        // displayAlertDialog("ShowAlertMessage_Msg_Short_Name_T_25");
                    }
                }
            }
            catch (e) {
                console.log("error in noteDT_saveParameterTableValues()", e);
                console.log("fullDataObj ", fullDataObj);
                fullDataObj = {};
            }
        } 
        
        function noteDT_tableCheckMandatoryField(dtTable)
        {
            var toReturn = true;
            dtTable.rows().nodes().to$().find('.mandatory-field').each( function (index, elem) {
                // console.log($(elem));
                var $el = $(elem);
                if($el.prop('tagName').toLowerCase() == 'select')
                {
                    if($el.prop('selectedIndex') <= 0)
                    {
                        displayAlertDialog($el.attr('fieldlabelname') + " is mandatory field.");
                        toReturn = false;
                        return false;
                    }
                }
            });
            return toReturn;
        }
        

        function noteDT_tableRowIsEmpty(tableId, tableName, rowId)
        {
            var dtTable = noteDT_getDataTable(tableId);
            var rowNode = dt_ext_getTableRowNodeById(dtTable,rowId);
            if(tableName == notebookTableNames.PARAMETER_TABLENAME)
            {
                if($(rowNode).find(".select-parameter-name").prop('selectedIndex') > 0) {
                    return false;
                }
            }
            
            return true;
        }

        function noteDT_beforeDeleteTableRow(tableId, tableName, rowIdToDelete)
        {
            var _message = "";
            if(tableName == notebookTableNames.PARAMETER_TABLENAME)
            {
                _message = "OpenConfirmDialog_Msg_Short_Name_T_4";
            }
            else if(tableName == notebookTableNames.MATERIAL_TABLENAME)
            {
                _message = "OpenConfirmDialog_Msg_Short_Name_T_5";
            }

            if(noteDT_tableRowIsEmpty(tableId, tableName, rowIdToDelete))
            {
                noteDT_deleteTableRow(tableId,tableName, rowIdToDelete);
            }
            else {
                openConfirmDialog({ message:_message,  
                                    onConfirm:function() 
                                    {
                                        noteDT_deleteTableRow(tableId,tableName, rowIdToDelete);
                                    }
                });
            }
        }

        function noteDT_deleteTableRow(tableId, tableName, rowIdToDelete)
        {
            var comment = "";//(commentObject.data("comment") == null)? "" : commentObject.data("comment");
            var dataSrc = "";
            if(tableName == notebookTableNames.PARAMETER_TABLENAME)
            {
                dataSrc = "action_id=deleteStdlParameters&idsToDelete=" + rowIdToDelete + "&auditTrailComments=" + encodeURIComponent(comment);
            }
            var dtTable = noteDT_getDataTable(tableId);
            dt_ext_deleteTableRowById(dtTable, rowIdToDelete);

            sendAjaxRequest({servletName:'notebookservlet', urlParams:dataSrc,
                            onSuccess: function(data) {
                                if( data == "-1" )
                                {
                                    displayAlertDialog("ShowAlertMessage_Msg_Short_Name_T_30", { title:"TitleInMess_Msg_Short_Name_T_2" });
                                    return;
                                }
                            }
            }); 
        }

        function noteDT_deleteTableBySection($section)
        {
            var rowIdsArr = [];
            var tableElem = $section.find('.notebook-table');
            var tableId = tableElem.attr('id');
            var tableName = tableElem.attr('tableName');
            var dtTable = noteDT_getDataTable(tableId);
            if(dtTable == null) return;
            
            dtTable.rows().nodes().each( function (row, index, rowApi) 
            {
                var rowData = rowApi.data()[index];
                rowIdsArr[index] = rowData[0];
            });

            if(rowIdsArr.length > 0)
            {
                noteDT_deleteTableRow(tableId,tableName, rowIdsArr);
                // openConfirmDialog({ message:"This action will remove all rows from table. Are you sure?",  
                //                     onConfirm:function() 
                //                     {
                //                         noteDT_deleteTableRow(tableId,tableName, rowIdsArr);
                //                     }
                //                 });
            }
        }

        function noteDT_updateList($currElem, parsedData, selectedArr)
        {
            return customSelectPopulate($currElem, parsedData, selectedArr, {useSingleDefaultOption:true});
        }

        return {
                "notebookSectionsList":notebookSectionsList
                ,"_getEditableSectionByType":function(type) {
                    return noteDT_getEditableSectionByType(type);
                }
                ,"_loadEditableSectionData":function ($section, type, savedData) {
                    noteDT_loadEditableSectionData($section, type, savedData);
                }
                ,"_saveTableData":function(tableId, tableName) {
                    return noteDT_saveTableData(tableId, tableName);
                }
                ,"_getSectionContentValue":function($content,type)
                {
                    return {};
                }
                ,"_onSectionRemove":function($section)
                {
                    noteDT_deleteTableBySection($section);
                }
        };
    }

    $.notebookStdlImplementationAPI = notebookStdlImplementationAPI;
})(jQuery)