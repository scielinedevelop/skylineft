(function ($)
{
	$.fn.richtext = function customRichtextEditor (param1, param2) {
		
		var $element = $(this);
		var arg_len = arguments.length;
		
		var _get_setHtmlValue = function(value) {
			if(arg_len == 1) {
				return $element.summernote('code');
			}
			else {
				$element.summernote('code', value);
				return true;
			}
		}
		var _getTextValue = function() {
			var _html = $element.summernote('code');
			var _text = _html.replace(/<\/p>/gi, "\n")
                			.replace(/<br\/?>/gi, "\n")
                			.replace(/<\/?[^>]+(>|$)/g, "");
			var plainText= $("<div />").html(_text).text().trim();
			return plainText;
		}
		var _set_disabled = function() {
			$element.summernote('disable');
		}
		var _set_enabled = function() {
			$element.summernote('enable');
		}
		var _destroyEditor = function() {
			$element.summernote('destroy');
			return true;
		}
		var _initRichtextEditor = function(options) 
		{
			var opts = (options)?options:{};
			var _isViewOnly = opts.viewonly;
			var _height = _isViewOnly?'auto':(opts.height)?opts.height:250;
				
			$element.summernote({
					tabsize: 2,
					height:_height,
					spellCheck: false,
					disableDragAndDrop: true,
					// focus: true,
					toolbar: [
					['style', ['style']],
					['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
					['fontname', ['fontname']],
					['fontsize', ['fontsize']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph','height']],
					['table', ['table']],
					['insert', ['link', 'picture']],
					['view', ['fullscreen', 'undo', 'redo', 'codeview', 'help']] // for develop use
					//['view', ['fullscreen']] // for production use
					],
					colorButton: {
						backColor: '#d9d9d9'
					},
					minHeight: null,             // set minimum height of editor
					maxHeight: null,             // set maximum height of editor
					callbacks: {
						onChange: function(contents, $editable) {
						//   console.log('onChange:', contents, $editable);
							if(!$editable.attr("justLoaded") || $editable.attr("justLoaded") == "0")
							{
						 		raiseChangeFlag();
							}
							else {
								$editable.attr("justLoaded","0");
							}
						}
					  }
				});
				
				if(_isViewOnly)
				{
					$element.next("div.note-editor.note-frame").find("div.note-editable").attr('contenteditable',false);
				}
				// hidden possibility to upload files from local machine, development necessary for treat uploaded file/image 
				$('div.note-group-select-from-files').remove();
		}

		if(arg_len <= 1 && (param1 == null || typeof arguments[0] == 'object'))
		{
			_initRichtextEditor(param1);
		}
		else
		{			
			switch(param1) {
				case 'html': return _get_setHtmlValue(param2);
				case 'text': return _getTextValue();
				case 'destroy': return _destroyEditor();
				case 'disable': return _set_disabled();
				case 'enable': return _set_enabled();
				default: return null;
			}
		}

	}
})(jQuery)

function saveRichtextContent($element)
{
	var content = $element.richtext('html');
	var oldContentId = $element.attr("content_id");
	oldContentId = (oldContentId)?oldContentId:"";

	// var text = $element.richtext('text');
	// console.log('text', text);
	// console.log('isEmpty', $element.summernote('isEmpty'));
	var newContentId = fnSaveClobContent(content, "RICHTEXT", oldContentId);
	$element.attr("content_id",newContentId);
	return newContentId;
}	

function loadRichtextContentById($element, id)
{	
	// console.log("loadRichtextContentById:", id);
	fnGetClobContent(id, function(content) {
		// set attribute to hold currently loaded content id
		$element.attr("content_id",id);

		// console.log("richtext load");
		var editor = $element.next("div.note-editor.note-frame").find("div.note-editable");
		editor.attr("justLoaded","1");
		// console.log("justLoaded", editor.attr("justLoaded"));
		$element.richtext('html', content);
		// editor.attr("justLoaded","0");		
		// console.log("justLoaded", editor.attr("justLoaded"));
	});
}	

function validateRichtextContent($element, obj)
{
	var alertMsg = '',
		_inFieldName = (obj.fieldName == null) ? '' : obj.fieldName,
		_isRequired = (obj.isRequired == null) ? false : obj.isRequired;
	var text = $element.richtext('text');
	console.log('validateRichtextContent', text);

	// if ($('#summernote').summernote('isEmpty')) {
	// 	alert('editor content is empty');
	//   }

	if(_isRequired && text == "") 
	{
		alertMsg = (_inFieldName == '') ? "" : (_inFieldName + " is a required field");
		
		if (text == null || text == "")
		{			
			displayAlertDialog(alertMsg, { title:"Required Field Missing" });
			return false;
		}
	}
	return true;
}

