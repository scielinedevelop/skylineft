<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8"  import="comply.SkyLine.bl.biz.User" %>
<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE><spring:message code="appTitle" text="" /> <spring:message code="appVersion" text="" /></TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <%@ include file="../../include/includeLoadingDiv.jsp"%>   
	<%@ include file="../../include/includeGeneralCSS_v3.jsp"%>            
    <%@ include file="../../include/includeGeneralJS_v3.jsp"%>
    <script src="skylineFtWebapp/structure/catalog/item/catalog_item.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>
    <%@ include file="../../include/includeFoundationJS.jsp"%> 

<style type="text/css">
    /* .ui-tabs .ui-tabs-panel {
        display: table;
    } */
    .cell-label-column-1 {
        width:210px;
    }
    .cell-label-column-2 {
        width:230px;
        margin-left: 70px;
    }
    .cell-label-column-3 {
        width: 90px;
        margin-left: 70px;
    }
    .cell-element {
        width:12%;
    }

    .floatingButtonsPanelContainer {
        min-width: 100px;
    }

</style>

</HEAD>
<BODY style="overflow-x: hidden;">
    <%
      
		String pageHeader = "<label>Item</label>"; 
        String pageTitle = "";
        request.setAttribute("NAVIGATION", "");
   %>
<div width="100%" align="center">
        <%@ include file="../../include/includeMainHeader.jsp" %>	
        <%@ include file="../../include/includePageHeader.jsp"%>
</div>
<div style="width:99%;margin-left:0.5%;" class="html-page-layout">
    <div >
        <label style="margin-right: 5px;">Company:</label><span>Gat</span>
        <label style="margin-right: 5px;margin-left: 15px;">Catalog Name:</label><span>Gat</span>
    </div>
    <div id="tabs" >
        <ul>
        <li><a href="#tabsGeneral" id="aTabsGeneral">General</a></li>
        <li><a href="#tabsCharacteristics" id="aTabsCharacteristics">Characteristics</a></li>
        <li><a href="#tabsComponents" id="aTabsComponents">Components (Product Tree)</a></li>
        <li><a href="#tabsAlternative" id="aTabsAlternative">Alternative and similar items</a></li>
        <li><a href="#tabsAttachments" id="aTabsAttachments">Attachments</a></li>
        </ul>
        <div id="tabsGeneral" style="display: table;">   
            <div style="width:99%;margin-left:0.5%;"> 
            <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>ID Number:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtIDNumber" id="txtIDNumber" style="width:100%">
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Kosher for the days of the year:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlKosherRegular" id="ddlKosherRegular" class="chosen_single"  ></select>
                </div>
                <div class="layout-cell cell-label-column-3 field-label"><label>Cost:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtCost" id="txtCost" style="width:100%">
                </div>
            </div>
            <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Item Name:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtItemName" id="txtItemName" style="width:100%">
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Kosher for Passover:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlKosherPass" id="ddlKosherPass" class="chosen_single"  ></select>
                </div>
                <div class="layout-cell cell-label-column-3 field-label"><label>Currency:</label></div>
                <div class="layout-cell cell-element"  >$</div>
            </div>
             <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Status:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlStatus" id="ddlStatus" class="chosen_single"  ></select>
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Euro 1:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="radio" name="rdEuro" id="rdEuroYes" value="1" ><label style="margin-left: 5px;margin-right: 15px;">Yes</label>
                    <input type="radio" name="rdEuro" id="rdEuroNo" value="0"><label style="margin-left: 5px;">No</label>
                </div>
            </div>
           <div class="layout-row">
                <div class="layout-cell cell-label-column-1">
                    <div class="layout-cell"><label class="field-label" style="margin-right: 5px;">Date:</label><span>10/10/2021</span></div>
                    <div class="layout-cell field-label" style="margin-left: 5px;"><label>User Name:</label></div>
                </div>
                <div class="layout-cell cell-element">
                    <span>admin</span>
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Type of preservation:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlPreservType" id="ddlPreservType" class="chosen_single"  ></select>
                </div>
            </div>
             <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Alternative Item:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="radio" name="rdAlter" id="rdAlterYes" value="1" ><label style="margin-left: 5px;margin-right: 15px;">Yes</label>
                    <input type="radio" name="rdAlter" id="rdAlterNo" value="0"><label style="margin-left: 5px;">No</label>
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Production:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlProduction" id="ddlProduction" class="chosen_single"  ></select>
                </div>
            </div>
           <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Manufacturer ID Number:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtManufId" id="txtManufId" style="width:100%">
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Intermediate Material:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="radio" name="rdInter" id="rdInterYes" value="1"><label style="margin-left: 5px;margin-right: 15px;">Yes</label>
                    <input type="radio" name="rdInter" id="rdInterNo" value="0"><label style="margin-left: 5px;">No</label>
                </div>
            </div>
             <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Manufacturer Description:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtManufDesc" id="txtManufDesc" style="width:100%">
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Standard Brix:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtStandBrix" id="txtStandBrix" style="width:100%">
                </div>
            </div>
            <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Manufacturer:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlManuf" id="ddlManuf" class="chosen_single"  ></select>
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Location:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtLocation" id="txtLocation" style="width:100%">
                </div>
            </div>
            <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Supplier:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlSupplier" id="ddlSupplier" class="chosen_single"  ></select>
                </div>
                <div class="layout-cell cell-label-column-2 field-label"><label>Comments:</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" name="txtComments" id="txtComments" style="width:100%">
                </div>
            </div>
            <div class="layout-row field-label"><label style="text-decoration: underline;font-weight: 600;">Classification of Materials</label></div>
            <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Item Type:</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlItemType" id="ddlItemType" class="chosen_single"  ></select>
                </div>
            </div>
            <div class="layout-row">
                <div class="layout-cell cell-label-column-1 field-label"><label>Item Category</label></div>
                <div class="layout-cell cell-element"  >
                    <select name="ddlItemCategory" id="ddlItemCategory" class="chosen_single"  ></select>
                </div>
            </div>
        </div>
        </div>     
        <div id="tabsCharacteristics"> 
            <div style="width:99%;margin-left:0.5%; margin-top:25px;" >
                <table id="tableCharacteristics" class="display" style="width:100%"></table>
            </div>
        </div> 
        <div id="tabsComponents"> 
            <div style="width:99%;margin-left:0.5%; margin-top:25px;" >
                <table id="tableComponents" class="display" style="width:100%"></table>
            </div>
        </div>   
        <div id="tabsAlternative"> 
            <div style="width:99%;margin-left:0.5%; margin-top:25px;" >
                <table id="tableAlternative" class="display" style="width:100%"></table>
            </div>
            <div style="width:99%;margin-left:0.5%; margin-top:25px;" >
                <table id="tableSimilar" class="display" style="width:100%"></table>
            </div>
        </div>  
        <div id="tabsAttachments"> 
            <div style="width:99%;margin-left:0.5%; margin-top:25px;" >
                <table id="tableAttachments" class="display" style="width:100%"></table>
            </div>
        </div>     
   </div>
</div>
    <FORM id="main" name="main" method="post" action="catalogitem">
        <input type="hidden" name="selItemId" id="selItemId" value='<%= request.getAttribute("ITEM_ID") %>'>
    </FORM>
    <%@ include file="../../include/includeDataTableFiles_v3.jsp"%>
</BODY>
</HTML>
