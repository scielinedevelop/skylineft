<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8"  import="comply.SkyLine.bl.biz.User" %>
<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE><spring:message code="appTitle" text="" /> <spring:message code="appVersion" text="" /></TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<%@ include file="../../include/includeGeneralCSS_v3.jsp"%>            
    <%@ include file="../../include/includeGeneralJS_v3.jsp"%>
    
    <script src="skylineFtWebapp/structure/catalog/catalog_main.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>

    <%@ include file="../../include/includeFoundationJS.jsp"%> 

<style type="text/css">
    .cell-label {
        margin-right: 10px;
    }
    #sectionOne .cell-element {
        margin-right: 20px;
        width:150px;
    }
    #sectionTwo .cell-element {
        margin-right: 20px;
    }

</style>
</HEAD>
<BODY >
    <%
      
		String pageHeader = "<label>Catalog</label>"; 
        String pageTitle = "";
        request.setAttribute("NAVIGATION", "");
   %>
<div width="100%" align="center">
    <%@ include file="../../include/includeMainHeader.jsp" %>	
    <%@ include file="../../include/includePageHeader.jsp"%>
</div>
<div class="html-page-layout" style="width:99%;margin-left:0.5%;">
    <div class="section-parent static-section ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="section-header">
            <div style="width:30%;float:left;"><label>Search Item</label></div>
        </div>
        <div class="section-content">
            <div class="layout-row" style="margin-left: 35%;margin-bottom: 0;width: 50%;">
                <div class="layout-cell" style="width:50%;margin-right: 10px;">
                    <input type="text" class="show-placeholder" name="txtSearch" id="txtSearch" style="width:100%" placeholder="Name(Description), ID(Part Number)">
                </div>
                <div class="layout-cell">
                    <button type="button" id="btnSearch" class="table-primary-btn search-button" style="width: 110px;"><span>Search</span></button>
		    	</div>
            </div>
        </div>
    </div>
    <div class="section-parent static-section ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="section-content" style="padding-left: 20px;" id="sectionOne">
            <div class="layout-row" style="margin-top: 15px;">
                <div class="layout-cell cell-label field-label"><label>Company</label></div>
                <div class="layout-cell cell-element">
                    <select name="ddlCompany" id="ddlCompany" class="chosen_single" style="width:100%;"></select>  
               </div>
               <div class="layout-cell cell-label field-label"><label>Catalog Name</label></div>
                <div class="layout-cell cell-element">
                    <select name="ddlCatalogName" id="ddlCatalogName" class="chosen_single" style="width:100%;"></select>  
               </div>
               <div class="layout-cell cell-label field-label"><label>Item Category</label></div>
                <div class="layout-cell cell-element">
                    <select name="ddlItemCategory" id="ddlItemCategory" class="chosen_single" style="width:100%;"></select>  
               </div>
               <div class="layout-cell cell-label field-label"><label>Item Type</label></div>
                <div class="layout-cell cell-element">
                    <select name="ddlItemType" id="ddlItemType" class="chosen_single" style="width:100%;"></select> 
               </div>
            </div>
        </div>
    </div>
     <div class="section-parent static-section ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="section-header">
            <div style="width:30%;float:left;"><label>Filter</label></div>
        </div>
        <div class="section-content" style="padding-left: 10px;" id="sectionTwo">
            <div id="divParameterFilterSection" class="layout-row" style="margin-top: 15px;"></div>
            <div class="layout-row">
                <button type="button" id="btnNewFilter" class="table-secondary-btn new-button" style="width: 130px;"><span>Add New Filter</span></button>
            </div>
            <div class="layout-row">
                <hr>
            </div>
            <div class="layout-row center">
                <button type="button" id="btnApplyFilter" class="table-primary-btn" style="width: 110px;"><span>Apply Filter</span></button>
            </div>
        </div>
    </div>
    <div class="section-parent static-section ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="section-header">
            <div style="width:30%;float:left;"><label>Items List</label></div>
        </div>
        <div class="section-content" style="padding-left: 20px;">
            <div style="margin-top:10px;width:100%;min-width:1200px;float:left;" >
                <table id="itemsTable" class="display" style="width:100%"></table>
            </div>
        </div>
    </div>
</div>

    <FORM id="main" name="main" method="post" action="catalog">
        <input type="hidden" name="selItemId" id="selItemId" value='-1'>
    </FORM>
    
    <%@ include file="../../include/includeDataTableFiles_v3.jsp"%>
    <%@ include file="../../include/includeDataTableFixedColumns.jsp"%>
</BODY>
</HTML>
