<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE>&nbsp;</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<%@ include file="include/includeGeneralCSS_v3.jsp"%>            
    <%@ include file="include/includeGeneralJS_v3.jsp"%>
    <script src="js/comply_js/skyline.dynamic-fields.js" type="text/javascript"></script>

    <%@ include file="include/includeFoundationJS.jsp"%> 
    

<style type="text/css">
    .css-main-div {
        width:250px;
        margin-top: 20px;
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        position: relative;
        min-width: 0;
        padding: 0;
        /* margin: 0; */
        border: 0;
        vertical-align: top;
    }
    .css-sub-div {
        font-family: "Roboto","Helvetica","Arial",sans-serif;
        font-weight: 400;
        font-size: 1rem;
        line-height: 1.4375em;
        letter-spacing: 0.00938em;
        color: rgba(0, 0, 0, 0.87);
        box-sizing: border-box;
        position: relative;
        cursor: text;
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        position: relative;
        border-radius: 4px;
    }

    
    .css-label {
        color: rgba(0, 0, 0, 0.6);
        font-family: Roboto, Helvetica, Arial, sans-serif;
        font-weight: 500;
        font-size: 1rem;
        line-height: 1.4375em;
        letter-spacing: 0.00938em;
        padding: 0px;
        display: block;
        transform-origin: left top;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: calc(133% - 24px);
        position: absolute;
        left: 0px;
        top: 0px;
        transform: translate(14px, -9px) scale(0.75);
        /* transition: color 200ms cubic-bezier(0, 0, 0.2, 1) 0ms, transform 200ms cubic-bezier(0, 0, 0.2, 1) 0ms, max-width 200ms cubic-bezier(0, 0, 0.2, 1) 0ms; */
        z-index: 1;
        pointer-events: none;
    }
    input[type="text"].css-input {
        font: inherit;
        letter-spacing: inherit;
        color: currentColor;
        padding: 4px 0 5px;
        border: 0;
        box-sizing: content-box;
        background: none;
        height: 1.4375em;
        margin: 0;
        -webkit-tap-highlight-color: transparent;
        display: block;
        min-width: 0;
        width: 100%;
        -webkit-animation-name: mui-auto-fill-cancel;
        animation-name: mui-auto-fill-cancel;
        -webkit-animation-duration: 10ms;
        animation-duration: 10ms;
        padding: 16.5px 14px;
    }
    fieldset {
        text-align: left;
        position: absolute;
        bottom: 0;
        right: 0;
        top: -5px;
        left: 0;
        margin: 0;
        padding: 0 8px;
        pointer-events: none;
        border-radius: inherit;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        min-width: 0%;
        /* border-color: #1976d2; */
        border-color: rgba(0, 0, 0, 0.23);
    }
    fieldset:focus {
        border-color: #1976d2;
        border-width: 2px;
    }
    fieldset:hover {
        border-color: #000000de;
    }
    legend {
        display: block;
        width: auto;
        padding: 0px;
        height: 11px;
        font-size: 0.75em;
        visibility: hidden;
        max-width: 100%;
        transition: max-width 100ms cubic-bezier(0, 0, 0.2, 1) 50ms;
    }
    legend>span {
        padding-left: 5px;
        padding-right: 5px;
        display: inline-block;
    }
    .css-chosen .chosen-container {
        padding: 14px;
    }
    .css-chosen .chosen-container .chosen-single {
        border: none;
        box-shadow: none;
    }
    .css-chosen .chosen-container-single .chosen-drop {
        margin-top: -15px;
        width: 89%;
    }
    .css-chosen .chosen-container-single .chosen-search input[type="text"] {
        box-sizing: border-box;
    }

    .label-top {
        transform: none;
    }

</style>

<script type="text/javascript">
    
    var pageMainServletName = "";
    
    $(document).ready(function () 
    {
        initDynamicFields({});
    });

</SCRIPT>
</HEAD>
<BODY >

    

<div style="width:99%;margin-left:0.5%;">

    <div id="dynamicFieldsPlaceholder" style="margin-top: 50px"></div>
    
    <!-- <div class="css-main-div">
        <label class="css-label" data-shrink="true">Request Date</label>
        <div class="css-sub-div">
            <input class="css-input" aria-invalid="false" placeholder="mm/dd/yyyy" type="text" class="" value="10/12/2021">
            <div class="">
                <i class="fa fa-calendar" aria-hidden="true" style="margin-right: 10px;color: #0000008a;font-size: 16px;"></i>
            </div>
            <fieldset aria-hidden="true" class="">
                <legend class="">
                    <span>For desktop</span>
                </legend>
            </fieldset>
        </div>
    </div>
    <div class="css-main-div">
        <label class="css-label" data-shrink="true" for="outlined-required" id="outlined-required-label">My input field
            <span aria-hidden="true" class=""> *</span>
        </label>
        <div class="css-sub-div">
            <input class="css-input" aria-invalid="false" id="outlined-required" required="" type="text" class="" value="Hello World">
            <fieldset aria-hidden="true" class="">
                <legend class="">
                    <span>Required&nbsp;*</span>
                </legend>
            </fieldset>
        </div>
    </div>
    <div class="css-main-div">
        <label class="css-label" data-shrink="true" for="outlined-required" id="outlined-required-label">Selection list
        </label>
        <div class="css-sub-div">
            <select class="chosen_single css-chosen">
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
            </select>
            <fieldset aria-hidden="true" class="">
                <legend class="">
                    <span>Selection list</span>
                </legend>
            </fieldset>
        </div>
    </div> -->
    <!-- <div>
        <div class="css-main-div">
            <label class="css-label label-top" data-shrink="true" for="bootstrap-input">Field Name</label>
            <div class="css-sub-div" style="border: 1px solid #c3c3c3;margin-top: 22px;">
                <input class="css-input" aria-invalid="false" style="padding: 8px;" type="text" class="" value="">
            </div>
        </div>
    </div> -->
    
        
</div>

    <FORM id="main" name="main" method="post" action="">
        <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>
    </FORM>
</BODY>
</HTML>
