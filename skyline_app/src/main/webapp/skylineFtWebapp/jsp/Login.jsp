<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 

	<title>
		<spring:message code="appTitle" text="" /> <spring:message code="appVersion" text="" />
	</title>
	
	<link rel="icon" href="images/favicon.ico" />
	<link href="CSS/comply_theme/jquery-ui-1.13.0.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="CSS/comply_theme/login.css?<spring:message code='appVersion' text='' />" rel="stylesheet" type="text/css" media="all"/> 
	<link href="CSS/comply_theme/app_v2.css?<spring:message code='appVersion' text='' />" rel="stylesheet" type="text/css" media="all"/> 

	<script src="js/comply_js/jquery-3.6.0.min.js" type="text/javascript"></script> 
	<script src="js/comply_js/jquery-ui-1.13.0.min.js" type="text/javascript"></script>
	<script src="js/comply_js/CommonFuncs.v2.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>
	
<SCRIPT type="text/javascript">
	
	$(document).ready( function() 
	{
		$('#txtUser').focus();
		
		var msg = $('#txtSysMessage').val();
		
		if (msg && msg != "")
		{
			alert(msg);
		}
	});
	
	
    function OnEnterKeyDown()
    { 
        if (event.keyCode == 13)
        {
			Login();
			event.returnValue = false;
        }
    }
    
	function Login() 
	{ 
		if($('#txtUser').val() == '' || $('#txtPassword').val() == '') {
		 	displayAlertDialog("Please, enter user name or/and password!");
		 	return;
		}
		$('#actionid').val('login');
		$('#form1').submit();
	}
    
	</SCRIPT>
	
</head>
<BODY oncontextmenu="return false;" onkeydown="OnEnterKeyDown();" class="login_body">
	<div class="loginmodal-container">					
		<form name="form1" id="form1" method="post" action="login">
				 <div class="login_top_section">
					<img src="images/logo_scieline_svg.svg" border="0" >
					<br/>
					<label class="title"><spring:message code="appTitle" text="" /> <spring:message code="appVersion" text="" /></label>
				 </div>
				<input type="text" id="txtUser" name="txtUser" placeholder="User name" value="">
				<input type="password" id="txtPassword" name="txtPassword" placeholder="Password" value="">
				<input type="button" id="btnLogin" name="btnLogin" class="login loginmodal-submit" value="Login" onclick="Login();">
				
				<input type="hidden" name="actionid" id="actionid">
				<input type="hidden" id="txtSysMessage" name="txtSysMessage" value='${userMessage}' />
				<input type="hidden" id="txtIsChangePassword" name="txtIsChangePassword" value='${CHANGE_PASSWORD}'/>
				<input type="hidden" id="txtUserId" name="txtUserId" value='${USER_ID}'/>
				<input type="hidden" id="txtUserName" name="txtUserName" value='${USER_NAME}'/>
		</form>									  
	</div>
</BODY>
</HTML>
