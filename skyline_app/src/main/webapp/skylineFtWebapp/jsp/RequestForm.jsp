<%@ page contentType="text/html;charset=UTF-8"  import="comply.SkyLine.bl.biz.User" %>
<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE>&nbsp;</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<%@ include file="../../include/includeLoadingDiv.jsp"%>   
	<%@ include file="../../include/includeGeneralCSS_v3.jsp"%>            
    <%@ include file="../../include/includeGeneralJS_v3.jsp"%>
    <script src="skylineFtWebapp/structure/request/request_form.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>
    <%@ include file="../../include/includeFoundationJS.jsp"%> 

<style type="text/css">
    .static-section-content .cell-label {
        width:250px;
    }
    .static-section-content .cell-element {
        width:250px;
    }
    .note-editor {
        font-size: 12px;
    }
    #tabsItemInfo .cell-label {
        width:170px;
    }
    #tabsItemInfo .cell-element {
        width:250px;
    }
</style>

<script type="text/javascript">
    
    var labelsArr = {};
    var pageMainServletName = "requestform";
    
    $(document).ready(function () 
    {
        labelsArr = LoadLabelsJson();
        $("#tabs").tabs();
        $("button" ).button();
        $('.chosen_single').chosen({width:'100%'});
        $('#txtDescrRichtext').richtext({height:170});
        $('.prev-path-link').on('click', function() { back();});
    });

    function back() 
    {
        confirmExitPageWarning({onConfirmFunc:function()
                                {
                                    // $('#actionid').val('back');
                                    $('#main').attr('action', 'requestmainservlet');
                                    $('#main').submit();
                                }});
    }

</SCRIPT>
</HEAD>
<BODY >
    <%
        String pageTitle = "<div class='breadcrumb'><div class='content prev-path-link' dest='requestmainservlet' >Application List</div>"+
			"<div class='arrow prev-path beforeActive'></div>"+
			"<div class='content active prev-path'>Application Form</div><div class='arrow active'></div></div>";

        String pageHeader = "Development Application";
        request.setAttribute("NAVIGATION", "");
   %>
<div width="100%" align="center">
    <%@ include file="../../include/includeMainHeader.jsp" %>	
    <%@ include file="../../include/includePageHeader.jsp"%>
</div>
<div class="html-page-layout" style="width:99%;margin-left:0.5%;">
    <div class="static-section ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="static-section-header">
            <div style="width:30%;float:left;">
                <label>General Information</label>
            </div>
        </div>
        <div class="static-section-content" style="padding-left: 20px;">
            <div style="float: left; width:50%">
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label">
                        <label>Application Number</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtApplicationNumber" style="width:100%">
                    </div>
                </div> 
                <div class="layout-row" >
                    <div class="layout-cell cell-label field-label">
                        <label>Initiator Name</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtInitiatorName" style="width:100%">
                    </div>
                </div> 
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label">
                        <label>Initiator Role</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtInitiatorRole" style="width:100%">
                    </div>
                </div>  
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label">
                        <label>Additional Required Personal</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtAdditionalPerson" style="width:100%">
                    </div>
                </div> 
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label">
                        <label>Customer Name</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtCustomerName" style="width:100%">
                    </div>
                </div> 
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label">
                        <label>Customer Code</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtCustomerCode" style="width:100%">
                    </div>
                </div>
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label"><label>Country</label></div>
                    <div class="layout-cell cell-element">
                        <select name="ddlCountry" id="ddlCountry" class="chosen_single" style="width:100%;"></select>  
                    </div>
                </div> 
            </div>
            <div style="float: left; width:45%">
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label">
                        <label>Company Reference</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtCompanyReference" style="width:100%">
                    </div>
                </div>
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label">
                        <label>Technical Owner</label>
                    </div>
                    <div class="layout-cell cell-element">
                        <input type="text" id="txtTechnicalOwner" style="width:100%">
                    </div>
                </div> 
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label"><label>Company</label></div>
                    <div class="layout-cell cell-element">
                        <select name="ddlCompany" id="ddlCompany" class="chosen_single" style="width:100%;"></select>  
                </div>
                </div>
                <div class="layout-row">
                    <div class="layout-cell cell-label field-label"><label>Status</label></div>
                    <div class="layout-cell cell-element">
                        <span></span>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <div>
        <div id="" style="float: left;">
            <div>
                <input type="text" style="width:70%">
                <span class='app-icon copy-icon'></span>
                <span class='app-icon edit-icon'></span>
            </div>
            <div>
                <span id="btnAddItem" style="padding-right: 5px;" class='app-icon plus-icon' onclick="addItem()">Add Item</span>
            </div>
        </div>
        <div id="tabs" style="float: left;width: 80%;">
            <ul>
                <li><a href="#tabsItemInfo">Item Information</a></li>
            </ul>
            <div id="tabsItemInfo" style="display: table;">
                <div style="float: left; width:60%">
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label lang_key="Description"></label></div>
                        <div class="layout-cell cell-element" style="width: 70%;">
                            <div  id="txtDescrRichtext" ></div>
                        </div>
                    </div>
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label">
                            <label>New Product</label>
                        </div>
                        <div class="layout-cell cell-element">
                            <input type="radio" name="rdNewProduct" id="rdNewProductYes" value="1" ><label style="margin-left: 5px;margin-right: 15px;">Yes</label>
                        <input type="radio" name="rdNewProduct" id="rdNewProductNo" value="0"><label style="margin-left: 5px;">No</label>
                        </div>
                    </div>
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label">
                            <label>Based On Code</label>
                        </div>
                        <div class="layout-cell cell-element">
                            <input type="text" id="txtBasedOnCode" style="width:100%">
                        </div>
                    </div> 
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label">
                            <label>Based On Description</label>
                        </div>
                        <div class="layout-cell cell-element">
                            <input type="text" id="txtBasedOnDesc" style="width:100%">
                        </div>
                    </div> 
                </div>
                <div style="float: right; width:37%">
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Quantity</label></div>
                        <div class="layout-cell cell-element">
                            <input type="text" id="txtQuantity" style="width:100%">
                        </div>
                    </div>
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Priority</label></div>
                        <div class="layout-cell cell-element">
                            <select name="ddlPriority" id="ddlPriority" class="chosen_single" style="width:100%;"></select>  
                       </div>
                    </div> 
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Concept</label></div>
                        <div class="layout-cell cell-element">
                            <input type="text" id="txtConcept" style="width:100%">
                        </div>
                    </div>
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Package</label></div>
                        <div class="layout-cell cell-element">
                            <select name="ddlPackage" id="ddlPackage" class="chosen_single" style="width:100%;"></select>  
                       </div>
                    </div> 
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Shipment Type</label></div>
                        <div class="layout-cell cell-element">
                            <input type="text" id="txtShipmentType" style="width:100%">
                        </div>
                    </div>
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Comments</label></div>
                        <div class="layout-cell cell-element">
                            <input type="text" id="txtComments" style="width:100%">
                        </div>
                    </div>
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Target Price</label></div>
                        <div class="layout-cell cell-element">
                            <input type="text" id="txtTargetPrice" style="width:100%">
                        </div>
                    </div>
                    <div class="layout-row">
                        <div class="layout-cell cell-label field-label"><label>Regulatory Compliance</label></div>
                        <div class="layout-cell cell-element">
                            <select name="ddlCompliance" id="ddlCompliance" class="chosen_single" style="width:100%;"></select>  
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- <FORM id="main" name="main" method="post" action="requestform"></FORM> -->
</BODY>
</HTML>
