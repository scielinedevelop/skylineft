<%@ page contentType="text/html;charset=UTF-8"  import="comply.SkyLine.bl.biz.User" %>
<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE>&nbsp;</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<%@ include file="include/includeGeneralCSS_v2.jsp"%>            
    <%@ include file="include/includeGeneralJS_v2.jsp"%>
    <!-- <script src="js/comply_js/jquery.ui.swappable.js" type="text/javascript"></script> -->
    
    
    <%@ include file="include/includeFoundationJS.jsp"%> 

<style type="text/css">
    .css-main-div {
        width:250px;
        margin-top: 20px;
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        position: relative;
        min-width: 0;
        padding: 0;
        /* margin: 0; */
        border: 0;
        vertical-align: top;
    }
    .css-sub-div {
        font-family: "Roboto","Helvetica","Arial",sans-serif;
        font-weight: 400;
        font-size: 1rem;
        line-height: 1.4375em;
        letter-spacing: 0.00938em;
        color: rgba(0, 0, 0, 0.87);
        box-sizing: border-box;
        position: relative;
        cursor: text;
        display: -webkit-inline-box;
        display: -webkit-inline-flex;
        display: -ms-inline-flexbox;
        display: inline-flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        position: relative;
        border-radius: 4px;
    }

    
    .css-label {
        color: rgba(0, 0, 0, 0.6);
        font-family: Roboto, Helvetica, Arial, sans-serif;
        font-weight: 500;
        font-size: 1rem;
        line-height: 1.4375em;
        letter-spacing: 0.00938em;
        padding: 0px;
        display: block;
        transform-origin: left top;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: calc(133% - 24px);
        position: absolute;
        left: 0px;
        top: 0px;
        transform: translate(14px, -9px) scale(0.75);
        /* transition: color 200ms cubic-bezier(0, 0, 0.2, 1) 0ms, transform 200ms cubic-bezier(0, 0, 0.2, 1) 0ms, max-width 200ms cubic-bezier(0, 0, 0.2, 1) 0ms; */
        z-index: 1;
        pointer-events: none;
    }
    input[type="text"].css-input {
        font: inherit;
        letter-spacing: inherit;
        color: currentColor;
        padding: 4px 0 5px;
        border: 0;
        box-sizing: content-box;
        background: none;
        height: 1.4375em;
        margin: 0;
        -webkit-tap-highlight-color: transparent;
        display: block;
        min-width: 0;
        width: 100%;
        -webkit-animation-name: mui-auto-fill-cancel;
        animation-name: mui-auto-fill-cancel;
        -webkit-animation-duration: 10ms;
        animation-duration: 10ms;
        padding: 16.5px 14px;
    }
    fieldset {
        text-align: left;
        position: absolute;
        bottom: 0;
        right: 0;
        top: -5px;
        left: 0;
        margin: 0;
        padding: 0 8px;
        pointer-events: none;
        border-radius: inherit;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        min-width: 0%;
        /* border-color: #1976d2; */
        border-color: rgba(0, 0, 0, 0.23);
    }
    fieldset:focus {
        border-color: #1976d2;
        border-width: 2px;
    }
    fieldset:hover {
        border-color: #000000de;
    }
    legend {
        display: block;
        width: auto;
        padding: 0px;
        height: 11px;
        font-size: 0.75em;
        visibility: hidden;
        max-width: 100%;
        transition: max-width 100ms cubic-bezier(0, 0, 0.2, 1) 50ms;
    }
    legend>span {
        padding-left: 5px;
        padding-right: 5px;
        display: inline-block;
    }
    .css-chosen .chosen-container {
        padding: 14px;
    }
    .css-chosen .chosen-container .chosen-single {
        border: none;
        box-shadow: none;
    }
    .css-chosen .chosen-container-single .chosen-drop {
        margin-top: -15px;
        width: 89%;
    }
    .css-chosen .chosen-container-single .chosen-search input[type="text"] {
        box-sizing: border-box;
    }

    .label-top {
        transform: none;
    }

    #formAddFields .cell-label {
        width:20%;
    }
    #formAddFields .cell-element {
        width:70%;
    }

    .ui-dialog-buttonset {
        width: 100%; 
        text-align: center
    }

    #sortableDynamicFields { list-style-type: none; margin: 0; padding: 0; width: 90%; }
    #sortableDynamicFields li { margin: 0 3px 3px 3px; padding: 0.4em; font-size: 1.2em }
    #sortableDynamicFields li div.div-icons {margin-right: 20px;}
    #sortableDynamicFields li div.div-icons .app-icon {margin-left: 10px;}

    #sortableDynamicLayout .cell-label{width:10%}
    #sortableDynamicLayout .cell-element{width:15%}

    #sortableDynamicLayout .div-icons {margin: 20px 25px 20px 10px;float: left;}

    .col-1 {width: 8.33%;}
    .col-2 {width: 16.66%;}
    .col-3 {width: 25%;}
    .col-4 {width: 33.33%;}
    .col-5 {width: 41.66%;}
    .col-6 {width: 50%;}
    .col-7 {width: 58.33%;}
    .col-8 {width: 66.66%;}
    .col-9 {width: 75%;}
    .col-10 {width: 83.33%;}
    .col-11 {width: 91.66%;}
    .col-12 {width: 100%;}

    * {
    box-sizing: border-box;
    }
    .row {
    border-bottom-style: dashed;
    border-bottom-width: 0.5px;
    border-bottom-color: grey;
    }
    .row::after {
    content: "";
    clear: both;
    display: table;
    }

    [class*="col-"] {
        float: left;
        height: 60px;
    }
    .first-col {
        border-right-style: dashed;
        border-right-width: 0.5px;
        border-right-color: grey;
    }

    #btnAddOption.plus-icon:before {
        margin-right: 10px;
    }
    .sortable-handler {
        float: left;width: 90%;margin-top: 18px;
    }
</style>

<script type="text/javascript">
    
    var labelsArr = {};
    var pageMainServletName = "requestservlet";
    
    $(document).ready(function () 
    {
        $("button" ).button();
        $('.chosen_single').chosen({width:'100%'});
        $( "#sortableDynamicFields" ).sortable();

        // $("#sortableDynamicLayout").swappable({
        //     items:'.sortable-handler', // Mandatory option, class only.
        //     cursorAt: {top:-20}, // MUST be set to negative. Default doesn't work!
        // });
        $("#sortableDynamicLayout").disableSelection();

        $('#sortableDynamicOptions').sortable();

        $('#sortableDynamicLayout').find('.plus-icon').on('click', function(e){
            manageFields($(this).closest('.col-6').attr('id'));
        });

        (function() {
            addListener();
        } ());
    });

    function addListener()
    {
        var droppableParent;
            
            $('.sortable-handler').off().draggable({
                revert: 'invalid',
                revertDuration: 200,
                start: function () {
                    droppableParent = $(this).parent();
                
                    $(this).addClass('being-dragged');
                },
                stop: function () {
                    $(this).removeClass('being-dragged');
                }
            });
            
            $('.col-6').off().droppable({
                hoverClass: 'drop-hover',
                drop: function (event, ui) {
                    var draggable = $(ui.draggable[0]),
                        draggableOffset = draggable.offset(),
                        container = $(event.target),
                        containerOffset = container.offset();
                    
                    $('.sortable-handler', event.target).appendTo(droppableParent).css({opacity: 0}).animate({opacity: 1}, 200);
                    
                    draggable.appendTo(container).css({left: draggableOffset.left - containerOffset.left, top: draggableOffset.top - containerOffset.top}).animate({left: 0, top: 0}, 200);
                }
            });
    }

    function manageFields(elem)
    {
        // var $dialogElem = createModalDialog({dialogId:"dialogManageDynamicFields", title:labelsArr["Manage fields"], 
        //                     width: 550, height:450,
        //                     buttons:{
        //                                 "Add Field": function() {
        //                                     toggleDialogForms();
        //                                 }
        //                             }
        //                 });	
		// 	$dialogElem.dialog('open');
        
        var id = elem;
        var $dialogElem = createModalDialog({dialogId:"dialogManageDynamicFields", isDestroyDialog:true, title:labelsArr["Add new field"], 
                            width: 550, height:450,
                            buttons:{
                                        "Save": function() {
                                            appendField(id)
                                        },
                                        Cancel: function() {
                                            $( this ).dialog( "close" );
                                        }
                                    }
                        });	
			$dialogElem.dialog('open');
    }

    // function toggleDialogForms()
    // {
    //     var form1 = ($('#formManageFields').css('display') == 'block')?true:false;
        
    //     $('#formManageFields').css('display',form1?'none':'block');
    //     $('#formAddFields').css('display',!form1?'none':'block');
    //     $( "#dialogManageDynamicFields" ).dialog( "option", "buttons",form1?
    //                                     {
    //                                         "Save": function() {saveFields()},
    //                                         Cancel: function() {
    //                                             toggleDialogForms();
    //                                         }
    //                                     }
    //                                     :
    //                                     {
    //                                         "Add Field": function() {
    //                                             toggleDialogForms();
    //                                         }
    //                                     }
    //                                     );
        
    // }

    // function saveFields()
    // {
        
    //     var form = $('#formAddFields');
    //     var obj = {
    //         fieldLabel:form.find('#txtFieldName').val().trim(),
    //         fieldTypeId:form.find('#ddlFieldType option:selected').val(),
    //         fieldType:form.find('#ddlFieldType option:selected').text(),
    //         fieldRequired:form.find('#chbFieldRequired').checkbox('val')
    //     }
    //     toggleDialogForms();

    //     var html = '<li class="ui-state-default">'
    //                 +'<div class="div-icons" style="position:relative;float: left;width: 50px;"><span class="app-icon remove-icon"></span><span class="app-icon edit-icon"></span></div>'
    //                 +'<label class="field-label">'+obj.fieldLabel+'</label>'
    //                 +'<span style="float: right;margin-right: 20px;">'+obj.fieldType+'</span>'
    //                 +'</li>';
    //     $('#sortableDynamicFields').append(html);
        
    //     switch(obj.fieldTypeId) {
    //         case 'text':createTextField(obj);
    //         break;
    //     }
    // }

    // function createTextField(propO)
    // {
    //     var html = '<div class="layout-row">'
    //                 +'<div class="layout-cell cell-label field-label"><label>'+propO.fieldLabel+'</label></div>'
    //                 +'<div class="layout-cell cell-element">'
    //                 +'  <input type="text" style="width:100%">'
    //                 +'</div>'
    //                 +'</div>';
    //     $('#sortableDynamicLayout').append(html);
    // }

    function appendField(id)
    {
        
        var form = $('#formAddFields');
        var obj = {
            fieldLabel:form.find('#txtFieldName').val().trim(),
            fieldTypeId:form.find('#ddlFieldType option:selected').val(),
            fieldType:form.find('#ddlFieldType option:selected').text(),
            fieldRequired:form.find('#chbFieldRequired').checkbox('val')
        }
        
        var htmlElem = getFieldHtmlByType(obj);
        $('#'+id).append(htmlElem);
        $('.chosen_single').chosen({width:'100%'});
        addListener();
        $("#dialogManageDynamicFields").dialog('close');
    }

    function getFieldHtmlByType(obj)
    {
        var html = '';
        if(obj.fieldTypeId == 'text')
        {
            html = '<div class="sortable-handler">'
                    +'<div style="float: left;width: 20%;" class="field-label"><label>'+obj.fieldLabel+'</label></div>'
                    +'<div style="float: left;width: 50%;">'
                    +'  <input type="text" style="width:100%">'
                    +'</div>'
                    +'</div>';
        }
        else if(obj.fieldTypeId == 'checkbox')
        {
            html = '<div class="sortable-handler">'
                    +'<div style="float: left;width: 20%;" class="field-label"><label>'+obj.fieldLabel+'</label></div>'
                    +'<div style="float: left;width: 50%;">'
                    +'  <span class="app-icon checkbox-icon" onclick="checkboxToggleCheck(this);"></span>'
                    +'</div>'
                    +'</div>';
        }
        else if(obj.fieldTypeId == 'dropdown')
        {
            html = '<div class="sortable-handler">'
                    +'<div style="float: left;width: 20%;" class="field-label"><label>'+obj.fieldLabel+'</label></div>'
                    +'<div style="float: left;width: 50%;">'
                    +'  <select class="chosen_single"></select>'
                    +'</div>'
                    +'</div>';
        }
        return html;
    }

    function onFieldTypeChange(type)
    {
        if(type == 'dropdown')
        {
            $('#divDropdownOptions').css('display','');
        }
        else {
            $('#divDropdownOptions').css('display','none');
        }
    }

    function addOption()
    {
        var html = '<div style="float: left;width: 70%;margin-bottom: 10px;">'
                    +'  <span class="app-icon fa fa-arrows" style="font-size: 16px;"></span>'
                    +'  <input type="text" style="width:75%">'
                    +'  <span class="app-icon remove-icon" style="font-size: 19px;"></span>'
                    +'</div>';
        $('#sortableDynamicOptions').append(html);
    }

</SCRIPT>
</HEAD>
<BODY >
    <%
      
		String pageHeader = "<label lang_key='Request' ></label>"; 
        String pageTitle = "";
        request.setAttribute("NAVIGATION", "");
   %>
<div width="100%" align="center">
        <%@ include file="include/includeMainHeader.jsp" %>	
        <%@ include file="include/includePageHeader.jsp"%>
</div>
<div style="width:99%;margin-left:0.5%;">
    <!-- <div style="float: right;width: 200px;position: absolute;right: 0;">
        <button type="button" class="table-primary-btn" onclick="manageFields()" style="width: 120px;"><span>Manage Fields</span></button>
    </div> -->
    <div id="sortableDynamicLayout" class="dynamic-page-layout" style="width:99%;margin-left:0.5%;float: left;margin-top: 50px">
        <div class="row" style="border-top: 0.5px dashed grey;">
            <div id="col1" class="col-6 first-col">
                <div class="div-icons" >
                    <span class="app-icon plus-icon"></span>
                </div>
                <!-- <div class="sortable-handler" style="float: left;width: 90%;">
                    <div style="width: 80%;margin-top: 12px;"><div style="float: left;width: 20%;" class="field-label"><label>Test</label></div><div style="float: left;width: 50%;">  <input type="text" style="width:100%"></div></div>
                </div> -->
            </div>
            <div id="col2" class="col-6">
                <div  class="div-icons">
                    <span class="app-icon plus-icon"></span>
                </div>
                <!-- <div class="sortable-handler" style="float: left;width: 90%;">
                    <div style="width: 80%;margin-top: 12px;"><div style="float: left;width: 20%;" class="field-label"><label>Test 2</label></div><div style="float: left;width: 50%;">  <input type="text" style="width:100%"></div></div>
                </div> -->
            </div>
        </div>
        <div class="row">
            <div id="col3" class="col-6 first-col">
                <div  class="div-icons">
                    <span class="app-icon plus-icon"></span>
                </div>
            </div>
            <div id="col4" class="col-6">
                <div  class="div-icons">
                    <span class="app-icon plus-icon"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="col5" class="col-6 first-col">
                <div  class="div-icons">
                    <span class="app-icon plus-icon"></span>
                </div>
            </div>
            <div id="col6" class="col-6">
                <div  class="div-icons">
                    <span class="app-icon plus-icon"></span>
                </div>
            </div>
        </div>
    </div>
    

    <div id="dialogManageDynamicFields" style="display:none;padding:20px 20px 20px 30px;">
        <div id="formManageFields" style="display: none;">
            <!-- <div id="sortableDynamicFields"></div> -->
            <ul id="sortableDynamicFields"></ul>
        </div>
        
        <div id="formAddFields"  class="html-popup-layout">
            <div class="layout-row">
                <div class="layout-cell cell-label field-label"><label>Name</label></div>
                <div class="layout-cell cell-element"  >
                    <input type="text" id="txtFieldName" style="width:100%">
                </div>
            </div>
            <div class="layout-row">
                <div class="layout-cell cell-label field-label"><label>Field Type</label></div>
                <div class="layout-cell cell-element"  style="width:200px">
                    <select id="ddlFieldType" class="chosen_single" onchange="onFieldTypeChange(this.value)">
                        <option value="text">Text</option>
                        <option value="number">Number</option>
                        <option value="richtext">Richtext</option>
                        <option value="dropdown">Dropdown</option>
                        <option value="radio">Radio</option>
                        <option value="checkbox">Checkbox</option>
                        <option value="date">Date</option>
                    </select>
                </div>
            </div>
            <div class="layout-row">
                <div class="layout-cell cell-label field-label"><label>Required</label></div>
                <div class="layout-cell cell-element">
                    <span id="chbFieldRequired" class='app-icon checkbox-icon' onclick="checkboxToggleCheck(this);"></span>
                </div>
            </div>
            <div class="layout-row" style="display: none;">
                <div class="layout-cell cell-label field-label"><label>Range</label></div>
                <div class="layout-cell cell-element"  >
                    <span id="chbFieldDateRange" class='app-icon checkbox-icon' onclick="checkboxToggleCheck(this);"></span>
                </div>
            </div>
            <div id="divDropdownOptions" class="layout-row" style="display: none;">
                <div class="layout-cell cell-label field-label"><label>Options</label></div>
                <div class="layout-cell cell-element"  >
                    <div id="sortableDynamicOptions" style="float: left;width: 100%;"></div>
                    <div><span id="btnAddOption" style="padding-right: 5px;" class='app-icon plus-icon' onclick="addOption()">Add Option</span></div>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- <div class="css-main-div">
        <label class="css-label" data-shrink="true">Request Date</label>
        <div class="css-sub-div">
            <input class="css-input" aria-invalid="false" placeholder="mm/dd/yyyy" type="text" class="" value="10/12/2021">
            <div class="">
                <i class="fa fa-calendar" aria-hidden="true" style="margin-right: 10px;color: #0000008a;font-size: 16px;"></i>
            </div>
            <fieldset aria-hidden="true" class="">
                <legend class="">
                    <span>For desktop</span>
                </legend>
            </fieldset>
        </div>
    </div>
    <div class="css-main-div">
        <label class="css-label" data-shrink="true" for="outlined-required" id="outlined-required-label">My input field
            <span aria-hidden="true" class=""> *</span>
        </label>
        <div class="css-sub-div">
            <input class="css-input" aria-invalid="false" id="outlined-required" required="" type="text" class="" value="Hello World">
            <fieldset aria-hidden="true" class="">
                <legend class="">
                    <span>Required&nbsp;*</span>
                </legend>
            </fieldset>
        </div>
    </div>
    <div class="css-main-div">
        <label class="css-label" data-shrink="true" for="outlined-required" id="outlined-required-label">Selection list
        </label>
        <div class="css-sub-div">
            <select class="chosen_single css-chosen">
                <option>A</option>
                <option>B</option>
                <option>C</option>
                <option>D</option>
            </select>
            <fieldset aria-hidden="true" class="">
                <legend class="">
                    <span>Selection list</span>
                </legend>
            </fieldset>
        </div>
    </div> -->
    <!-- <div>
        <div class="css-main-div">
            <label class="css-label label-top" data-shrink="true" for="bootstrap-input">Field Name</label>
            <div class="css-sub-div" style="border: 1px solid #c3c3c3;margin-top: 22px;">
                <input class="css-input" aria-invalid="false" style="padding: 8px;" type="text" class="" value="">
            </div>
        </div>
    </div> -->
    
        
</div>

    <FORM id="main" name="main" method="post" action="requestservlet">
        <input type="hidden" name="actionid" id="actionid" value='<%= request.getAttribute("ACTIONID") %>'>
    </FORM>
</BODY>
</HTML>
