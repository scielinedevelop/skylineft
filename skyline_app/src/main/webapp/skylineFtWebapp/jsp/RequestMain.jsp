<%@ page contentType="text/html;charset=UTF-8"  import="comply.SkyLine.bl.biz.User" %>
<!DOCTYPE html>
<HTML>
<HEAD>
<TITLE>&nbsp;</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<%@ include file="../../include/includeLoadingDiv.jsp"%>   
	<%@ include file="../../include/includeGeneralCSS_v3.jsp"%>            
    <%@ include file="../../include/includeGeneralJS_v3.jsp"%>
    <script src="skylineFtWebapp/structure/request/request_main.js?<spring:message code='appVersion' text='' />" type="text/javascript"></script>
    <%@ include file="../../include/includeFoundationJS.jsp"%> 
    
    <!-- <script type="text/javascript">
    
    var dtTable = null;
	var labelsArr = {};
    
    $(document).ready(function () 
	{
			$("button" ).button();
			labelsArr = LoadLabelsJson(); 
			var tableAttr = [{"pageLength":10,dom:'lftip', "exportButtons":false}
							,{"title":"row_id","visible":false,"searchable":false, "removable":false}
							,{"title":"Application Number"}
							,{"title":"Initiator Name", "orderable":false} 
							,{"title":"Initiator Role"} 
							,{"title":"Customer Name"} 
							,{"title":"Customer Code"} 
							,{"title":"Company"} 
							,{"title":"Country"} 
                            ,{"title":"Technical Owner"} 
							,{"title":"Status"}
                            ,{"title":"", "searchable":false, "orderable":false, "removable":false}
						];

			var additOptions = {"createdRow":onCreatedRow,  "onRowClick":onRowClicked, "onRowDblClick":onRowDblClicked};		
			dtTable = drawTable("requestsTable",tableAttr,[],additOptions,labelsArr);
			getTableData();
			$('#requestsTable_wrapper').prepend($('#divNewButton'));
     });

	function onRowClicked(currRowData, tableId, isRowSelected, event)    /* where aData - clicked row data */
	{    
		$('#currRequestId').val(currRowData[0]);
		var act = fnGetTableRowIconActionNameByEvent(event);
		if(act)
		{ 
			if(!isRowSelected)$(event.currentTarget).addClass('selected'); 
			switch(act) {
				case "edit": editRequest();
					break;
				default: "";
			}
			
		}		
	}

	function onRowDblClicked(currRowData, tableID)
    {
		$('#currRequestId').val(currRowData[0]); 
		editRequest();
    }

	function onCreatedRow(nRow, aData, iDataIndex, currTableID)
	{
		var rowIcons = [
					{iconType:"edit", tooltip:"Update"}
				];  
        $("td:eq(10)", nRow).addClass("table-column-with-icons").html(fnCreateTableRowActionIcons(rowIcons));
	}

    function getTableData()
    {
    	sendAjaxRequest({servletName:"requestmainservlet",urlParams:"actionid=getTableData",dataType: "json",
						onSuccess:function(data) {
							if(data != "-1") {
								dt_ext_updateTableData(dtTable, data);			 	
							}
						}
		});
    }
    
    function buildNew()
    {
        $('#currRequestId').val(-1);
		$('#actionid').val('');
		$('#main').attr("action","requestformservlet");
        $('#main').submit();		
    }

	function editRequest()
    {
		$('#actionid').val('');
        $('#main').attr("action","requestformservlet");
        $('#main').submit();		
    }

</SCRIPT> -->
</HEAD>
<BODY >
<%     
        String pageTitle = "<div class='breadcrumb'><div class='content active'>Application List</div><div class='arrow active'></div></div>";
        String pageHeader = "Development Application";
        request.setAttribute("NAVIGATION", "");
       
   %>
<div width="100%" align="center">
	<%@ include file="../../include/includeMainHeader.jsp" %>	
	<%@ include file="../../include/includePageHeader.jsp"%>
</div>

<div style="width:99%;margin-left:0.5%;"> 
<div class="html-page-layout">
    <div class="layout-row">
    	<div class="layout-row" style="margin-top:10px;">
					<!-- <div id="divNewButton" style="float: left;position: absolute;left:200px">	
						<div style="float:left;margin-left: 20px;">
						<button type="button" id="btnNew"  onclick="buildNew()" class="table-primary-btn new-button"><span lang_key="New Application"></span></button>
						</div>
					</div> -->
					<div style="width:99%;margin-left:0.5%; margin-top:25px;margin-bottom:25px;" >
	    				<table id="requestsTable" class="display" style="width:100%"></table>
					</div>
	    	</div>
    </div>

</div>

</div>
    <FORM id="main" name="main" method="post" action="request">
        <input type="hidden" name="currRequestId" id="currRequestId" value='-1'>
    </FORM>
	<%@ include file="../../include/includeDataTableFiles_v3.jsp"%>   
</BODY>
