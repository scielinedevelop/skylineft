 var pageMainServletName = "catalog";
 var parameterTypeDataListHolder = [];
    
 $(document).ready(function () 
 {
     $("button" ).button();
     $('.chosen_single').chosen();
     addNewFilter();
     
     getCompanyList();
     getItemCategoryList();
     getItemTypeList();
    
     $("#ddlCompany").on('change',function(){getCatalogNameList(this.value)});
     $('#btnSearch').on('click',function(){searchItem()});
     $('#btnNewFilter').on('click',function(){addNewFilter()});
     $('#btnApplyFilter').on('click',function(){applyFilter()});
  
     initWaitMessageDiv();
 });

 function addNewFilter()
 {
     var html = '<div class="layout-row parameter-row-filter">\
                    <div class="layout-cell cell-element" style="width: 250px;">\
                        <select class="chosen_single parameter-type-filter" data-placeholder="Choose a Parameter Type" style="width:100%;"></select> \
                    </div>\
                    <div class="layout-cell cell-element" style="width: 250px;">\
                        <select class="chosen_single parameter-filter" data-placeholder="Choose a Parameter" style="width:100%;"></select>  \
                    </div>\
                    <div class="layout-cell cell-element" style="width: 60px;">\
                        <select class="chosen_single parameter-sign-filter" data-placeholder="=" style="width:100%;"></select> \
                    </div>\
                    <div class="layout-cell cell-element" style="width: 150px;">\
                        <input type="text" class="parameter-value-filter" style="width:100%">\
                    </div>\
                    <div class="layout-cell cell-element">\
                        <span class="app-icon remove-icon remove-parameter-filter" title="Remove filter" style="font-size: 20px;line-height: 25px;"></span>\
                    </div>\
                </div>';
    var $html = $(html);
    $('#divParameterFilterSection').append($html);
    var $paramTypeElem = $html.find('.parameter-type-filter');
    if(parameterTypeDataListHolder.length == 0)
    {
        getParameterTypeList($paramTypeElem);
    }
    else {
        customSelectPopulate($paramTypeElem, parameterTypeDataListHolder, [], {useSingleDefaultOption:true});
    }
    $html.find('.parameter-sign-filter').html(getSignOptions());
    $html.find('.chosen_single').chosen();

    $paramTypeElem.on('change', function(){ getParameterList(this.value, $(this).closest('div.parameter-row-filter').find('.parameter-filter'))});
    $html.find('.remove-parameter-filter').on('click', function(){$(this).closest('div.parameter-row-filter').remove();});
 }

 function getParameterTypeList($paramTypeElem)
 {
    sendAjaxRequest({servletName:pageMainServletName+"/getParameterTypes",dataType:"json",
                            onSuccess:function(returnedList) {
                                customSelectPopulate($paramTypeElem, returnedList, [], {useSingleDefaultOption:true});
                                parameterTypeDataListHolder = returnedList;
                            }
                    });
 }

 function getParameterList(parentId, $paramElem)
 {
     populateByAjax($paramElem,"getParameters","parameterTypeId="+parentId);
 }

 function getCompanyList()
 {
     populateByAjax($("#ddlCompany"),"getCompanies");
 }

 function getCatalogNameList(parentId)
 {
     populateByAjax($("#ddlCatalogName"),"getCatalogNames","companyId="+parentId);
 }

 function getItemCategoryList()
 {
     populateByAjax($("#ddlItemCategory"),"getItemCategories");
 }

 function getItemTypeList()
 {
     populateByAjax($("#ddlItemType"),"getItemTypes");
 }

 function populateByAjax($ddl, action, urlParameters)
 {
     ajax_populateSelectByJsonList({element:$ddl,servletName: pageMainServletName+"/"+action, urlParams:urlParameters,selectedVal:-1,selectOptions:{useSingleDefaultOption:true}});
 }

 function searchItem()
 {
     var filter1 = $('#txtSearch').val();
     var obj = {
         item_number:filter1
     }
     sendAjaxRequest({servletName: pageMainServletName+"/getItemsTable", urlParams:"filter="+JSON.stringify(obj),dataType:"json",
                 onSuccess:function(dataObj) {
                     initItemsTable(dataObj);
                 }       
     });
 }

 function initItemsTable(fullData)
 {
     var $currTableElem = $('#itemsTable');
     // destroy table if exist
     dt_ext_destroyTable($currTableElem);

     // init table
     var tableConfigArr = [{"scrollY": 300,"scrollX": true,"scrollCollapse": true,"fixedColumns": {leftColumns: 3},"paging": false,"destroy": true, "dom":"Bfrti"}];
     var header = fullData.header;
     for (var i=0; i < header.length; i++ ) 
     {
        var isVisible = (i>0)?true:false;
        var isRemovable = (i>0)?true:false;
        var isSearchable = (i>0)?true:false;
        tableConfigArr.push({"title":header[i],"visible":isVisible,"searchable":isSearchable, "removable":isRemovable});
     }
     drawTable("itemsTable",tableConfigArr,fullData.data,{"onRowDblClick":onTableRowDblClick});
 }

 function onTableRowDblClick(currRowData, tableID)
 {
    $('#selItemId').val(currRowData[0]);
    $('#main').attr('action','catalogitem');
    $('#main').submit();
 }