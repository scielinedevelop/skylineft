var pageMainServletName = "catalogitem";
    
$(document).ready(function () 
{
    $("#tabs").tabs({
        activate: function (event, ui) {
            var tableElem = $(ui.newPanel[0]).find('table');
            for(var i=0;i<tableElem.length;i++)
            {
                var tableId = $(tableElem[i]).attr('id');
                getItemTabsDataTable(tableId,true);
            }
        }
    });
    $("button" ).button();
    $('.chosen_single').chosen({width:'100%'});
    // sendAjaxRequest({servletName: pageMainServletName, urlParams:"actionid=getItemData&itemId="+$('#selItemId').val(),dataType:"json",
    //             onSuccess:function(dataObj) {
    //                 populateItemWithData(dataObj);
    //             }       
    // });

    initFloatingButtonPanel();
    
});

function populateItemWithData(data)
{
    $('#txtIDNumber').val(data.itemNumber);
    $('#txtItemName').val(data.itemDescription);
    // $('#ddlItemType').val(data.itemTypeId);
    // $('#ddlItemCategory').val(data.itemCategoryId);
}

function getItemCategoryList()
{
    populateByAjax($("#ddlItemCategory"),"actionid=getItemCategory");
}

function getItemTypeList()
{
    populateByAjax($("#ddlItemType"),"actionid=getItemType");
}

function populateByAjax($ddl,url)
{
    ajax_populateSelectByJsonList({element:$ddl,servletName: pageMainServletName, urlParams:url,selectedVal:-1,selectOptions:{useSingleDefaultOption:true}});
}

function pageGlobalSave()
{
    var activeTab = getActiveTabId();
    if(activeTab == "aTabsGeneral")
    {
        saveFieldsData();
    }
    else {
        saveTableData(activeTab);
    }
}

function saveTableData(tabId)
{
    var $tableElem = $($('#'+tabId).attr('href')).find('.editable-table');

    $tableElem.each(function(inx,el) {
        var tableId = $(el).attr('id');
        var dataFromTable = dt_ext_getDataFromEditableTable(tableId);
        //save to DB
    });
    
}

function saveFieldsData()
{

}

function onRowActionBtnClick(actionName, dtTable, currRowData, event)
{
    switch(actionName) {
        case "remove": onDeleteTableRow(dtTable, currRowData);
            break;
        default: "";
    }
}

function onDeleteTableRow(dtTable, rowData)
{
    var rowId = rowData[0];
    // var currTableId = dtTable.context[0].sTableId;
    // if(currTableId == 'tableCharacteristics') {}

    dt_ext_deleteTableRowById(dtTable, rowId);
}

function getItemTabsDataTable(currTableId, isCreate)
{
    var dtTable = null;
    var $currTableElem = $('#'+currTableId);
    var createTableIfNotExists = isCreate;
    var isDataTableExists = dt_ext_isDataTable($currTableElem[0]); 

    if(isDataTableExists)
    {
        dtTable = $currTableElem.DataTable();
    }
    else if(createTableIfNotExists)
    {
        var tableAttr = [];
        var additOptions = {"onRowActionBtnClick":onRowActionBtnClick};
        var defaultCustomButtons = [{text:"Add Row",action:"addrow",afterAction:function(){console.log("row added")}}];
        if(currTableId == 'tableCharacteristics')
        {
            tableAttr = [{"editable":true, customButtons:defaultCustomButtons}
                        ,{"title":"row_id","visible":false,"searchable":false, "removable":false}
                        ,{"title":"hidden_object","visible":false,"searchable":false, "removable":false}
                        ,{"title":"Parameter Code (Number)", "columnHtmlType":"select", "orderDataType":"dom-select", "columnId":"parameter_code"}
                        ,{"title":"Parameter Name"}
                        ,{"title":"Specification (avg)", "columnHtmlType":"text", "orderDataType": "dom-text", type: 'string', "columnId":"specification_avg"}
                        ,{"title":"Specification (a)", "columnHtmlType":"text", "orderDataType": "dom-text", type: 'string', "columnId":"specification_a"}
                        ,{"title":"Uom", "columnHtmlType":"select", "orderDataType":"dom-select", "columnId":"parameter_uom"}    
                        ,{"title":"", "searchable":false, "orderable":false, "removable":false, "filterType":"none", "rowActionButtons":[{iconType:"remove", tooltip:"Delete Row"}], width:'100px'} 
                        ];
        }
        else if(currTableId == 'tableComponents')
        {
            tableAttr = [{}
                        ,{"title":"row_id","visible":false,"searchable":false}
                        ,{"title":"hidden_object","visible":false,"searchable":false}
                        ,{"title":"ID"}
                        ,{"title":"Start Date"}
                        ,{"title":"Quantity"}
                        ,{"title":"Uom"}    
                        ,{"title":"Reference"}
                        ,{"title":"Sequence"} 
                        ];
        }
        else if(currTableId == 'tableAlternative')
        {
            tableAttr = [{}
                        ,{"title":"row_id","visible":false,"searchable":false}
                        ,{"title":"hidden_object","visible":false,"searchable":false}
                        ,{"title":"Alternative Item ID (Number)"}
                        ,{"title":"Item Description"}
                        ,{"title":"Ratio"}
                        ,{"title":"Supplier"}    
                        ,{"title":"Reason for Replacement"}
                        ];
        }
        else if(currTableId == 'tableSimilar')
        {
            tableAttr = [{}
                        ,{"title":"row_id","visible":false,"searchable":false}
                        ,{"title":"hidden_object","visible":false,"searchable":false}
                        ,{"title":"Similar Item ID (Number)"}
                        ,{"title":"Item Description"}
                        ,{"title":"Company"}
                        ,{"title":"Catalog Name"}    
                        ];
        }
        else if(currTableId == 'tableAttachments')
        {
            tableAttr = [{}
                        ,{"title":"row_id","visible":false,"searchable":false}
                        ,{"title":"hidden_object","visible":false,"searchable":false}
                        ,{"title":"File Name"}
                        ,{"title":"Comments"}
                        ,{"title":"Include in Grid"}    
                        ];
        }
        
        dtTable = drawTable(currTableId,tableAttr,[],additOptions);
    }
    return dtTable;
}
