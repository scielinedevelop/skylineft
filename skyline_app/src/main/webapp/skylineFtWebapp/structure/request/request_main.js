var pageMainServletName = "request";

$(document).ready(function () 
{
    $("button" ).button();
    var tableAttr = [{customButtons:[{text:"New Application",action:buildNew}]}
                    ,{"title":"row_id","visible":false,"searchable":false, "removable":false}
                    ,{"title":"Application Number"}
                    ,{"title":"Initiator Name", "orderable":false} 
                    ,{"title":"Initiator Role"} 
                    ,{"title":"Customer Name"} 
                    ,{"title":"Customer Code"} 
                    ,{"title":"Company"} 
                    ,{"title":"Country"} 
                    ,{"title":"Technical Owner"} 
                    ,{"title":"Status"}
                    ,{"title":"", "searchable":false, "orderable":false, "removable":false, "filterType":"none", "rowActionButtons":[{iconType:"edit", tooltip:"Update"}], width:'100px'} 
                ];

    var additOptions = {"onRowDblClick":onRowDblClicked, "onRowActionBtnClick":onRowActionBtnClick};		
    dtTable = drawTable("requestsTable",tableAttr,[],additOptions);
    // getTableData();
});

function onRowActionBtnClick(actionName, dtTable, currRowData, event)
{
    switch(actionName) {
        case "edit": editRequest(currRowData[0]);
            break;
        default: "";
    }
}

function onRowDblClicked(currRowData, tableID)
{
    editRequest(currRowData[0]);
}

function getTableData()
{
    sendAjaxRequest({servletName:pageMainServletName+"/getTableData",dataType: "json",
                    onSuccess:function(data) {
                        if(data != "-1") {
                            dt_ext_updateTableData(dtTable, data);			 	
                        }
                    }
    });
}

function buildNew()
{
    $('#currRequestId').val(-1);
    $('#main').attr("action","requestform");
    $('#main').submit();		
}

function editRequest(currId)
{
    $('#currRequestId').val(currId);
    $('#main').attr("action","requestform");
    $('#main').submit();		
}